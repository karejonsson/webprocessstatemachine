package se.prv.errends.application.servlet;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionDestroyEvent;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.OrganizationuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

@SpringBootApplication
@SpringUI(path="/org")
public class OrganizationuserUIServlet extends OrganizationuserUI {

	@WebServlet(value = "/org/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = AdminuserUI.class)
	public static class Servlet extends VaadinServlet implements SessionInitListener, SessionDestroyListener {
		
		@Override
	    protected void servletInitialized() throws ServletException {
			super.servletInitialized();
	        getService().addSessionInitListener(this); 
	        getService().addSessionDestroyListener(this);
		}
		
	    @Override
	    public void sessionInit(SessionInitEvent event)
	            throws ServiceException {
	    }

	    @Override
	    public void sessionDestroy(SessionDestroyEvent event) {
	    	Session sess = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
	    	sess.setFinish(new Date(System.currentTimeMillis()));
	    	sess.save();
	    }
	}

}
