package se.prv.errends.application.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbc.RoledefDB;
import se.prv.errends.dbc.TransitionDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Transition;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.reuse.cli.PropertyNames;

public class TransitionInErrendTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	private static Actor getActor(long errendId, long personId, long roledefId) {
		List<Actor> all = ActorDB.getAllActors();
		for(Actor actor : all) {
			Long actorsRoledefId = actor.getRoledefId();
			if(actorsRoledefId == null) {
				continue;
			}
			Long actorsErrendId = actor.getErrendId();
			if(actorsErrendId == null) {
				continue;
			}
			Long actorsPersonId = actor.getAgent().getPersonId();
			
			if(actorsRoledefId == roledefId && actorsErrendId == errendId && actorsPersonId == personId) {
				return actor;
			}
		}
		return null;
	}
	
	@Test
	public void run() throws Exception {
		RunFromAfterInitialization.setupData();
		
		PersistentValuesDB.setStringValue(PropertyNames.admintomellantitle, "admin to mellan title");
		PersistentValuesDB.setStringValue(PropertyNames.admintomellanmessage, "admin to mellan message");
		
		PersistentValuesDB.setStringValue(PropertyNames.smtpserver, "smtp.prv.se");
		PersistentValuesDB.setStringValue(PropertyNames.smtpauth, "true");
		PersistentValuesDB.setStringValue(PropertyNames.systemdefaultemailaddress, "systemdefault@emailaddress");
		PersistentValuesDB.setStringValue(PropertyNames.systemdefaultemailpassword, "systemdefault@emailpassword");
		PersistentValuesDB.setStringValue(PropertyNames.smtpport, "1234");
		PersistentValuesDB.setStringValue(PropertyNames.starttlsenable, "true");
		PersistentValuesDB.setStringValue(PropertyNames.blockmail, "true");
		PersistentValuesDB.setStringValue(PropertyNames.cron_schema_respite, "0 0 0/2 * * ?");
		
		Errend errend = ErrendDB.getErrendFromId(0);
		Person k = PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101031"));
		Person l = PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101032"));
		List<Roledef> roledefs = RoledefDB.getAllRoledefsOfProcessdef(errend.getManagedErrend().getProcessdefId());
		Roledef applicant = roledefs.get(0);
		Roledef admin = roledefs.get(1);
		
		Actor ka = getActor(errend.getId(), k.getId(), applicant.getId());
		Assert.assertTrue(ka != null);
		
		ErrendManagement em = ErrendManagement.getErrendFromDBStructure(ka);
		Assert.assertTrue(em.getCurrentStatename().equals("Början"));

		Session kasess = new Session();
		kasess.setActor(ka);
		kasess.setStart(new Date(System.currentTimeMillis()));
		kasess.save();
		
		try {
			System.out.println("Försöker en övergång som inte finns med rätt person.");
			em.performTransition("felaktig", kasess); // Non existing transition, right actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			System.out.println("Försöker en övergång som finns men från ett annat tillstånd med rätt person.");
			em.performTransition("avsluta", kasess); // Existing transition that is not from this state, right actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}

		Actor la = getActor(errend.getId(), l.getId(), admin.getId());
		Assert.assertTrue(la != null);
		
		Session lasess = new Session();
		lasess.setActor(la);
		lasess.setStart(new Date(System.currentTimeMillis()));
		lasess.save();		
		try {
			System.out.println("Försöker en övergång som finns i detta tillstånd med fel person.");
			em.performTransition("över", lasess); // Correct transition, wrong actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}

		Assert.assertTrue(em.getCurrentStatename().equals("Början"));
		em.performTransition("över", kasess); // Correct
		Assert.assertTrue(em.getCurrentStatename().equals("Mellan"));
		
		List<Transition> allTrans = TransitionDB.getAllTransitions();
		Assert.assertTrue(allTrans.size() == 1);
		
		for(Transition t : allTrans) {
			System.out.println(""+t);
		}
		
		try {
			System.out.println("Försöker en övergång som inte finns med rätt person.");
			em.performTransition("felaktig", lasess); // Non existing transition, right actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			System.out.println("Försöker en övergång som finns men från ett annat tillstånd med rätt person.");
			em.performTransition("över", lasess); // Existing transition that is not from this state, right actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			System.out.println("Försöker en övergång som finns i detta tillstånd med fel person.");
			em.performTransition("avsluta", kasess); // Correct transition, wrong actor
			Assert.assertTrue(false);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}

		Assert.assertTrue(em.getCurrentStatename().equals("Mellan"));
		em.performTransition("avsluta", lasess); // Correct
		Assert.assertTrue(em.getCurrentStatename().equals("Slut"));

		allTrans = TransitionDB.getAllTransitions();
		Assert.assertTrue(allTrans.size() == 2);
		
		for(Transition t : allTrans) {
			System.out.println(""+t);
		}
		
		Assert.assertTrue(em.getCurrentStatename().equals("Slut"));
		
		/*
		System.out.println("PROCESSDEF SIZE "+ProcessdefDB.getAllProcessdefs().size());
		System.out.println("PROCESSFILE SIZE "+ProcessfileDB.getAllProcessfiles().size());
		System.out.println("SESSION SIZE "+SessionDB.getAllSessions().size());
		System.out.println("TRANSITION SIZE "+TransitionDB.getAllTransitions().size());
		System.out.println("ERREND SIZE "+ErrendDB.getAllErrends().size());
		*/
		List<Processfile> pfs = ProcessfileDB.getAllProcessfiles();
		for(Processfile pf : pfs) {
			pf.deleteRecursively();
		}
		/*
		System.out.println("PROCESSDEF SIZE "+ProcessdefDB.getAllProcessdefs().size());
		System.out.println("PROCESSFILE SIZE "+ProcessfileDB.getAllProcessfiles().size());
		System.out.println("SESSION SIZE "+SessionDB.getAllSessions().size());
		System.out.println("TRANSITION SIZE "+TransitionDB.getAllTransitions().size());
		System.out.println("ERREND SIZE "+ErrendDB.getAllErrends().size());
		*/
		Assert.assertTrue(ProcessdefDB.getAllProcessdefs().size() == 0);
		Assert.assertTrue(ProcessfileDB.getAllProcessfiles().size() == 0);
		Assert.assertTrue(RoledefDB.getAllRoledefs().size() == 0);
		Assert.assertTrue(TransitionDB.getAllTransitions().size() == 0);
	}

}
