package se.prv.errends.application.servlet;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersonTest;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.initialize.DBInitializationTest;

public class RunOneProcessdef {
	
	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	@Test
	public void run() throws Exception {
		DBInitializationTest.initializeData();
		PersonTest.setupPeople();
		Processfile pf = ProcessfileDB.getAllProcessfiles().get(0);
		DBInitialisation.initializeProcessNodes(pf, "Some description", "Some process name");
	
	    PersonUIServlet.main(new String[] {});
	    Thread.sleep(1000000000);
	}

}

