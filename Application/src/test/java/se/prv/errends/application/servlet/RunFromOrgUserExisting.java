package se.prv.errends.application.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import junit.framework.Assert;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.ReportTemplateReuse;
import se.prv.errends.domain.Roledef;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.mainweb.ui.PersonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.processengine.reuse.StringFileManipulation;
import se.prv.errends.reuse.cli.PropertyNames;

public class RunFromOrgUserExisting {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	private void setupData() throws Exception {
		Organisation org = new Organisation();
		org.setActive(true);
		org.setOrgname("En firma");
		org.setOrgnr("56745");
		org.setRegisterred(new Date(System.currentTimeMillis()));
		org.save();
		Agent ag = new Agent();
		ag.setActive(true);
		ag.setOrganization(org);
		ag.setRegisterred(new Date(System.currentTimeMillis()));
		ag.setPerson(PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101024")));
		ag.save();
		OrganizationUser orguser = new OrganizationUser();
		orguser.setActive(true);
		orguser.setOrganization(org);
		orguser.setPerson(PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101022")));
		orguser.setUsername("Olle");
		orguser.setPassword(Adminuser.calculateChecksum("alm"));
		orguser.save();
		
		PersistentValuesDB.setStringValue(PropertyNames.time_format, "HH:mm:SS");
		PersistentValuesDB.setStringValue(PropertyNames.datetime_format, "yyyy-MM-dd hh:mm:ss");
		PersistentValuesDB.setStringValue(PropertyNames.date_format, "yyyy-MM-dd");
		
		ClassLoader cl = RunFromOrgUserExisting.class.getClassLoader();
		InputStream is = cl.getResourceAsStream("reports/InternalMacros.jasper");
		byte[] internalMacros = StringFileManipulation.readFullyToBytearray(is);
		ReportTemplate rt = new ReportTemplate();
		rt.setDescription("Tillhandahållna parametrar");
		rt.setReporttitle("Parametrar");
		rt.setFilename("InternalMacros.jasper");
		rt.setAdminuserId(AdminuserDB.getAllAdminusers().get(0).getId());
		rt.save();
		rt.setJasperReportData(internalMacros);
		is = cl.getResourceAsStream("demo2.graphml");
		byte[] demo2 = StringFileManipulation.readFullyToBytearray(is);
		Processfile pf = new Processfile();
		pf.setAdminuserId(AdminuserDB.getAllAdminusers().get(0).getId());
		pf.setDescription("4 tillstånd, 5 övergångar");
		pf.setFilename("demo2.graphml");
		pf.save();
		pf.setData(demo2);
		Processdef pd = DBInitialisation.initializeProcessNodes(pf, "Första enkla men förfinad. 4 tillstånd, 5 övergångar.", "4, 5");
		ReportTemplateReuse rtr = new ReportTemplateReuse();
		rtr.setProcessdef(pd);
		rtr.setReportTemplate(rt);
		rtr.save();
		
		Person kare = PersonDB.getPersonFromSwedishPnr("196905010150");
		Assert.assertTrue(kare != null);
		Agent agent = new Agent();
		agent.setActive(true);
		agent.setOrganization(org);
		agent.setPerson(kare);
		agent.save();
		Processdef pd45 = ProcessdefDB.getAllProcessdefs().get(2);
		ErrendManagement em = ErrendManagement.createErrendFromDBStructureAndInitialize(agent, pd45, "Svart ljus");
		
		Report report = new Report();
		report.setReportTemplateReuse(rtr);
		System.out.println("Errends "+ErrendDB.getAllErrends().size());
		Errend errend = em.getErrend();
		Roledef roledef = errend.getManagedErrend().getProcessdef().getRoledefs().get(0);
		System.out.println("Filnamn: "+errend.getManagedErrend().getProcessdef().getProcessfile().getFilename());
		
		List<Actor> actors = ActorDB.getAllActorsOfErrendIdAndAgentId(errend.getId(), agent.getId());
		report.setActor(actors.get(0));
		report.setReporttitle("En titel");
		report.setErrend(errend);
		report.save();
	}

	@Test
	public void run() throws Exception {
		RunFromAfterInitialization.setupData();
		setupData();
	    PersonUIServlet.main(new String[] {});
	    Thread.sleep(1000000000);
	}

}
