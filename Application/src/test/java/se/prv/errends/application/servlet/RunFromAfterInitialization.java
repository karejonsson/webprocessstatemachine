package se.prv.errends.application.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbc.RoledefDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.mainweb.ui.PersonUI;
import se.prv.errends.mgmt.ActorManagement;
import se.prv.errends.mgmt.AgentWithRoledef;

public class RunFromAfterInitialization {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	public static void setupData() throws Exception {
		RunFromBeforeInitialization.setupData();
		Errend errend = ErrendDB.getErrendFromId(0);
		Assert.assertTrue(errend != null);
		Person k = PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101031"));
		Organisation org = new Organisation();
		org.setActive(true);
		org.setOrgname("En org");
		org.setOrgnr("123321");
		org.save();
		Agent koa = new Agent();
		koa.setPerson(k);
		koa.setOrganization(org);
		koa.save();
		Person l = PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101032"));
		Agent loa = new Agent();
		loa.setPerson(l);
		loa.setOrganization(org);
		loa.save();
		List<Roledef> roledefs = RoledefDB.getAllRoledefsOfProcessdef(errend.getManagedErrend().getProcessdefId());
		List<AgentWithRoledef> personsWithRoledef = new ArrayList<AgentWithRoledef>();
		Roledef applicant = roledefs.get(0);
		Assert.assertTrue(applicant.getRolename().equals("applicant"));
		Roledef admin = roledefs.get(1);
		Assert.assertTrue(admin.getRolename().equals("admin"));
		personsWithRoledef.add(new AgentWithRoledef(koa, applicant));
		personsWithRoledef.add(new AgentWithRoledef(loa, admin));
		ActorManagement.saveErrendSetupReturnUserText(errend, roledefs, "adminusername", personsWithRoledef, ActorManagement.getErrendlessActor(l));
	}
	
	@Test
	public void run() throws Exception {
		setupData();
	    PersonUIServlet.main(new String[] {});
	    Thread.sleep(1000000000);
	}

}
