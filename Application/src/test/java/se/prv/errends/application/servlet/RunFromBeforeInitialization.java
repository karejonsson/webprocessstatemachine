package se.prv.errends.application.servlet;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.mainweb.ui.PersonUI;
import se.prv.errends.mgmt.ErrendManagement;

public class RunFromBeforeInitialization {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	public static void setupData() throws Exception {
		RunOneErrend.setupData();
		Processdef pd = ProcessdefDB.getAllProcessdefs().get(1);
		Person p = PersonDB.getPersonFromSwedishPnr(Person.makeCompletionWithChecknumber("19700101031"));
		Organisation org = new Organisation();
		org.setActive(true);
		org.setOrgname("En org");
		org.setOrgnr("123");
		org.save();
		Agent oa = new Agent();
		oa.setPerson(p);
		oa.setOrganization(org);
		oa.save();
		ErrendManagement em = ErrendManagement.retreiveErrendFromDBStructure(oa, pd, "Ett namn");
	}
	
	@Test
	public void run() throws Exception {
		setupData();
	    PersonUIServlet.main(new String[] {});
	    Thread.sleep(1000000000);
	}

}
