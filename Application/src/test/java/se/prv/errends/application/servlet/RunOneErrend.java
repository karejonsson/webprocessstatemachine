package se.prv.errends.application.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersistentValuesTest;
import se.prv.errends.dbc.PersonTest;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.initialize.DBInitializationTest;
import se.prv.errends.mainweb.ui.PersonUI;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class RunOneErrend {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	public static void setupData() throws Exception {
		DBInitializationTest.initializeData();
		Processfile pf = ProcessfileDB.getAllProcessfiles().get(0);
		DBInitialisation.initializeProcessNodes(pf, "Some description", "Some process name");
		
		Adminuser au = AdminuserDB.getAllAdminusers().get(0);
		ClassLoader cl = RunOneErrend.class.getClassLoader();
		InputStream is = cl.getResourceAsStream("dev1.graphml");

		Processfile dev1 = new Processfile();
		dev1.setDescription("En beskrivning av annan fil");
		dev1.setAdminuserId(au.getId());
		dev1.setFilename("dev1.graphml");
		ProcessfileDB.createProcessfile(dev1);
		byte[] data = StringFileManipulation.readFullyToBytearray(is);
		ProcessfileDB.setData(dev1.getId(), data);
		
		PersonTest.setupPeople();
		PersistentValuesTest.setupData();

		DBInitialisation.initializeProcessNodes(dev1, "Testärende", "Finns för utveckling");
	}
	
	@Test
	public void run() throws Exception {
		setupData();
	    PersonUIServlet.main(new String[] {});
	    Thread.sleep(1000000000);
	}

}
