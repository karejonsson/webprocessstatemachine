package se.prv.errends.application.servlet;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.initialize.DBInitializationTest;
import se.prv.errends.mainweb.ui.PersonUI;

public class RunOneProcessfile {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

	@Test
	public void run() throws Exception {
		DBInitializationTest.initializeData();

        PersonUIServlet.main(new String[] {});
        Thread.sleep(1000000000);
	}
	
}
