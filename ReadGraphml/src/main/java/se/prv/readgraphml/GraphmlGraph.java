package se.prv.readgraphml;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

public class GraphmlGraph {

    private Map<String, GraphmlNode> nodes = new HashMap<String, GraphmlNode>();
    private List<GraphmlEdge> edges = new ArrayList<GraphmlEdge>();

    public void addNode(GraphmlNode node) {
        nodes.put(node.getId(), node);
        String longText = node.getLongText();
    }

    public Set<String> getNodeKeyset() {
        return nodes.keySet();
    }

    public int getEdgeAmount() {
        return edges.size();
    }

    public void addEdge(GraphmlEdge edge) {
        edges.add(edge);
    }

    public GraphmlNode getNode(String id) {
        return nodes.get(id);
    }

    public GraphmlEdge getEdge(int idx) {
        return edges.get(idx);
    }

    public Set<GraphmlEdge> getEgdesOut(String nodeId) {
        Set<GraphmlEdge> out = new HashSet<GraphmlEdge>();
        for(GraphmlEdge edge : edges) {
            if(edge.getSourceId().equals(nodeId)) {
                out.add(edge);
            }
        }
        return out;
    }

    public Set<GraphmlEdge> getEgdesIn(String nodeId) {
        Set<GraphmlEdge> out = new HashSet<GraphmlEdge>();
        for(GraphmlEdge edge : edges) {
            if(edge.getTargetId().equals(nodeId)) {
                out.add(edge);
            }
        }
        return out;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for(String nodeId : nodes.keySet()) {
            GraphmlNode node = nodes.get(nodeId);
            sb.append(node.toString()+"\n");
        }
        for(GraphmlEdge edge : edges) {
            sb.append(edge.toString()+"\n");
        }
        return sb.toString();
    }

}
