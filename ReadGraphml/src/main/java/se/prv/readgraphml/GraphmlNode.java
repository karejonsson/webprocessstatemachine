package se.prv.readgraphml;

public class GraphmlNode {

    private String id = null;
    private String shortText = null;
    private String longText = null;

    public GraphmlNode(String id, String shortText, String longText) {
        this.id = id;
        this.shortText = shortText;
        this.longText = longText;
    }

    public String getId() {
        return id;
    }

    public String getShortText() {
        return shortText;
    }

    public String getLongText() {
        return longText;
    }

    public String toString() {
        return "Node - id "+id+", shortText "+shortText+", longText "+longText;
    }

}
