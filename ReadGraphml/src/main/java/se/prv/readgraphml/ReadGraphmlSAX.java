package se.prv.readgraphml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;

public class ReadGraphmlSAX {

    public static GraphmlGraph getGraph(byte b[]) throws Exception {
        return getGraph(new ByteArrayInputStream(b));
    }

    public static GraphmlGraph getGraph(InputStream is) throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        SAXLocalNameCount collector = new SAXLocalNameCount();
        saxParser.parse(is, collector);
        return collector.getGraph();
    }

    public static class SAXLocalNameCount extends DefaultHandler {

        public GraphmlGraph gg = new GraphmlGraph();

        public SAXLocalNameCount() {
        }

        public GraphmlGraph getGraph() {
            return gg;
        }

        public void startDocument() {
            //System.out.println("startDocument");
        }

        boolean inNode = false;
        boolean inEdge = false;

        //     <node id="n0">
        //       <data key="d5"><![CDATA[desc: Ärendet skapat]]></data>
        //         <y:NodeLabel >Nyskapat ärende<y:LabelModel>

        String nodeId = null;
        String nodeLongText = null;
        String nodeShortText = null;
        boolean inNodeDataKeyD5 = false;
        boolean inYNodeLabel = false;

        private void handleInNode(String uri, String localName, String qName, Attributes attributes) {
            if(qName.equals("data") && getValueFromId(attributes, "key").equals("d5")) {
                inNodeDataKeyD5 = true;
            }
            else {
                inNodeDataKeyD5 = false;
            }
            if(qName.equals("y:NodeLabel")) {
                inYNodeLabel = true;
            }
            else {
                inYNodeLabel = false;
            }
        }

        private void handleStartNode(String uri, String localName, String qName, Attributes attributes) {
            nodeId = getValueFromId(attributes, "id");
        }

        private void handleFinalizeNode(String uri, String localName, String qName) {
            GraphmlNode n = new GraphmlNode(nodeId, nodeShortText, nodeLongText);
            nodeShortText = null;
            nodeLongText = null;
            gg.addNode(n);
            //System.out.println("Node "+n);
        }

        //     <edge id="e7" source="n6" target="n7">
        //       <data key="d9"><![CDATA[role: admin]]></data>
        //         <y:EdgeLabel >Avslår<y:LabelModel>

        String edgeId = null;
        String edgeTarget = null;
        String edgeSource = null;
        String edgeLongText = null;
        String edgeShortText = null;

        boolean inEdgeDataKeyD9 = false;
        boolean inYEdgeLabel = false;

        private void handleInEdge(String uri, String localName, String qName, Attributes attributes) {
            if(qName.equals("data") && getValueFromId(attributes, "key").equals("d9")) {
                inEdgeDataKeyD9 = true;
            }
            else {
                inEdgeDataKeyD9 = false;
            }
            if(qName.equals("y:EdgeLabel")) {
                inYEdgeLabel = true;
            }
            else {
                inYEdgeLabel = false;
            }
        }

        private void handleStartEdge(String uri, String localName, String qName, Attributes attributes) {
            edgeId = getValueFromId(attributes, "id");
            edgeTarget = getValueFromId(attributes, "target");
            edgeSource = getValueFromId(attributes, "source");
        }

        private void handleFinalizeEdge(String uri, String localName, String qName) {
            GraphmlEdge e = new GraphmlEdge(edgeId, edgeSource, edgeTarget, edgeShortText, edgeLongText);
            edgeShortText = null;
            edgeLongText = null;
            gg.addEdge(e);
            //System.out.println("Edge "+e);
        }

        private String getValueFromId(Attributes attributes, String id) {
            int length = attributes.getLength();
            for(int i=0 ; i < length; i++) {
                if(attributes.getQName(i).equals(id)) {
                    return attributes.getValue(i);
                }
            }
            return null;
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if(inNode) {
                handleInNode(uri, localName, qName, attributes);
                return;
            }
            if(inEdge) {
                handleInEdge(uri, localName, qName, attributes);
                return;
            }
            if(qName.equals("node")) {
                inNode = true;
                handleStartNode(uri, localName, qName, attributes);
                return;
            }
            if(qName.equals("edge")) {
                inEdge = true;
                handleStartEdge(uri, localName, qName, attributes);
                return;
            }
        }

        public void endElement (String uri, String localName, String qName) throws SAXException {
            if(qName.equals("node")) {
                inNode = false;
                handleFinalizeNode(uri, localName, qName);
                return;
            }
            if(qName.equals("edge")) {
                inEdge = false;
                handleFinalizeEdge(uri, localName, qName);
                return;
            }
        }

        public void characters (char ch[], int start, int length)throws SAXException {
            String out = new String(ch, start, length).trim();
            if(out.length() < 1) {
                return;
            }
            if(inEdgeDataKeyD9) {
                edgeLongText = out;
            }
            if(inYEdgeLabel) {
                edgeShortText = out;
            }
            if(inNodeDataKeyD5) {
                nodeLongText = out;
            }
            if(inYNodeLabel) {
                nodeShortText = out;
            }
        }

        public void endDocument() throws SAXException {
            //System.out.println("endDocument");
        }

    }

}
