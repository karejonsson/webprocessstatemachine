package se.prv.readgraphml;

public class GraphmlEdge {

    private String id = null;
    private String sourceId = null;
    private String targetId = null;
    private String shortText = null;
    private String longText = null;

    public GraphmlEdge(String id, String sourceId, String targetId, String shortText, String longText) {
        this.id = id;
        this.sourceId = sourceId;
        this.targetId = targetId;
        this.shortText = shortText;
        this.longText = longText;
    }

    public String getId() {
        return id;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getLongText() {
		return longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public String toString() {
        return "Edge - id "+id+", sourceId "+sourceId+", targetId "+targetId+", shortText "+shortText+", longText "+longText;
    }

}
