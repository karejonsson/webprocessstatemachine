package se.prv.readgraphml;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class ReadGraphmlSAXTest {

    @Test
    public void readGraph() throws Exception {
        ClassLoader cl = ReadGraphmlSAXTest.class.getClassLoader();
        InputStream is = cl.getResourceAsStream("1.graphml");
        GraphmlGraph gmg = ReadGraphmlSAX.getGraph(is);
        System.out.println("Graph : "+gmg);

        Assert.assertTrue(gmg.getNodeKeyset().size() == 8);
        Assert.assertTrue(gmg.getEdgeAmount() == 13);
    }

}
