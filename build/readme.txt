sed -i 's/^#PermitRootLogin no/PermitRootLogin no/' /etc/ssh/sshd_config
sed -i 's/^PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
sed -i 's/^#PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
grep PermitRootLogin /etc/ssh/sshd_config

firewall-cmd --zone=public --add-port=http/tcp --permanent

useradd -G wheel -c "Ärenderhanteringen" errender
usermod -G wheel -a errender
passwd errender