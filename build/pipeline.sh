# Assumes sshpass
# sudo apt-get install sshpass
#
# Example call
# $ ./pipeline vbcentos

user=`grep 'user' $1.cfg | tr '=' ' ' | awk '{ print $2 }'`
domain=`grep 'domain' $1.cfg | tr '=' ' ' | awk '{ print $2 }'`
passwd=`grep 'passwd' $1.cfg | tr '=' ' ' | awk '{ print $2 }'`
myname=`grep 'myname' $1.cfg | tr '=' ' ' | cut -d' ' -f2-`

sshpass -p "$passwd" scp artifacts/errender_deploy.tar.gz $user@$domain:
sshpass -p "$passwd" ssh $user@$domain << PIPELINE
 echo "$passwd" | sudo -S ./commands/stopall.sh 
 echo "$passwd" | sudo -S ./errenderinstall.sh errender_deploy.tar.gz $myname
 echo "$passwd" | sudo -S ./commands/startall.sh 
PIPELINE