#!/bin/bash

yum -y update
yum -y install java-1.8.0-openjdk-headless httpd

iptables -I INPUT 4 -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
firewall-cmd --zone=public --add-port=3000/tcp --permanent

mkdir -p ./../../commands
mkdir -p ./../../history
cp startall.sh ./../../commands/
cp stopall.sh ./../../commands/

touch /var/www/robots.txt
chmod 766 /var/www/robots.txt
echo 'User-agent: *' > /var/www/robots.txt
echo 'Disallow: /' >> /var/www/robots.txt

setsebool -P httpd_can_network_connect 1
setsebool -P httpd_can_network_relay 1
cp vh.conf /etc/httpd/conf.d/
systemctl enable httpd

mkdir -p /etc/prvconfig
cp prverrends.properties /etc/prvconfig/
chmod -R 755 /etc/prvconfig
chown errender:errender -R ./../../commands
chmod -R 755 ./../../commands
mkdir -p /var/lib/errender/bin
cp errender.service /etc/systemd/system/
systemctl daemon-reload

yum -y install postgresql-server postgresql-contrib
postgresql-setup initdb
sed -i s/ident/trust/g /var/lib/pgsql/data/pg_hba.conf
sed -i s/peer/trust/g /var/lib/pgsql/data/pg_hba.conf
systemctl enable postgresql
systemctl start postgresql

sudo -u postgres createuser prvuser
sudo -u postgres createdb -O prvuser -Eutf8 errendsdb
sudo -u postgres psql errendsdb < psql-schema.sql
sudo -u postgres psql errendsdb < psql-setup.sql

echo "alter database errendsdb owner to prvuser;" | psql -U postgres
echo "alter user prvuser password 'prvpwd';" | psql -U postgres

database='errendsdb'
new_owner='prvuser'

tables=`psql -U postgres -qAt -c "select tablename from pg_tables where schemaname = 'public';" $database`

for tbl in $tables ; do
  psql -U postgres -c "alter table $tbl owner to $new_owner" $database ;
done

./../../commands/stopall.sh

INSTALLERSNAME=$1
now="$(date +'%Y-%m-%d %H:%M')"
echo $now' Errender installation by '$INSTALLERSNAME >> ./../../history/actions.log
