#!/bin/bash

if [ -d "/var/lib/errender/bin" ]; then
	rm -rf /var/lib/errender/bin
fi
	
mkdir -p /var/lib/errender/bin

cp errender.jar /var/lib/errender/bin/

now="$(date +'%Y-%m-%d %H:%M')"
echo $now' Errender deploy by '$1 >> ./../../history/actions.log
