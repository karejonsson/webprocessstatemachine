#!/bin/bash

if [ -f "../MainWeb/src/main/resources/datetime.txt" ]; then
	rm -rf ../MainWeb/src/main/resources/datetime.txt
fi

echo "$(date +'%Y-%m-%d %H:%M:%S')" > ../MainWeb/src/main/resources/datetime.txt
git log -1 | grep commit | awk '{ print $2}' >> ../MainWeb/src/main/resources/datetime.txt

( cd .. ; mvn clean install )

if [ -f "../MainWeb/src/main/resources/datetime.txt" ]; then
	rm -rf ../MainWeb/src/main/resources/datetime.txt
fi

./dist_deploy.sh
