#!/bin/bash

./dist_setup_centos7.sh  
./dist_setup_ubuntu.sh
./dist_deploy.sh  

today="$(date +'%Y%m%d')"

curl -X PUT -v -u admin:admin123 --upload-file artifacts/errender_setup_centos7.tar.gz http://nexus.default.mypages.test.prv.se/content/repositories/system2020/poc/kare/$today/errender_setup_centos7.tar.gz

curl -X PUT -v -u admin:admin123 --upload-file artifacts/errender_setup_ubuntu.tar.gz http://nexus.default.mypages.test.prv.se/content/repositories/system2020/poc/kare/$today/errender_setup_ubuntu.tar.gz

curl -X PUT -v -u admin:admin123 --upload-file artifacts/errender_deploy.tar.gz http://nexus.default.mypages.test.prv.se/content/repositories/system2020/poc/kare/$today/errender_deploy.tar.gz
