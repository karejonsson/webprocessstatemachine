#!/bin/bash

now="$(date +'%Y-%m-%dT%H_%M')"
mkdir -p installs/$now/pkg
mv $1 installs/$now/pkg/
cd installs/$now
tar -zxf pkg/$1 --warning=no-timestamp

shift

./install.sh "$@" 2>&1 | tee install_output.txt
