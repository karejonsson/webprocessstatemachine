#!/bin/bash

if [ -d "tmp" ]; then
	rm -rf tmp
fi
if [ ! -d "tmp" ]; then
	mkdir tmp
fi

cp deployinstall.sh tmp/install.sh
cp ../MainWeb/target/Mainweb-0.1.jar tmp/errender.jar

chmod a+x tmp/*.sh

if [ ! -d "artifacts" ]; then
	mkdir artifacts
fi

(cd tmp && tar -zcf ../artifacts/errender_deploy.tar.gz * )

rm -rf tmp
