#!/bin/bash

apt-get -y update
apt-get -y install openjdk-8-jre-headless apache2

mkdir -p ./../../commands
mkdir -p ./../../history
cp startall.sh ./../../commands/
cp stopall.sh ./../../commands/

touch /var/www/robots.txt
chmod 766 /var/www/robots.txt
echo 'User-agent: *' > /var/www/robots.txt
echo 'Disallow: /' >> /var/www/robots.txt

#setsebool -P httpd_can_network_connect 1
#setsebool -P httpd_can_network_relay 1
cp vh.conf /etc/apache2/sites-available/
a2enmod proxy
a2enmod proxy_http
a2dissite 000-default
a2ensite vh
systemctl enable apache2

mkdir -p /etc/prvconfig
cp prverrends.properties /etc/prvconfig/
chmod -R 755 /etc/prvconfig
chown errender:errender -R ./../../commands
chmod -R 755 ./../../commands
mkdir -p /var/lib/errender/bin
cp errender.service /etc/systemd/system/
systemctl daemon-reload

apt-get -y install postgresql-server-dev-9.5 postgresql-contrib
#postgresql-setup initdb
#sed -i s/ident/trust/g /var/lib/pgsql/data/pg_hba.conf
#sed -i s/peer/trust/g /var/lib/pgsql/data/pg_hba.conf
#systemctl enable postgresql
#systemctl start postgresql

sudo -u postgres createuser prvuser
sudo -u postgres createdb -O prvuser -Eutf8 errendsdb
sudo -u postgres psql errendsdb < psql-schema.sql
sudo -u postgres psql errendsdb < psql-setup.sql

echo "alter database errendsdb owner to prvuser;" | sudo -u postgres psql -U postgres
echo "alter user prvuser password 'prvpwd';" | sudo -u postgres psql -U postgres

database='errendsdb'
new_owner='prvuser'

tables=`sudo -u postgres psql -U postgres -qAt -c "select tablename from pg_tables where schemaname = 'public';" $database`

for tbl in $tables ; do
  sudo -u postgres psql -U postgres -c "alter table $tbl owner to $new_owner" $database ;
done

./../../commands/stopall.sh

INSTALLERSNAME=$1
now="$(date +'%Y-%m-%d %H:%M')"
echo $now' Errender installation by '$INSTALLERSNAME >> ./../../history/actions.log
