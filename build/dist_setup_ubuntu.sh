#!/bin/bash

if [ -d "tmp" ]; then
	rm -rf tmp
fi
if [ ! -d "tmp" ]; then
	mkdir tmp
fi

cp setupinstall_ubuntu.sh tmp/install.sh
cp vh.conf tmp/

cp ../Databasemodel/documentation/psql-setup.sql tmp/
cp ../Databasemodel/documentation/psql-schema.sql tmp/

cp startall_template.sh tmp/startall.sh
sed -i s/WEBSERVERPACKAGENAME/apache2/g tmp/startall.sh
cp stopall_template.sh tmp/stopall.sh
sed -i s/WEBSERVERPACKAGENAME/apache2/g tmp/stopall.sh

cp errender.service tmp/
cp prverrends_template.properties tmp/prverrends.properties
sed -i s/DATABASENAME/errendsdb/g tmp/prverrends.properties
sed -i s/USERNAME/prvuser/g tmp/prverrends.properties
sed -i s/PASSWORD/prvpwd/g tmp/prverrends.properties

chmod a+x tmp/*.sh

if [ ! -d "artifacts" ]; then
	mkdir artifacts
fi

(cd tmp && tar -zcf ../artifacts/errender_setup_ubuntu.tar.gz * )

rm -rf tmp
