package se.prv.errends.parametervalue.struct;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import se.prv.errends.parametervalue.parse.PrvErrendParameterValueParser;
import se.prv.errends.parametervalue.struct.ParameterValue;

public class ParameterValueHandling {

	public static List<ParameterValue> createParameterList(String s, String sourceSpecifcation) throws Exception {
		List<ParameterValue> out = PrvErrendParameterValueParser.parseParameterValue(s.getBytes("UTF8"), sourceSpecifcation);
		return out;
	}

	public static List<ParameterValue> createParameterList(byte bytes[], String sourceSpecifcation) throws Exception {
		List<ParameterValue> out = PrvErrendParameterValueParser.parseParameterValue(bytes, sourceSpecifcation);
		return out;
	}

	public static List<ParameterValue> createParameterList(InputStream is, String sourceSpecifcation) throws Exception {
		List<ParameterValue> out = PrvErrendParameterValueParser.parseParameterValue(is, sourceSpecifcation);
		return out;
	}

	public static String serializeParameterListToString(List<ParameterValue> l) throws UnsupportedEncodingException {
		if(l == null) {
			return new String();
		}
		if(l.size() == 0) {
			return "[]";
		}
		if(l.size() == 1) {
			return "[ "+l.get(0).toString()+" ]";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("[ "+l.get(0));
		for(int i = 1 ; i < l.size(); i++) {
			sb.append(", "+l.get(i));
		}
		return sb.toString()+" ]";
	}

	public static byte[] serializeParameterListToBytes(List<ParameterValue> l) throws UnsupportedEncodingException {
		return serializeParameterListToString(l).getBytes("UTF8");
	}

}
