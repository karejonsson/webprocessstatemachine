package se.prv.errends.parametervalue.struct;

public class ParameterValue {
	
	private String name = null;
	private String value = null;
	
	public String toString() {
		return "{ \""+name+"\", "+(value != null ? "\'\'\'\'\'\'"+value+"\'\'\'\'\'\'" : "")+" }";
	}
	
	public ParameterValue(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
