package se.prv.errends.edgeconfig.struct;

import java.util.List;

public interface EdgeAction {

	List<String> getRoles();
	List<String> getStringFunctions();
	List<String> getBooleanFunctions();
	
	void accept(EdgeActionVisitor visitor);
	
}
