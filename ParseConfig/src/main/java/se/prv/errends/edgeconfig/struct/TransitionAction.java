package se.prv.errends.edgeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class TransitionAction {
	
	private List<String> totalroles = new ArrayList<String>();
	private List<String> applyingroles = new ArrayList<String>();

	public void addRole(String role) {
		if(!totalroles.contains(role)) {
			totalroles.add(role);
		}
		if(!applyingroles.contains(role)) {
			applyingroles.add(role);
		}
	}
	
	public List<String> getRoles() {
		return totalroles;
	}
	
	private ArrayList<EdgeAction> actions = new ArrayList<EdgeAction>();

	public void addEdgeAction(EdgeAction ea) {
		if(ea == null) {
			return;
		}
		actions.add(ea);
		for(String role : ea.getRoles()) {
			if(!totalroles.contains(role)) {
				totalroles.add(role);
			}
		}
	}
	
	public List<EdgeAction> getActions() {
		return actions;
	}

	public boolean appliesTo(String role) {
		boolean out = applyingroles.contains(role);
		return out;
	}
	
	public List<String> getStringFunctions() {
		List<String> out = new ArrayList<String>();
		for(EdgeAction ea : actions) {
			List<String> functions = ea.getStringFunctions();
			if(functions == null) {
				continue;
			}
			for(String function : functions) {
				if(!out.contains(function)) {
					out.add(function);
				}
 			}
		}
		return out;
	}
	
	public List<String> getBooleanFunctions() {
		List<String> out = new ArrayList<String>();
		for(EdgeAction ea : actions) {
			List<String> functions = ea.getBooleanFunctions();
			if(functions == null) {
				continue;
			}
			for(String function : functions) {
				if(!out.contains(function)) {
					out.add(function);
				}
 			}
		}
		return out;
	}

}
