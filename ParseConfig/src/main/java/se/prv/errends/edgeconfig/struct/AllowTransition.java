package se.prv.errends.edgeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class AllowTransition {
	
	public List<String> roles = new ArrayList<String>();
	
	public void addRole(String role) {
		if(!roles.contains(role)) {
			roles.add(role);
		}
	}
	
	public List<String> getRoles() {
		return roles;
	}
	
	private String functionName = null;
	
	public String getFunction() {
		return functionName;
	}

	public void setFunction(String functionName) {
		this.functionName = functionName;
	}

}
