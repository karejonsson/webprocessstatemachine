package se.prv.errends.edgeconfig.struct;

public interface EdgeActionVisitor {

	void visit(EmailnotificationAction emailnotificationAction);

}
