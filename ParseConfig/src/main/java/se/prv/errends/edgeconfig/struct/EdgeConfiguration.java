package se.prv.errends.edgeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class EdgeConfiguration {
	
	public List<String> allroles = new ArrayList<String>();
	public List<String> transitionroles = new ArrayList<String>();
	public List<AllowTransition> allowedtransitions = new ArrayList<AllowTransition>();
	private String explanationFunctionName;
	private String explanationLiteral;
		
	public EdgeConfiguration() {
	}
	
	public void addAllowTransition(AllowTransition allowtransition) {
		allowedtransitions.add(allowtransition);
		for(String role : allowtransition.getRoles()) {
			if(!transitionroles.contains(role)) {
				transitionroles.add(role);
			}
			if(!allroles.contains(role)) {
				allroles.add(role);
			}
		}
	}
	
	public AllowTransition getFirstAllowedTransitionOnRole(String role) {
		for(AllowTransition at : allowedtransitions) {
			if(at.getRoles().contains(role)) {
				return at;
			}
		}
		return null;
	}

    public List<String> getRoles() {
        return allroles;
    }
    
	public List<String> getStringFunctions() {
		List<String> out = new ArrayList<String>();
		for(TransitionAction ea : tactions) {
			List<String> functions = ea.getStringFunctions();
			if(functions == null) {
				continue;
			}
			for(String function : functions) {
				if(!out.contains(function)) {
					out.add(function);
				}
 			}
		}
		if(!out.contains(explanationFunctionName)) {
			out.add(explanationFunctionName);
		}
		return out;
	}

	public List<String> getBooleanFunctions() {
		List<String> out = new ArrayList<String>();
		for(AllowTransition at : allowedtransitions) {
			String function = at.getFunction();
			if(function == null) {
				continue;
			}
			if(!out.contains(function)) {
				out.add(function);
			}
		}
		for(TransitionAction ea : tactions) {
			List<String> functions = ea.getBooleanFunctions();
			if(functions == null) {
				continue;
			}
			for(String function : functions) {
				if(!out.contains(function)) {
					out.add(function);
				}
 			}
		}
		return out;
	}
		
	public boolean allowsTransitionOnRolename(String rolename) {
		if(rolename.equals("deamon") && expirations.size() != 0) {
			return true;
		}
		return transitionroles.contains(rolename);
	}
	
	private ArrayList<TransitionAction> tactions = new ArrayList<TransitionAction>();
	
	public void addTransitionAction(TransitionAction ta) {
		List<String> troles = ta.getRoles();
		for(String trole : troles) {
			if(!allroles.contains(trole)) {
				allroles.add(trole);
			}
		}
		tactions.add(ta);
	}
	
	public List<TransitionAction> getApplyingTransitionActions(String role) {
		List<TransitionAction> out = new ArrayList<TransitionAction>();
		for(TransitionAction action : tactions) {
			if(action.appliesTo(role)) {
				out.add(action);
			}
		}
		return out;
	}
	
	private ArrayList<StatetimeExpiration> expirations = new ArrayList<StatetimeExpiration>();

	public void addStatetimeExpiration(StatetimeExpiration se) {
		expirations.add(se);
	}
	
	public List<StatetimeExpiration> getStatetimeExpirations() {
		return expirations;
	}
	
	public String getExplanationFunctionName() {
		return explanationFunctionName;
	}
	
	public void setExplanationFunctionName(String explanationFunctionName) {
		this.explanationFunctionName = explanationFunctionName;
	}

	public String getExplanationLiteral() {
		return explanationLiteral;
	}
	
	public void setExplanationLiteral(String explanationLiteral) {
		this.explanationLiteral = explanationLiteral;
	}

	
}
