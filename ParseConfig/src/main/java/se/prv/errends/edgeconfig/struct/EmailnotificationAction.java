package se.prv.errends.edgeconfig.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailnotificationAction implements EdgeAction {

	private Map<String, String> kvp = new HashMap<String, String>();
	
	public void addKeyValuePair(String key, String value) {
		if(key.equals(inlineTitleKey) && kvp.get(functionTitleKey) != null) {
			return; // Only the first title method
		}
		if(key.equals(functionTitleKey) && kvp.get(inlineTitleKey) != null) {
			return; // Only the first title method
		}
		if(key.equals(inlineMessageKey) && kvp.get(functionMessageKey) != null) {
			return; // Only the first message method
		}
		if(key.equals(functionMessageKey) && kvp.get(inlineMessageKey) != null) {
			return; // Only the first message method
		}
		kvp.put(key, value);
	}
	
	public boolean hasKey(String key) {
		return kvp.keySet().contains(key);
	}

	public String getValue(String key) {
		return kvp.get(key);
	}

	@Override
	public void accept(EdgeActionVisitor visitor) {
		visitor.visit(this);
	}
	
	private List<String> roles = new ArrayList<String>();
	
	public void addRole(String role) {
		if(!roles.contains(role)) {
			roles.add(role);
		}
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	@Override
	public List<String> getStringFunctions() {
		List<String> out = new ArrayList<String>();
		String candidate = kvp.get(functionTitleKey);
		if(candidate != null) {
			out.add(candidate);
		}
		candidate = kvp.get(functionMessageKey);
		if(candidate != null) {
			if(!out.contains(candidate)) {
				out.add(candidate);
			}
		}
		return out;
	}
	
	public boolean useInline() {
		if(kvp.get(inlineTitleKey) == null) {
			return false;
		}
		if(kvp.get(inlineMessageKey) == null) {
			return false;
		}
		return true;
	}

	public boolean useFunction() {
		if(kvp.get(functionTitleKey) == null) {
			return false;
		}
		if(kvp.get(functionMessageKey) == null) {
			return false;
		}
		return true;
	}

	@Override
	public List<String> getBooleanFunctions() {
		List<String> out = new ArrayList<String>();
		return out;
	}

	public static final String inlineTitleKey = "title";
	public static final String inlineMessageKey = "message";
	
	public static final String functionTitleKey = "title-function";
	public static final String functionMessageKey = "message-function";
	
}
