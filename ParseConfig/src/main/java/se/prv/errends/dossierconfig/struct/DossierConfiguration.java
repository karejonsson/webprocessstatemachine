package se.prv.errends.dossierconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class DossierConfiguration {
	
	private static String peelOf(String in) {
		if(!in.startsWith("'")) {
			return in;
		}
		return in.substring(1, in.length()-1);
	}
	
	private List<String> types = new ArrayList<String>();
	
	public void addType(String type) {
		types.add(peelOf(type));
	}
	
	public List<String> getTypes() {
		return types;
	}
	
	private List<String> attributes = new ArrayList<String>();

	public void addAttribute(String attribute) {
		attributes.add(peelOf(attribute));
	}
	
	public List<String> getAttributes() {
		return attributes;
	}	

}
