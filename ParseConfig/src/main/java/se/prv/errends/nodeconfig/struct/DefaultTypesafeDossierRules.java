package se.prv.errends.nodeconfig.struct;

public class DefaultTypesafeDossierRules implements TypesafeDossierRules {
	
	private boolean answerToAll;
	
	public DefaultTypesafeDossierRules(boolean answerToAll) {
		this.answerToAll = answerToAll;
	}

	@Override
	public boolean enableDocumentDownloadAmongCurrents() {
		return answerToAll;
	}

	@Override
	public boolean enableActivationOnCurrentTab() {
		return answerToAll;
	}

	@Override
	public boolean enableActivationOnAllTab() {
		return answerToAll;
	}

	@Override
	public boolean enableNewDocumentOnCurrentTab() {
		return answerToAll;
	}

	@Override
	public boolean enableDocumentDownloadAmongAll() {
		return answerToAll;
	}

}
