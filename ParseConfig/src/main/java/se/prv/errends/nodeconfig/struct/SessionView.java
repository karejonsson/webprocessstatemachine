package se.prv.errends.nodeconfig.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SessionView implements NodeView {

	public SessionView() {
	}

	@Override
	public List<String> getRoles() {
		return new ArrayList<String>();
	}

	@Override
	public void accept(NodeViewVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String getName() {
		return "Sessioner";
	}
	
}
