package se.prv.errends.nodeconfig.struct;

import java.util.List;

public interface NodeLayout {
	
	List<String> getRoles();

	boolean hasRole(String role);
	
	void accept(NodeLayoutVisitor visitor);

}
