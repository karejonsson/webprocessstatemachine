package se.prv.errends.nodeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class GuiFunctions implements NodeLayout { 
	
	private DossierRules dr;
	
	public void setDossierRules(DossierRules dr) {
		this.dr = dr;
	}
	
	public DossierRules getDossierRules() {
		return dr;
	}
	
	private List<NodeView> nodeViews = new ArrayList<NodeView>();
	
	public void addNodeView(NodeView nv) {
		nodeViews.add(nv);
		for(String role : nv.getRoles()) {
			addRole(role);
		}
	}
	
	public List<NodeView> getNodeViews() {
		return nodeViews;
	}

	private List<String> roles = new ArrayList<String>();

	public void addRole(String role) {
		if(!roles.contains(role)) {
			roles.add(role);
		}
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	@Override
	public boolean hasRole(String role) {
		return roles.contains(role);
	}

	@Override
	public void accept(NodeLayoutVisitor visitor) {
		visitor.visit(this);		
	}

}
