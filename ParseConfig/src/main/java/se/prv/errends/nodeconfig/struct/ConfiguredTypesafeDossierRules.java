package se.prv.errends.nodeconfig.struct;

public class ConfiguredTypesafeDossierRules implements TypesafeDossierRules {
	
	private DossierRules dr = null;
	
	public ConfiguredTypesafeDossierRules(DossierRules dr) {
		this.dr = dr;
	}

	@Override
	public boolean enableDocumentDownloadAmongCurrents() {
		return evalBooleanProperty("downloadcurrent", false) || evalBooleanProperty("dc", false);
	}

	@Override
	public boolean enableActivationOnCurrentTab() {
		return evalBooleanProperty("activationcurrent", false) || evalBooleanProperty("ac", false);
	}

	@Override
	public boolean enableActivationOnAllTab() {
		return evalBooleanProperty("activationall", false) || evalBooleanProperty("aa", false);
	}

	@Override
	public boolean enableNewDocumentOnCurrentTab() {
		return evalBooleanProperty("uploadcurrent", false) || evalBooleanProperty("uc", false);
	}

	@Override
	public boolean enableDocumentDownloadAmongAll() {
		return evalBooleanProperty("downloadall", false) || evalBooleanProperty("da", false);
	}

	private boolean evalBooleanProperty(String key, boolean defaultResponse) {
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: "+key+", default "+defaultResponse);
		if(!dr.hasKey(key)) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 1 "+defaultResponse);
			return defaultResponse;
		}
		String value = dr.getValue(key);
		if(value == null) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 2 "+defaultResponse);
			return defaultResponse;
		}
		if(value.toLowerCase().contains("n")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 3 false");
			return false;
		}
		if(value.toLowerCase().contains("y") || value.toLowerCase().contains("j")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 true");
			return true;
		}
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 "+defaultResponse);
		return defaultResponse;
	}

}
