package se.prv.errends.nodeconfig.struct;

import java.util.HashMap;
import java.util.Map;

public class DossierRules {

	private Map<String, String> kvp = new HashMap<String, String>();
	
	public void addKeyValuePair(String key, String value) {
		kvp.put(key, value);
	}

	public boolean hasKey(String key) {
		return kvp.keySet().contains(key);
	}

	public String getValue(String key) {
		return kvp.get(key);
	}

}
