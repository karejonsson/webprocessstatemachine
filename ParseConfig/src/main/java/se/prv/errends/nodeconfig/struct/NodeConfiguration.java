package se.prv.errends.nodeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class NodeConfiguration {

	private List<String> roles = new ArrayList<String>();
	private String explanationFunctionName;
	private String explanationLiteral;
	
	public void addRole(String role) {
		if(!roles.contains(role)) {
			roles.add(role);
		}
	}
	
	private String anonymousStartRole = null;
	
	public void setAnonymousStartRole(String role) {
		addRole(role);
		anonymousStartRole = role;
	}

	public String getAnonymousStartRole() {
		return anonymousStartRole;
	}

    public List<String> getAllRolenames() {
        return roles;
    }

	public boolean isAnonymousStartValid() {
		return anonymousStartRole != null;
	}
	
	private boolean spontaneousEnd = false;
	
	public boolean isSpontaneousEnd() {
		return spontaneousEnd;
	}
	
	public void setSpontaneousEnd() {
		spontaneousEnd = true;
	}
	
	private List<NodeLayout> nodeLayouts = new ArrayList<NodeLayout>();
	
	public void addNodeLayout(NodeLayout nl) {
		nodeLayouts.add(nl);
	}
	 
	public List<NodeLayout> getNodelayouts(String rolename) {
		List<NodeLayout> out = new ArrayList<NodeLayout>();
		for(NodeLayout nl : nodeLayouts) {
			if(nl.hasRole(rolename)) {
				out.add(nl);
			}
		}
		return out;
	}
	
	public DossierRules getDossierRules(String rolename) {
		List<NodeLayout> out = new ArrayList<NodeLayout>();
		for(NodeLayout nl : nodeLayouts) {
			if(nl.hasRole(rolename)) {
				if(nl instanceof GuiFunctions) {
					return ((GuiFunctions) nl).getDossierRules();
				}
			}
		}
		return null;
	}
	
	public String getExplanationFunctionName() {
		return explanationFunctionName;
	}
	
	public void setExplanationFunctionName(String explanationFunctionName) {
		this.explanationFunctionName = explanationFunctionName;
	}
	
	public String getExplanationLiteral() {
		return explanationLiteral;
	}
	
	public void setExplanationLiteral(String explanationLiteral) {
		this.explanationLiteral = explanationLiteral;
	}

	public List<String> getStringFunctions() {
		List<String> out = new ArrayList<String>();
		out.add(explanationFunctionName);
		return out;
	}

}
