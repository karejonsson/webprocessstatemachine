package se.prv.errends.nodeconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class HistoryView implements NodeView {

	public HistoryView() {
	}

	@Override
	public List<String> getRoles() {
		return new ArrayList<String>();
	}

	@Override
	public void accept(NodeViewVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String getName() {
		return "Händelser";
	}
	
}
