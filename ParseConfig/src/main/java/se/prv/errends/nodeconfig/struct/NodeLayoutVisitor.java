package se.prv.errends.nodeconfig.struct;

public interface NodeLayoutVisitor {

	void visit(GuiFunctions tabbedLayout);

}
