package se.prv.errends.nodeconfig.struct;

public interface NodeViewVisitor {

	void visit(ReportView reportView);
	void visit(CommentView commentView);
	void visit(SessionView sessionView);
	void visit(HistoryView sessionView);
	void visit(ErrendstateView sessionView);
	void visit(ParticipantView sessionView);

}
