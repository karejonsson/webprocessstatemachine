package se.prv.errends.nodeconfig.struct;

import java.util.List;

public interface NodeView {
	
	List<String> getRoles();
	
	void accept(NodeViewVisitor visitor);

	String getName();

}
