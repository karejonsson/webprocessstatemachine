package se.prv.errends.nodeconfig.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentView implements NodeView {

	@Override
	public List<String> getRoles() {
		return new ArrayList<String>();
	}

	@Override
	public void accept(NodeViewVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String getName() {
		return "Kommentarer";
	}
	
	private Map<String, String> kvp = new HashMap<String, String>();
	
	public void addKeyValuePair(String key, String value) {
		kvp.put(key, value);
	}

	public boolean hasKey(String key) {
		return kvp.keySet().contains(key);
	}

	public String getValue(String key) {
		return kvp.get(key);
	}

}