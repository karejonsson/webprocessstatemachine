package se.prv.errends.nodeconfig.struct;

public interface TypesafeDossierRules {
	
	boolean enableDocumentDownloadAmongCurrents();
	boolean enableActivationOnCurrentTab();
	boolean enableActivationOnAllTab();
	boolean enableNewDocumentOnCurrentTab();
	boolean enableDocumentDownloadAmongAll();
	
}
