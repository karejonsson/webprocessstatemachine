package se.prv.errends.parametertemplateconfig.struct;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import se.prv.errends.parametertemplateconfig.parse.PrvErrendParameterTemplateParser;

public class ParameterTemplateHandling {

	public static List<ParameterTemplate> createParameterList(String s, String sourceSpecifcation) throws Exception {
		if(s == null) {
			return null;
		}
		List<ParameterTemplate> out = PrvErrendParameterTemplateParser.parseParameterTemplate(s.getBytes("UTF8"), sourceSpecifcation);
		return out;
	}

	public static List<ParameterTemplate> createParameterList(byte bytes[], String sourceSpecifcation) throws Exception {
		if(bytes == null) {
			return null;
		}
		List<ParameterTemplate> out = PrvErrendParameterTemplateParser.parseParameterTemplate(bytes, sourceSpecifcation);
		return out;
	}

	public static List<ParameterTemplate> createParameterList(InputStream is, String sourceSpecifcation) throws Exception {
		if(is == null) {
			return null;
		}
		List<ParameterTemplate> out = PrvErrendParameterTemplateParser.parseParameterTemplate(is, sourceSpecifcation);
		return out;
	}

	public static String serializeParameterListToString(List<ParameterTemplate> l) throws UnsupportedEncodingException {
		if(l == null) {
			return new String();
		}
		if(l.size() == 0) {
			return "[]";
		}
		if(l.size() == 1) {
			return "[ "+l.get(0).toString()+" ]";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("[ "+l.get(0));
		for(int i = 1 ; i < l.size(); i++) {
			sb.append(", "+l.get(i));
		}
		return sb.toString()+" ]";
	}

	public static byte[] serializeParameterListToBytes(List<ParameterTemplate> l) throws UnsupportedEncodingException {
		return serializeParameterListToString(l).getBytes("UTF8");
	}

}

/*
PrvErrendParameterMetaParser implements PrvErrendParameterMetaParserConstants {

public static List<ParameterMeta> parseParametermeta(byte bytes[], String sourceSpecifcation) throws Exception {
   return parseParametermeta(new ByteArrayInputStream(bytes), sourceSpecifcation);
}

public static List<ParameterMeta> parseParametermeta(InputStream is, String sourceSpecifcation)
*/