package se.prv.errends.parametertemplateconfig.struct;

public class ParameterTemplate {
	
	private Integer maxlen = null;
	private Integer minlen = null;
	private String name = null;
	private String description = null;
	
	public String toString() {
		return "{ MAX "+(maxlen != null ? ""+maxlen : "")+", MIN "+(minlen != null ? ""+minlen : "")+", \""+name+"\", "+(description != null ? "\'\'\'\'\'\'"+description+"\'\'\'\'\'\'" : "")+" }";
	}
	
	public ParameterTemplate(Integer maxlen, Integer minlen, String name, String description) {
		this.maxlen = maxlen;
		this.minlen = minlen;
		this.name = name;
		this.description = description;
	}
	
	public Integer getMaxlen() {
		return maxlen;
	}
	public String getMaxlenS() {
		if(maxlen == null) {
			return "";
		}
		return ""+maxlen;
	}
	public void setMaxlen(Integer maxlen) {
		this.maxlen = maxlen;
	}
	public void setMaxlenS(String maxlen) {
		if(maxlen == null) {
			this.maxlen = null;
			return;
		}
		if(maxlen.trim().length() == 0) {
			this.maxlen = null;
			return;
		}
		this.maxlen = Integer.parseInt(maxlen);
	}
	public Integer getMinlen() {
		return minlen;
	}
	public String getMinlenS() {
		if(minlen == null) {
			return "";
		}
		return ""+minlen;
	}
	public void setMinlen(Integer minlen) {
		this.minlen = minlen;
	}
	public void setMinlenS(String minlen) {
		if(minlen == null) {
			this.minlen = null;
			return;
		}
		if(minlen.trim().length() == 0) {
			this.minlen = null;
			return;
		}
		this.minlen = Integer.parseInt(minlen);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
