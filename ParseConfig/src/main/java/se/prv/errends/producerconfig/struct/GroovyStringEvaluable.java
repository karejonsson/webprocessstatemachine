package se.prv.errends.producerconfig.struct;

public class GroovyStringEvaluable extends GroovyEvaluable {
	
	public static String typename = "string";

	public GroovyStringEvaluable(String script) {
		super(script);
	}

	public String eval() {
		return null;
	}

	@Override
	public String getType() {
		return typename;
	}

	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
