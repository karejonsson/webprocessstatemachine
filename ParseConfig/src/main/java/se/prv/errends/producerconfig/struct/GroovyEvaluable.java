package se.prv.errends.producerconfig.struct;

public abstract class GroovyEvaluable {
	
	private String script = null;
	
	public GroovyEvaluable(String script) {
		this.script = script;
	}
	
	public String getScript() {
		return script;
	}
	
	public String toStringReproduce() {
		return "groovy-evaluable ''''''"+script+"''''''";
	}
	
	public abstract String getType();
	public abstract void accept(ProducerConfigVisitor pcv);

}
