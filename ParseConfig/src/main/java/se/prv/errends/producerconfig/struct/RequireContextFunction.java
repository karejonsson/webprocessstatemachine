package se.prv.errends.producerconfig.struct;

public interface RequireContextFunction {

	String toStringReproduce();
	Object eval();
	void accept(ProducerConfigVisitor pcv);

}
