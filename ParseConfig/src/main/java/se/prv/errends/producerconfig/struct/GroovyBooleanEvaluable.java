package se.prv.errends.producerconfig.struct;

public class GroovyBooleanEvaluable extends GroovyEvaluable {

	public static String typename = "boolean";

	public GroovyBooleanEvaluable(String script) {
		super(script);
	}
	
	public String eval() {
		return null;
	}

	@Override
	public String getType() {
		return typename;
	}

	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
