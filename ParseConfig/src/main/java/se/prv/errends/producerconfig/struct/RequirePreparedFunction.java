package se.prv.errends.producerconfig.struct;

public interface RequirePreparedFunction {
	
	String toStringReproduce();
	Object eval();
	void accept(ProducerConfigVisitor pcv);

}
