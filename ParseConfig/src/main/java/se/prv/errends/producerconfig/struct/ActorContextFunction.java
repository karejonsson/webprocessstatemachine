package se.prv.errends.producerconfig.struct;

public class ActorContextFunction implements RequireContextFunction {

	private String name = null;
	
	public ActorContextFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public Object eval() {
		return null;
	}

	@Override
	public String toStringReproduce() {
		return "require-context actor-instance "+name;
	}
	
	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}