package se.prv.errends.producerconfig.struct;

public class AllNotInitiatedErrendsFunction implements RequirePreparedFunction {
	
	private String name;

	public AllNotInitiatedErrendsFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toStringReproduce() {
		return "require-prepared all-not-initiated-errends "+name;
	}
	
	@Override
	public Object eval() {
		return null;
	}

	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
