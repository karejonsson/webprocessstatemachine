package se.prv.errends.producerconfig.struct;

public class InstallationIdentifierCurrentFunction implements RequireCurrentFunction {

	private String name = null;
	
	public InstallationIdentifierCurrentFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Object eval() {
		return null;
	}

	@Override
	public String toStringReproduce() {
		return "require-current installation-identifier "+name;
	}
	
	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
