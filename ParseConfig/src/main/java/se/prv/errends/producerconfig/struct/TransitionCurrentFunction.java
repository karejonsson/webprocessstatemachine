package se.prv.errends.producerconfig.struct;

public class TransitionCurrentFunction implements RequireCurrentFunction {

	private String name = null;
	
	public TransitionCurrentFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Object eval() {
		return null;
	}

	@Override
	public String toStringReproduce() {
		return "require-current transition-instance "+name;
	}
	
	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
