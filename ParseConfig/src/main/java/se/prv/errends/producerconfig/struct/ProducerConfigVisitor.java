package se.prv.errends.producerconfig.struct;

public interface ProducerConfigVisitor {

	void visit(ActorCurrentFunction aif);
	void visit(ActorContextFunction aif);
	void visit(TransitionCurrentFunction aif);
	void visit(SuperuserCurrentFunction suif);
	void visit(AllNotInitiatedErrendsFunction anief);
	void visit(GroovyBooleanEvaluable gbe);
	void visit(GroovyStringEvaluable gse);
	void visit(ProducerConfiguration pc);
	void visit(MessageCurrentFunction mcf);
	void visit(InstallationIdentifierCurrentFunction iicf);
	
}
