package se.prv.errends.producerconfig.struct;

public class SuperuserCurrentFunction implements RequireCurrentFunction {

	private String name = null;
	public SuperuserCurrentFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Object eval() {
		return null;
	}

	@Override
	public String toStringReproduce() {
		return "require-current superuser-instance "+name;
	}

	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
