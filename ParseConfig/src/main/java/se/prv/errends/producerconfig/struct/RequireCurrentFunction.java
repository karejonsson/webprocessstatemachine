package se.prv.errends.producerconfig.struct;

public interface RequireCurrentFunction {

	String toStringReproduce();
	Object eval();
	void accept(ProducerConfigVisitor pcv);
	
}
