package se.prv.errends.producerconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class ProducerConfiguration {
	
	private String name = null;

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	private List<RequirePreparedFunction> preparedfunctions = new ArrayList<RequirePreparedFunction>();
	
	public void addRequiredPreparedFunction(RequirePreparedFunction rpf) {
		preparedfunctions.add(rpf);
	}
	
	public int getNoPreparedFunctions() {
		return preparedfunctions.size();
	}
	
	public RequirePreparedFunction getPreparedFunctions(int idx) {
		if(idx < 0) {
			return null;
		}
		if(idx >= preparedfunctions.size()) {
			return null;
		}
		return preparedfunctions.get(idx);
	}
	
	private List<RequireCurrentFunction> currentfunctions = new ArrayList<RequireCurrentFunction>();

	public void addRequiredCurrentFunction(RequireCurrentFunction rcf) {
		currentfunctions.add(rcf);
	}
	
	public int getNoCurrentFunctions() {
		return currentfunctions.size();
	}
	
	public RequireCurrentFunction getCurrentFunctions(int idx) {
		if(idx < 0) {
			return null;
		}
		if(idx >= currentfunctions.size()) {
			return null;
		}
		return currentfunctions.get(idx);
	}
	
	private List<RequireContextFunction> contextfunctions = new ArrayList<RequireContextFunction>();

	public void addRequiredContextFunction(RequireContextFunction rcf) {
		contextfunctions.add(rcf);
	}
	
	public int getNoContextFunctions() {
		return contextfunctions.size();
	}
	
	public RequireContextFunction getContextFunctions(int idx) {
		if(idx < 0) {
			return null;
		}
		if(idx >= contextfunctions.size()) {
			return null;
		}
		return contextfunctions.get(idx);
	}
	
	private GroovyEvaluable groovyevaluable = null;
	
	public String getTypename() {
		return groovyevaluable.getType();
	}
	
	public void setGroovyEvaluable(GroovyEvaluable ge) {
		groovyevaluable = ge;
	}
	
	public GroovyEvaluable getGroovyEvaluable() {
		return groovyevaluable;
	}
	
	public String toStringReproduce() {
		StringBuffer sb = new StringBuffer();
		
		sb.append(groovyevaluable.getType()+"-producer "+name+"\n");
		
		for(RequirePreparedFunction preparedfunction : preparedfunctions) {
			sb.append(preparedfunction.toStringReproduce()+"\n");
		}
		
		for(RequireCurrentFunction currentfunction : currentfunctions) {
			sb.append(currentfunction.toStringReproduce()+"\n");
		}
		
		for(RequireContextFunction contextfunction : contextfunctions) {
			sb.append(contextfunction.toStringReproduce()+"\n");
		}
		
		sb.append(groovyevaluable.toStringReproduce()+"\n");
		
		return sb.toString();
	}
	
	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
