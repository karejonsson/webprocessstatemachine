package se.prv.errends.producerconfig.struct;

import java.util.ArrayList;
import java.util.List;

public class CreateExhaustiveInstances {
	
	/*
	 * Maintain the list below.
	 * Let this be a complete list with all allowed permutations
	 */
	public static ProducerConfiguration getComplete() {
		ProducerConfiguration out = new ProducerConfiguration();
		out.setName("NAME");
		out.addRequiredContextFunction(new ActorContextFunction("instancename"));
		out.addRequiredCurrentFunction(new SuperuserCurrentFunction("instancename"));
		out.addRequiredCurrentFunction(new TransitionCurrentFunction("instancename"));
		out.addRequiredCurrentFunction(new ActorCurrentFunction("instancename"));
		out.addRequiredPreparedFunction(new AllNotInitiatedErrendsFunction("instancename"));
		out.addRequiredCurrentFunction(new MessageCurrentFunction("messageinstancename"));
		out.addRequiredCurrentFunction(new InstallationIdentifierCurrentFunction("installationidentifierinstancename"));
		out.setGroovyEvaluable(new GroovyStringEvaluable("return 'x';"));
		return out;
	}
	
	public static ProducerConfiguration getBooleanProducerForAllowTransition() {
		ProducerConfiguration out = new ProducerConfiguration();
		out.setName("unique-name-of-function");
		out.addRequiredContextFunction(new ActorContextFunction("recievingactorinstancename"));
		out.addRequiredCurrentFunction(new TransitionCurrentFunction("transitioninstancename"));
		out.addRequiredCurrentFunction(new ActorCurrentFunction("clickingactorinstancename"));
		out.setGroovyEvaluable(new GroovyBooleanEvaluable("return true;"));
		return out;
	}
	
	public static ProducerConfiguration getStringProducerForRegularTransition() {
		ProducerConfiguration out = new ProducerConfiguration();
		out.setName("unique-name-of-function");
		out.addRequiredContextFunction(new ActorContextFunction("recievingactorinstancename"));
		out.addRequiredCurrentFunction(new TransitionCurrentFunction("transitioninstancename"));
		out.addRequiredCurrentFunction(new ActorCurrentFunction("clickingactorinstancename"));
		out.addRequiredCurrentFunction(new InstallationIdentifierCurrentFunction("installationidentifierinstancename"));
		out.setGroovyEvaluable(new GroovyStringEvaluable("return 'x';"));
		return out;
	}
	
	public static ProducerConfiguration getStringProducerForMessageSent() {
		ProducerConfiguration out = new ProducerConfiguration();
		out.setName("unique-name-of-function");
		out.addRequiredContextFunction(new ActorContextFunction("recievingactorinstancename"));
		out.addRequiredCurrentFunction(new MessageCurrentFunction("messageinstancename"));
		out.addRequiredCurrentFunction(new ActorCurrentFunction("clickingactorinstancename"));
		out.addRequiredCurrentFunction(new InstallationIdentifierCurrentFunction("installationidentifierinstancename"));
		out.setGroovyEvaluable(new GroovyStringEvaluable("return 'x';"));
		return out;
	}
	
	public static ProducerConfiguration getStringProducerForSuperuserToInitiateErrendsMail() {
		ProducerConfiguration out = new ProducerConfiguration();
		out.setName("superuser-to-inititate");
		out.addRequiredCurrentFunction(new ActorCurrentFunction("systeminstancename"));
		out.addRequiredPreparedFunction(new AllNotInitiatedErrendsFunction("errendslistinstancename"));
		out.addRequiredCurrentFunction(new InstallationIdentifierCurrentFunction("installationidentifierinstancename"));
		out.setGroovyEvaluable(new GroovyStringEvaluable("return 'x';"));
		return out;
	}
	
	public static String getStringProducerForRegularTransitionInComment() {
		return "/*\n"+
				"// This produces a mail of title for a transition\n"+
				getStringProducerForRegularTransition().toStringReproduce()+
				"\n*/\n";
	}
	
	public static String getStringProducerForMessageSentInComment() {
		return "/*\n"+
				"// This produces a mail when sending a mail\n"+
				getStringProducerForMessageSent().toStringReproduce()+
				"\n*/\n";
	}
	
	public static String getStringProducerForSuperuserToInitiateErrendsMailInComment() {
		return "/*\n"+
				"// This produces a mail or title for the system to alert the\n"+
				"// superuser about errends needing initiation.\n"+
				getStringProducerForSuperuserToInitiateErrendsMail().toStringReproduce()+
				"\n*/\n";
	}
	
	public static String getBooleanProducerForAllowTransitionInComment() {
		return "/*\n"+
				"// This produces a response as weather a transition is\n"+
				"// allowed given some state.\n"+
				getBooleanProducerForAllowTransition().toStringReproduce()+
				"\n*/\n";
	}
	
	public static List<ProducerConfiguration> getAllPermutations() {
		List<ProducerConfiguration> out = new ArrayList<ProducerConfiguration>();
		out.add(getStringProducerForSuperuserToInitiateErrendsMail());
		out.add(getStringProducerForRegularTransition());
		out.add(getBooleanProducerForAllowTransition());
		return out;
	}

	public static String getAllInComments() {
		StringBuffer out = new StringBuffer();
		out.append(getStringProducerForRegularTransitionInComment());
		out.append(getStringProducerForMessageSentInComment());
		out.append(getStringProducerForSuperuserToInitiateErrendsMailInComment());
		out.append(getBooleanProducerForAllowTransitionInComment());
		return out.toString();
	}

}
