package se.prv.errends.producerconfig.struct;

public class MessageCurrentFunction implements RequireCurrentFunction {

	private String name = null;
	
	public MessageCurrentFunction(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Object eval() {
		return null;
	}

	@Override
	public String toStringReproduce() {
		return "require-current message-instance "+name;
	}
	
	public void accept(ProducerConfigVisitor pcv) {
		pcv.visit(this);
	}

}
