options {
  JAVA_UNICODE_ESCAPE = true;
  STATIC = false;
} 

PARSER_BEGIN(PrvErrendNodeConfigParser)

package se.prv.errends.nodeconfig.parse;

import java.io.*;
import java.util.*;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;
import se.prv.errends.nodeconfig.struct.NodeLayout;
import se.prv.errends.nodeconfig.struct.NodeView;
import se.prv.errends.nodeconfig.struct.GuiFunctions;
import se.prv.errends.nodeconfig.struct.DossierRules;
import se.prv.errends.nodeconfig.struct.ReportView;
import se.prv.errends.nodeconfig.struct.CommentView;
import se.prv.errends.nodeconfig.struct.SessionView;
import se.prv.errends.nodeconfig.struct.HistoryView;
import se.prv.errends.nodeconfig.struct.ParticipantView;
import se.prv.errends.nodeconfig.struct.ErrendstateView;

public class PrvErrendNodeConfigParser
{

  public static NodeConfiguration parseNodeConfig(InputStreamReader isr, String sourceSpecifcation) throws Exception {
     PrvErrendNodeConfigParser parser = new PrvErrendNodeConfigParser(isr);
     NodeConfiguration nc = new NodeConfiguration();
     parser.parseNodeConfig(nc, sourceSpecifcation);
     return nc;
  }

}

PARSER_END(PrvErrendNodeConfigParser)

/* WHITE SPACE */

SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
| "\f"
}

/* COMMENTS */

MORE :
{
  "//" : IN_SINGLE_LINE_COMMENT
|
  <"/**" ~["/"]> { input_stream.backup(1); } : IN_FORMAL_COMMENT
|
  "/*" : IN_MULTI_LINE_COMMENT
}

<IN_SINGLE_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <SINGLE_LINE_COMMENT: "\n" | "\r" | "\r\n" > : DEFAULT
}

<IN_FORMAL_COMMENT>
SPECIAL_TOKEN :
{
  <FORMAL_COMMENT: "*/" > : DEFAULT
}

<IN_MULTI_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <MULTI_LINE_COMMENT: "*/" > : DEFAULT
}

<IN_SINGLE_LINE_COMMENT,IN_FORMAL_COMMENT,IN_MULTI_LINE_COMMENT>
MORE :
{
  < ~[] >
}

/* RESERVED WORDS AND LITERALS */

TOKEN :
{
  <ANONYMOUSSTART: ("anonymous-start") >
| <SEMICOLON: (";") >
| <GUIFUNCTIONS: ("gui-functions") >
| <DOCUMENTS: ("documents") >
| <REPORTS: ("reports") >
| <COMMENTS: ("comments") >
| <SESSIONS: ("sessions") >
| <HISTORY: ("history") >
| <PARTICIPANTS: ("participants") >
| <ERRENDSTATE: ("errendstate") >
| <LEFTCURLY: ("{") >
| <RIGTHCURLY: ("}") >
| <SPONTANEOUSEND: ("spontaneous-end") >
| <COMMA: (",") >
| <EXPLANATIONEVALUATE: ("explanation-evaluate") >
| <EXPLANATIONLITERAL: ("explanation-literal") >
}

/* LITERALS */ 

TOKEN :
{
  < INTEGER_LITERAL:
        <DECIMAL_LITERAL> (["l","L"])?
      | <HEX_LITERAL> (["l","L"])?
      | <OCTAL_LITERAL> (["l","L"])?
  >
|
  < #DECIMAL_LITERAL: ("-")? ["1"-"9"] (["0"-"9"])* >
|
  < #HEX_LITERAL: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"])+ >
|
  < #OCTAL_LITERAL: "0" (["0"-"7"])* >
|
  < FLOATING_POINT_LITERAL:
        ("-")? (["0"-"9"])+ "." (["0"-"9"])* (<EXPONENT>)? (["f","F","d","D"])?
      | "." (["0"-"9"])+ (<EXPONENT>)? (["f","F","d","D"])?
      | (["0"-"9"])+ <EXPONENT> (["f","F","d","D"])?
      | (["0"-"9"])+ (<EXPONENT>)? ["f","F","d","D"]
  >
|
  < #EXPONENT: ["e","E"] (["+","-"])? (["0"-"9"])+ >
|
  < CHARACTER_LITERAL:
      "'"
      (   (~["'","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )
      "'"
  >
|
  < STRING_CITATION_LITERAL:
    ( 
      "\'"
      (   (~["\\","\n","\r","'"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      "\'"
    |
      "`"
      (   (~["\\","\n","\r","`"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      "`"
    )
  >
|
  < STRING_LITERAL:
      "\""
      (   (~["\"","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      "\""
  >
|
  < GROOVY_LITERAL_1:
      (["\""]){3}
      (
          (["\""]){1, 2} (~["\""])
        |
          (~["\""])
        |
          ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      (["\""]){3}
  >
|
  < GROOVY_LITERAL_2:
      (["\'"]){3}
      (
          (["\'"]){1, 2} (~["\'"])
        |
          (~["\'"])
        |
          ("\\"
            ( ["n","t","b","r","f","\\","\"","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      (["\'"]){3}
  >
|
  < GROOVYSCRIPT_LITERAL_1:
      (["\""]){6}
      (
          (["\""]){1, 5} (~["\""])
        |
          (~["\""])
        |
          ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      (["\""]){6}
  >
|
  < GROOVYSCRIPT_LITERAL_2:
      (["\'"]){6}
      (
          (["\'"]){1, 5} (~["\'"])
        |
          (~["\'"])
        |
          ("\\"
            ( ["n","t","b","r","f","\\","\"","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      (["\'"]){6}
  >
}

/* IDENTIFIERS */

/*
å: 00E5
ä: 00E4
ö: 00F6
Å: 00C5
Ä: 00C4
Ö: 00D6
*/
TOKEN :
{
  <IDENTIFIER: <LETTER> (<LETTER>|<DIGIT>|<PARTICULARS>)* >
|
  <#LETTER:
      [
       "\u0024",
       "\u0041"-"\u005a",
       "\u005f",
       "\u0061"-"\u007a",
       "\uc384","\uc385","\uc396","\uc3a4","\uc3a5","\uc3b6", "\u2030", 
       "\u00c0"-"\u00d6",
       "\u00d8"-"\u00f6",
       "\u00f8"-"\u00ff",
       "\u0100"-"\u1fff",
       "\u3040"-"\u318f",
       "\u3300"-"\u337f",
       "\u3400"-"\u3d2d",
       "\u4e00"-"\u9fff",
       "\uf900"-"\ufaff"
      ]
  >
|
  <#DIGIT:
      [
       "\u0030"-"\u0039",
       "\u0660"-"\u0669",
       "\u06f0"-"\u06f9",
       "\u0966"-"\u096f",
       "\u09e6"-"\u09ef",
       "\u0a66"-"\u0a6f",
       "\u0ae6"-"\u0aef",
       "\u0b66"-"\u0b6f",
       "\u0be7"-"\u0bef",
       "\u0c66"-"\u0c6f",
       "\u0ce6"-"\u0cef",
       "\u0d66"-"\u0d6f",
       "\u0e50"-"\u0e59",
       "\u0ed0"-"\u0ed9",
       "\u1040"-"\u1049"
      ]
  >
|
  <#PARTICULARS:
      [
       "\u002d"
      ]
  >
}

/*********************************************************
 *    THE PRV ERREND NODE CONFIG GRAMMAR STARTS HERE     *
 *********************************************************/

/*
 * Program structuring syntax follows.
 */

void parseNodeConfig(NodeConfiguration nc, String sourceSpecifcation) throws Exception :
{
    Token t = null;
}
{
  (
    parseAnonymousStart(nc, sourceSpecifcation)
  |
    parseExplanation(nc, sourceSpecifcation)
  |
    parseSpontaneousEnd(nc, sourceSpecifcation)
  |
    parseLayouts(nc, sourceSpecifcation)
  )*
  <EOF>
}

void parseAnonymousStart(NodeConfiguration nc, String sourceSpecifcation) throws Exception :
{
    Token t = null;
}
{
  <ANONYMOUSSTART> 
  t = <IDENTIFIER> { nc.setAnonymousStartRole(t.image); } 
  <SEMICOLON>
}

void parseExplanation(NodeConfiguration nc, String sourceSpecification) throws Exception :
{
    String s = null;
}
{
  (
	<EXPLANATIONEVALUATE> 
	s = parseComplexCitation(sourceSpecification)
    { nc.setExplanationFunctionName(s); } 
  |
    <EXPLANATIONLITERAL> 
	s = parseComplexCitation(sourceSpecification)
    { nc.setExplanationLiteral(s); } 
  )
  <SEMICOLON>
}

void parseSpontaneousEnd(NodeConfiguration nc, String sourceSpecifcation) throws Exception :
{
    Token t = null;
}
{
  <SPONTANEOUSEND> 
  { nc.setSpontaneousEnd(); } 
  <SEMICOLON>
}

void parseLayouts(NodeConfiguration nc, String sourceSpecifcation) throws Exception :
{
    Token t = null;
    NodeLayout nodelayout = null;
}
{
  (
    nodelayout = parseTabbedLayout(sourceSpecifcation) { nc.addNodeLayout(nodelayout); }
  )
}

GuiFunctions parseTabbedLayout(String sourceSpecifcation) throws Exception :
{
    Token t = null;
    NodeView nv = null;
    GuiFunctions gf = new GuiFunctions();
    DossierRules dr = null;
}
{
  (
    <GUIFUNCTIONS> t = <IDENTIFIER> { gf.addRole(t.image); } ( <COMMA> t = <IDENTIFIER> { gf.addRole(t.image); } )* <LEFTCURLY>
    (
      dr = parseDossierRules(sourceSpecifcation) { gf.setDossierRules(dr); }
    |
      nv = parseReportView(sourceSpecifcation) { gf.addNodeView(nv); }
    |
      nv = parseCommentView(sourceSpecifcation) { gf.addNodeView(nv); }
    |
      nv = parseSessionView(sourceSpecifcation) { gf.addNodeView(nv); }
    |
      nv = parseHistoryView(sourceSpecifcation) { gf.addNodeView(nv); }
    |
      nv = parseErrendstateView(sourceSpecifcation) { gf.addNodeView(nv); }
    |
      nv = parseParticipantView(sourceSpecifcation) { gf.addNodeView(nv); }
    )+    
    <RIGTHCURLY>
  )
  {
    return gf;
  }
}

DossierRules parseDossierRules(String sourceSpecifcation) throws Exception :
{
    Token k = null;
    Token v = null;
    DossierRules dv = new DossierRules();
}
{
  (
    <DOCUMENTS> 
    [
      k = <IDENTIFIER> 
      ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
      { dv.addKeyValuePair(k.image, v.image); }
      ( <COMMA>
	      k = <IDENTIFIER> ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
	      { dv.addKeyValuePair(k.image, v.image); }
      )*
    ]
    <SEMICOLON>
  )
  {
    return dv;
  }
}

NodeView parseReportView(String sourceSpecifcation) throws Exception :
{
    Token k = null;
    Token v = null;
    ReportView dv = new ReportView();
}
{
  (
    <REPORTS> 
    [
      k = <IDENTIFIER> 
      ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
      { dv.addKeyValuePair(k.image, v.image); }
      ( <COMMA>
	      k = <IDENTIFIER> ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
	      { dv.addKeyValuePair(k.image, v.image); }
      )*
    ]
    <SEMICOLON>
  )
  {
    return dv;
  }
}

NodeView parseCommentView(String sourceSpecifcation) throws Exception :
{
    Token k = null;
    Token v = null;
    CommentView dv = new CommentView();
}
{
  (
    <COMMENTS> 
    [
      k = <IDENTIFIER> 
      ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
      { dv.addKeyValuePair(k.image, v.image); }
      ( <COMMA>
	      k = <IDENTIFIER> ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
	      { dv.addKeyValuePair(k.image, v.image); }
      )*
    ]
    <SEMICOLON>
  )
  {
    return dv;
  }
}

NodeView parseSessionView(String sourceSpecifcation) throws Exception :
{
    SessionView sv = new SessionView();
}
{
  (
    <SESSIONS> <SEMICOLON>
  )
  {
    return sv;
  }
}

NodeView parseHistoryView(String sourceSpecifcation) throws Exception :
{
    HistoryView hv = new HistoryView();
}
{
  (
    <HISTORY> <SEMICOLON>
  )
  {
    return hv;
  }
}

NodeView parseErrendstateView(String sourceSpecifcation) throws Exception :
{
    ErrendstateView ev = new ErrendstateView();
}
{
  (
    <ERRENDSTATE> <SEMICOLON>
  )
  {
    return ev;
  }
}

NodeView parseParticipantView(String sourceSpecifcation) throws Exception :
{
    Token k = null;
    Token v = null;
    ParticipantView pv = new ParticipantView();
}
{
  (
    <PARTICIPANTS> 
    [
      k = <IDENTIFIER> 
      ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
      { pv.addKeyValuePair(k.image, v.image); }
      ( <COMMA>
	      k = <IDENTIFIER> ( v = <IDENTIFIER> | v = <STRING_LITERAL> | v = <INTEGER_LITERAL> ) 
	      { pv.addKeyValuePair(k.image, v.image); }
      )*
    ]
    <SEMICOLON>
  )
  {
    return pv;
  }
}

String parseComplexCitation(String sourceSpecification) throws Exception :
{
  Token v = null;
  String out = null;
}
{
  ( 
  	v = <IDENTIFIER>
    { out = v.image; }
  | 
  	v = <STRING_LITERAL>
    { out = v.image.substring(1, v.image.length()-1); }
  | 
  	v = <STRING_CITATION_LITERAL> 
    { out = v.image.substring(1, v.image.length()-1); }
  | 
  	v = <GROOVY_LITERAL_1> 
    { out = v.image.substring(3, v.image.length()-3); }
  | 
  	v = <GROOVY_LITERAL_2> 
    { out = v.image.substring(3, v.image.length()-3); }
  | 
  	v = <GROOVYSCRIPT_LITERAL_1> 
    { out = v.image.substring(6, v.image.length()-6); }
  | 
  	v = <GROOVYSCRIPT_LITERAL_2> 
    { out = v.image.substring(6, v.image.length()-6); }
  ) 
  {
    return out;
  }
}
