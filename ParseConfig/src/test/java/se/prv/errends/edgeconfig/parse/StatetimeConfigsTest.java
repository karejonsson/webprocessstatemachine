package se.prv.errends.edgeconfig.parse;

import java.io.InputStreamReader;

import org.junit.Test;

import se.prv.errends.edgeconfig.struct.EdgeConfiguration;

public class StatetimeConfigsTest {

    @Test
    public void minutes() throws Exception {
        ClassLoader cl = StatetimeConfigsTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/statetime-minutes.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Empty test");
    }

    @Test
    public void minutesfloat() throws Exception {
        ClassLoader cl = StatetimeConfigsTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/statetime-minutefloat.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Empty test");
    }

}
