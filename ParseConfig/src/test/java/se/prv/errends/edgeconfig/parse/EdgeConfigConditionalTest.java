package se.prv.errends.edgeconfig.parse;

import java.io.InputStreamReader;

import org.junit.Test;

import se.prv.errends.edgeconfig.struct.EdgeConfiguration;

public class EdgeConfigConditionalTest {

    @Test
    public void allowtransconditional() throws Exception {
        ClassLoader cl = EdgeConfigConditionalTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/conditional.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Conditional test");
    }

    @Test
    public void explanation() throws Exception {
        ClassLoader cl = EdgeConfigConditionalTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/explanation.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Explanation test");
    }

    @Test
    public void semicolon() throws Exception {
        ClassLoader cl = EdgeConfigConditionalTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/semicolon.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Semicolon test");
    }

}
