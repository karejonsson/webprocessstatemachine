package se.prv.errends.edgeconfig.parse;

import org.junit.Assert;
import org.junit.Test;

import se.prv.errends.edgeconfig.struct.EdgeConfiguration;

import java.io.InputStreamReader;
import java.util.List;

public class EdgeConfigSimpleTest {

    @Test
    public void empty() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/empty.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Empty test");
    }

    @Test
    public void allowtrans() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/allowtrans.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Allow trans test");
    }

    @Test
    public void email1() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/email1.edgeconfig"));
        EdgeConfiguration ec = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Allow trans test");
        List<String> roles = ec.getRoles();
        for(String role : roles) {
        	System.out.println("R: "+role);
        }
        Assert.assertTrue(roles.size() == 2);
    }

    @Test
    public void email2() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/email2.edgeconfig"));
        EdgeConfiguration ec = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Allow trans test");
        List<String> roles = ec.getRoles();
        for(String role : roles) {
        	System.out.println("R: "+role);
        }
        Assert.assertTrue(roles.size() == 2);
    }

    @Test
    public void email3() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/email3.edgeconfig"));
        EdgeConfiguration ec = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Allow trans test");
        List<String> roles = ec.getRoles();
        for(String role : roles) {
        	System.out.println("R: "+role);
        }
        Assert.assertTrue(roles.size() == 2);
    }

    @Test
    public void swedish() throws Exception {
        ClassLoader cl = EdgeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("edge/swedish.edgeconfig"));
        EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Allow trans test");
    }

}
