package se.prv.errends.parametervalue.parse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import se.prv.errends.parametervalue.struct.ParameterValue;
import se.prv.errends.parametervalue.struct.ParameterValueHandling;

public class BasicParsingTest {
	
	@Test
	public void t1() throws Exception {
		List<ParameterValue> l1 = new ArrayList<ParameterValue>();

		ParameterValue pm1 = new ParameterValue("namn", "beskrivning");
		l1.add(pm1);
		
		ParameterValue pm2 = new ParameterValue("namnx", "beskrivningx");
		l1.add(pm2);
		
		String ser1 = ParameterValueHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterValue> l2 = ParameterValueHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterValueHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void noDescr() throws Exception {
		List<ParameterValue> l1 = new ArrayList<ParameterValue>();

		ParameterValue pm1 = new ParameterValue("namn", null);
		l1.add(pm1);
		
		ParameterValue pm2 = new ParameterValue("namnx", null);
		l1.add(pm2);
		
		String ser1 = ParameterValueHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterValue> l2 = ParameterValueHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterValueHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

}
