package se.prv.errends.producers.parse;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.CreateExhaustiveInstances;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class ExhaustiveTest {

	@Test
	public void testAllPermutations() throws Exception {
		List<ProducerConfiguration> all = CreateExhaustiveInstances.getAllPermutations();
		
		for(ProducerConfiguration one : all) {
    		String source = one.toStringReproduce();
    		System.out.println("Source: "+source);
    		InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(source.getBytes("UTF-8")));
        	List<ProducerConfiguration> repropducedProducer = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Retry");
        	Assert.assertTrue(repropducedProducer.size() == 1);
        	String reproducedSource = repropducedProducer.get(0).toStringReproduce();
        	Assert.assertTrue(source.equals(reproducedSource));			
		}
	}

	@Test
	public void comlpete() throws Exception {
		String source = CreateExhaustiveInstances.getComplete().toStringReproduce();
		System.out.println("Source: "+source);
		InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(source.getBytes("UTF-8")));
    	List<ProducerConfiguration> repropducedProducer = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Retry");
    	Assert.assertTrue(repropducedProducer.size() == 1);
    	String reproducedSource = repropducedProducer.get(0).toStringReproduce();
    	Assert.assertTrue(source.equals(reproducedSource));			
	}
	
}
