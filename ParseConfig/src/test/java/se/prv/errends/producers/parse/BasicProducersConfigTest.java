package se.prv.errends.producers.parse;

import java.io.InputStreamReader;
import java.util.List;

import org.junit.Test;
import org.junit.Assert;

import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class BasicProducersConfigTest {
	
	private static InputStreamReader getStream(String streampath) {
        ClassLoader cl = BasicProducersConfigTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream(streampath));
        return isr;
	}

    @Test
    public void empty() throws Exception {
    	List<ProducerConfiguration> producers = PrvErrendProducersConfigParser.parseProducerConfig(getStream("producers/empty.producers"), "Empty test");
    	Assert.assertTrue(producers.size() == 0);
    }

    @Test
    public void first() throws Exception {
    	List<ProducerConfiguration> producers = PrvErrendProducersConfigParser.parseProducerConfig(getStream("producers/first.producers"), "First test");
    	//System.out.println("SIZE "+producers.size());
    	Assert.assertTrue(producers.size() == 3);
    }

}
