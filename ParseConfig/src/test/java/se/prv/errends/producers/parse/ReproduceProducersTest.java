package se.prv.errends.producers.parse;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class ReproduceProducersTest {

	private static InputStreamReader getStream(String streampath) {
        ClassLoader cl = BasicProducersConfigTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream(streampath));
        return isr;
	}

    @Test
    public void first() throws Exception {
    	List<ProducerConfiguration> producers = PrvErrendProducersConfigParser.parseProducerConfig(getStream("producers/first.producers"), "First test");
    	Assert.assertTrue(producers.size() == 3);
    	
    	for(ProducerConfiguration producer : producers) {
    		String source = producer.toStringReproduce();
    		System.out.println("Source: "+source);
    		InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(source.getBytes("UTF-8")));
        	List<ProducerConfiguration> repropducedProducer = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Retry");
        	Assert.assertTrue(repropducedProducer.size() == 1);
        	String reproducedSource = repropducedProducer.get(0).toStringReproduce();
        	Assert.assertTrue(source.equals(reproducedSource));
    	}
    }

}
