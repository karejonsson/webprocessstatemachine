package se.prv.errends.parametertemplateconfig.parse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import se.prv.errends.parametertemplateconfig.struct.ParameterTemplate;
import se.prv.errends.parametertemplateconfig.struct.ParameterTemplateHandling;

public class BasicParsingTest {
	
	@Test
	public void all() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(30, 5, "namn", "beskrivning");
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(31, 6, "namnx", "beskrivningx");
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void notMax() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(null, 5, "namn", "beskrivning");
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(null, 6, "namnx", "beskrivningx");
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void notMin() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(16, null, "namn", "beskrivning");
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(16, null, "namnx", "beskrivningx");
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void notLimits() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(null, null, "namn", "beskrivning");
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(null, null, "namnx", "beskrivningx");
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void notDescr() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(30, 5, "namn", null);
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(31, 6, "namnx", null);
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}

	@Test
	public void nothing() throws Exception {
		List<ParameterTemplate> l1 = new ArrayList<ParameterTemplate>();

		ParameterTemplate pm1 = new ParameterTemplate(null, null, "namn", null);
		l1.add(pm1);
		
		ParameterTemplate pm2 = new ParameterTemplate(null, null, "namnx", null);
		l1.add(pm2);
		
		String ser1 = ParameterTemplateHandling.serializeParameterListToString(l1);
		System.out.println(ser1);
		
		List<ParameterTemplate> l2 = ParameterTemplateHandling.createParameterList(ser1, "spec");

		String ser2 = ParameterTemplateHandling.serializeParameterListToString(l2);
		System.out.println(ser2);
		
		Assert.assertTrue(ser1.equals(ser2));
	}


}
