package se.prv.errends.nodeconfig.parse;

import org.junit.Test;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

import java.io.InputStreamReader;

public class NodeConfigSimpleTest {

    @Test
    public void empty() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/empty.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void start0() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/start0.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void start1() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/start1.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void start2() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/start2.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void start2a() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/start2a.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void session() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/session.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void history() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/history.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void documentparams() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/documentparams.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void swedish() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/swedish.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void report1() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/report1.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

    @Test
    public void report2params() throws Exception {
        ClassLoader cl = NodeConfigSimpleTest.class.getClassLoader();
        InputStreamReader isr = new InputStreamReader(cl.getResourceAsStream("node/report2.nodeconfig"));
        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "NodeConfigSimpleTest");
    }

}
