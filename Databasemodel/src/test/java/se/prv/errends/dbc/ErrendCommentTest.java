package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.ErrendComment;

public class ErrendCommentTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(ErrendCommentDB.getAllErrendComments().size() == 0);
    }

    public static ErrendComment getTestErrendComment(Long errendId, Long sessionId) throws Exception {
        ErrendComment ec = new ErrendComment();
        ec.setComment("Kommentar");
        ec.setErrendId(errendId);
        ec.setSessionId(sessionId);
        ec.setTitle("Titel");
        return ec;
    }

    public static void assertTestErrendComment(ErrendComment ec, Long errendId, Long sessionId) {
        Assert.assertTrue(ec.getSessionId() == sessionId);
        Assert.assertTrue(ec.getErrendId() == errendId);
        Assert.assertTrue(ec.getComment().equals("Kommentar"));
        Assert.assertTrue(ec.getTitle().equals("Titel"));
    }

    public static void recursiveSetup() throws Exception {
        ManagedErrendTest.recursiveSetup();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        Long errendId = ErrendDB.getAllErrends().get(0).getId();
        ErrendComment me = getTestErrendComment(errendId, sessionId);
        ErrendCommentDB.createErrendComment(me);
    }

    @Test
    public void recursiveTest() throws Exception {
        recursiveSetup();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        Long errendId = ErrendDB.getAllErrends().get(0).getId();
        List<ErrendComment> errendcomments = ErrendCommentDB.getAllErrendComments();
        Assert.assertTrue(errendcomments.size() == 1);
        assertTestErrendComment(errendcomments.get(0), errendId, sessionId);

        ErrendComment ec = errendcomments.get(0);
        ec.setComment("Annan kommentar");
        Assert.assertTrue(ErrendCommentDB.updateErrendComment(ec));
        {
            ErrendComment ec2 = ErrendCommentDB.getErrendCommentFromId(ec.getId());
            Assert.assertTrue(ec2.getComment().equals("Annan kommentar"));
        }

        ErrendCommentDB.deleteErrendComment(ec);
        errendcomments = ErrendCommentDB.getAllErrendComments();
        Assert.assertTrue(errendcomments.size() == 0);
    }

}
