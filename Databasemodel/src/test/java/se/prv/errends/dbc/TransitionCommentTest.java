package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.TransitionComment;

public class TransitionCommentTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(TransitionCommentDB.getAllTransitionComments().size() == 0);
    }

    public static TransitionComment getTestTransitionComment(Long documentfileId, Long sessionId) throws Exception {
        TransitionComment tc = new TransitionComment();
        tc.setComment("Kommentar");
        tc.setTransitionId(documentfileId);
        tc.setSessionId(sessionId);
        tc.setTitle("Titel");
        return tc;
    }

    public static void assertTestTransitionComment(TransitionComment tc, Long documentfileId, Long sessionId) {
        Assert.assertTrue(tc.getSessionId() == sessionId);
        Assert.assertTrue(tc.getTransitionId() == documentfileId);
        Assert.assertTrue(tc.getComment().equals("Kommentar"));
        Assert.assertTrue(tc.getTitle().equals("Titel"));
    }

    /*
    public static void recursiveSetup() throws Exception {
        DocumentfileCommentTest.recursiveSetup();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        Long documentfileId = DocumentfileDB.getAllDocumentfiles().get(0).getId();
        TransitionComment tc = getTestTransitionComment(documentfileId, sessionId);
        TransitionCommentDB.createTransitionComment(tc);
    }

    @Test
    public void recursiveTest() throws Exception {
        recursiveSetup();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        Long documentfileId = DocumentfileDB.getAllDocumentfiles().get(0).getId();
        List<TransitionComment> transitioncomments = TransitionCommentDB.getAllTransitionComments();
        Assert.assertTrue(transitioncomments.size() == 1);
        assertTestTransitionComment(transitioncomments.get(0), documentfileId, sessionId);

        TransitionComment tc = transitioncomments.get(0);
        tc.setComment("Annan kommentar");
        Assert.assertTrue(TransitionCommentDB.updateTransitionComment(tc));
        {
            TransitionComment tc2 = TransitionCommentDB.getTransitionCommentFromId(tc.getId());
            Assert.assertTrue(tc2.getComment().equals("Annan kommentar"));
        }

        TransitionCommentDB.deleteTransitionComment(tc);
        transitioncomments = TransitionCommentDB.getAllTransitionComments();
        Assert.assertTrue(transitioncomments.size() == 0);
    }
	*/
    
}
