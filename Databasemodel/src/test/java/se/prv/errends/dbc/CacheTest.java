package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Person;

public class CacheTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

	public static Person getTestPerson(long now, int nr) throws Exception {
		Person p = new Person();
		p.setActive(true);
		p.setEmail("a.b@c.d");
		p.setFamilyname("Fam name");
		p.setFirstname("first name");
		String nnn = ""+nr;
		while(nnn.length() < 3) {
			nnn = "0"+nnn;
		}
		p.setSwedishpnr(Person.makeCompletionWithChecknumber("690301"+nnn));
		p.setRegisterred(new Date(now));
		return p;
	}

    @Test
    public void cache1() throws Exception {
    	Map<Integer, String> m = new HashMap<Integer, String>();
    	for(int i = 0  ; i < 100 ; i++) {
    		Person p = getTestPerson(System.currentTimeMillis(), i);
    		String fn = "X"+i;
    		p.setFamilyname(fn);
    		PersonDB.createPerson(p);
    		m.put(i, fn);
    	}
    	for(Integer key : m.keySet()) {
    		Person p = PersonDB.getPersonFromId(key);
    		String fn = m.get(key);
    		Assert.assertTrue(p.getFamilyname().equals(fn));
    	}
    }
    
}
