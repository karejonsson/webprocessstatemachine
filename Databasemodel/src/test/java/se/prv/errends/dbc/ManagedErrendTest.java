package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.ManagedErrend;

public class ManagedErrendTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(ManagedErrendDB.getAllManagedErrends().size() == 0);
    }

    public static ManagedErrend getTestManagedErrend(Long currentstateId, Long errendId, Long processdefId, Long lasttransitionId) throws Exception {
        ManagedErrend me = new ManagedErrend();
        me.setCurrentstateId(currentstateId);
        me.setErrendId(errendId);
        me.setLasttransitionId(processdefId);
        me.setProcessdefId(lasttransitionId);
        return me;
    }

    public static void assertTestManagedErrend(ManagedErrend t, Long currentstateId, Long errendId, Long processdefId, Long lasttransitionId) {
        Assert.assertTrue(t.getCurrentstateId() == currentstateId);
        Assert.assertTrue(t.getErrendId() == errendId);
        Assert.assertTrue(t.getProcessdefId() == processdefId);
        Assert.assertTrue(t.getLasttransitionId() == lasttransitionId);
    }

    public static void recursiveSetup() throws Exception {
        TransitionTest.recursiveSetup();
        Long currentstateId = StatedefDB.getAllStatedefs().get(0).getId();
        Long errendId = ErrendDB.getAllErrends().get(0).getId();
        Long processdefId = ProcessdefDB.getAllProcessdefs().get(0).getId();
        Long transitiondefId = TransitiondefDB.getAllTransitiondefs().get(0).getId();
        ManagedErrend me = getTestManagedErrend(currentstateId, errendId, processdefId, transitiondefId);
        ManagedErrendDB.createManagedErrend(me);
    }

    @Test
    public void recursiveTest() throws Exception {
        recursiveSetup();
        Long currentstateId = StatedefDB.getAllStatedefs().get(0).getId();
        Long errendId = ErrendDB.getAllErrends().get(0).getId();
        Long processdefId = ProcessdefDB.getAllProcessdefs().get(0).getId();
        Long transitiondefId = TransitiondefDB.getAllTransitiondefs().get(0).getId();
        List<ManagedErrend> managederrends = ManagedErrendDB.getAllManagedErrends();
        Assert.assertTrue(managederrends.size() == 1);
        assertTestManagedErrend(managederrends.get(0), currentstateId, errendId, processdefId, transitiondefId);

        ManagedErrend me = managederrends.get(0);
        me.setLasttransitionId(null);
        Assert.assertTrue(ManagedErrendDB.updateManagedErrend(me));
        {
            ManagedErrend me2 = ManagedErrendDB.getManagedErrendFromId(me.getId());
            Assert.assertTrue(me2.getLasttransitionId() == null);
        }

        ManagedErrendDB.deleteManagedErrend(me);
        managederrends = ManagedErrendDB.getAllManagedErrends();
        Assert.assertTrue(managederrends.size() == 0);
    }

}
