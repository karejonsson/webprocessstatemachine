package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;

public class ErrendTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    public static Errend getTestErrend(long oaid, long now) throws Exception {
    	Errend e = new Errend();
    	e.setActive(true);
    	e.setOrderby(3);
    	e.setOrganizationId(oaid);
    	e.setCreated(new Date(now));
    	return e;
    }
    
    public static void assertTestErrend(Errend e, long oaid, long now) {
    	Assert.assertTrue(e.isActive());
    	Assert.assertTrue(e.getOrderby() == 3);
    	Assert.assertTrue(e.getOrganizationId() == oaid);
    	Assert.assertTrue(e.getCreated().getTime() == now);
    }
    
    public static void recursiveSetup() throws Exception {
    	AdminuserTest.recursiveSetup();
    	Adminuser au = AdminuserDB.getAllAdminusers().get(0);
    	Organisation org = OrganizationTest.getOrganization();
    	org.save();
    	Agent oa = new Agent();
    	oa.setPerson(au.getPerson());
    	oa.setRegisterred(new Date(System.currentTimeMillis()));
    	oa.setOrganization(org);
    	oa.save();
    	Errend e = getTestErrend(oa.getId(), System.currentTimeMillis());
    	ErrendDB.createErrend(e);
    }
    
    @Test
    public void errend1() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	PersonDB.createPerson(p);
    	Adminuser au = AdminuserTest.getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);
    	Agent oacc = AgentTest.getAgent(p);
    	Organisation org = OrganizationTest.getOrganization();
    	org.save();
    	oacc.setOrganization(org);
    	Errend e = ErrendTest.getTestErrend(org.getId(), now);
    	ErrendDB.createErrend(e);
    	CommonDB.clearCache();
    	
    	Person p2 = PersonDB.getPersonFromId(p.getId());
    	PersonTest.assertTestPerson(p2, now);
    	
    	Adminuser au2 = AdminuserDB.getAdminuserFromId(au.getId());
    	AdminuserTest.assertTestAdminuser(au2, p.getId());
    	
    	Errend e2 = ErrendDB.getErrendFromId(e.getId());
    	assertTestErrend(e2, org.getId(), now);
    }
    
    @Test
    public void lengthZero() throws Exception {
    	List<Errend> errends = ErrendDB.getAllErrends();
    	Assert.assertTrue(errends.size() == 0);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Errend> errends = ErrendDB.getAllErrends();
    	Assert.assertTrue(errends.size() == 1);

    	Errend e = errends.get(0);
    	long now = System.currentTimeMillis();
    	e.setCreated(new Date(now));
    	ErrendDB.updateErrend(e);
    	{
    		Errend e2 = ErrendDB.getErrendFromId(e.getId());
	    	
	    	Assert.assertTrue(e2.getCreated().getTime() == now);
    	}
    	
    	ErrendDB.deleteErrend(e);
    	errends = ErrendDB.getAllErrends();
    	Assert.assertTrue(errends.size() == 0);
    }

}
