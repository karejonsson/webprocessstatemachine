package se.prv.errends.dbc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processfile;

public class ProcessfileTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    private static byte[] data = "asehfajebvlabweflu abwfajbwsbca,caejc .JSDV.JSNDV.JNSV".getBytes(Charset.forName("UTF-8"));

    @Test
    public void processfile1() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	PersonDB.createPerson(p);
    	Adminuser au = AdminuserTest.getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);
    	Agent oag = AgentTest.getAgent(p);
    	Organisation org = OrganizationTest.getOrganization();
    	org.save();
    	oag.setOrganization(org);

    	Errend e = ErrendTest.getTestErrend(org.getId(), now);
    	ErrendDB.createErrend(e);
    	Processfile pf = getTestProcessfile(au.getId());
    	ProcessfileDB.createProcessfile(pf);
    	CommonDB.clearCache();
    	
    	ProcessfileDB.setData(pf.getId(), data);
    	
    	Person p2 = PersonDB.getPersonFromId(p.getId());
    	PersonTest.assertTestPerson(p2, now);
    	
    	Adminuser au2 = AdminuserDB.getAdminuserFromId(au.getId());
    	AdminuserTest.assertTestAdminuser(au2, p.getId());
    	
    	Errend e2 = ErrendDB.getErrendFromId(e.getId());
    	ErrendTest.assertTestErrend(e2, org.getId(), now);

    	byte[] data2 = ProcessfileDB.getData(pf.getId());
    	
    	Assert.assertTrue(Arrays.equals(data, data2));
    }
    
    @Test
    public void lengthZero() throws Exception {
    	List<Processfile> processfiles = ProcessfileDB.getAllProcessfiles();
    	Assert.assertTrue(processfiles.size() == 0);
    }

	public static Processfile getTestProcessfile(long auid) throws Exception {
		Processfile pf = new Processfile();
		pf.setDescription("abcdef");
		pf.setOrderby(3);
		pf.setAdminuserId(auid);
		pf.setFilename("filename");
		return pf;
	}
	
	public static void setupProcessfile(long adminuserId) throws Exception {
		Processfile pf = getTestProcessfile(adminuserId);
		ProcessfileDB.createProcessfile(pf);
		ProcessfileDB.setData(pf.getId(), data);		
	}

	public static void recursiveSetup() throws Exception {
		ErrendTest.recursiveSetup();
		Adminuser au = AdminuserDB.getAllAdminusers().get(0);
		setupProcessfile(au.getId());
	}

	public static void assertTestProcessfile(Processfile pf, long auid) {
		Assert.assertTrue(pf.getDescription().equals("abcdef"));
		Assert.assertTrue(pf.getOrderby() == 3);
		Assert.assertTrue(pf.getAdminuserId() == auid);
		Assert.assertTrue(pf.getFilename().equals("filename"));
	}

	@Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Processfile> processfiles = ProcessfileDB.getAllProcessfiles();
    	Assert.assertTrue(processfiles.size() == 1);
    	Adminuser au = AdminuserDB.getAllAdminusers().get(0);
    	assertTestProcessfile(processfiles.get(0), au.getId());
    	
    	Processfile pf = processfiles.get(0);
    	pf.setDescription("other text");
    	ProcessfileDB.updateProcessfile(pf);
    	{
	    	Processfile pf2 = ProcessfileDB.getProcessfileFromId(pf.getId());
	    	
	    	Assert.assertTrue(pf2.getDescription().equals(pf.getDescription()));
	    	Assert.assertTrue(pf2.getFilename().equals(pf.getFilename()));
    	}
    	
    	ProcessfileDB.deleteProcessfile(pf);
    	processfiles = ProcessfileDB.getAllProcessfiles();
    	Assert.assertTrue(processfiles.size() == 0);
    }

}
