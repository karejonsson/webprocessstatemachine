package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Person;

public class AdminuserTest {
	
    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    public static Adminuser getTestAdminuser(long pid) throws Exception {
    	Adminuser au = new Adminuser();
    	au.setActive(true);
    	au.setOrderby(3);
    	au.setPassword("abc123");
    	au.setPersonId(pid);
    	au.setSuperprivilegies(true);
    	au.setUsername("me");
    	return au;
    }
    
    public static void recursiveSetup() throws Exception {
    	AgentTest.recursiveSetup();
    	Person p = PersonDB.getAllActivePersons().get(0);
    	Adminuser au = getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);
    }
    
    public static void assertTestAdminuser(Adminuser au, long pid) {
    	Assert.assertTrue(au.isActive());
    	Assert.assertTrue(au.getOrderby() == 3);
    	Assert.assertTrue(au.getPassword().equals("abc123"));
    	Assert.assertTrue(au.getPersonId() == pid);
    	Assert.assertTrue(au.isSuperprivilegies());
    	Assert.assertTrue(au.getUsername().equals("me"));
    }

    @Test
    public void adminuser1() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	
    	PersonDB.createPerson(p);
    	long pid = p.getId();
    	
    	Adminuser au = getTestAdminuser(pid);
    	
    	AdminuserDB.createAdminuser(au);
    	long auid = au.getId();
    	CommonDB.clearCache();

    	au = AdminuserDB.getAdminuserFromId(auid);
    	
    	PersonTest.assertTestPerson(p, now);
    	assertTestAdminuser(au, pid);
    }
    
    @Test
    public void adminuser2() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	
    	PersonDB.createPerson(p);
    	long pid = p.getId();
    	
    	Adminuser au = getTestAdminuser(pid);

    	AdminuserDB.createAdminuser(au);
    	long auid = au.getId();
    	CommonDB.clearCache();

    	au = AdminuserDB.getAdminuserFromId(auid);

    	PersonTest.assertTestPerson(p, now);
    	assertTestAdminuser(au, pid);
    }

    @Test
    public void adminuser3() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	PersonDB.createPerson(p);
    	Adminuser au = getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);
    	long auid = au.getId();
    	CommonDB.clearCache();

    	PersonTest.assertTestPerson(p, now);
    	assertTestAdminuser(au, p.getId());
    }
    
    @Test
    public void lengthZero() throws Exception {
    	List<Person> persons = PersonDB.getAllActivePersons();
    	Assert.assertTrue(persons.size() == 0);
    }
    
    @Test
    public void lengthOneClearing() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	PersonDB.createPerson(p);
    	Adminuser au = getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);

    	CommonDB.clearCache();
    	
    	List<Person> persons = PersonDB.getAllActivePersons();
    	Assert.assertTrue(persons.size() == 1);
    	PersonTest.assertTestPerson(persons.get(0), now);
    	
    	List<Adminuser> adminusers = AdminuserDB.getAllAdminusers();
    	Assert.assertTrue(adminusers.size() == 1);
    	AdminuserTest.assertTestAdminuser(adminusers.get(0), p.getId());
    }
    
    @Test
    public void lengthOneNotClearing() throws Exception {
    	long now = System.currentTimeMillis();
    	Person p = PersonTest.getTestPerson(now);
    	PersonDB.createPerson(p);
    	Adminuser au = getTestAdminuser(p.getId());
    	AdminuserDB.createAdminuser(au);

    	List<Person> persons = PersonDB.getAllActivePersons();
    	Assert.assertTrue(persons.size() == 1);
    	PersonTest.assertTestPerson(persons.get(0), now);
    	
    	List<Adminuser> adminusers = AdminuserDB.getAllAdminusers();
    	Assert.assertTrue(adminusers.size() == 1);
    	AdminuserTest.assertTestAdminuser(adminusers.get(0), p.getId());
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Adminuser> adminusers = AdminuserDB.getAllAdminusers();
    	Assert.assertTrue(adminusers.size() == 1);

    	Adminuser au = adminusers.get(0);
    	au.setUsername("whatever");
    	au.setOrderby(null);
    	Assert.assertTrue(au.getOrderby() == null);
    	Assert.assertTrue(AdminuserDB.updateAdminuser(au));
    	{
    		AdminuserDB.clearCache();
    		Adminuser au2 = AdminuserDB.getAdminuserFromId(au.getId());
	    	Assert.assertTrue(au2.getUsername().equals("whatever"));
	    	Assert.assertTrue(au2.getOrderby() == null);
    	}
    	
    	AdminuserDB.deleteAdminuser(au);
    	adminusers = AdminuserDB.getAllAdminusers();
    	Assert.assertTrue(adminusers.size() == 0);
    }

}
