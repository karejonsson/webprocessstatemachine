package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Statedef;

public class StatedefTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(StatedefDB.getAllStatedefs().size() == 0);
    }
    
    public static Statedef getTestStatedef(Long processdefId) throws Exception {
    	Statedef sd = new Statedef();
    	sd.setLayout("layout");
    	sd.setOrderby(5);
    	sd.setProcessdefId(processdefId);
    	sd.setStatename("name");
    	return sd;
    }
    
    public static void assertTestStatedef(Statedef pd, Long processdefId) {
    	Assert.assertTrue(pd.getLayout().equals("layout"));
    	Assert.assertTrue(pd.getOrderby() == 5);
    	Assert.assertTrue(pd.getProcessdefId().equals(processdefId));
    	Assert.assertTrue(pd.getStatename().equals("name"));
    }
    
    public static void recursiveSetup() throws Exception {
    	ProcessdefTest.recursiveSetup();
    	Statedef sd = getTestStatedef(ProcessdefDB.getAllProcessdefs().get(0).getId());
    	StatedefDB.createStatedef(sd);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Statedef> statedefs = StatedefDB.getAllStatedefs();
    	Assert.assertTrue(statedefs.size() == 1);
    	assertTestStatedef(statedefs.get(0), ProcessdefDB.getAllProcessdefs().get(0).getId());
    	
    	Statedef sd = statedefs.get(0);
    	sd.setLayout("other text");
    	sd.setOrderby(null);
    	Assert.assertTrue(StatedefDB.updateStatedef(sd));
    	{
    		Statedef sd2 = StatedefDB.getStatedefFromId(sd.getId());
	    	Assert.assertTrue(sd2.getLayout().equals(sd.getLayout()));
	    	Assert.assertTrue(sd2.getOrderby() == null);
    	}
    	
    	StatedefDB.deleteStatedef(sd);
    	statedefs = StatedefDB.getAllStatedefs();
    	Assert.assertTrue(statedefs.size() == 0);
    }

}
