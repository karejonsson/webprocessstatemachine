package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;

public class AgentTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
		CommonDB.clearCache();
		pool = TestUtilities.setupPool();
	}

	@After
	public void tearDown() throws Exception {
		TestUtilities.destroyPool(pool);
		CommonDB.clearCache();
	}
	
	public static Agent getAgent(Person p) {
		Organisation org = new Organisation();
		org.setOrgname("En org");
		org.setOrgnr("123");
		org.setActive(true);
		org.save();
    	Agent oa = new Agent();
    	oa.setPerson(p);
    	oa.setActive(true);
    	oa.setRegisterred(new Date(System.currentTimeMillis()));
    	oa.setOrganization(org);
    	oa.save();
    	return oa;
	}

    public static void recursiveSetup() throws Exception {
    	PersonTest.recursiveSetup();
    }

    public static void recursiveSetup(Person p) throws Exception {
    	PersonTest.recursiveSetup();
    	getAgent(p);
    }

	@Test
	public void agent1() throws Exception {
		recursiveSetup();
		Assert.assertTrue(PersonDB.getAllActivePersons().size() == 1);
		Assert.assertTrue(AgentDB.getAllAgents().size() == 0);
		getAgent(PersonDB.getAllActivePersons().get(0));
		Assert.assertTrue(AgentDB.getAllAgents().size() == 1);
	}
	
}
