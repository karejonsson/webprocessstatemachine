package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Processdef;

public class ProcessdefTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(ProcessdefDB.getAllProcessdefs().size() == 0);
    }
    
    public static Processdef getTestProcessdef(Long processfileId) throws Exception {
    	Processdef pd = new Processdef();
    	pd.setDescription("abcdef");
    	pd.setOrderby(4);
    	pd.setProcessfileId(processfileId);
    	pd.setProcessname("name");
    	return pd;
    }
    
    public static void assertTestProcessdef(Processdef pd, Long processfileId) {
    	Assert.assertTrue(pd.getDescription().equals("abcdef"));
    	Assert.assertTrue(pd.getOrderby() == 4);
    	Assert.assertTrue(pd.getProcessfileId().equals(processfileId));
    	Assert.assertTrue(pd.getOrderby().equals(4));
    }
    
    public static void recursiveSetup() throws Exception {
    	ProcessfileTest.recursiveSetup();
    	Processdef pd = getTestProcessdef(ProcessfileDB.getAllProcessfiles().get(0).getId());
    	ProcessdefDB.createProcessdef(pd);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Processdef> processdefs = ProcessdefDB.getAllProcessdefs();
    	Assert.assertTrue(processdefs.size() == 1);
    	assertTestProcessdef(processdefs.get(0), ProcessfileDB.getAllProcessfiles().get(0).getId());
    	
    	Processdef pf = processdefs.get(0);
    	pf.setDescription("other text");
    	pf.setOrderby(null);
    	Assert.assertTrue(ProcessdefDB.updateProcessdef(pf));
    	{
	    	Processdef pf2 = ProcessdefDB.getProcessdefFromId(pf.getId());
	    	Assert.assertTrue(pf2.getDescription().equals(pf.getDescription()));
	    	Assert.assertTrue(pf2.getOrderby() == null);
    	}
    	
    	ProcessdefDB.deleteProcessdef(pf);
    	processdefs = ProcessdefDB.getAllProcessdefs();
    	Assert.assertTrue(processdefs.size() == 0);
    }

}
