package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.FunctionDefReuse;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;

public class FunctionDefReuseTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(FunctionDefReuseDB.getAllFunctionDefReuses().size() == 0);
    }

    @Test
    public void writeRetrieve() throws Exception {
        FunctionDefTest.recursiveSetup();
        
    	List<FunctionDef> functiondefs = FunctionDefDB.getAllFunctionDefs();
    	List<Processfile> processfiles = ProcessfileDB.getAllProcessfiles();
    	List<Processdef> processdefs = ProcessdefDB.getAllProcessdefs();

    	Assert.assertTrue(functiondefs.size() == 1);
    	Assert.assertTrue(processfiles.size() == 1);
    	Assert.assertTrue(processdefs.size() == 0);
    	
    	Processdef pd = ProcessdefTest.getTestProcessdef(ProcessfileDB.getAllProcessfiles().get(0).getId());
    	ProcessdefDB.createProcessdef(pd);
    	processdefs = ProcessdefDB.getAllProcessdefs();
    	
    	Assert.assertTrue(processdefs.size() == 1);
    	
    	FunctionDefReuse fdr = new FunctionDefReuse();
    	fdr.setFunctionDef(functiondefs.get(0));
    	fdr.setProcessdef(processdefs.get(0));
    	fdr.save();
    	
    	List<FunctionDefReuse> functiondefreuses = FunctionDefReuseDB.getAllFunctionDefReuses();
    	
    	Assert.assertTrue(functiondefreuses.size() == 1);
    	
    	FunctionDef fd = pd.getFunctionDef("");
    	Assert.assertTrue(fd == null);
    	
    	fd = pd.getFunctionDef("name");
    	Assert.assertTrue(fd != null);
    	
    	fdr = new FunctionDefReuse();
    	fdr.setFunctionDef(functiondefs.get(0));
    	fdr.setProcessdef(processdefs.get(0));
    	try {
        	fdr.save();
        	Assert.assertTrue(false);
    	}
    	catch(Throwable e) {
    		Assert.assertTrue(true);
    	}
    }

}
