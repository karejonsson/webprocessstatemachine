package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.reuse.cli.PropertyNames;

public class PersistentValuesTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(ActorDB.getAllActors().size() == 0);
    }

    @Test
    public void various() throws Exception {
    	PersistentValuesDB.setStringValue("key", "value");
    	String value = PersistentValuesDB.getStringValue("key");
    	Assert.assertTrue(value.equals("value"));
    	
    	PersistentValuesDB.setLongValue("long", 298374l);
    	Long aLong = PersistentValuesDB.getLongValue("long");
    	Assert.assertTrue(aLong == 298374l);
    	
    	PersistentValuesDB.setBooleanValue("bool", true);
    	Boolean aBool = PersistentValuesDB.getBooleanValue("bool");
    	Assert.assertTrue(aBool);
    }
    
    public static void setupData() throws Exception {
    	PersistentValuesDB.setStringValue(PropertyNames.smtpserver, "smtp.prv.se");
    	PersistentValuesDB.setBooleanValue(PropertyNames.blockmail, true);
    	PersistentValuesDB.setBooleanValue(PropertyNames.smtpauth, true);
    	PersistentValuesDB.setIntegerValue(PropertyNames.smtpport, 25);
    }

}
/*
mail.smtp.auth = true
mail.smtp.host = smtp.dom.se
mail.smtp.port = 25
*/
