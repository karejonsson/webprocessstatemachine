package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Actor;

public class ActorTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(ActorDB.getAllActors().size() == 0);
    }
    
    public static Actor getTestActor(Long agentId, Long roledefId, Long errendId) throws Exception {
    	Actor a = new Actor();
    	a.setErrendId(errendId);
    	a.setOrderby(7);
    	a.setAgentId(agentId);
    	a.setRoledefId(roledefId);
    	return a;
    }
    
    public static void assertTestActor(Actor a, Long agentId, Long roledefId, Long errendId) {
		//System.out.println("E A "+a.getErrendId()+", L "+errendId);
    	Assert.assertTrue(a.getErrendId() == errendId);
    	Assert.assertTrue(a.getOrderby() == 7);
    	Assert.assertTrue(a.getAgentId() == agentId);
		//System.out.println("R A "+a.getRoledefId()+", L "+roledefId);
    	Assert.assertTrue(a.getRoledefId() == roledefId);
    }
    
    public static void recursiveSetup() throws Exception {
    	RoledefTest.recursiveSetup();
    	Actor a = getTestActor(
    			AgentDB.getAllAgents().get(0).getId(),
    			RoledefDB.getAllRoledefs().get(0).getId(),
    			ErrendDB.getAllErrends().get(0).getId()
    			);
    	ActorDB.createActor(a);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Actor> actors = ActorDB.getAllActors();
    	Assert.assertTrue(actors.size() == 1);
		Actor a = actors.get(0);
    	assertTestActor(
    			a,
    			PersonDB.getAllActivePersons().get(0).getId(),
    			RoledefDB.getAllRoledefs().get(0).getId(),
    			ErrendDB.getAllErrends().get(0).getId()
    			);

		a.setOrderby(null);
		a.setErrendId(null);
		a.setRoledefId(null);
		Assert.assertTrue(ActorDB.updateActor(a));
		{
			Actor a2 = ActorDB.getActorFromId(a.getId());
			Assert.assertTrue(a2.getOrderby() == null);
			Assert.assertTrue(a2.getErrendId() == null);
			Assert.assertTrue(a2.getRoledefId() == null);
		}

		a.setErrendId(null);
		a.setRoledefId(RoledefDB.getAllRoledefs().get(0).getId());
		Assert.assertTrue(ActorDB.updateActor(a));
		{
			Actor a2 = ActorDB.getActorFromId(a.getId());
			Assert.assertTrue(a2.getOrderby() == null);
			Assert.assertTrue(a2.getErrendId() == null);
			Assert.assertTrue(a2.getRoledefId() != null);
		}

		a.setErrendId(ErrendDB.getAllErrends().get(0).getId());
		a.setRoledefId(null);
		Assert.assertTrue(ActorDB.updateActor(a));
		{
			Actor a2 = ActorDB.getActorFromId(a.getId());
			Assert.assertTrue(a2.getOrderby() == null);
			Assert.assertTrue(a2.getErrendId() != null);
			Assert.assertTrue(a2.getRoledefId() == null);
		}

		ActorDB.deleteActor(a);
    	actors = ActorDB.getAllActors();
    	Assert.assertTrue(actors.size() == 0);
    }

}
