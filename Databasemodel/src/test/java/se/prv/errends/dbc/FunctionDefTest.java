package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.FunctionDef;

public class FunctionDefTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(FunctionDefDB.getAllFunctionDefs().size() == 0);
    }
    
    public static FunctionDef getTestFunctionDef(Long processfileId) throws Exception {
    	FunctionDef pd = new FunctionDef();
    	pd.setSource("abcdef");
    	pd.setOrderby(4);
    	pd.setProcessfileId(processfileId);
    	pd.setFunctionname("name");
    	pd.setTypename("string");
    	return pd;
    }
    
    public static void assertTestFunctionDef(FunctionDef pd, Long processfileId) {
    	Assert.assertTrue(pd.getSource().equals("abcdef"));
    	Assert.assertTrue(pd.getOrderby() == 4);
    	Assert.assertTrue(pd.getProcessfileId().equals(processfileId));
    	Assert.assertTrue(pd.getOrderby().equals(4));
    }
    
    public static void recursiveSetup() throws Exception {
    	ProcessfileTest.recursiveSetup();
    	FunctionDef pd = getTestFunctionDef(ProcessfileDB.getAllProcessfiles().get(0).getId());
    	FunctionDefDB.createFunctionDef(pd);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<FunctionDef> functiondefs = FunctionDefDB.getAllFunctionDefs();
    	Assert.assertTrue(functiondefs.size() == 1);
    	assertTestFunctionDef(functiondefs.get(0), ProcessfileDB.getAllProcessfiles().get(0).getId());
    	
    	FunctionDef pf = functiondefs.get(0);
    	pf.setSource("other text");
    	pf.setOrderby(null);
    	Assert.assertTrue(FunctionDefDB.updateFunctionDef(pf));
    	{
    		FunctionDef pf2 = FunctionDefDB.getFunctionDefFromId(pf.getId());
	    	Assert.assertTrue(pf2.getSource().equals(pf.getSource()));
	    	Assert.assertTrue(pf2.getOrderby() == null);
    	}
    	
    	FunctionDefDB.deleteFunctionDef(pf);
    	functiondefs = FunctionDefDB.getAllFunctionDefs();
    	Assert.assertTrue(functiondefs.size() == 0);
    }

}
