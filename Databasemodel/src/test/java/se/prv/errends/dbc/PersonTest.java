package se.prv.errends.dbc;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;

import org.junit.Before;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;

public class PersonTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
		CommonDB.clearCache();
		pool = TestUtilities.setupPool();
	}

	@After
	public void tearDown() throws Exception {
		TestUtilities.destroyPool(pool);
		CommonDB.clearCache();
	}

	public static Person getTestPerson(long now) throws Exception {
		Person p = new Person();
		p.setActive(true);
		p.setEmail("a.b@c.d");
		p.setFamilyname("Fam name");
		p.setFirstname("first name");
		p.setSwedishpnr(Person.makeCompletionWithChecknumber("690301015"));
		p.setRegisterred(new Date(now));
		return p;
	}

	public static void recursiveSetup() throws Exception {
		Person p = getTestPerson(System.currentTimeMillis());
		PersonDB.createPerson(p);
	}

	public static void assertTestPerson(Person p, long now) {
		Assert.assertTrue(p.isActive());
		Assert.assertTrue(p.getEmail().equals("a.b@c.d"));
		Assert.assertTrue(p.getFamilyname().equals("Fam name"));
		Assert.assertTrue(p.getFirstname().equals("first name"));
		Assert.assertTrue(p.getRegisterred().getTime() == now);
		Assert.assertTrue(p.getSwedishpnr().equals("196903010152"));
	}

	@Test
	public void person1() throws Exception {
		long now = System.currentTimeMillis();
		Person p = getTestPerson(now);

		PersonDB.createPerson(p);
		long id = p.getId();
		CommonDB.clearCache();
		p = PersonDB.getPersonFromId(id);
		assertTestPerson(p, now);
	}

	@Test
	public void person2() throws Exception {
		long now = System.currentTimeMillis();
		Person p = getTestPerson(now);
		PersonDB.createPerson(p);
		CommonDB.clearCache();
		long wrongId = p.getId()+1l;
		Person p2 = PersonDB.getPersonFromId(wrongId);
		Assert.assertTrue(p2 == null);
	}

	@Test
	public void person3() throws Exception {
		long now = System.currentTimeMillis();
		Person p = getTestPerson(now);
		PersonDB.createPerson(p);

		long wrongId = p.getId()+1l;
		Person p2 = PersonDB.getPersonFromId(wrongId);
		Assert.assertTrue(p2 == null);
	}

	@Test
	public void person4() throws Exception {
		Person p1 = getTestPerson(System.currentTimeMillis());
		p1.save();

		Person p2 = new Person();
		p2.setActive(true);
		p2.setEmail("a.b@c.e");
		p2.setFamilyname("Fam name");
		p2.setFirstname("first name");
		p2.setSwedishpnr("6911020128");
		p2.setRegisterred(new Date(System.currentTimeMillis()));
		Assert.assertTrue(!p1.getId().equals(p2.getId()));
	}

	@Test
	public void lengthZero() throws Exception {
		List<Person> persons = PersonDB.getAllActivePersons();
		Assert.assertTrue(persons.size() == 0);
	}

	@Test
	public void lengthOneClearing() throws Exception {
		long now = System.currentTimeMillis();
		Person p = getTestPerson(now);
		PersonDB.createPerson(p);
		CommonDB.clearCache();
		List<Person> persons = PersonDB.getAllActivePersons();
		Assert.assertTrue(persons.size() == 1);
		assertTestPerson(persons.get(0), now);
	}

	@Test
	public void lengthOneNotClearing() throws Exception {
		long now = System.currentTimeMillis();
		Person p = getTestPerson(now);
		PersonDB.createPerson(p);    	
		List<Person> persons = PersonDB.getAllActivePersons();
		Assert.assertTrue(persons.size() == 1);
		assertTestPerson(persons.get(0), now);
	}

	@Test
	public void recursiveTest() throws Exception {
		recursiveSetup();
		List<Person> persons = PersonDB.getAllActivePersons();
		Assert.assertTrue(persons.size() == 1);

		Person p = persons.get(0);
		p.setFirstname("whatever");
		Assert.assertTrue(PersonDB.updatePerson(p));
		{
			PersonDB.clearCache();
			Person p2 = PersonDB.getPersonFromId(p.getId());
			Assert.assertTrue(p2.getFirstname().equals("whatever"));
		}

		PersonDB.deletePerson(p);
		persons = PersonDB.getAllActivePersons();
		Assert.assertTrue(persons.size() == 0);

	}

	private static void setupOnePerson(boolean active, String familyname, String firstname, String swedishpnr, String domain) throws Exception {
		Person p = new Person();
		p.setActive(active);
		String email = firstname.toLowerCase()+"."+familyname.toLowerCase()+"@"+domain;
		email = email.replace("å", "a");
		email = email.replace("ä", "ä");
		email = email.replace("ö", "o");
		email = email.replace("å", "a");
		p.setEmail(email);
		p.setFamilyname(familyname);
		p.setFirstname(firstname);
		p.setRegisterred(new Date(System.currentTimeMillis()));
		p.setSwedishpnr(Person.makeCompletionWithChecknumber(swedishpnr));
		p.save();
	}

	public static void setupPeople() throws Exception {
		//setupOnePerson(true, "Jonsson", "Kåre", "196905010150");
		setupOnePerson(true, "Albertsson", "Henrik", "19700101001", "prv.se");
		setupOnePerson(true, "Enoksson", "Peter", "19700101002", "prv.se");
		setupOnePerson(true, "Söderberg", "Partik", "19700101003", "prv.se");
		setupOnePerson(true, "Skälvik", "Marcus", "19700101004", "prv.se");
		setupOnePerson(true, "Wallenius", "Lars", "19700101005", "prv.se");
		setupOnePerson(true, "Dickman", "Bengt-Åke", "19700101006", "prv.se");
		setupOnePerson(true, "Gunnfors", "Fredrik", "19700101007", "prv.se");
		setupOnePerson(true, "Svensson", "Stefan", "19700101008", "prv.se");
		setupOnePerson(true, "Gustafsson", "Christina", "19700101009", "prv.se");
		setupOnePerson(true, "Westermark", "Magnus", "19700101010", "prv.se");
		setupOnePerson(true, "Fasth", "Tomas", "19700101011", "prv.se");
		setupOnePerson(true, "Rubbestad", "Tina", "19700101026", "prv.se");
		setupOnePerson(true, "Lundell", "Erik",   "19700101027", "prv.se");
		setupOnePerson(true, "Edenbro", "Patrik", "19700101028", "prv.se");
		setupOnePerson(true, "Lindell", "Maj-Lis", "19700101029", "prv.se");
		setupOnePerson(true, "Ohlsson", "Christoffer", "19700101030", "prv.se");
		setupOnePerson(true, "Jansdotter", "Karin", "19700101031", "prv.se");
		setupOnePerson(true, "Törnqvist", "Lisbeth", "19700101032", "prv.se");
		setupOnePerson(true, "Viken", "Åsa", "19700101033", "prv.se");
		setupOnePerson(true, "Persson", "Anders", "19700101034", "prv.se");
		setupOnePerson(false, "Alfredsson", "Hans", "19700101012", "gmail.se");
		setupOnePerson(false, "Danielsson", "Tage", "19700101013", "gmail.se");
		setupOnePerson(false, "Löfvén", "Stefan", "19700101014", "regeringen.se");
		setupOnePerson(false, "Sjöstedt", "Jonas", "19700101015", "regeringen.se");
		setupOnePerson(false, "Fridolin", "Gustav", "19700101016", "regeringen.se");
		setupOnePerson(false, "Kristersson", "Ulf", "19591212017", "regeringen.se");
		setupOnePerson(false, "Bush-Thor", "Ebba", "19700101018", "regeringen.se");
		setupOnePerson(false, "Åkesson", "Jimmie", "19700101019", "regeringen.se");
		setupOnePerson(false, "Björklund", "Jan", "19700101020", "regeringen.se");
		setupOnePerson(false, "Lööf", "Annie", "19700101021", "regeringen.se");
		setupOnePerson(false, "Musk", "Elon", "19700101022", "tesla.com");
		setupOnePerson(false, "Gates", "Bill", "19700101023", "microsoft.com");
		setupOnePerson(false, "Borg", "Björn", "19700101024", "tennis.se");
		setupOnePerson(false, "Wallner", "Jan-Ove", "19700101025", "bordtennis.se");
		setupOnePerson(false, "Berg", "Markus", "19800101001", "fotboll.se");
		setupOnePerson(false, "Åby-Ericsson", "Georg", "19600101001", "fotboll.se");
	}

	public static void print(String leadingName, List<Person> hits) {
		System.out.println(leadingName);
		for(Person p : hits) {
			System.out.println(""+p);
		}
	}

	@Test
	public void search() throws Exception {
		setupPeople();
		List<Person> hits = PersonDB.searchActive("Enok%");
		print("Enok%", hits);
		Assert.assertTrue(hits.size() == 1);
		hits = PersonDB.searchActive("%5912%");
		print("%5912%", hits);
		Assert.assertTrue(hits.size() == 1);
		hits = PersonDB.searchActive("%son");
		print("%son", hits);
		Assert.assertTrue(hits.size() == 11);
	}


}


