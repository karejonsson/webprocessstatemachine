package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Actor;
import java.util.Date;

public class SessionTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(SessionDB.getAllSessions().size() == 0);
    }

    public static Session getTestSession(long now, Long actorId) throws Exception {
        Session s = new Session();
        s.setFinish(new Date(now));
        s.setStart(new Date(now));
        s.setActorId(actorId);
        return s;
    }

    public static void assertTestSession(Session s, long now, Long actorId) {
        Assert.assertTrue(s.getFinish().getTime() == now);
        Assert.assertTrue(s.getStart().getTime() == now);
        Assert.assertTrue(s.getActorId().equals(actorId));
    }

    public static void recursiveSetup(long now) throws Exception {
        ActorTest.recursiveSetup();
        Actor a = ActorDB.getAllActors().get(0);
        Session s = getTestSession(now, a.getId());
        SessionDB.createSession(s);
    }

    @Test
    public void recursiveTest() throws Exception {
        long now = System.currentTimeMillis();
        recursiveSetup(now);
        Actor a = ActorDB.getAllActors().get(0);
        List<Session> sessions = SessionDB.getAllSessions();
        Assert.assertTrue(sessions.size() == 1);
        assertTestSession(sessions.get(0), now, a.getId());

        long later = System.currentTimeMillis();
        Session s = sessions.get(0);
        s.setFinish(new Date(later));
        Assert.assertTrue(SessionDB.updateSession(s));
        {
            Session a2 = SessionDB.getSessionFromId(s.getId());
            Assert.assertTrue(a2.getFinish().getTime() == later);
        }

        SessionDB.deleteSession(s);
        sessions = SessionDB.getAllSessions();
        Assert.assertTrue(sessions.size() == 0);
    }

}
