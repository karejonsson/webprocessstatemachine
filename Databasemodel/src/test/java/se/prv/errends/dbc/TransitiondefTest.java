package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Transitiondef;

public class TransitiondefTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(TransitiondefDB.getAllTransitiondefs().size() == 0);
    }

    public static Transitiondef getTestTransitiondef(Long statedefId) throws Exception {
    	Transitiondef td = new Transitiondef();
    	td.setLayout("lay out");
    	td.setOrderby(5);
    	td.setSourcestatedefId(statedefId);
    	td.setTargetstatedefId(statedefId);
    	td.setTransitionname("nom");
    	return td;
    }
    
    public static void assertTestTransitiondef(Transitiondef td, Long statedefId) {
    	Assert.assertTrue(td.getLayout().equals("lay out"));
    	Assert.assertTrue(td.getOrderby() == 5);
    	Assert.assertTrue(td.getSourcestatedefId().equals(statedefId));
    	Assert.assertTrue(td.getTargetstatedefId().equals(statedefId));
    	Assert.assertTrue(td.getTransitionname().equals("nom"));
    }
    
    public static void recursiveSetup() throws Exception {
    	StatedefTest.recursiveSetup();
    	Transitiondef sd = getTestTransitiondef(StatedefDB.getAllStatedefs().get(0).getId());
    	TransitiondefDB.createTransitiondef(sd);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Transitiondef> transitiondefs = TransitiondefDB.getAllTransitiondefs();
    	Assert.assertTrue(transitiondefs.size() == 1);
    	assertTestTransitiondef(transitiondefs.get(0), ProcessdefDB.getAllProcessdefs().get(0).getId());
    	
    	Transitiondef td = transitiondefs.get(0);
    	td.setLayout("other text");
    	td.setOrderby(null);
    	Assert.assertTrue(TransitiondefDB.updateTransitiondef(td));
    	{
    		Transitiondef td2 = TransitiondefDB.getTransitiondefFromId(td.getId());
	    	Assert.assertTrue(td2.getLayout().equals(td.getLayout()));
	    	Assert.assertTrue(td2.getOrderby() == null);
    	}
    	
    	TransitiondefDB.deleteTransitiondef(td);
    	transitiondefs = TransitiondefDB.getAllTransitiondefs();
    	Assert.assertTrue(transitiondefs.size() == 0);
    }

}
