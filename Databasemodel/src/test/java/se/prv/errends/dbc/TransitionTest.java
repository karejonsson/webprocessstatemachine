package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Transition;

public class TransitionTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(TransitionDB.getAllTransitions().size() == 0);
    }

    public static Transition getTestTransition(Long transitiondefId, Long sessionId) throws Exception {
        Transition s = new Transition();
        s.setTransitiondefId(transitiondefId);
        s.setSessionId(sessionId);
        s.setPredecessorId(null);
        return s;
    }

    public static void assertTestTransition(Transition t, Long transitiondefId, Long sessionId) {
        Assert.assertTrue(t.getPredecessorId() == null);
        Assert.assertTrue(t.getSessionId() == sessionId);
        Assert.assertTrue(t.getTransitiondefId() == transitiondefId);
    }

    public static void recursiveSetup() throws Exception {
        SessionTest.recursiveSetup(System.currentTimeMillis());
        Long transitiondefId = TransitiondefDB.getAllTransitiondefs().get(0).getId();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        Transition t = getTestTransition(transitiondefId, sessionId);
        TransitionDB.createTransition(t);
    }

    @Test
    public void recursiveTest() throws Exception {
        recursiveSetup();
        Long transitiondefId = TransitiondefDB.getAllTransitiondefs().get(0).getId();
        Long sessionId = SessionDB.getAllSessions().get(0).getId();
        List<Transition> transitions = TransitionDB.getAllTransitions();
        Assert.assertTrue(transitions.size() == 1);
        assertTestTransition(transitions.get(0), transitiondefId, sessionId);

        Transition t = transitions.get(0);
        Assert.assertTrue(TransitionDB.updateTransition(t));

        TransitionDB.deleteTransition(t);
        transitions = TransitionDB.getAllTransitions();
        Assert.assertTrue(transitions.size() == 0);
    }

}
