package se.prv.errends.dbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Transitiondef;

public class RoledefTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(RoledefDB.getAllRoledefs().size() == 0);
    }

    public static Roledef getTestRoledef(Long processdefId) throws Exception {
    	Roledef rd = new Roledef();
    	rd.setProcessdefId(processdefId);
    	rd.setOrderby(6);
    	rd.setRolename("rolle");
    	return rd;
    }
    
    public static void assertTestRoledef(Roledef rd, Long processdefId) {
    	Assert.assertTrue(rd.getProcessdefId().equals(processdefId));
    	Assert.assertTrue(rd.getOrderby() == 6);
    	Assert.assertTrue(rd.getRolename().equals("rolle"));
    }
    
    public static void recursiveSetup() throws Exception {
    	TransitiondefTest.recursiveSetup();
    	Roledef rd = getTestRoledef(TransitiondefDB.getAllTransitiondefs().get(0).getId());
    	RoledefDB.createRoledef(rd);
    }
    
    @Test
    public void recursiveTest() throws Exception {
    	recursiveSetup();
    	List<Roledef> roledefs = RoledefDB.getAllRoledefs();
    	Assert.assertTrue(roledefs.size() == 1);
    	assertTestRoledef(roledefs.get(0), ProcessdefDB.getAllProcessdefs().get(0).getId());
    	
    	Roledef rd = roledefs.get(0);
    	rd.setRolename("other text again");
    	rd.setOrderby(null);
    	Assert.assertTrue(RoledefDB.updateRoledef(rd));
    	{
    		Roledef rd2 = RoledefDB.getRoledefFromId(rd.getId());
	    	Assert.assertTrue(rd2.getRolename().equals(rd.getRolename()));
	    	Assert.assertTrue(rd2.getOrderby() == null);
    	}
    	
    	RoledefDB.deleteRoledef(rd);
    	roledefs = RoledefDB.getAllRoledefs();
    	Assert.assertTrue(roledefs.size() == 0);
    }

}
