package se.prv.errends.dbc;

import java.util.Date;

import se.prv.errends.domain.Organisation;

public class OrganizationTest {

	public static Organisation getOrganization() {
		Organisation out = new Organisation();
		out.setOrgname("En organisation");
		out.setOrgnr("12345678890");
		out.setRegisterred(new Date(System.currentTimeMillis()));
		out.setActive(true);
		return out;
	}

}
