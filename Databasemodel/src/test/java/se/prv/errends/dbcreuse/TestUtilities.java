package se.prv.errends.dbcreuse;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

import general.reuse.database.pool.JDBCConnectionPool;
import general.reuse.database.pool.SimpleJDBCConnectionPool;

public class TestUtilities {

    public static JDBCConnectionPool setupPool() throws SQLException, IOException {
    	JDBCConnectionPool pool = getHSQLDBConnectionPool(
    			"org.hsqldb.jdbc.JDBCDriver", 
    			"jdbc:hsqldb:mem:aname;shutdown=true",
    			"sa", 
                "");
    	Commons.setPool(pool);
    	Connection c = pool.reserveConnection();
        String project = System.getProperty("user.dir")+File.separator+".."+File.separator+"Databasemodel";
        String path = project+File.separator+"documentation"+File.separator+"psql-schema.sql";
        String psqlschema = new String(Files.readAllBytes(Paths.get(path)), Charset.forName("UTF-8"));
        String i1 = psqlschema.replaceAll("bigserial primary key,", "INTEGER IDENTITY PRIMARY KEY,");
        String i2 = i1.replace("bool", "boolean");
        String hsqldbschema = i2.replace("bytea", "BLOB");
        String tables[] = hsqldbschema.split(";");
        for(String t : tables) {
            if(t.length() < 5) {
                continue;
            }
            PreparedStatement ps;
            try {
                ps = c.prepareStatement(t+";");
                ps.executeUpdate();
                //System.out.println("OK "+t);
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Ej ok");
            }
        }
        return pool;
    }
    
    public static void destroyPool(JDBCConnectionPool pool) {
    	pool.destroy();
    	Commons.setPool(null);
    }

    private static void clearMemory(Connection connection) throws Exception {
        try {
            try {
                Statement stmt = connection.createStatement();
                try {
                    stmt.execute("TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK");
                    //stmt.execute("TRUNCATE SCHEMA PUBLIC AND COMMIT");
                    connection.commit();
                } finally {
                    stmt.close();
                }
            } catch (SQLException e) {
                connection.rollback();
                throw new Exception(e);
            }
        } catch (SQLException e) {
            throw new Exception(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
    
    private static JDBCConnectionPool getHSQLDBConnectionPool(String driverName, String connectionUri,
            String userName, String password) throws SQLException {
    	return new SimpleJDBCConnectionPool(driverName, connectionUri, userName, password) {
			@Override
			public void destroy() {
		        for (Connection c : availableConnections) {
		            try {
		            	clearMemory(c);
		            } catch (Exception e) {
		                // No need to do anything
		            }
		        }
		        for (Connection c : reservedConnections) {
		            try {
		            	clearMemory(c);
		            } catch (Exception e) {
		                // No need to do anything
		            }
		        }
			} 
    	};
    }

}
