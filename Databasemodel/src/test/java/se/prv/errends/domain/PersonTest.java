package se.prv.errends.domain;

import org.junit.Test;
import junit.framework.Assert;

public class PersonTest {

    @Test
    public void checkPnr() {
        Assert.assertTrue(Person.acceptablePnr("6905010150"));
        Assert.assertTrue(Person.acceptablePnr("690501-0150"));
        Assert.assertTrue(Person.acceptablePnr("690501-0150"));
        Assert.assertTrue(Person.acceptablePnr("170504-0150"));
        Assert.assertTrue(Person.acceptablePnr("171106-0150"));
        Assert.assertTrue(Person.acceptablePnr("181113-0150"));
        Assert.assertTrue(Person.acceptablePnr("10000101-0008"));
        Assert.assertTrue(Person.acceptablePnr("100001010008"));
    }

    @Test
    public void checkEmail() {
        Assert.assertTrue(Person.acceptableEmail("\"Fred Bloggs\"@example.com"));
        Assert.assertTrue(!Person.acceptableEmail("user@.invalid.com"));
        Assert.assertTrue(Person.acceptableEmail("Chuck Norris <gmail@chucknorris.com>"));
        Assert.assertTrue(!Person.acceptableEmail("Chuck Norris <gmail@.chucknorris.com>"));
        Assert.assertTrue(Person.acceptableEmail("webmaster@müller.de"));
        Assert.assertTrue(Person.acceptableEmail("matteo@78.47.122.114"));
    }

    @Test
    public void getChecknumber() throws Exception {
    	Assert.assertTrue(Person.makeCompletionWithChecknumber("690501015").equals("196905010150"));
    	Assert.assertTrue(Person.makeCompletionWithChecknumber("19690501015").equals("196905010150"));
    	Assert.assertTrue(Person.makeCompletionWithChecknumber("19690501-015").equals("196905010150"));
    }
    
    @Test
    public void fix() throws Exception {
    	Assert.assertTrue(Person.fix("6905010150").equals("196905010150"));
    	Assert.assertTrue(Person.fix("196905010150").equals("196905010150"));
    }
    
}
/*
https://stackoverflow.com/questions/8204680/java-regex-email

"Fred Bloggs"@example.com is valid
user@.invalid.com is invalid
Chuck Norris <gmail@chucknorris.com> is valid
webmaster@müller.de is valid
matteo@78.47.122.114 is valid
 */