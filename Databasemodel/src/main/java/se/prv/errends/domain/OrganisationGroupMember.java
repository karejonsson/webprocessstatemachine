package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.OrganisationGroupMemberDB;
import se.prv.errends.dbc.OrganisationRoleGroupDB;
import se.prv.errends.dbc.RoledefDB;

public class OrganisationGroupMember {

	/*
create table organisationgroupmember (
  id bigserial primary key,
  agentId bigint not null,
  rolegroupId bigint not null,
  unique(agentId, rolegroupId),
  foreign key ( agentId ) references agent (id),
  foreign key ( rolegroupId ) references organisationrolegroup (id)
);
	 */

	private Long id;
	private Long agentId;
	private Long rolegroupId;
	
	public String toString() {
		return "OrganisationGroupMember{id="+id+
				", agentId="+agentId+", rolegroupId="+rolegroupId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAgentId() {
		return agentId;
	}
	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}
	public Long getRolegroupId() {
		return rolegroupId;
	}
	public void setRolegroupId(Long rolegroupId) {
		this.rolegroupId = rolegroupId;
		organisationRoleGroup = null;
	}
	
	private Agent agent = null;
	
	public Agent getAgent() {
		if(agentId == null) {
			return null;
		}
		if(agent != null) {
			return agent;
		}
		agent = AgentDB.getAgentFromId(agentId);
		return agent;
	}
	
	private OrganisationRoleGroup organisationRoleGroup = null;
	
	public OrganisationRoleGroup getOrganisationRoleGroup() {
		if(rolegroupId == null) {
			return null;
		}
		if(organisationRoleGroup != null) {
			return organisationRoleGroup;
		}
		organisationRoleGroup = OrganisationRoleGroupDB.getOrganisationRoleGroupFromId(rolegroupId);
		return organisationRoleGroup;
	}
	
	public boolean save() {
		if(id == null) {
			return OrganisationGroupMemberDB.createOrganisationGroupMember(this); 
		}
		else {
			return OrganisationGroupMemberDB.updateOrganisationGroupMember(this);
		}
	}
	
	public boolean delete() {
		if(OrganisationGroupMemberDB.deleteOrganisationGroupMember(this)) {
			id = null;
			return true;
		}
		return false;
	}

}
