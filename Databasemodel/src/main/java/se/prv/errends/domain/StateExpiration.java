package se.prv.errends.domain;

import java.util.Date;

import se.prv.errends.dbc.ManagedErrendDB;
import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.dbc.TransitiondefDB;

public class StateExpiration {

	/*
create table stateexpiration (
  id bigserial primary key,
  managederrendId bigint not null,
  transitiondefId bigint not null,
  expirey timestamp not null,
  foreign key ( managederrendId ) references managederrend (id),
  foreign key ( transitiondefId ) references transitiondef (id)
);
	 */
	
	private Long id;
	private Long managederrendId;
	private Long transitiondefId;
	private Date expirey;
	
	public String toString() {
		return "StateExpiration: id "+id+", managederrendId "+managederrendId+", transitiondefId "+transitiondefId+", expirey "+expirey;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getManagederrendId() {
		return managederrendId;
	}
	public void setManagederrendId(Long managederrendId) {
		this.managederrendId = managederrendId;
	}
	public Long getTransitiondefId() {
		return transitiondefId;
	}
	public void setTransitiondefId(Long transitiondefId) {
		this.transitiondefId = transitiondefId;
	}
	public Date getExpirey() {
		return expirey;
	}
	public void setExpirey(Date expirey) {
		this.expirey = expirey;
	}
	
	public void save() {
		if(id == null) {
			StateExpirationDB.createStateExpiration(this);
		}
		else {
			StateExpirationDB.updateStateExpiration(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		StateExpirationDB.deleteStateExpiration(this);
		id = null;
	}
	
	private Transitiondef transitiondef = null;
	
	public Transitiondef getTransitiondef() {
		if(transitiondef != null) {
			return transitiondef;
		}
		if(transitiondefId == null) {
			return null;
		}
		transitiondef = TransitiondefDB.getTransitiondefFromId(transitiondefId);
		return transitiondef;
	}
	
	public void setTransitiondef(Transitiondef td) {
		transitiondefId = td.getId();
		transitiondef = td;
	}

	private ManagedErrend managederrend = null;
	
	public ManagedErrend getManagedErrend() {
		if(managederrend != null) {
			return managederrend;
		}
		if(managederrendId == null) {
			return null;
		}
		managederrend = ManagedErrendDB.getManagedErrendFromId(managederrendId);
		return managederrend;
	}
	
	public void setManagedErrend(ManagedErrend me) {
		managederrendId = me.getId();
		managederrend = me;
	}

}
