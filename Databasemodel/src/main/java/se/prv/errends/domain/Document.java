package se.prv.errends.domain;

import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.DocumentAttributeDB;
import se.prv.errends.dbc.DocumentDB;
import se.prv.errends.dbc.DocumentEventDB;
import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.DossierDoctypeDefDB;
import se.prv.errends.dbc.SessionDB;

public class Document {

	/*
create table document (
  id bigserial primary key,
  filename varchar(180) not null,
  mimetype varchar(180) not null,
  active bool default true not null,
  origin varchar(600) not null,
  description varchar(1200) default null,
  dateOfReception timestamp not null,
  data bytea,
  sessionId bigint not null,
  dossierdoctypedefId bigint default null,
  dossierId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdoctypedefId ) references dossierdoctypedef (id),
  foreign key ( dossierId ) references dossier (id)
);
	 */
	
	private Long id;
	public static final int FILENAME_FIELD_LENGTH = 180;
	private String filename;
	public static final int MIMETYPE_FIELD_LENGTH = 180;
	private String mimetype;
	private boolean active;
	public static final int ORIGIN_FIELD_LENGTH = 600;
	private String origin;
	private String description;
	public static final int DESCRIPTION_FIELD_LENGTH = 1200;
	private Date dateOfReception;
	private Long sessionId;
	private Long dossierdoctypedefId;
	private Long dossierId;

	public String toString() {
		return "Document{id="+id+
				", filename="+filename+", mimetype="+mimetype+", active="+active+
				", origin="+origin+
				", description="+description+
				", dateOfReception="+dateOfReception+
				", sessionId="+sessionId+
				", dossierdoctypedefId="+dossierdoctypedefId+", dossierId="+dossierId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		if(filename != null && filename.length() > FILENAME_FIELD_LENGTH) {
			filename = filename.substring(0, FILENAME_FIELD_LENGTH);
		}
		this.filename = filename;
	}
	public String getMimetype() {
		return mimetype;
	}
	public void setMimetype(String mimetype) {
		if(mimetype != null && mimetype.length() > MIMETYPE_FIELD_LENGTH) {
			mimetype = mimetype.substring(0, MIMETYPE_FIELD_LENGTH);
		}
		this.mimetype = mimetype;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		if(origin != null && origin.length() > ORIGIN_FIELD_LENGTH) {
			origin = origin.substring(0, ORIGIN_FIELD_LENGTH);
		}
		this.origin = origin;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if(description != null && description.length() > DESCRIPTION_FIELD_LENGTH) {
			description = description.substring(0, DESCRIPTION_FIELD_LENGTH);
		}
		this.description = description;
	}
	public Date getDateOfReception() {
		return dateOfReception;
	}

	public void setDateOfReception(Date dateOfReception) {
		this.dateOfReception = dateOfReception;
	}

	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getDossierDoctypeDefId() {
		return dossierdoctypedefId;
	}
	public void setDossierDoctypeDefId(Long dossierdoctypedefId) {
		this.dossierdoctypedefId = dossierdoctypedefId;
		dossierdoctypedef = null;
	}
	public Long getDossierId() {
		return dossierId;
	}
	public void setDossierId(Long dossierId) {
		this.dossierId = dossierId;
	}
	public byte[] getData() {
		return DocumentDB.getData(id);
	}
	public void setData(byte[] byteArray) {
		DocumentDB.setData(id, byteArray);
	}
	
	public boolean delete() {
		if(DocumentDB.deleteDocument(this)) {
			id = null;
			return true;
		}
		return false;
	}
	
	public boolean deleteRecursive() {
		List<DocumentEvent> events = DocumentEventDB.getAllDocumentEventsOfDocument(id);
		for(DocumentEvent event : events) {
			event.delete();
		}
		return delete();
	}
	
	public boolean save() {
		if(id == null) {
			return DocumentDB.createDocument(this);
		}
		else {
			return DocumentDB.updateDocument(this);
		}
	}
	
	public List<DocumentEvent> getReferringDocumentEvents() {
		return DocumentEventDB.getAllDocumentEventsOfDocument(id);
	}
	
	private Session session = null;
	
	public Session getSession() {
		if(sessionId == null) {
			return null;
		}
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}

	private DossierDoctypeDef dossierdoctypedef = null;
	
	public DossierDoctypeDef getDossierDoctypeDef() {
		if(dossierdoctypedefId == null) {
			return null;
		}
		if(dossierdoctypedef != null) {
			return dossierdoctypedef;
		}
		dossierdoctypedef = DossierDoctypeDefDB.getDossierDoctypeDefFromId(dossierdoctypedefId);
		return dossierdoctypedef;
	}

	private Dossier dossier = null;
	
	public Dossier getDossier() {
		if(dossierId == null) {
			return null;
		}
		if(dossier != null) {
			return dossier;
		}
		dossier = DossierDB.getDossierFromId(dossierId);
		return dossier;
	}
	
	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
		dossierId = dossier.getId();
	}

	public List<DocumentAttribute> getReferringDocumentAttributes() {
		return DocumentAttributeDB.getAllDocumentAttributesOfDocument(id);
	}

}
