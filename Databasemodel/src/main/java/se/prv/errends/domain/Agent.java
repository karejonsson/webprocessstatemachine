package se.prv.errends.domain;

import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.OrganisationGroupMemberDB;
import se.prv.errends.dbc.PersonDB;

public class Agent {

	/*
create table agent (
  id bigserial primary key,
  personId bigint not null,
  organizationId bigint not null,
  active bool default true,
  registerred timestamp not null,
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);
	 */
	
	private Long id;
	private Long personId;
	private Long organizationId;
	private Boolean active = true;
	private Date registerred;

	public String toString() {
		return "Agent{id "+id+", personId "+personId+", organizationId "+organizationId+", active "+active+
				", registerred "+registerred+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public Boolean isActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getRegisterred() {
		return registerred;
	}
	public void setRegisterred(Date registerred) {
		this.registerred = registerred;
	}
	
	private Person person = null;
	
	public Person getPerson() {
		if(person != null) {
			return person;
		}
		person = PersonDB.getPersonFromId(personId);
		return person;
	}
	
	public void setPerson(Person p) {
		personId = p.getId();
		person = p;
	}
	
    public boolean isPersisted() {
        return id != null;
    }

    public void delete() {
        AgentDB.deleteAgent(this);
        id = null;
    }
    
    public void deleteRecursively() {
    	List<Actor> actors = ActorDB.getAllActorsOfAgentId(id);
    	if(actors != null) {
    		for(Actor actor : actors) {
    			actor.deleteRecursively();
    		}
    	}
        AgentDB.deleteAgent(this);
        id = null;
    }
    
	public void save() {
		if(id == null) {
			AgentDB.createAgent(this);
		}
		else {
			AgentDB.updateAgent(this);
		}
	}
	
	private Organisation organization = null;
	
	public Organisation getOrganisation() {
		if(organization != null) {
			return organization;
		}
		if(organizationId == null) {
			return null;
		}
		organization = OrganizationDB.getOrganizationFromId(organizationId);
		return organization;
	}
	
	public void setOrganization(Organisation organization) {
		organizationId = organization.getId();
		this.organization = organization;
	}

	public List<OrganisationGroupMember> getReferringOrganisationGroupMembers() {
		return OrganisationGroupMemberDB.getAllOrganisationGroupMembersOfAgent(id);
	}
	
}
