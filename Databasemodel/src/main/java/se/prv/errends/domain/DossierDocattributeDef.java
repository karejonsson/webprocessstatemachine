package se.prv.errends.domain;

import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.dbc.DossierDocattributeDefDB;
import se.prv.errends.dbc.DossierDoctypeDefDB;

public class DossierDocattributeDef {

/*
create table dossierdocattributedef {
  id bigserial primary key,
  dossierdocattributename varchar(50) not null,
  dossierdefId bigint,
  foreign key ( dossierdefId ) references dossierdef (id)
};
 */
	
	private Long id;
	private String dossierdocattributename;
	private Long dossierdefId;
	
	public String toString() {
		return "DossierDoctypeDef{id "+id+", dossierdocattributename "+dossierdocattributename+", dossierdefId "+dossierdefId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDossierdocattributename() {
		return dossierdocattributename;
	}
	public void setDossierdocattributename(String dossierdocattributename) {
		this.dossierdocattributename = dossierdocattributename;
	}
	public Long getDossierdefId() {
		return dossierdefId;
	}
	public void setDossierdefId(Long dossierdefId) {
		this.dossierdefId = dossierdefId;
		dossierdef = null;
	}
	
	private DossierDef dossierdef;
	
	public DossierDef getDossierDef() {
		if(dossierdef != null) {
			return dossierdef;
		}
		if(dossierdefId == null) {
			return null;
		}
		dossierdef = DossierDefDB.getDossierDefFromId(dossierdefId);
		return dossierdef;
	}

	public void setDossierdef(DossierDef dossierdef) {
		dossierdefId = dossierdef.getId();
		this.dossierdef = dossierdef;
	}
	
	public boolean save() {
		if(id == null) {
			return DossierDocattributeDefDB.createDossierDocattributeDef(this);
		}
		else {
			return DossierDocattributeDefDB.updateDossierDocattributeDef(this);
		}
	}
	
	public boolean delete() {
		if(DossierDocattributeDefDB.deleteDossierDocattributeDef(this)) {
			id = null;
			return true;
		}
		return false;
	}

}
