package se.prv.errends.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import se.prv.errends.dbc.DocumentDB;
import se.prv.errends.dbc.DocumentEventDB;
import se.prv.errends.dbc.SessionDB;

public class DocumentEvent {

	/*
create table documentevent (
  id bigserial primary key,
  description varchar(1200) default null,
  code int not null,
  systeminfo varchar(800) not null,
  timeofevent timestamp not null,
  sessionId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( documentId ) references document (id)
);
	 */
	
	private Long id;
	public static final int DESCRIPTION_FIELD_LENGTH = 1200;
	private String description;
	private int code;
	public static final int SYSTEMINFO_FIELD_LENGTH = 800;
	private String systeminfo;
	private Date timeofevent;
	private Long sessionId;
	private Long documentId;
	
	public String toString() {
		return "DocumentEvent{id="+id+
				", description="+description+", code="+code+", systeminfo="+systeminfo+
				", timeofevent="+timeofevent+", sessionId="+sessionId+", documentId="+documentId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if(description != null && description.length() > DESCRIPTION_FIELD_LENGTH) {
			description = description.substring(0, DESCRIPTION_FIELD_LENGTH);
		}
		this.description = description;
	}
	public int getCode() {
		return code;
	}
	public DocumentEventType getCodeEnum() {
		return DocumentEventType.get(code);
	}
	public void setCode(int code) {
		this.code = code;
	}
	public void setCodeEnum(DocumentEventType det) {
		code = det.code();
	}
	public String getSysteminfo() {
		return systeminfo;
	}
	public void setSysteminfo(String systeminfo) {
		if(systeminfo != null && systeminfo.length() > SYSTEMINFO_FIELD_LENGTH) {
			systeminfo = systeminfo.substring(0, SYSTEMINFO_FIELD_LENGTH);
		}
		this.systeminfo = systeminfo;
	}
	public Date getTimeofevent() {
		return timeofevent;
	}
	public void setTimeofevent(Date timeofevent) {
		this.timeofevent = timeofevent;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	
	public void setDocument(Document document) {
		this.documentId = document == null ? null : document.getId();;
		this.document = document;
	}
	
	public boolean delete() {
		if(DocumentEventDB.deleteDocumentEvent(this)) {
			id = null;
			return true;
		}
		return false;
	}
	
	public boolean save() {
		if(id == null) {
			return DocumentEventDB.createDocumentEvent(this);
		}
		else {
			return DocumentEventDB.updateDocumentEvent(this);
		}
	}

	private Session session = null;
	
	public Session getSession() {
		if(sessionId == null) {
			return null;
		}
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}

	private Document document = null;
	
	public Document getDocument() {
		if(documentId == null) {
			return null;
		}
		if(document != null) {
			return document;
		}
		document = DocumentDB.getDocumentFromId(documentId);
		return document;
	}

	public enum DocumentEventType {
		
		CREATE_GUI("Skapat från webb-GUI", 1),
		ACTIVATED_GUI("Aktiverat från webb-GUI", 2),
		INACTIVATED_GUI("Inaktiverat från webb-GUI", 3), 
		ADD_ATTRIBUTE("Tillägg attribut från webb-GUI", 4),
		REMOVE_ATTRIBUTE("Borttag attribut från webb-GUI", 5),
		CREATENOMETA_GUI("Skapat utan fildata från webb-GUI", 6);
		
		private String text;
		private int code;
		DocumentEventType(String text, int code) {
			this.text = text;
			this.code = code;
		}
		public String text() { return text; }
		public int code() { return code; }
		
		private static final Map<Integer, DocumentEventType> MAP = new HashMap<Integer, DocumentEventType>();
		static {
			for(DocumentEventType s : DocumentEventType.values()) {
				MAP.put(s.code(), s);
			}
		}
		public static DocumentEventType get(int code) {
			return MAP.get(code);
		}
		
	}
	
}
