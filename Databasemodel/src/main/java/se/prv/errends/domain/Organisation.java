package se.prv.errends.domain;

import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.OrganisationRoleGroupDB;
import se.prv.errends.dbc.OrganizationDB;

public class Organisation {
	
	/*
create table organization (
  id bigserial primary key,
  orgname varchar(100) not null,
  orgnr varchar(14) not null,
  active bool default true,
  registerred timestamp not null,
  unique(orgnr)
);
	 */
	
	private Long id;
	private String orgname;
	private String orgnr;
	private Boolean active = true;
	private Date registerred;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrgname() {
		return orgname;
	}
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}
	public String getOrgnr() {
		return orgnr;
	}
	public void setOrgnr(String orgnr) {
		this.orgnr = orgnr;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getRegisterred() {
		return registerred;
	}
	public void setRegisterred(Date registerred) {
		this.registerred = registerred;
	}

    public boolean isPersisted() {
        return id != null;
    }

    public void delete() { 
        OrganizationDB.deleteOrganization(this);
        id = null;
    }

	public void save() {
		if(id == null) {
			OrganizationDB.createOrganization(this);
		}
		else {
			OrganizationDB.updateOrganization(this);
		}
	}
	
	public List<Agent> getReferringAgents() {
		return AgentDB.getAllAgentsOfOrganization(id);
	}

	public List<OrganisationRoleGroup> getReferringOrganisationRoleGroups() {
		return OrganisationRoleGroupDB.getAllOrganisationRoleGroupsOfOrganisation(id);
	}

}
