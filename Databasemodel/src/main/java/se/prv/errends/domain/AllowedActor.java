package se.prv.errends.domain;

public class AllowedActor {

	public Errend errend;
	public OrganisationGroupMember groupMember;
	
	public String toString() {
		return "AllowedActor{errend="+errend+", groupMember="+groupMember+"}";
	}

}
