package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.RoledefDB;

public class Roledef {

/*
create table roledef (
  id bigserial primary key,
  rolename varchar(50) not null,
  orderby int default null,
  processdefId bigint not null,
  foreign key ( processdefId ) references processdef (id)
);
 */

	private Long id;
	private String rolename;
	private Integer orderby;
	private Long processdefId;
	
	public String toString() {
		return rolename;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getProcessdefId() {
		return processdefId;
	}
	public void setProcessdefId(Long processdefId) {
		this.processdefId = processdefId;
		processdef = null;
	}

	private Processdef processdef;
	
	public Processdef getProcessdef() {
		if(processdef != null) {
			return processdef;
		}
		if(processdefId == null) {
			return null;
		}
		processdef = ProcessdefDB.getProcessdefFromId(processdefId);
		return processdef;
	}
	
	public void setProcessdef(Processdef pd) {
		processdefId = pd.getId();
		this.processdef = pd;
	}
	
	public void save() {
		if(id == null) {
			RoledefDB.createRoledef(this);
		}
		else {
			RoledefDB.updateRoledef(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void deleteRecursive() {
		List<Actor> actors = ActorDB.getAllActorsOfRoledefId(id);
		for(Actor actor : actors) {
			try {
				actor.deleteRecursively();
			} 
			catch(Exception e) {}
		}
		delete();
	}

	public void delete() {
		RoledefDB.deleteRoledef(this);
		id = null;
	}

}
