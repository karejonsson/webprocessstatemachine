package se.prv.errends.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.ErrendCommentDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.ManagedErrendDB;
import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.dbc.ReportDB;
import se.prv.errends.dbc.SessionDB;
import se.prv.errends.dbc.TransitionDB;

public class Errend {

/*
create table errend (
  id bigserial primary key,
  created timestamp not null,
  initiated bool default false,
  active bool default true,
  orderby int default null,
  adminusername varchar(40),
  personname varchar(40),
  organizationId bigint not null,
  foreign key ( organizationId ) references organization (id)
);
 */

	private Long id;
	private Date created;
	private boolean initiated = false;
	private boolean active = true;
	private Integer orderby = null;
	private String adminusername = null;
	private String personname = null;
	private Long organizationId = -1l;

	public String toString() {
		return "Errend{id "+id+", created "+created+", initiated "+initiated+", active "+active+
				", orderby "+orderby+", adminusername "+adminusername+", personname "+personname+
				", organizationId "+organizationId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public boolean isInitiated() {
		return initiated;
	}
	public void setInitiated(boolean initiated) {
		this.initiated = initiated;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public String getAdminusername() {
		return adminusername;
	}
	public void setAdminusername(String adminusername) {
		this.adminusername = adminusername;
	}
	public String getPersonname() {
		return personname;
	}
	public void setPersonname(String personname) {
		this.personname = personname;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
		organization = null;
	}
	
	private Organisation organization = null;
	
	public Organisation getOrganization() {
		if(organization != null) {
			return organization;
		}
		organization = OrganizationDB.getOrganizationFromId(organizationId);
		return organization;
	}
	
	public void setOrganization(Organisation organization) {
		organizationId = organization.getId();
		this.organization = organization;
	}
	
	public List<Actor> getReferringActors() {
		return ActorDB.getAllActorsOfErrendId(id);
	}
	
	public void save() {
		if(id == null) {
			ErrendDB.createErrend(this);
		}
		else {
			ErrendDB.updateErrend(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		ErrendDB.deleteErrend(this);
		id = null;
	}
	
	public ManagedErrend getManagedErrend() {
		return ManagedErrendDB.getManagedErrendOfErrendId(id);
	}
	
	public Dossier getReferringDossier() {
		return DossierDB.getDossierOfErrendId(id);
	}
	
	public void deleteRecursively() {
		ManagedErrend me = ManagedErrendDB.getManagedErrendOfErrendId(id);
		if(me != null) {
			me.deleteRecursively();
		}
		List<Transition> transs = TransitionDB.getAllTransitionsOfErrend(id);
		complexDelete(transs);
		
		List<ErrendComment> ecomms = ErrendCommentDB.getAllErrendCommentsOfErrend(id);
		if(ecomms != null) {
			for(ErrendComment ecomm : ecomms) {
				ecomm.delete();
			}
		}

		List<Session> sessions = SessionDB.getAllSessionsOfErrend(id);
		if(sessions != null) {
			for(Session sess : sessions) {
				try {
					sess.deleteRecursively();
				}
				catch(Exception e) {
				}
			}
		}
		List<Actor> acts = ActorDB.getAllActorsOfErrendId(id);
		if(acts != null) {
			for(Actor act : acts) {
				act.deleteRecursively();
			}
		}
		
		List<Report> reports = ReportDB.getAllReportsOfErrend(id);
		if(reports != null) {
			for(Report report : reports) {
				report.deleteRecursively();
			}
		}
		Dossier dossier = DossierDB.getDossierOfErrendId(id);
		if(dossier != null) {
			dossier.setErrendId(null);
			dossier.save();
		}
		delete();
	}
	
	private static void complexDelete(List<Transition> transs) {
		//System.out.println("Errend.complexDelete 1 : "+transs.size());
		HashMap<Long, Transition> lookup = new HashMap<Long, Transition>();
		Set<Long> ids = new HashSet<Long>();
		Set<Long> predecessorIds = new HashSet<Long>();
		for(Transition trans : transs) {
			Long x = trans.getId();
			lookup.put(x,  trans);
			ids.add(x);
			x = trans.getPredecessorId();
			if(x != null) {
				predecessorIds.add(x);				
			}
		}
		for(Long predecessorId : predecessorIds) {
			ids.remove(predecessorId);
		}
		Iterator<Long> iter = ids.iterator();
		if(!iter.hasNext()) {
			return;
		}
		Long toDelete = iter.next();
		//System.out.println("Errend.complexDelete 2.1 : toDelete = "+toDelete);
		
		Transition t = lookup.get(toDelete);
		//System.out.println("ERREND TRANSITION SIZE "+TransitionDB.getAllTransitions().size());

		while(t != null) {
			t.deleteRecursively();
			//System.out.println("ERREND TRANSITION SIZE "+TransitionDB.getAllTransitions().size());
			//if(true) { System.exit(-1); }
			//System.out.println("Errend.complexDelete 2.2 : toDelete = "+toDelete);
			toDelete = t.getPredecessorId();
			//System.out.println("Errend.complexDelete 2.3 : toDelete = "+toDelete);
			t = lookup.get(toDelete);
		}
		//System.out.println("Errend.complexDelete 3 Slut");
		//System.out.println("ERREND TRANSITION SIZE "+TransitionDB.getAllTransitions().size());

	}

}
