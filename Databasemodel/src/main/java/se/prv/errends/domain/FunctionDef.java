package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.FunctionDefReuseDB;
import se.prv.errends.dbc.ProcessfileDB;

public class FunctionDef {

	/*
create table functiondef (
  id bigserial primary key,
  functionname varchar(100) not null,
  typename varchar(10) default null,
  source varchar(30000) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);
	 */

	private Long id;
	private String functionname;
	private String typename;
	private String source;
	private Integer orderby;
	private Long processfileId;
	
	public String toString() {
		return "ID "+id+", functionname "+functionname+", typename "+typename+", processfileId "+processfileId;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFunctionname() {
		return functionname;
	}
	public void setFunctionname(String functionname) {
		this.functionname = functionname;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getProcessfileId() {
		return processfileId;
	}
	public void setProcessfileId(Long processfileId) {
		this.processfileId = processfileId;
		processfile = null;
	}
	
	private Processfile processfile;
	
	public Processfile getProcessfile() {
		if(processfile != null) {
			return processfile;
		}
		processfile = ProcessfileDB.getProcessfileFromId(processfileId);
		return processfile;
	}

	public void setProcessfile(Processfile processfile) {
		this.processfile = processfile;
		processfileId = processfile.getId();
	}
	
	public void save() {
		if(id == null) {
			FunctionDefDB.createFunctionDef(this);
		}
		else {
			FunctionDefDB.updateFunctionDef(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		FunctionDefDB.deleteFunctionDef(this);
		id = null;
	}
	
	public void deleteRecursive() {
		List<FunctionDefReuse> reuses = getReferringFunctionDefReuses();
		if(reuses != null) {
			for(FunctionDefReuse reuse : reuses) {
				reuse.delete();
			}
		}
		delete();
	}
	
	public List<FunctionDefReuse> getReferringFunctionDefReuses() {
		if(id == null) {
			return null;
		}
		return FunctionDefReuseDB.getAllFunctionDefReusesOfFunctionDef(id);
	}

}
