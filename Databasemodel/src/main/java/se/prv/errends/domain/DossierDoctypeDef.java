package se.prv.errends.domain;

import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.dbc.DossierDocattributeDefDB;
import se.prv.errends.dbc.DossierDoctypeDefDB;

public class DossierDoctypeDef {

/*
create table dossierdoctypedef {
  id bigserial primary key,
  dossierdoctypename varchar(50) not null,
  dossierdefId bigint,
  foreign key ( dossierdefId ) references dossierdef (id)
};
 */
	
	private Long id;
	private String dossierdoctypename;
	private Long dossierdefId;
	
	public String toString() {
		return "DossierDoctypeDef{id "+id+", dossierdoctypename "+dossierdoctypename+", dossierdefId "+dossierdefId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDossierdoctypename() {
		return dossierdoctypename;
	}
	public void setDossierdoctypename(String dossierdoctypename) {
		this.dossierdoctypename = dossierdoctypename;
	}
	public Long getDossierdefId() {
		return dossierdefId;
	}
	public void setDossierdefId(Long dossierdefId) {
		this.dossierdefId = dossierdefId;
		dossierdef = null;
	}
	
	private DossierDef dossierdef;
	
	public DossierDef getDossierDef() {
		if(dossierdef != null) {
			return dossierdef;
		}
		if(dossierdefId == null) {
			return null;
		}
		dossierdef = DossierDefDB.getDossierDefFromId(dossierdefId);
		return dossierdef;
	}
	
	public void setDossierdef(DossierDef dossierdef) {
		dossierdefId = dossierdef.getId();
		this.dossierdef = dossierdef;
	}


	public boolean save() {
		if(id == null) {
			return DossierDoctypeDefDB.createDossierDoctypeDef(this);
		}
		else {
			return DossierDoctypeDefDB.updateDossierDoctypeDef(this);
		}
	}
	
	public boolean delete() {
		if(DossierDoctypeDefDB.deleteDossierDoctypeDef(this)) {
			id = null;
			return true;
		}
		return false;
	}

}
