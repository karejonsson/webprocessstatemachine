package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;

public class Processfile {

    /*
create table processfile (
  id bigserial primary key,
  filename varchar(50) not null,
  description varchar(250) default null,
  data bytea,
  orderby int default null,
  adminuserId bigint not null,
  unique(filename, description),
  foreign key ( adminuserId ) references adminuser (id)
);
     */

    private Long id;
    private String filename;
    private String description;
    private Integer orderby;
    private Long adminuserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        dirty = true;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
        dirty = true;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        dirty = true;
    }

    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
        dirty = true;
    }

    public Long getAdminuserId() {
        return adminuserId;
    }

    public void setAdminuserId(Long adminuserId) {
        this.adminuserId = adminuserId;
        adminuser = null;
        dirty = true;
    }

	private Adminuser adminuser;
	
	public Adminuser getAdminuser() {
		if(adminuser != null) {
			return adminuser;
		}
		adminuser = AdminuserDB.getAdminuserFromId(adminuserId);
		return adminuser;
	}
	
	public void save() {
		if(id == null) {
			ProcessfileDB.createProcessfile(this);
		}
		else {
			ProcessfileDB.updateProcessfile(this);
		}
        dirty = false;
	}

    public boolean isPersisted() {
        return id != null;
    }

    public void delete() {
    	//System.out.println("Processfile.delete före : ID = "+id);
        ProcessfileDB.deleteProcessfile(this);
    	//System.out.println("Processfile.delete efter : ID = "+id);
        id = null;
        dirty = false;
    }
    
    public void deleteRecursively() {
    	List<Processdef> pds = getReferringProcessdefs();
    	for(Processdef pd : pds) {
    		pd.deleteRecursiveWithErrends();
    	}
    	List<FunctionDef> fds = getReferringFunctionDefs();
    	for(FunctionDef fd : fds) {
    		fd.deleteRecursive();
    	}
    	delete();
    }
    
    public byte[] getData() {
    	return ProcessfileDB.getData(id);
    }

    public void setData(byte[] data) {
    	ProcessfileDB.setData(id, data);
    }
    
    private boolean dirty = false;
    
    public void setDirty(boolean dirty) {
    	this.dirty = dirty;
    }
    
    public boolean isDirty() {
    	return dirty;
    }
    
    public List<Processdef> getReferringProcessdefs() {
    	return ProcessdefDB.getAllProcessdefsOfProcessfile(id);
    }

    public List<FunctionDef> getReferringFunctionDefs() {
    	return FunctionDefDB.getAllFunctionDefsOfProcessfile(id);
    }

}
