package se.prv.errends.domain;

import se.prv.errends.dbc.StatedefDB;
import se.prv.errends.dbc.TransitiondefDB;

public class Transitiondef {

/*
create table transitiondef (
  id bigserial primary key,
  transitionname varchar(50) not null,
  layout varchar(30000) default null,
  orderby int default null,
  sourcestatedefId bigint not null,
  targetstatedefId bigint not null,
  foreign key ( sourcestatedefId ) references statedef (id),
  foreign key ( targetstatedefId ) references statedef (id)
);
 */

	private Long id;
	private String transitionname;
	private String layout = null;
	private Integer orderby = null;
	private Long sourcestatedefId;
	private Long targetstatedefId;
	
	public String toString() {
		return "{ Transitiondef: id "+id+
				", transitionname "+transitionname+
				", layout length "+layout.length()+
				", orderby "+orderby+
				", sourcestatedefId "+sourcestatedefId+
				", targetstatedefId "+targetstatedefId+
				" }";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTransitionname() {
		return transitionname;
	}
	public void setTransitionname(String transitionname) {
		this.transitionname = transitionname;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getSourcestatedefId() {
		return sourcestatedefId;
	}
	public void setSourcestatedefId(Long sourcestatedefId) {
		this.sourcestatedefId = sourcestatedefId;
		sourcestatedef = null;
	}
	public Long getTargetstatedefId() {
		return targetstatedefId;
	}
	public void setTargetstatedefId(Long targetstatedefId) {
		this.targetstatedefId = targetstatedefId;
		targetstatedef = null;
	}
	
	private Statedef sourcestatedef;
	
	public Statedef getSourcestatedef() {
		if(sourcestatedef != null) {
			return sourcestatedef;
		}
		sourcestatedef = StatedefDB.getStatedefFromId(sourcestatedefId);
		return sourcestatedef;
	}
	
	private Statedef targetstatedef;
	
	public Statedef getTargetstatedef() {
		if(targetstatedef != null) {
			return targetstatedef;
		}
		targetstatedef = StatedefDB.getStatedefFromId(targetstatedefId);
		return targetstatedef;
	}
	
	public void save() {
		if(id == null) {
			TransitiondefDB.createTransitiondef(this);
		}
		else {
			TransitiondefDB.updateTransitiondef(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		TransitiondefDB.deleteTransitiondef(this);
		id = null;
	}

}
