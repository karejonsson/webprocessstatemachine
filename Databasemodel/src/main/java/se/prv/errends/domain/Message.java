package se.prv.errends.domain;

import java.util.Date;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.MessageDB;

public class Message {

	/*
create table message (
  id bigserial primary key,
  title varchar(80) default null,
  writtenmessage varchar(2000) default null,
  unread bool default true not null,
  send timestamp not null,
  sourceActorId bigint,
  targetActorId bigint,
  foreign key ( sourceActorId ) references actor (id),
  foreign key ( targetActorId ) references actor (id)
);
	 */
	
	private Long id = null;
	private String title;
	private String writtenmessage;
	private Boolean unread = true;
	private Date send;
	private Long sourceActorId;
	private Long targetActorId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getWrittenmessage() {
		return writtenmessage;
	}
	public void setWrittenmessage(String writtenmessage) {
		this.writtenmessage = writtenmessage;
	}
	public Date getSend() {
		return send;
	}
	public void setSend(Date send) {
		this.send = send;
	}
	public Boolean isUnread() {
		return unread;
	}
	public void setUnread(boolean unread) {
		this.unread = unread;
	}
	public Long getSourceActorId() {
		return sourceActorId;
	}
	public void setSourceActorId(Long sourceActorId) {
		this.sourceActorId = sourceActorId;
	}
	public Long getTargetActorId() {
		return targetActorId;
	}
	public void setTargetActorId(Long targetActorId) {
		this.targetActorId = targetActorId;
	}
	
	private Actor sourceActor = null;
	
	public Actor getSourceActor() {
		if(sourceActor != null) {
			return sourceActor;
		}
		if(sourceActorId == null) {
			return null;
		}
		sourceActor = ActorDB.getActorFromId(sourceActorId);
		return sourceActor;
	}
	
	public void setSourceActor(Actor sourceActor) {
		sourceActorId = sourceActor.getId();
		this.sourceActor = sourceActor;
	}
	
	private Actor targetActor = null;
	
	public Actor getTargetActor() {
		if(targetActor != null) {
			return targetActor;
		}
		if(targetActorId == null) {
			return null;
		}
		targetActor = ActorDB.getActorFromId(targetActorId);
		return targetActor;
	}
	
	public void setTargetActor(Actor targetActor) {
		targetActorId = targetActor.getId();
		this.targetActor = targetActor;
	}
	
	public void save() {
		if(sourceActorId == null && targetActorId == null) {
			if(id != null) {
				MessageDB.deleteMessage(id);
			}
			return;
		}
		if(id == null) {
			MessageDB.createMessage(this);
		}
		else {
			MessageDB.updateMessage(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		MessageDB.deleteMessage(this);
		id = null;
	}

}
