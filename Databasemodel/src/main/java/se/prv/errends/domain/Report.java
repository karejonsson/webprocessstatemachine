package se.prv.errends.domain;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.ReportDB;
import se.prv.errends.dbc.ReportTemplateReuseDB;

public class Report {

/*
create table report (
  id bigserial primary key,
  reporttitle varchar(100) default null,
  data bytea,
  reporttemplatereuseId bigint not null,
  errendId bigint not null,
  actorId bigint not null,
  foreign key ( reporttemplatereuseId ) references reporttemplatereuse (id),
  foreign key ( errendId ) references errend (id),
  foreign key ( actorId ) references actor (id)
); */

	private Long id;
	private String reporttitle;
	private Long reporttemplatereuseId;
	private Long errendId;
	private Long actorId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getReporttitle() {
		return reporttitle;
	}
	public void setReporttitle(String reporttitle) {
		this.reporttitle = reporttitle;
	}
	public Long getReportTemplateReuseId() {
		return reporttemplatereuseId;
	}
	public void setReportTemplateReuseId(Long reporttemplatereuseId) {
		this.reporttemplatereuseId = reporttemplatereuseId;
	}
	public Long getErrendId() {
		return errendId;
	}
	public void setErrendId(Long errendId) {
		this.errendId = errendId;
	}
	public Long getActorId() {
		return actorId;
	}
	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}
	
	public void save() {
		if(id == null) {
			ReportDB.createReport(this);
		}
		else {
			ReportDB.updateReport(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	private ReportTemplateReuse reporttemplatereuse = null;
	
	public ReportTemplateReuse getReportTemplateReuse() {
		if(reporttemplatereuse != null) {
			return reporttemplatereuse;
		}
		if(reporttemplatereuseId == null) {
			return null;
		}
		reporttemplatereuse = ReportTemplateReuseDB.getReportTemplateReuseFromId(reporttemplatereuseId);
		return reporttemplatereuse;
	}
	
	public void setReportTemplateReuse(ReportTemplateReuse reporttemplatereuse) {
		reporttemplatereuseId = reporttemplatereuse.getId();
		this.reporttemplatereuse = reporttemplatereuse;
	}

	private Errend errend;
	
	public Errend getErrend() {
		if(errend != null) {
			return errend;
		}
		if(errendId == null) {
			return null;
		}
		errend = ErrendDB.getErrendFromId(errendId);
		return errend;
	}
	
	public void setErrend(Errend errend) {
		this.errend = errend;
		this.errendId = errend.getId();
	}
	
	private Actor actor;
	
	public Actor getActor() {
		if(actor != null) {
			return actor;
		}
		if(actorId == null) {
			return null;
		}
		actor = ActorDB.getActorFromId(actorId);
		return actor;
	}
	
	public void setActor(Actor actor) {
		this.actor = actor;
		this.actorId = actor.getId();
	}
	
	public void delete() {
		ReportDB.deleteReport(this);
		id = null;
	}
	
	public void deleteRecursively() {
		delete();		
	}

	public byte[] getData() {
		return ReportDB.getData(id);
	}

	public void setData(byte[] byteArray) {
		ReportDB.setData(id, byteArray);
	}

}
