package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.DocumentEventDB;
import se.prv.errends.dbc.OrganisationGroupMemberDB;
import se.prv.errends.dbc.OrganisationRoleGroupDB;
import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.dbc.RoledefDB;

public class OrganisationRoleGroup {

	private Long id;
	private Long organisationId;
	private Long roledefId;
	private String name;
	
	public String toString() {
		return "OrganisationRoleGroup{id="+id+
				", organisationId="+organisationId+", roledefId="+roledefId+", name="+name+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
		organisation = null;
	}
	public Long getRoledefId() {
		return roledefId;
	}
	public void setRoledefId(Long roledefId) {
		this.roledefId = roledefId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	private Roledef roledef = null;
	
	public Roledef getRoledef() {
		if(roledefId == null) {
			return null;
		}
		if(roledef != null) {
			return roledef;
		}
		roledef = RoledefDB.getRoledefFromId(roledefId);
		return roledef;
	}
	
	public void setRoledef(Roledef roledef) {
		this.roledefId = roledef == null ? null : roledef.getId();;
		this.roledef = roledef;
	}
	
	private Organisation organisation = null;
	
	public Organisation getOrganisation() {
		if(organisationId == null) {
			return null;
		}
		if(organisation != null) {
			return organisation;
		}
		organisation = OrganizationDB.getOrganizationFromId(organisationId);
		return organisation;
	}
	
	public void setOrganisation(Organisation organisation) {
		this.organisationId = organisation == null ? null : organisation.getId();;
		this.organisation = organisation;
	}
	
	public boolean save() {
		if(id == null) {
			return OrganisationRoleGroupDB.createOrganisationRoleGroup(this);
		}
		else {
			return OrganisationRoleGroupDB.updateOrganisationRoleGroup(this);
		}
	}
	
	public List<OrganisationGroupMember> getReferringOrganisationGroupMembers() {
		return OrganisationGroupMemberDB.getAllOrganisationGroupMembersOfRoleGroup(id);
	}
	
	public boolean delete() {
		if(OrganisationRoleGroupDB.deleteOrganisationRoleGroup(this)) {
			id = null;
			return true;
		}
		return false;
	}
	
	public boolean deleteRecursively() {
		List<OrganisationGroupMember> memberships = getReferringOrganisationGroupMembers();
		for(OrganisationGroupMember membership : memberships) {
			membership.delete();
		}
		return delete();
	}

}
