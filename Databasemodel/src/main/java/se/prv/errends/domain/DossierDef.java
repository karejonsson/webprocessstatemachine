package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.dbc.DossierDocattributeDefDB;
import se.prv.errends.dbc.DossierDoctypeDefDB;
import se.prv.errends.dbc.ProcessfileDB;

public class DossierDef {

	/*
create table dossierdef (
  id bigserial primary key,
  dossiertypename varchar(50) not null,
  description varchar(250) default null,
  processfileId bigint default null,
  foreign key ( processfileId ) references processfile (id)
);
	 */
	
	private Long id;
	private String dossiertypename;
	private String description;
	private Long processfileId;
	
	public String toString() {
		return "DossierDef{id "+id+", dossiertypename "+dossiertypename+", description "+description+", processfileId "+processfileId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDossiertypename() {
		return dossiertypename;
	}
	public void setDossiertypename(String dossiertypename) {
		this.dossiertypename = dossiertypename;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getProcessfileId() {
		return processfileId;
	}
	public void setProcessfileId(Long processfileId) {
		this.processfileId = processfileId;
		processfile = null;
	}
	
	private Processfile processfile;
	
	public Processfile getProcessfile() {
		if(processfile != null) {
			return processfile;
		}
		processfile = ProcessfileDB.getProcessfileFromId(processfileId);
		return processfile;
	}

	public boolean save() {
		if(id == null) {
			return DossierDefDB.createDossierDef(this);
		}
		else {
			return DossierDefDB.updateDossierDef(this);
		}
	}

	public List<DossierDoctypeDef> getReferringDossierDoctypeDefs() {
		return DossierDoctypeDefDB.getAllDossierDoctypeDefsOfDossierDef(id);
	}

	public List<DossierDocattributeDef> getReferringDossierDocattributeDef() {
		return DossierDocattributeDefDB.getAllDossierDocattributeDefsOfDossierDef(id);
	}
	
	public boolean deleteRecursive() {
		List<DossierDoctypeDef> tdefs = getReferringDossierDoctypeDefs();
		for(DossierDoctypeDef tdef : tdefs) {
			tdef.delete();
		}
		List<DossierDocattributeDef> adefs = getReferringDossierDocattributeDef();
		for(DossierDocattributeDef adef : adefs) {
			adef.delete();
		}
		return delete();
	}
	
	public boolean delete() {
		if(DossierDefDB.deleteDossierDef(this)) {
			id = null;
			return true;
		}
		return false;
	}

}
