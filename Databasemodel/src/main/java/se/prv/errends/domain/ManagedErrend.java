package se.prv.errends.domain;

import java.util.List;

//import se.prv.errends.dbc.*;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.dbc.StatedefDB;
import se.prv.errends.dbc.TransitionDB;
import se.prv.errends.dbc.ManagedErrendDB;

public class ManagedErrend {

/*
create table managederrend (
  id bigserial primary key,
  errendId bigint not null,
  processdefId bigint not null,
  currentstateId bigint not null,
  lasttransitionId bigint not null,
  unique(errendId),
  foreign key ( errendId ) references errend (id),
  foreign key ( processdefId ) references processdef (id),
  foreign key ( currentstateId ) references statedef (id),
  foreign key ( lasttransitionId ) references transitiondef (id)
);
 */

	private Long id;
	private Long errendId;
	private Long processdefId;
	private Long currentstateId;
	private Long lasttransitionId;
	
	public String toString() {
		return "{ ManagedErrend: id "+id+", errendId "+errendId+", processdefId "+processdefId+", currentstateId "+currentstateId+", lasttransitionId "+lasttransitionId+" }";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getErrendId() {
		return errendId;
	}
	public void setErrendId(Long errendId) {
		this.errendId = errendId;
		errend = null;
	}
	public Long getProcessdefId() {
		return processdefId;
	}
	public void setProcessdefId(Long processdefId) {
		this.processdefId = processdefId;
		processdef = null;
	}
	public Long getCurrentstateId() {
		return currentstateId;
	}
	public void setCurrentstateId(Long currentstateId) {
		this.currentstateId = currentstateId;
		statedef = null;
	}
	public Long getLasttransitionId() {
		return lasttransitionId;
	}
	public void setLasttransitionId(Long lasttransitionId) {
		this.lasttransitionId = lasttransitionId;
		lasttransition = null;
	}
	
	private Errend errend;
	
	public Errend getErrend() {
		if(errend != null) {
			return errend;
		}
		errend = ErrendDB.getErrendFromId(errendId);
		return errend;
	}
	
	public void setErrend(Errend e) {
		errendId = e.getId();
		errend = e;
	}

	private Processdef processdef;
	
	public Processdef getProcessdef() {
		if(processdef != null) {
			return processdef;
		}
		if(processdefId == null) {
			return null;
		}
		processdef = ProcessdefDB.getProcessdefFromId(processdefId);
		return processdef;
	}
	
	public void setProcessdef(Processdef pd) {
		processdef = pd;
		processdefId = pd.getId();
	}

	private Statedef statedef;
	
	public Statedef getCurrentStatedef() {
		if(statedef != null) {
			return statedef;
		}
		if(currentstateId == null) {
			return null;
		}
		statedef = StatedefDB.getStatedefFromId(currentstateId);
		return statedef;
	}
	
	public void setCurrentState(Statedef csd) {
		this.statedef = csd;
		currentstateId = csd.getId();
	}

	/*
	private Transitiondef lasttransitiondef;
	
	public Transitiondef getLastTransitiondef() {
		if(lasttransitiondef != null) {
			return lasttransitiondef;
		}
		if(lasttransitiondefId == null) {
			return null;
		}
		lasttransitiondef = TransitiondefDB.getTransitiondefFromId(lasttransitiondefId);
		return lasttransitiondef;
	}
	*/
	
	private Transition lasttransition;
	
	public Transition getLastTransition() {
		if(lasttransition != null) {
			return lasttransition;
		}
		if(lasttransitionId == null) {
			return null;
		}
		lasttransition = TransitionDB.getTransitionFromId(lasttransitionId);
		return lasttransition;
	}
	
	public void setLastTransition(Transition transition) {
		this.lasttransition = transition;
		lasttransitionId = transition.getId();
	}

	public void save() {
		if(id == null) {
			ManagedErrendDB.createManagedErrend(this);
		}
		else {
			ManagedErrendDB.updateManagedErrend(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		ManagedErrendDB.deleteManagedErrend(this);
		id = null;
	}
	
	public void deleteExpirations() {
		List<StateExpiration> expirations = StateExpirationDB.getAllStateExpirationsOfManagedErrend(id);
		for(StateExpiration expiration : expirations) {
			expiration.delete();
		}
	}
	
	public void deleteRecursively() {
		deleteExpirations();
		delete();
	}
	
}
