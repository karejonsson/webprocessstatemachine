package se.prv.errends.domain;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.reuse.cli.PropertyNames;

public class Adminuser {

	/*
create table adminuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(40) not null,
  active bool default true,
  superprivilegies bool default false,
  orderby int default null,
  personId bigint not null,
  foreign key ( personId ) references person (id)
);
	 */

	private Long id;
	private String username;
	private String password;
	private boolean active = true;
	private boolean superprivilegies = false;
	private Integer orderby;
	private Long personId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isSuperprivilegies() {
		return superprivilegies;
	}

	public void setSuperprivilegies(boolean superprivilegies) {
		this.superprivilegies = superprivilegies;
	}

	public Integer getOrderby() {
		return orderby;
	}

	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
		person = null;
	}
	
	private Person person = null;
	
	public Person getPerson() {
		if(person != null) {
			return person;
		}
		if(personId == null) {
			return null;
		}
		person = PersonDB.getPersonFromId(personId);
		return person;
	}
	
	public void setPerson(Person p) {
		personId = p.getId();
		person = p;
	}

	public void save() {
		if(id == null) {
			AdminuserDB.createAdminuser(this);
		}
		else {
			AdminuserDB.updateAdminuser(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		AdminuserDB.deleteAdminuser(this);
		id = null;
	}
	
	private static final String HEXES = "0123456789abcdef";

	private static String getHex(byte[] raw) {
	    final StringBuilder hex = new StringBuilder(2 * raw.length);
	    for (final byte b : raw) {
	        hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
	    }
	    return hex.toString();
	}
	
	public static String calculateChecksum(String pass) throws NoSuchAlgorithmException {
	    MessageDigest m = MessageDigest.getInstance("MD5");
	    byte[] data = pass.getBytes();
	    return getHex(m.digest(data));
	}
	
	private static SimpleDateFormat formatter = null;
	
	public static String presentDate(Date date) {
		if(date == null) {
			return "--";
		}
		if(formatter == null) {
			String datetimeformat = "yyyy-MM-dd HH:mm:SS";
			try {
				datetimeformat = PersistentValuesDB.getStringValue(PropertyNames.datetime_format);
			} catch (Exception e) {
				e.printStackTrace();
			}
			formatter = new SimpleDateFormat(datetimeformat);
		}
		return formatter.format(date);
	}


}
