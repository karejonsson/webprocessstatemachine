package se.prv.errends.domain;

import se.prv.errends.dbc.StatedefDB;
import se.prv.errends.dbc.TransitiondefDB;

import java.util.List;
import java.util.Vector;

import se.prv.errends.dbc.ProcessdefDB;

public class Statedef {

/*
create table statedef (
  id bigserial primary key,
  statename varchar(50) not null,
  layout varchar(30000) default null,
  orderby int default null,
  processdefId bigint not null,
  foreign key ( processdefId ) references processdef (id)
);
 */

	private Long id;
	private String statename;
	private String layout = null;
	private Integer orderby = null;
	private Long processdefId;
	
	public String toString() {
		return "{ Statedef: id "+id+", statename "+statename+", layouts length "+layout.length()+", orderby "+orderby+", processdefId "+processdefId+" }";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getProcessdefId() {
		return processdefId;
	}
	public void setProcessdefId(Long processdefId) {
		this.processdefId = processdefId;
		processdef = null; 
	}
	
	private Processdef processdef;
	
	public Processdef getProcessdef() {
		if(processdef != null) {
			return processdef;
		}
		processdef = ProcessdefDB.getProcessdefFromId(processdefId);
		return processdef;
	}

	public void setProcessdef(Processdef pd) {
		this.processdef = pd;
		processdefId = pd.getId();
	}
	
	public void save() {
		//(new Throwable()).printStackTrace();
		if(id == null) {
			StatedefDB.createStatedef(this);
		}
		else {
			StatedefDB.updateStatedef(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		StatedefDB.deleteStatedef(this);
		id = null;
	}
	
	public void deleteRecursive() {
		List<Transitiondef> transitions = getUniqueTransitiondefs();
		for(Transitiondef td : transitions) {
			td.delete();
		}
		delete();
	}
	
	public List<Transitiondef> getSourceTransitiondefs() {
		return TransitiondefDB.getAllSourceTransitiondefsOfStatedef(id);
	}

	public List<Transitiondef> getTargetTransitiondefs() {
		return TransitiondefDB.getAllTargetTransitiondefsOfStatedef(id);
	}
	
	public List<Transitiondef> getUniqueTransitiondefs() {
		Vector<Long> ids = new Vector<>();
		Vector<Transitiondef> out = new Vector<>();
		List<Transitiondef> sources = getSourceTransitiondefs();
		for(Transitiondef td : sources) {
			ids.add(td.getId());
			out.add(td);
		}
		List<Transitiondef> targets = getTargetTransitiondefs();
		for(Transitiondef td : targets) {
			if(!ids.contains(td.getId())) {
				ids.add(td.getId());
				out.add(td);
			}
		}
		return out;
	}
	
}
