package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.SessionDB;
import se.prv.errends.dbc.TransitionCommentDB;
import se.prv.errends.dbc.TransitionDB;
import se.prv.errends.dbc.TransitiondefDB;

public class Transition {

	/*
	create table transition (
	  id bigserial primary key,
	  transitiondefId bigint not null,
	  sessionId bigint not null,
	  predecessorId bigint,
	  foreign key ( transitiondefId ) references transitiondef (id),
	  foreign key ( sessionId ) references session (id),
	  foreign key ( predecessorId ) references transition (id)
	);
	 */

	private Long id;
	private Long transitiondefId;
	private Long sessionId;
	private Long predecessorId;
	
	public String toString() {
		return "Transition: id="+id+", transitiondefId="+transitiondefId+", sessionId="+sessionId+", predecessorId="+predecessorId;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTransitiondefId() {
		return transitiondefId;
	}
	public void setTransitiondefId(Long transitiondefId) {
		this.transitiondefId = transitiondefId;
		transitiondef = null;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
		session = null;
	}
	public Long getPredecessorId() {
		return predecessorId;
	}
	public void setPredecessorId(Long predecessorId) {
		this.predecessorId = predecessorId;
		predecessor = null;
	}

	private Transitiondef transitiondef;
	
	public Transitiondef getTransitiondef() {
		if(transitiondef != null) {
			return transitiondef;
		}
		transitiondef = TransitiondefDB.getTransitiondefFromId(transitiondefId);
		return transitiondef;
	}

	private Session session;
	
	public Session getSession() {
		if(session != null) {
			return session;
		}
		if(sessionId == null) {
			return null;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}
	
	public void setSession(Session session) {
		this.session = session;
		this.sessionId = session.getId();
	}

	private Transition predecessor;
	
	public Transition getPrececessor() {
		if(predecessor != null) {
			return predecessor;
		}
		if(predecessorId == null) {
			return null;
		}
		predecessor = TransitionDB.getTransitionFromId(predecessorId);
		return predecessor;
	}
	
	public void setPrececessor(Transition predecessor) {
		this.predecessor = predecessor;
		if(predecessor != null) {
			this.predecessorId = predecessor.getId();
		}
	}
	
	public void save() {
		if(id == null) {
			TransitionDB.createTransition(this);
		}
		else {
			TransitionDB.updateTransition(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		TransitionDB.deleteTransition(this);
		id = null;
	}
	
	public void deleteRecursively() {
		List<TransitionComment> transcoms = TransitionCommentDB.getAllTransitionCommentsOfTransition(id);
		for(TransitionComment transcom : transcoms) {
			transcom.delete();
		}
		delete();
	}

}
