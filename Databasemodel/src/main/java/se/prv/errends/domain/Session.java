package se.prv.errends.domain;

import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.ErrendCommentDB;
import se.prv.errends.dbc.SessionDB;

public class Session {

/*
create table session (
  id bigserial primary key,
  start timestamp not null,
  finish timestamp not null,
  actorId bigint not null,
  foreign key ( actorId ) references actor (id)
);
 */

	private Long id;
	private Date start;
	private Date finish;
	private Long actorId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public Long getActorId() {
		return actorId;
	}

	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}
	
	private Actor actor = null;
	
	public Actor getActor() {
		if(actor != null) {
			return actor;
		}
		if(actorId == null) {
			return null;
		}
		actor = ActorDB.getActorFromId(actorId);
		return actor;
	}
	
	public void setActor(Actor a) {
		actorId = a.getId();
		actor = a;
	}
	
	public void save() {
		if(id == null) {
			SessionDB.createSession(this);
		}
		else {
			SessionDB.updateSession(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		SessionDB.deleteSession(this);
		id = null;
	}
	
	public void deleteRecursively() {
		List<ErrendComment> ecoms = ErrendCommentDB.getAllErrendCommentsOfSession(id);
		for(ErrendComment ecom : ecoms) {
			ecom.delete();
		}
		delete();
	}

}
