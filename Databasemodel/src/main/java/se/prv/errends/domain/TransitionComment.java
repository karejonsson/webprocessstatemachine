package se.prv.errends.domain;

import se.prv.errends.dbc.TransitionCommentDB;

public class TransitionComment {

	private Long id;
	private String title;
	private String comment;
	private Long sessionId;
	private Long transitionId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getTransitionId() {
		return transitionId;
	}
	public void setTransitionId(Long transitionId) {
		this.transitionId = transitionId;
	}

	public void save() {
		if(id == null) {
			TransitionCommentDB.createTransitionComment(this);
		}
		else {
			TransitionCommentDB.updateTransitionComment(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		TransitionCommentDB.deleteTransitionComment(this);
		id = null;
	}

}
