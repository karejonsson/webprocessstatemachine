package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.ReportTemplateDB;
import se.prv.errends.dbc.ReportTemplateReuseDB;

public class ReportTemplate {

    /*
create table reporttemplate (
  id bigserial primary key,
  filename varchar(180) not null,
  reporttitle varchar(250) default null,
  description varchar(1000) default null,
  jasperreportdata bytea,
  parameterdata bytea,
  adminuserId bigint not null,
  unique(reportname, description),
  foreign key ( adminuserId ) references adminuser (id)
);
     */

    private Long id;
    private String filename;
    private String reporttitle;
    private String description;
    private Long adminuserId;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getReporttitle() {
		return reporttitle;
	}
	public void setReporttitle(String reporttitle) {
		this.reporttitle = reporttitle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getAdminuserId() {
		return adminuserId;
	}
	public void setAdminuserId(Long adminuserId) {
		this.adminuserId = adminuserId;
	}
	
	private Adminuser adminuser = null;
	
	public Adminuser getAdminuser() {
		if(adminuser != null) {
			return adminuser;
		}
		if(adminuserId == null) {
			return null;
		}
		adminuser = AdminuserDB.getAdminuserFromId(adminuserId);
		return adminuser;
	}
	
	public void setAdminuser(Adminuser adminuser) {
		adminuserId = adminuser.getId();
		this.adminuser = adminuser;
	}
	
	public List<ReportTemplateReuse> getReferringReportTemplateReuses() {
		List<ReportTemplateReuse> out = ReportTemplateReuseDB.getAllReportTemplateReusesOfReportTemplate(id);
		return out;
	}
	
	public void delete() {
		ReportTemplateDB.deleteReportTemplate(this);
		id = null;
	}
	
	public void deleteRecursively() {
		List<ReportTemplateReuse> temps = ReportTemplateReuseDB.getAllReportTemplateReusesOfReportTemplate(id);
		if(temps != null) {
			for(ReportTemplateReuse temp : temps) {
				temp.deleteRecursively();
			}
		}
		delete();
	}
	
	public void save() {
		if(id == null) {
			ReportTemplateDB.createReportTemplate(this);
		}
		else {
			ReportTemplateDB.updateReportTemplate(this);
		}
	}
	
    public byte[] getJasperReportData() {
    	return ReportTemplateDB.getJasperReportData(id);
    }

    public void setJasperReportData(byte[] data) {
    	ReportTemplateDB.setJasperReportData(id, data);
    }
    
    public byte[] getParameterData() {
    	return ReportTemplateDB.getParameterData(id);
    }

    public void setParameterData(byte[] data) {
    	ReportTemplateDB.setParameterData(id, data);
    }
	public boolean isPersisted() {
		return id != null;
	}
    
}
