package se.prv.errends.domain;

import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.dbc.OrganizationUserDB;
import se.prv.errends.dbc.PersonDB;

public class OrganizationUser {
	
	/*
create table organizationuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(60) not null,
  active bool default true,
  superprivilegies bool default false,
  personId bigint not null,
  organizationId bigint not null,
  unique(organizationId),
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);
	 */
	
	private Long id;
	private String username;
	private String password;
	private Boolean active = true;
	private Boolean superprivilegies = false;
	private Long organizationId;
	private Long personId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Boolean getSuperprivilegies() {
		return superprivilegies;
	}
	public void setSuperprivilegies(Boolean superprivilegies) {
		this.superprivilegies = superprivilegies;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	
    public boolean isPersisted() {
        return id != null;
    }

    public void delete() {
        OrganizationUserDB.deleteOrganizationuser(this);
        id = null;
    }

	public void save() {
		if(id == null) {
			OrganizationUserDB.createOrganizationuser(this);
		}
		else {
			OrganizationUserDB.updateOrgaccountmanager(this);
		}
	}
	
	private Person person = null;
	
	public Person getPerson() {
		if(person != null) {
			return person;
		}
		person = PersonDB.getPersonFromId(personId);
		return person;
	}
	
	public void setPerson(Person p) {
		personId = p.getId();
		person = p;
	}

	private Organisation organization = null;
	
	public Organisation getOrganization() {
		if(organization != null) {
			return organization;
		}
		if(organizationId == null) {
			return null;
		}
		organization = OrganizationDB.getOrganizationFromId(organizationId);
		return organization;
	}
	
	public void setOrganization(Organisation organization) {
		organizationId = organization.getId();
		this.organization = organization;
	}

}
