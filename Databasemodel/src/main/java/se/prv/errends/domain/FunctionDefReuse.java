package se.prv.errends.domain;

import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.FunctionDefReuseDB;
import se.prv.errends.dbc.ProcessdefDB;

public class FunctionDefReuse {

	/*
create table functiondefreuse (
  id bigserial primary key,
  processdefId bigint not null,
  functiondefId bigint not null,
  foreign key ( processdefId ) references processdef (id),
  foreign key ( functiondefId ) references functiondef (id)
);
	 */
	
    private Long id;
    private Long processdefId;
    private Long functiondefId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProcessdefId() {
		return processdefId;
	}
	public void setProcessdefId(Long processdefId) {
		this.processdefId = processdefId;
	}
	public Long getFunctionDefId() {
		return functiondefId;
	}
	public void setFunctionDefId(Long functiondefId) {
		this.functiondefId = functiondefId;
	}
	
	private Processdef processdef = null;
	
	public Processdef getProcessdef() {
		if(processdef != null) {
			return processdef;
		}
		if(processdefId == null) {
			return null;
		}
		processdef = ProcessdefDB.getProcessdefFromId(processdefId);
		return processdef;
	}
	
	public void setProcessdef(Processdef processdef) {
		processdefId = processdef.getId();
		this.processdef = processdef;
	}

	private FunctionDef functiondef = null;
	
	public FunctionDef getFunctionDef() {
		if(functiondef != null) {
			return functiondef;
		}
		if(functiondefId == null) {
			return null;
		}
		functiondef = FunctionDefDB.getFunctionDefFromId(functiondefId);
		return functiondef;
	}
	
	public void setFunctionDef(FunctionDef functiondef) {
		functiondefId = functiondef.getId();
		this.functiondef = functiondef;
	}
	
	public void delete() {
		FunctionDefReuseDB.deleteFunctionDefReuse(this);
		id = null;
	}
	
	public boolean isPersisted() {
		return id != null;
	}
	
	public void save() {
		if(id == null) {
			FunctionDefReuseDB.createFunctionDefReuse(this);
		}
		else {
			FunctionDefReuseDB.updateFunctionDefReuse(this);
		}
	}

}
