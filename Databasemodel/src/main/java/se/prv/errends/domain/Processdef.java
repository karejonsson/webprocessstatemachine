package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.FunctionDefReuseDB;
import se.prv.errends.dbc.ManagedErrendDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbc.ReportTemplateReuseDB;
import se.prv.errends.dbc.RoledefDB;
import se.prv.errends.dbc.StatedefDB;

public class Processdef {

	/*
create table processdef (
  id bigserial primary key,
  processname varchar(50) not null,
  description varchar(250) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);
	 */

	private Long id;
	private String processname;
	private String description;
	private Integer orderby;
	private Long processfileId;
	
	public String toString() {
		return "{ Processdef: id "+id+", processname "+processname+", description "+description+", orderby "+orderby+", processfileId "+processfileId+" }";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProcessname() {
		return processname;
	}
	public void setProcessname(String processname) {
		this.processname = processname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getProcessfileId() {
		return processfileId;
	}
	public void setProcessfileId(Long processfileId) {
		this.processfileId = processfileId;
		processfile = null;
	}
	
	private Processfile processfile;
	
	public Processfile getProcessfile() {
		if(processfile != null) {
			return processfile;
		}
		processfile = ProcessfileDB.getProcessfileFromId(processfileId);
		return processfile;
	}
	
	public FunctionDef getFunctionDef(String name) {
		return FunctionDefDB.getReusedFunctionDefOfProcessDefByName(name, id);
	}

	public void setProcessfile(Processfile processfile) {
		this.processfile = processfile;
		processfileId = processfile.getId();
	}
	
	public void save() {
		if(id == null) {
			ProcessdefDB.createProcessdef(this);
		}
		else {
			ProcessdefDB.updateProcessdef(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		ProcessdefDB.deleteProcessdef(this);
		id = null;
	}
	
	public void deleteRecursive() {
		List<Statedef> statedefs = getReferringStatedefs();
		for(Statedef sd : statedefs) {
			sd.deleteRecursive();
		}
		List<Roledef> roledefs = getRoledefs();
		for(Roledef rd : roledefs) {
			rd.deleteRecursive();
		}
		List<ReportTemplateReuse> reporttemplatereuses = getReferringReportTemplateReuses();
		for(ReportTemplateReuse reporttemplatereuse : reporttemplatereuses) {
			reporttemplatereuse.deleteRecursively();
		}
		List<FunctionDefReuse> functiondefreuses = getFunctionDefReuses();
		for(FunctionDefReuse functiondefreuse : functiondefreuses) {
			functiondefreuse.delete();
		}
		delete();
	}
	
	public void deleteRecursiveWithErrends() {
		List<ManagedErrend> mes = ManagedErrendDB.getAllManagedErrendsOfProcessdef(id);
		for(ManagedErrend me : mes) {
			Errend errend = me.getErrend();
			me.deleteRecursively();
			errend.deleteRecursively();
		}
		deleteRecursive();
	}
	
	public List<Roledef> getRoledefs() {
		return RoledefDB.getAllRoledefsOfProcessdef(id);
	}

	public List<Statedef> getReferringStatedefs() {
		return StatedefDB.getAllStatedefsOfProcessdef(id);
	}

	public List<ReportTemplateReuse> getReferringReportTemplateReuses() {
		return ReportTemplateReuseDB.getAllReportTemplateReusesOfProcessdef(id);
	}

	public List<FunctionDef> getFunctionDefs() {
		return FunctionDefDB.getReusedFunctionDefsOfProcessDef(id);
	}
	
	public List<FunctionDefReuse> getFunctionDefReuses() {
		return FunctionDefReuseDB.getAllFunctionDefReusesOfProcessdef(id);
	}
	
	public List<Statedef> getAllStatedefs() {
		return StatedefDB.getAllStatedefsOfProcessdef(id);
	}
	
}
