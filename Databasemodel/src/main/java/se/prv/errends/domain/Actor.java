package se.prv.errends.domain;

import java.io.Serializable;
import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.MessageDB;
import se.prv.errends.dbc.OrganisationGroupMemberDB;
import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.ReportDB;
import se.prv.errends.dbc.RoledefDB;
//import se.prv.errends.dbc.AuthorityDB;
import se.prv.errends.dbc.SessionDB;

public class Actor implements Serializable {

/*
create table actor (
  id bigserial primary key,
  orderby int default null,
  roledefId bigint default null,
  agentId bigint not null,
  errendId bigint default null,
  unique(roledefId, agentId, errendId),
  foreign key ( agentId ) references agent (id),
  foreign key ( roledefId ) references roledef (id),
  foreign key ( errendId ) references errend (id)
);
 */
	
	private Long id;
	private Integer orderby = null;
	private Long roledefId;
	private Long agentId;
	private Long errendId;
	
	public String toString() {
		return "Actor{id "+id+", orderby "+orderby+", roledefId "+roledefId+", agentId "+agentId+", errendId "+errendId+"}";
	}
	
	public boolean isSameAs(Actor actor) {
		return isSameAs(actor.roledefId, actor.agentId, actor.errendId);
	}
	
	public boolean isSameAs(Long roledefId, Long agentId, Long errendId) {
		if(roledefId == null) {
			return false;
		}
		if(this.roledefId == null) {
			return false;
		}
		if(!this.roledefId.equals(roledefId)) {
			return false;
		}
		if(agentId == null) {
			return false;
		}
		if(!this.agentId.equals(agentId)) {
			return false;
		}
		if(errendId == null) {
			return false;
		}
		if(!this.errendId.equals(errendId)) {
			return false;
		}
		return true;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public Long getRoledefId() {
		return roledefId;
	}
	public void setRoledefId(Long roledefId) {
		this.roledefId = roledefId;
		roledef= null;
	}
	public Long getAgentId() {
		return agentId;
	}
	public void setAgentId(Long agentId) {
		this.agentId = agentId;
		agent = null;
	}
	public Long getErrendId() {
		return errendId;
	}
	public void setErrendId(Long errendId) {
		this.errendId = errendId;
		errend = null;
	}
	
	private Agent agent = null;
	
	public Agent getAgent() {
		if(agent != null) {
			return agent;
		}
		agent = AgentDB.getAgentFromId(agentId);
		return agent;
	}
	
	public void setAgent(Agent agent) {
		agentId = agent.getId();
		this.agent = agent;
	}

	private Errend errend = null;
	
	public Errend getErrend() {
		if(errend != null) {
			return errend;
		}
		if(errendId == null) {
			return null;
		}
		errend = ErrendDB.getErrendFromId(errendId);
		return errend;
	}
	
	public void setErrend(Errend e) {
		errendId = e.getId();
		errend = e;
	}

	private Roledef roledef = null;
	
	public Roledef getRoledef() {
		if(roledef != null) {
			return roledef;
		}
		if(roledefId == null) {
			return null;
		}
		roledef = RoledefDB.getRoledefFromId(roledefId);
		return roledef;
	}
	
	public void setRoledef(Roledef e) {
		roledefId = e.getId();
		roledef = e;
	}
	
	public void save() {
		if(id == null) {
			ActorDB.createActor(this);
		}
		else {
			ActorDB.updateActor(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		ActorDB.deleteActor(this);
		id = null;
	}
	
	public void deleteRecursively() {
		List<Message> messs = MessageDB.getAllMessagesOfActor(id);
		if(messs != null) {
			for(Message mess : messs) {
				mess.delete();
			}
		}
		List<Session> sesss = SessionDB.getAllSessionsOfActor(id);
		if(sesss != null) {
			for(Session sess : sesss) {
				sess.deleteRecursively();
			}
		}
		List<Report> reps = ReportDB.getAllReportsOfActor(id);
		if(reps != null) {
			for(Report rep : reps) {
				try { 
					rep.deleteRecursively();
				}
				catch(Exception e) {}
			}
		}
		delete();
	}
	
}
