package se.prv.errends.domain;

import se.prv.errends.dbc.ErrendCommentDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.SessionDB;

public class ErrendComment {

/*
create table errendcomment (
  id bigserial primary key,
  title varchar(100) default null,
  comment varchar(30000) default null,
  sessionId bigint not null,
  errendId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( errendId ) references errend (id)
);
 */

	private Long id;
	private String title;
	private String comment;
	private Long sessionId;
	private Long errendId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getErrendId() {
		return errendId;
	}
	public void setErrendId(Long errendId) {
		this.errendId = errendId;
	}
	
	public void save() {
		if(id == null) {
			ErrendCommentDB.createErrendComment(this);
		}
		else {
			ErrendCommentDB.updateErrendComment(this);
		}
	}

	public boolean isPersisted() {
		return id != null;
	}

	public void delete() {
		ErrendCommentDB.deleteErrendComment(this);
		id = null;
	}
	
	private Errend errend;
	
	public Errend getErrend() {
		if(errend != null) {
			return errend;
		}
		errend = ErrendDB.getErrendFromId(sessionId);
		return errend;
	}
	
	public void setErrend(Errend errend) {
		this.errend = errend;
		this.errendId = errend.getId();
	}

	private Session session;
	
	public Session getSession() {
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}
	
	public void setSession(Session session) {
		this.session = session;
		this.sessionId = session.getId();
	}

}
