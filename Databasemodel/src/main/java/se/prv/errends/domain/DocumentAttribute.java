package se.prv.errends.domain;

import se.prv.errends.dbc.DocumentAttributeDB;
import se.prv.errends.dbc.DocumentDB;
import se.prv.errends.dbc.DocumentEventDB;
import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.DossierDocattributeDefDB;
import se.prv.errends.dbc.SessionDB;

public class DocumentAttribute {

	/*
create table documentattribute (
  id bigserial primary key,
  sessionId bigint not null,
  dossierdocattributedefId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdocattributedefId ) references dossierdocattributedef (id),
  foreign key ( documentId ) references document (id)
);
	 */
	
	private Long id;
	private Long sessionId;
	private Long dossierdocattributedefId;
	private Long documentId;
	
	public String toString() {
		return "DocumentAttribute{id="+id+
				", sessionId="+sessionId+
				", dossierdocattributedefId="+dossierdocattributedefId+
				", documentId="+documentId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
		session = null;
	}
	public Long getDossierdocattributedefId() {
		return dossierdocattributedefId;
	}
	public void setDossierdocattributedefId(Long dossierdocattributedefId) {
		this.dossierdocattributedefId = dossierdocattributedefId;
		dossierdocattributedef = null;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	private Session session = null;
	
	public Session getSession() {
		if(sessionId == null) {
			return null;
		}
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}

	private Document document = null;
	
	public Document getDocument() {
		if(documentId == null) {
			return null;
		}
		if(document != null) {
			return document;
		}
		document = DocumentDB.getDocumentFromId(documentId);
		return document;
	}

	private DossierDocattributeDef dossierdocattributedef = null;
	
	public DossierDocattributeDef getDossierDocattributeDef() {
		if(dossierdocattributedefId == null) {
			return null;
		}
		if(dossierdocattributedef != null) {
			return dossierdocattributedef;
		}
		dossierdocattributedef = DossierDocattributeDefDB.getDossierDocattributeDefFromId(dossierdocattributedefId);
		return dossierdocattributedef;
	}

	public boolean delete() {
		if(DocumentAttributeDB.deleteDocumentAttribute(this)) {
			id = null;
			return true;
		}
		return false;
	}

	public boolean save() {
		if(id == null) {
			return DocumentAttributeDB.createDocumentAttribute(this);
		}
		else {
			return DocumentAttributeDB.updateDocumentAttribute(this);
		}
	}

}
