package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.DocumentDB;
import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.dbc.DossierEventDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.SessionDB;

public class Dossier {

	/*
create table dossier (
  id bigserial primary key,
  name varchar(50) not null,
  description varchar(400) default null,
  sessionId bigint not null,
  dossierdefId bigint not null,
  errendId bigint default null,
  unique(errendId),
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdefId ) references dossierdef (id),
  foreign key ( errendId ) references errend (id)
);
	 */
	
	private Long id = null;
	public static final int NAME_FIELD_LENGTH = 50;
	private String name = null;
	public static final int DESCRIPTION_FIELD_LENGTH = 400;
	private String description = null;
	private Long sessionId = null;
	private Long dossierdefId = null;
	private Long errendId = null;
	
	public String toString() {
		return "Dossier{id="+id+", name="+name+", description="+description+", sessionId="+sessionId+", dossierdefId="+dossierdefId+", errendId="+errendId+"}";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name != null && name.length() > NAME_FIELD_LENGTH) {
			name = name.substring(0, NAME_FIELD_LENGTH);
		}
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if(description != null && description.length() > DESCRIPTION_FIELD_LENGTH) {
			description = description.substring(0, DESCRIPTION_FIELD_LENGTH);
		}
		this.description = description;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getDossierDefId() {
		return dossierdefId;
	}
	public void setDossierDefId(Long dossierdefId) {
		this.dossierdefId = dossierdefId;
		dossierDef = null;
	}
	public Long getErrendId() {
		return errendId;
	}
	public void setErrendId(Long errendId) {
		this.errendId = errendId;
		errend = null;
	}
	
	public boolean save() {
		if(id == null) {
			return DossierDB.createDossier(this);
		}
		else {
			return DossierDB.updateDossier(this);
		}
	}

	public boolean delete() {
		if(DossierDB.deleteDossier(this)) {
			id = null;
			return true;
		}
		return false;
	}
	
	public boolean deleteRecursive() {
		List<Document> docs = DocumentDB.getAllDocumentsOfDossier(id);
		for(Document doc : docs) {
			doc.deleteRecursive();
		}
		List<DossierEvent> events = DossierEventDB.getAllDossierEventsOfDossier(id);
		for(DossierEvent event : events) {
			event.delete();
		}
		return delete();
	}
	
	private Errend errend = null;
	
	public Errend getErrend() {
		if(errendId == null) {
			return null;
		}
		if(errend != null) {
			return errend;
		}
		errend = ErrendDB.getErrendFromId(errendId);
		return errend;
	}
	
	private DossierDef dossierDef = null;
	
	public DossierDef getDossierDef() {
		if(dossierdefId == null) {
			return null;
		}
		if(dossierDef != null) {
			return dossierDef;
		}
		dossierDef = DossierDefDB.getDossierDefFromId(dossierdefId);
		return dossierDef;
	}
	
	private Session session = null;
	
	public Session getSession() {
		if(sessionId == null) {
			return null;
		}
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}
	
	public void setErrend(Errend errend) {
		errendId = errend.getId();
		this.errend = errend;
	}
	
	public List<Document> getReferringDocuments() {
		return DocumentDB.getAllDocumentsOfDossier(id);
	}
	
	public List<DossierEvent> getReferringDossierEvents() {
		return DossierEventDB.getAllDossierEventsOfDossier(id);
	}
	
}
