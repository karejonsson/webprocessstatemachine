package se.prv.errends.domain;

import java.util.List;

import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ReportDB;
import se.prv.errends.dbc.ReportTemplateDB;
import se.prv.errends.dbc.ReportTemplateReuseDB;

public class ReportTemplateReuse {

	/*
create table reporttemplatereuse (
  id bigserial primary key,
  processdefId bigint not null,
  reporttemplateId bigint not null,
  foreign key ( processdefId ) references processdef (id),
  foreign key ( reporttemplateId ) references reporttemplate (id)
);
	 */
	
    private Long id;
    private Long processdefId;
    private Long reporttemplateId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProcessdefId() {
		return processdefId;
	}
	public void setProcessdefId(Long processdefId) {
		this.processdefId = processdefId;
	}
	public Long getReporttemplateId() {
		return reporttemplateId;
	}
	public void setReporttemplateId(Long reporttemplateId) {
		this.reporttemplateId = reporttemplateId;
	}
	
	private Processdef processdef = null;
	
	public Processdef getProcessdef() {
		if(processdef != null) {
			return processdef;
		}
		if(processdefId == null) {
			return null;
		}
		processdef = ProcessdefDB.getProcessdefFromId(processdefId);
		return processdef;
	}
	
	public void setProcessdef(Processdef processdef) {
		processdefId = processdef.getId();
		this.processdef = processdef;
	}

	private ReportTemplate reporttemplate = null;
	
	public ReportTemplate getReportTemplate() {
		if(reporttemplate != null) {
			return reporttemplate;
		}
		if(reporttemplateId == null) {
			return null;
		}
		reporttemplate = ReportTemplateDB.getReportTemplateFromId(reporttemplateId);
		return reporttemplate;
	}
	
	public void setReportTemplate(ReportTemplate reporttemplate) {
		reporttemplateId = reporttemplate.getId();
		this.reporttemplate = reporttemplate;
	}
	
	public void delete() {
		ReportTemplateReuseDB.deleteReportTemplateReuse(this);
		id = null;
	}
	
	public void deleteRecursively() {
		List<Report> reps = ReportDB.getAllReportsOfReportTemplateReuse(id);
		if(reps != null) {
			for(Report rep : reps) {
				rep.deleteRecursively();
			}
		}
		delete();
	}
	public boolean isPersisted() {
		return id != null;
	}
	
	public void save() {
		if(id == null) {
			ReportTemplateReuseDB.createReportTemplateReuse(this);
		}
		else {
			ReportTemplateReuseDB.updateReportTemplateReuse(this);
		}
	}

}
