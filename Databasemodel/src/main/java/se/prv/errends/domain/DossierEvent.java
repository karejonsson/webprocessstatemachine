package se.prv.errends.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.DossierEventDB;
import se.prv.errends.dbc.SessionDB;

public class DossierEvent {
	
	/*
create table dossierevent (
  id bigserial primary key,
  description varchar(1200) not null,
  code int not null,
  systeminfo varchar(800) not null,
  timeofevent timestamp not null,
  sessionId bigint not null,
  dossierId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierId ) references dossier (id)
);
	 */
	
	private Long id;
	public static final int DESCRIPTION_FIELD_LENGTH = 1200;
	private String description;
	private int code;
	public static final int SYSTEMINFO_FIELD_LENGTH = 800;
	private String systeminfo;
	private Date timeofevent;
	private Long sessionId;
	private Long dossierId;
	
	public String toString() {
		return "DossierEvent{id="+id+
				", description="+description+", code="+code+", systeminfo="+systeminfo+
				", timeofevent="+timeofevent+", sessionId="+sessionId+", dossierId="+dossierId+"}";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if(description != null && description.length() > DESCRIPTION_FIELD_LENGTH) {
			description = description.substring(0,  DESCRIPTION_FIELD_LENGTH);
		}
		this.description = description;
	}
	public int getCode() {
		return code;
	}
	public DossierEventType getCodeEnum() {
		return DossierEventType.get(code);
	}
	public void setCode(int code) {
		this.code = code;
	}
	public void setCodeEnum(DossierEventType det) {
		code = det.code();
	}
	public String getSysteminfo() {
		return systeminfo;
	}
	public void setSysteminfo(String systeminfo) {
		if(systeminfo != null && systeminfo.length() > SYSTEMINFO_FIELD_LENGTH) {
			systeminfo = systeminfo.substring(0,  SYSTEMINFO_FIELD_LENGTH);
		}
		this.systeminfo = systeminfo;
	}
	public Date getTimeofevent() {
		return timeofevent;
	}
	public void setTimeofevent(Date timeofevent) {
		this.timeofevent = timeofevent;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public Long getDossierId() {
		return dossierId;
	}
	public void setDossierId(Long dossierId) {
		this.dossierId = dossierId;
		dossier = null;
	}
	
	public boolean save() {
		if(id == null) {
			return DossierEventDB.createDossierEvent(this);
		}
		else {
			return DossierEventDB.updateDossierEvent(this);
		}
	}
	
	public boolean delete() {
		if(DossierEventDB.deleteDossierEvent(this)) {
			id = null;
			return true;
		}
		return false;
	}

	private Session session = null;

	
	public Session getSession() {
		if(sessionId == null) {
			return null;
		}
		if(session != null) {
			return session;
		}
		session = SessionDB.getSessionFromId(sessionId);
		return session;
	}

	private Dossier dossier = null;
	
	public Dossier getDossier() {
		if(dossierId == null) {
			return null;
		}
		if(dossier != null) {
			return dossier;
		}
		dossier = DossierDB.getDossierFromId(dossierId);
		return dossier;
	}

	public void setDossier(Dossier dossier) {
		this.dossierId = dossier == null ? null : dossier.getId();;
		this.dossier = dossier;
	}

	public enum DossierEventType {
		
		CREATE_GUI("Skapat från webb-GUI", 1),
		ERRENDIZED("Flyttat till ärende", 2),
		REERRENDIZED("Flyttat från ett ärende till annat", 3),
		UNERRENDIZED("Flyttat från ärende", 4),
		METADATACHANGE("Metadata ändrades", 5);
		
		private String text;
		private int code;
		DossierEventType(String text, int code) {
			this.text = text;
			this.code = code;
		}
		public String text() { return text; }
		public int code() { return code; }
		
		private static final Map<Integer, DossierEventType> MAP = new HashMap<Integer, DossierEventType>();
		static {
			for(DossierEventType s : DossierEventType.values()) {
				MAP.put(s.code(), s);
			}
		}
		public static DossierEventType get(int code) {
			return MAP.get(code);
		}
		
	}

}

