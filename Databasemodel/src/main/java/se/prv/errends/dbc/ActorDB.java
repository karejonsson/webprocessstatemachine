package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Actor;

public class ActorDB {

	private static final Cache<Actor> cache = new Cache<Actor>(20);

	public static void clearCache() {
		cache.clear();
	}

	/*
create table actor (
  id bigserial primary key,
  orderby int default null,
  roledefId bigint default null,
  agentId bigint not null,
  errendId bigint default null,
  unique(roledefId, agentId, errendId),
  foreign key ( agentId ) references agent (id),
  foreign key ( roledefId ) references roledef (id),
  foreign key ( errendId ) references errend (id)
);
	 */

	public static Actor getActorFromId(long id) {
		Actor out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select orderby, roledefId, agentId, errendId from actor where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Actor();
				Object orderby = rs.getObject(1);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						out.setOrderby((Integer) orderby);
					}
				}
				Object roledefId = rs.getObject(2);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						out.setRoledefId((Long) roledefId);
					}
				}
				out.setAgentId(rs.getLong(3));
				Object errendId = rs.getObject(4);
				if(errendId != null) {
					if(errendId instanceof Long) {
						out.setErrendId((Long) errendId);
					}
				}
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(out != null) {
        	cache.add(out, out.getId());
        }
		return out;
	}

	public static List<Actor> getAllActorsOfRoledefId(long roledefId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, agentId, errendId from actor where roledefId = ?");
			stmt.setLong(1, roledefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				a.setAgentId(rs.getLong(3));
				Object errendId = rs.getObject(4);
				if(errendId != null) {
					if(errendId instanceof Long) {
						a.setErrendId((Long) errendId);
					}
				}
				a.setRoledefId(roledefId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Actor> getAllActorsOfRoledefIdAndErrendId(long roledefId, long errendId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, agentId from actor where roledefId = ? and errendId = ?");
			stmt.setLong(1, roledefId);
			stmt.setLong(2, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				a.setAgentId(rs.getLong(3));
				a.setErrendId(errendId);
				a.setRoledefId(roledefId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}
	
	public static List<Actor> getAllActiveActorsOfPersonId(long personId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select actor.id, actor.orderby, actor.roledefId, actor.errendId, actor.agentId from actor, agent where actor.agentId = agent.id and agent.active = true and agent.personId = ?");
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out.add(getActorSaturateOneRow(rs));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}
	
	public static List<Actor> getAllActiveActorsOfOrganizationId(long organizationId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select actor.id, actor.orderby, actor.roledefId, actor.errendId, actor.agentId from actor, agent where actor.agentId = agent.id and agent.active = true and agent.organizationId = ?");
			stmt.setLong(1, organizationId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out.add(getActorSaturateOneRow(rs));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}
	
	private static Actor getActorSaturateOneRow(ResultSet rs) throws SQLException {
		Actor a = new Actor();
		a.setId(rs.getLong(1));
        Object orderby = rs.getObject(2);
        if(orderby != null) {
        	if(orderby instanceof Integer || orderby instanceof Long) {
                a.setOrderby((Integer) orderby);
        	}
        }
		Object roledefId = rs.getObject(3);
		if(roledefId != null) {
			if(roledefId instanceof Long) {
				a.setRoledefId((Long) roledefId);
			}
		}
		Object errendId = rs.getObject(4);
		if(errendId != null) {
			if(errendId instanceof Long) {
				a.setErrendId((Long) errendId);
			}
		}
		Object agentId = rs.getObject(5);
		if(agentId != null) {
			if(agentId instanceof Long) {
				a.setAgentId((Long) agentId);
			}
		}
		return a;
	}

	public static List<Actor> getAllActorsOfAgentId(long agentId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, roledefId, errendId from actor where agentId = ?");
			stmt.setLong(1, agentId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				Object roledefId = rs.getObject(3);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						a.setRoledefId((Long) roledefId);
					}
				}
				Object errendId = rs.getObject(4);
				if(errendId != null) {
					if(errendId instanceof Long) {
						a.setErrendId((Long) errendId);
					}
				}
				a.setAgentId(agentId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Actor> getAllActorsOfErrendId(long errendId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, roledefId, agentId from actor where errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				Object roledefId = rs.getObject(3);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						a.setRoledefId((Long) roledefId);
					}
				}
				a.setAgentId(rs.getLong(4));
				a.setErrendId(errendId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Actor> getAllActorsOfErrendIdAndAgentId(long errendId, long agentId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, roledefId from actor where errendId = ? and agentId = ?");
			stmt.setLong(1, errendId);
			stmt.setLong(2, agentId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				Object roledefId = rs.getObject(3);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						a.setRoledefId((Long) roledefId);
					}
				}
				a.setAgentId(agentId);
				a.setErrendId(errendId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Actor> getAllActorsOfErrendIdAndPersonId(long errendId, long personId) {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select actor.id, actor.orderby, actor.roledefId, actor.agentId from actor, agent where actor.errendId = ? and actor.agentId = agent.id and agent.personId = ?");
			stmt.setLong(1, errendId);
			stmt.setLong(2, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				Object roledefId = rs.getObject(3);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						a.setRoledefId((Long) roledefId);
					}
				}
				a.setAgentId(rs.getLong(4));
				a.setErrendId(errendId);
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createActor(Actor a) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("insert into actor ( orderby, roledefId, agentId, errendId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            Integer orderby = a.getOrderby();
            if(orderby != null) {
                stmt.setInt(1, orderby);
            }
            else {
            	stmt.setNull(1, Types.NULL);
            }
			Long roledefId = a.getRoledefId();
			if(roledefId != null) {
				stmt.setLong(2, roledefId);
			}
			else {
				stmt.setNull(2, Types.NULL);
			}
			stmt.setLong(3, a.getAgentId());
			Long errendId = a.getErrendId();
			if(errendId != null) {
				stmt.setLong(4, errendId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				a.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(a != null) {
        	cache.add(a, a.getId());
        }
		return a != null;
	}

	public static boolean deleteActor(Actor a) {
		return deleteActor(a.getId());
	}

	public static boolean deleteActor(long actorId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from actor where id = ?");
			stmt.setLong(1, actorId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(actorId);
		return true;
	}

	public static boolean updateActor(Actor a) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("update actor set orderby = ?, roledefId = ?, agentId = ?, errendId = ? where id = ?");
            Integer orderby = a.getOrderby();
            if(orderby != null) {
                stmt.setInt(1, orderby);
            }
            else {
            	stmt.setNull(1, Types.NULL);
            }
			Long roledefId = a.getRoledefId();
			if(roledefId != null) {
				stmt.setLong(2, roledefId);
			}
			else {
				stmt.setNull(2, Types.NULL);
			}
			stmt.setLong(3, a.getAgentId());
			Long errendId = a.getErrendId();
			if(errendId != null) {
				stmt.setLong(4, errendId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}
			stmt.setLong(5, a.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(a != null) {
        	cache.add(a, a.getId());
        }
		return a != null;
	}

	public static List<Actor> getAllActors() {
		List<Actor> out = new ArrayList<Actor>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement("select id, orderby, roledefId, agentId, errendId from actor");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Actor a = new Actor();
				a.setId(rs.getLong(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        a.setOrderby((Integer) orderby);
                	}
                }
				Object roledefId = rs.getObject(3);
				if(roledefId != null) {
					if(roledefId instanceof Long) {
						a.setRoledefId((Long) roledefId);
					}
				}
				a.setAgentId(rs.getLong(4));
				a.setErrendId(rs.getLong(5));
				Object errendId = rs.getObject(5);
				if(errendId != null) {
					if(errendId instanceof Long) {
						a.setErrendId((Long) errendId);
					}
				}
				cache.add(a, a.getId());
				out.add(a);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
