package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;

public class OrganizationDB {
	
    private static final Cache<Organisation> cache = new Cache<Organisation>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table organization (
  id bigserial primary key,
  orgname varchar(100) not null,
  orgnr varchar(14) not null,
  active bool default true,
  registerred timestamp not null,
  unique(orgnr)
);
	 */
    
    public static Organisation getOrganizationFromOrgNr(String orgNr) {
    	Organisation out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, orgname, active, registerred from organization where orgnr = ?");
            stmt.setString(1, orgNr);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Organisation();
                out.setId(rs.getLong(1));
                out.setOrgname(rs.getString(2));
                out.setActive(rs.getBoolean(3));
                out.setRegisterred(rs.getTimestamp(4));
                out.setOrgnr(orgNr);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

    public static List<Organisation> searchActive(String whatever) {
        List<Organisation> out = new ArrayList<Organisation>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, orgname, active, registerred, orgnr from organization where active = true and orgname LIKE ? or orgnr LIKE ?");
            stmt.setString(1, whatever);
            stmt.setString(2, whatever);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Organisation org = new Organisation();
                org.setId(rs.getLong(1));
                org.setOrgname(rs.getString(2));
                org.setActive(rs.getBoolean(3));
                org.setRegisterred(rs.getTimestamp(4));
                org.setOrgnr(rs.getString(5));
                out.add(org);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

    public static Organisation getOrganizationFromId(long id) {
    	Organisation out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select orgname, orgnr, active, registerred from organization where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Organisation();
                out.setOrgname(rs.getString(1));
                out.setOrgnr(rs.getString(2));
                out.setActive(rs.getBoolean(3));
                out.setRegisterred(rs.getTimestamp(4));
                 out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static boolean createOrganization(Organisation oa) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("insert into organization ( orgname, orgnr, active, registerred ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, oa.getOrgname());
            stmt.setString(2, oa.getOrgnr());
            stmt.setBoolean(3, oa.getActive());           
            Date registerred = oa.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                oa.setRegisterred(registerred);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(registerred.getTime()));

            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                oa.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(oa != null) {
        	cache.add(oa, oa.getId());
        }
        return oa != null;
    }

    public static boolean deleteOrganization(Organisation p) {
        return deleteOrganization(p.getId());
    }

    public static boolean deleteOrganization(long orgaccountId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from organization where id = ?");
            stmt.setLong(1, orgaccountId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(orgaccountId);
        return true;
    }

    public static boolean updateOrganization(Organisation oa) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();// orgname, orgnr, active, registerred
            stmt = conn.prepareStatement("update organization set orgname = ?, orgnr = ?, active = ?, registerred = ? where id = ?");
            stmt.setString(1, oa.getOrgname());
            stmt.setString(2, oa.getOrgnr());
            stmt.setBoolean(3, oa.getActive());           
            Date registerred = oa.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                oa.setRegisterred(registerred);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(registerred.getTime()));
            stmt.setLong(5, oa.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(oa != null) {
        	cache.add(oa, oa.getId());
        }
        return oa != null;
    }

    public static List<Organisation> getAllOrganizations() {
        List<Organisation> out = new ArrayList<Organisation>();

        JDBCConnectionPool pool = null;

        Connection conn = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, orgname, orgnr, active, registerred from organization");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Organisation oa = new Organisation();
                oa.setId(rs.getLong(1));
                oa.setOrgname(rs.getString(2));
                oa.setOrgnr(rs.getString(3));
                oa.setActive(rs.getBoolean(4));
                oa.setRegisterred(rs.getTimestamp(5));
                cache.add(oa, oa.getId());
                out.add(oa);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}


