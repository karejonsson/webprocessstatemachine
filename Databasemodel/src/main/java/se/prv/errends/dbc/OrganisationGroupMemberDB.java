package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.OrganisationGroupMember;
import se.prv.errends.domain.OrganisationRoleGroup;

public class OrganisationGroupMemberDB {
	
	/*
create table organisationgroupmember (
  id bigserial primary key,
  agentId bigint not null,
  rolegroupId bigint not null,
  unique(agentId, rolegroupId),
  foreign key ( agentId ) references agent (id),
  foreign key ( rolegroupId ) references organisationrolegroup (id)
);
	 */

	public static OrganisationGroupMember getOrganisationGroupMemberFromId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select agentId, rolegroupId from organisationgroupmember where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationGroupMember org = new OrganisationGroupMember();
				org.setAgentId(rs.getLong(1));
				org.setRolegroupId(rs.getLong(2));
				org.setId(id);
				return org;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}
	
	public static List<OrganisationGroupMember> getAllOrganisationGroupMembersOfRoleGroup(long rolegroupId) {
		List<OrganisationGroupMember> out = new ArrayList<OrganisationGroupMember>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, agentId from organisationgroupmember where rolegroupId = ?");
			stmt.setLong(1, rolegroupId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationGroupMember sd = new OrganisationGroupMember();
				sd.setId(rs.getLong(1));
				sd.setAgentId(rs.getLong(2));
				sd.setRolegroupId(rolegroupId);
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static List<OrganisationGroupMember> getAllOrganisationGroupMembersOfAgent(long agentId) {
		List<OrganisationGroupMember> out = new ArrayList<OrganisationGroupMember>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, rolegroupId from organisationgroupmember where agentId = ?");
			stmt.setLong(1, agentId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationGroupMember sd = new OrganisationGroupMember();
				sd.setId(rs.getLong(1));
				sd.setRolegroupId(rs.getLong(2));
				sd.setAgentId(agentId);
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteOrganisationGroupMember(OrganisationGroupMember de) {
		return deleteOrganisationGroupMember(de.getId());
	}

	public static boolean deleteOrganisationGroupMember(long roleGroupId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from organisationgroupmember where id = ?");
			stmt.setLong(1, roleGroupId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}
	/*
create table organisationgroupmember (
  id bigserial primary key,
  agentId bigint not null,
  rolegroupId bigint not null,
  unique(agentId, rolegroupId),
  foreign key ( agentId ) references agent (id),
  foreign key ( rolegroupId ) references organisationrolegroup (id)
);
	 */

	public static boolean createOrganisationGroupMember(OrganisationGroupMember de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into organisationgroupmember ( agentId, rolegroupId ) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, de.getAgentId());
			stmt.setLong(2, de.getRolegroupId());
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				de.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}

	public static boolean updateOrganisationGroupMember(OrganisationGroupMember de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("update organisationgroupmember set agentId = ?, rolegroupId = ? where id = ?");
			stmt.setLong(1, de.getAgentId());
			stmt.setLong(2, de.getRolegroupId());
			stmt.setLong(3, de.getId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}

}
