package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.AllowedActor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.OrganisationGroupMember;

public class AllowedActorDB {
	
	public final static String errendAttributes = "errend.id, errend.created, errend.initiated, errend.active, errend.orderby, errend.adminusername, errend.personname, errend.organizationId";
	public final static String groupmemberAttributes = "organisationgroupmember.id, organisationgroupmember.agentId, organisationgroupmember.rolegroupId";
	public final static String from = "organisationgroupmember, organisationrolegroup, errend, roledef, managederrend, agent";
	public final static String where = 
			"organisationgroupmember.rolegroupId = organisationrolegroup.id "+
			"and organisationrolegroup.roledefId = roledef.id "+
			"and roledef.processdefId = managederrend.processdefId "+
			"and managederrend.errendId = errend.id "+
			"and errend.active = true "+
			"and errend.organizationId = organisationrolegroup.organisationId "+
			"and organisationgroupmember.agentId = agent.id "+
			"and agent.active = true "+
			"and agent.personId = ?";
	
	public final static String sql = "select "+groupmemberAttributes+", "+errendAttributes+" from "+from+" where "+where;

	public static List<AllowedActor> getAllowedActorsForPerson(long personId) {
		List<AllowedActor> out = new ArrayList<AllowedActor>();
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // orderby, roledefId, agentId, errendId
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationGroupMember groupMember = new OrganisationGroupMember();
				groupMember.setId(rs.getLong(1));
				groupMember.setAgentId(rs.getLong(2));
				groupMember.setRolegroupId(rs.getLong(3));
				
				Errend errend = new Errend();
                errend.setId(rs.getLong(4));
				errend.setCreated(rs.getTimestamp(5));
				errend.setInitiated(rs.getBoolean(6));
				errend.setActive(rs.getBoolean(7));
                if(rs.getObject(8) != null) {
                	errend.setOrderby(rs.getInt(8));
                }
                errend.setAdminusername(rs.getString(9));
                errend.setPersonname(rs.getString(10));
                errend.setOrganizationId(rs.getLong(11));
				
				AllowedActor aa = new AllowedActor();
				aa.errend = errend;
				aa.groupMember = groupMember;
				out.add(aa);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}
	
	public static void main(String[] args) {
		System.out.println("SQL: "+sql);
		List<AllowedActor> aas = getAllowedActorsForPerson(9);
		for(AllowedActor aa : aas) {
			System.out.println(aa.toString());
		}
	}
	
}
