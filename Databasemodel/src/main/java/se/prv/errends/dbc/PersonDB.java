package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Person;

public class PersonDB {

    private static final Cache<Person> cache = new Cache<Person>(20);

    public static void clearCache() {
        cache.clear();
    }

/*
create table person (
  id bigserial primary key,
  firstname varchar(100) not null,
  familyname varchar(150) not null,
  swedishpnr varchar(12) not null,
  email varchar(150) not null,
  active bool default true,
  registerred timestamp not null
);
 */

    public static Person getPersonFromSwedishPnr(String swedishPnr) {
        Person out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, firstname, familyname, email, active, registerred from person where swedishpnr = ?");
            stmt.setString(1, swedishPnr);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Person();
                out.setId(rs.getLong(1));
                out.setFirstname(rs.getString(2));
                out.setFamilyname(rs.getString(3));
                out.setEmail(rs.getString(4));
                out.setActive(rs.getBoolean(5));
                out.setRegisterred(rs.getTimestamp(6));
                out.setSwedishpnr(swedishPnr);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

    public static List<Person> searchActive(String name, String famname, String pnr) {
        List<Person> out = new ArrayList<Person>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, firstname, familyname, email, active, registerred, swedishpnr from person where active = true and firstname LIKE ? and familyname LIKE ? and swedishpnr LIKE ?");
            stmt.setString(1, name);
            stmt.setString(2, famname);
            stmt.setString(3, pnr);
            rs = stmt.executeQuery();
            while(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong(1));
                p.setFirstname(rs.getString(2));
                p.setFamilyname(rs.getString(3));
                p.setEmail(rs.getString(4));
                p.setActive(rs.getBoolean(5));
                p.setRegisterred(rs.getTimestamp(6));
                p.setSwedishpnr(rs.getString(7));
                out.add(p);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

    public static List<Person> searchActive(String whatever) {
        List<Person> out = new ArrayList<Person>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, firstname, familyname, email, active, registerred, swedishpnr from person where active = true and firstname LIKE ? or familyname LIKE ? or swedishpnr LIKE ?");
            stmt.setString(1, whatever);
            stmt.setString(2, whatever);
            stmt.setString(3, whatever);
            rs = stmt.executeQuery();
            while(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong(1));
                p.setFirstname(rs.getString(2));
                p.setFamilyname(rs.getString(3));
                p.setEmail(rs.getString(4));
                p.setActive(rs.getBoolean(5));
                p.setRegisterred(rs.getTimestamp(6));
                p.setSwedishpnr(rs.getString(7));
                out.add(p);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

    public static Person getPersonFromId(long id) {
        Person out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select firstname, familyname, swedishpnr, email, active, registerred from person where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Person();
                out.setFirstname(rs.getString(1));
                out.setFamilyname(rs.getString(2));
                out.setSwedishpnr(rs.getString(3));
                out.setEmail(rs.getString(4));
                out.setActive(rs.getBoolean(5));
                out.setRegisterred(rs.getTimestamp(6));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static boolean createPerson(Person p) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("insert into person ( firstname, familyname, swedishPnr, email, active, registerred ) values ( ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, p.getFirstname());
            stmt.setString(2, p.getFamilyname());
            stmt.setString(3, p.getSwedishpnr());
            stmt.setString(4, p.getEmail());
            stmt.setBoolean(5, p.isActive());

            Date registerred = p.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                p.setRegisterred(registerred);
            }
            stmt.setTimestamp(6, new java.sql.Timestamp(registerred.getTime()));

            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                p.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(p != null) {
        	cache.add(p, p.getId());
        }
        return p != null;
    }

    public static boolean deletePerson(Person p) {
        return deletePerson(p.getId());
    }

    public static boolean deletePerson(long personId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from person where id = ?");
            stmt.setLong(1, personId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(personId);
        return true;
    }

    public static boolean updatePerson(Person p) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();// firstname, familyname, swedishPnr, email, active, registerred
            stmt = conn.prepareStatement("update person set firstname = ?, familyname = ?, swedishpnr = ?, email = ?, active = ?, registerred = ? where id = ?");
            stmt.setString(1, p.getFirstname());
            stmt.setString(2, p.getFamilyname());
            stmt.setString(3, p.getSwedishpnr());
            stmt.setString(4, p.getEmail());
            stmt.setBoolean(5, p.isActive());

            Date registerred = p.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                p.setRegisterred(registerred);
            }
            stmt.setTimestamp(6, new java.sql.Timestamp(registerred.getTime()));

            stmt.setLong(7, p.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(p != null) {
        	cache.add(p, p.getId());
        }
        return p != null;
    }

    public static List<Person> getAllActivePersons() {
        List<Person> out = new ArrayList<Person>();
        JDBCConnectionPool pool = null;

        Connection conn = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, firstname, familyname, swedishPnr, email, registerred from person where active = true");
            rs = stmt.executeQuery();
            while(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong(1));
                p.setFirstname(rs.getString(2));
                p.setFamilyname(rs.getString(3));
                p.setSwedishpnr(rs.getString(4));
                p.setEmail(rs.getString(5));
                p.setActive(true);
                p.setRegisterred(rs.getTimestamp(6));
                cache.add(p, p.getId());
                out.add(p);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

    public static List<Person> getAllPersons() {
        List<Person> out = new ArrayList<Person>();
        JDBCConnectionPool pool = null;

        Connection conn = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, firstname, familyname, swedishPnr, email, active, registerred from person");
            rs = stmt.executeQuery();
            while(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong(1));
                p.setFirstname(rs.getString(2));
                p.setFamilyname(rs.getString(3));
                p.setSwedishpnr(rs.getString(4));
                p.setEmail(rs.getString(5));
                p.setActive(rs.getBoolean(6));
                p.setRegisterred(rs.getTimestamp(7));
                cache.add(p, p.getId());
                out.add(p);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}


