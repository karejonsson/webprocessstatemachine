package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.StateExpiration;

public class StateExpirationDB {

	/*
create table stateexpiration (
  id bigserial primary key,
  managederrendId bigint not null,
  transitiondefId bigint not null,
  expirey timestamp not null,
  foreign key ( managederrendId ) references managederrend (id),
  foreign key ( transitiondefId ) references transitiondef (id)
);
	 */

    public static void clearCache() {
    }

	public static StateExpiration getStateExpirationFromId(long id) {
		StateExpiration out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("select managederrendId, expirey from stateexpiration where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new StateExpiration();
				out.setManagederrendId(rs.getLong(1));
				out.setTransitiondefId(rs.getLong(2));
				out.setExpirey(rs.getTimestamp(3));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<StateExpiration> getAllStateExpirationsOfManagedErrend(long managederrendId) {
		List<StateExpiration> out = new ArrayList<StateExpiration>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // id, managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("select id, transitiondefId, expirey from stateexpiration where managederrendId = ?");
			stmt.setLong(1, managederrendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				StateExpiration t = new StateExpiration();
				t.setId(rs.getLong(1));
				t.setTransitiondefId(rs.getLong(2));
				t.setExpirey(rs.getTimestamp(3));
				t.setManagederrendId(managederrendId);
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createStateExpiration(StateExpiration se) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // id, managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("insert into stateexpiration ( managederrendId, transitiondefId, expirey ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, se.getManagederrendId());
			stmt.setLong(2, se.getTransitiondefId());

            Date expirey = se.getExpirey();
            if(expirey == null) {
            	throw new Exception("Cannot create state expiration without date for the expirey.");
            }
            stmt.setTimestamp(3, new java.sql.Timestamp(expirey.getTime()));

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				se.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return se.getId() != null;
	}

	public static boolean deleteStateExpiration(StateExpiration me) {
		return deleteStateExpiration(me.getId());
	}

	public static boolean deleteStateExpiration(long stateexpirationId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from stateexpiration where id = ?");
			stmt.setLong(1, stateexpirationId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateStateExpiration(StateExpiration se) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // id, managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("update stateexpiration set managederrendId = ?, transitiondefId = ?, expirey = ? where id = ?");
			stmt.setLong(1, se.getManagederrendId());
			stmt.setLong(2, se.getTransitiondefId());

            Date expirey = se.getExpirey();
            if(expirey == null) {
            	throw new Exception("Cannot update state expiration without date for the expirey.");
            }
            stmt.setTimestamp(3, new java.sql.Timestamp(expirey.getTime()));

			stmt.setLong(4, se.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return se.getId() != null;
	}

	public static List<StateExpiration> getAllStateExpirationsExpired() {
		List<StateExpiration> out = new ArrayList<StateExpiration>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // id, managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("select id, expirey, managederrendId, transitiondefId from stateexpiration where expirey <= ?");
			stmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
			rs = stmt.executeQuery();
			while(rs.next()) {
				StateExpiration t = new StateExpiration();
				t.setId(rs.getLong(1));
				t.setExpirey(rs.getTimestamp(2));
				t.setManagederrendId(rs.getLong(3));
				t.setTransitiondefId(rs.getLong(4));
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<StateExpiration> getAllStateExpirations() {
		List<StateExpiration> out = new ArrayList<StateExpiration>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // id, managederrendId, transitiondefId, expirey : stateexpiration
			stmt = conn.prepareStatement("select id, expirey, managederrendId, transitiondefId from stateexpiration");
			rs = stmt.executeQuery();
			while(rs.next()) {
				StateExpiration t = new StateExpiration();
				t.setId(rs.getLong(1));
				t.setExpirey(rs.getTimestamp(2));
				t.setManagederrendId(rs.getLong(3));
				t.setTransitiondefId(rs.getLong(4));
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

}

