package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.DossierDocattributeDef;

public class DossierDocattributeDefDB {

    private static final Cache<DossierDocattributeDef> cache = new Cache<DossierDocattributeDef>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table dossierdocattributedef {
  id bigserial primary key,
  dossierdocattributename varchar(50) not null,
  dossierdefId bigint not null,
  foreign key ( dossierdefId ) references dossierdef (id)
};
	 */

    public static DossierDocattributeDef getDossierDocattributeDefFromId(long id) {
    	DossierDocattributeDef out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select dossierdocattributename, processfileId from dossierdocattributedef where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new DossierDocattributeDef();
                out.setDossierdocattributename(rs.getString(1));
                out.setDossierdefId(rs.getLong(2));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
	public static List<DossierDocattributeDef> getAllDossierDocattributeDefsOfDossierDef(long dossierdefId) {
		List<DossierDocattributeDef> out = new ArrayList<DossierDocattributeDef>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // select processname, description, orderby, processfileId from adminuser where id = ?
		    stmt = conn.prepareStatement("select id, dossierdocattributename from dossierdocattributedef where dossierdefId = ?");
		    stmt.setLong(1, dossierdefId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	DossierDocattributeDef pd = new DossierDocattributeDef();
		    	pd.setId(rs.getLong(1));
		    	pd.setDossierdocattributename(rs.getString(2));
		    	pd.setDossierdefId(dossierdefId);
		    	out.add(pd);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createDossierDocattributeDef(DossierDocattributeDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("insert into dossierdocattributedef ( dossierdocattributename, dossierdefId ) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pd.getDossierdocattributename());
			stmt.setLong(2, pd.getDossierdefId());
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                pd.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static boolean deleteDossierDocattributeDef(DossierDocattributeDef pd) {
        return deleteDossierDocattributeDef(pd.getId());
    }

    public static boolean deleteDossierDocattributeDef(long dossierDocattributeDefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from dossierdocattributedef where id = ?");
            stmt.setLong(1, dossierDocattributeDefId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(dossierDocattributeDefId);
        return true;
    }

    public static boolean updateDossierDocattributeDef(DossierDocattributeDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("update dossierdocattributedef set dossierdocattributename = ?, dossierdefId = ? where id = ?");
            stmt.setString(1, pd.getDossierdocattributename());
			stmt.setLong(2, pd.getDossierdefId());
            stmt.setLong(3, pd.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static List<DossierDocattributeDef> getAllDossierDocattributeDefs() {
        List<DossierDocattributeDef> out = new ArrayList<DossierDocattributeDef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, dossierdocattributename, dossierdefId from dossierdocattributedef");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	DossierDocattributeDef pd = new DossierDocattributeDef();
            	pd.setId(rs.getLong(1));
            	pd.setDossierdocattributename(rs.getString(2));
            	pd.setDossierdefId(rs.getLong(3));
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
