package se.prv.errends.dbc;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Processfile;
import se.prv.errends.internal.Services;

public class ProcessfileDB {

    public static void clearCache() {
    }

/*
create table processfile (
  id bigserial primary key,
  filename varchar(50) not null,
  description varchar(250) default null,
  data bytea,
  orderby int default null,
  adminuserId bigint not null,
  unique(filename, description),
  foreign key ( adminuserId ) references adminuser (id)
);
 */

	public static List<Processfile> getAllProcessfiles() {
		List<Processfile> out = new ArrayList<Processfile>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, filename, description, orderby, adminuserId from processfile");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
				Processfile pf = new Processfile();
		    	pf.setId(rs.getLong(1));
		    	pf.setFilename(rs.getString(2));
		    	pf.setDescription(rs.getString(3));
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						pf.setOrderby((Integer) orderby);
					}
				}
		    	pf.setAdminuserId(rs.getLong(5));
		    	pf.setDirty(false);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortProcessfiles(out);
		return out;
	}
	
	public static List<Processfile> getAllProcessfilesOfAdminuser(long adminuserId) {
		List<Processfile> out = new ArrayList<Processfile>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, filename, description, orderby from processfile where adminuserId = ?");
		    stmt.setLong(1, adminuserId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
				Processfile pf = new Processfile();
		    	pf.setId(rs.getLong(1));
		    	pf.setFilename(rs.getString(2));
		    	pf.setDescription(rs.getString(3));
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						pf.setOrderby((Integer) orderby);
					}
				}
		    	pf.setAdminuserId(adminuserId);
		    	pf.setDirty(false);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortProcessfiles(out);
		return out;
	}

	public static void sortProcessfiles(List<Processfile> array) {
		Collections.sort(array, new Comparator<Processfile>() {
		    public int compare(Processfile tf1, Processfile tf2) {
		    	try {
			        return tf1.getOrderby() - tf2.getOrderby();
		    	}
		    	catch(Exception e) {
		    		return 0;
		    	}
		    } }
		);
	}

	public static boolean deleteProcessfile(Processfile pf) {
	 	boolean outcome = deleteProcessfile(pf.getId());
	 	pf.setDirty(false);
	 	return outcome;
	}

	public static boolean deleteProcessfile(long processfileId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from processfile where id = ?");
		    stmt.setLong(1, processfileId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateProcessfile(Processfile pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("update processfile set filename = ?, description = ?, orderby = ?, adminuserId = ? where id = ?");
		    stmt.setString(1, pf.getFilename());
		    stmt.setString(2, pf.getDescription());
			Integer orderby = pf.getOrderby();
			if(orderby != null) {
				stmt.setInt(3, orderby);
			}
			else {
				stmt.setNull(3, Types.NULL);
			}
			stmt.setLong(4, pf.getAdminuserId());
		    stmt.setLong(5, pf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		    pf.setDirty(false);
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createProcessfile(Processfile pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createProcessfile(pf, conn);
		    if(outcome) {
			    conn.commit();
			    pf.setDirty(false);
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createProcessfile(Processfile pf, byte data[]) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!createProcessfile(pf, conn)) {
			    conn.rollback();
			    return false;
		    }
		    if(!setData(pf.getId(), data, conn)) {
			    conn.rollback();
			    return false;
		    }
		    conn.commit();
		    pf.setDirty(false);
		    return true;
		} 
		catch(Exception e) {
			e.printStackTrace();
		    if(conn != null) { 
		    	try {
		    		conn.rollback();
		    	} 
		    	catch (SQLException e1) {
		    		e1.printStackTrace();
		    	} 
		    }
		    return false;
		} 
		finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createProcessfile(Processfile pf, Connection conn) {
		PreparedStatement stmt = null;
		try { // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("insert into processfile (filename, description, orderby, adminuserId) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, pf.getFilename());
		    stmt.setString(2, pf.getDescription());
			Integer orderby = pf.getOrderby();
			if(orderby != null) {
				stmt.setInt(3, orderby);
			}
			else {
				stmt.setNull(3, Types.NULL);
			}
			stmt.setLong(4, pf.getAdminuserId());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            pf.setId(generatedKeys.getLong(1));
	            pf.setDirty(false);
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Processfile createProcessfileOffensive(String name, String description, long adminuserId) {
		Processfile pf = new Processfile();
		pf.setFilename(name);
		pf.setDescription(description);
		pf.setAdminuserId(adminuserId);
		if(createProcessfile(pf)) {
			return pf;
		};
		return null;
	}

	public static Processfile createProcessfileDefensive(String filename, String description, long adminuserId) {
		Processfile pf = getProcessfileFromNameAndDescription(filename, description);
		if(pf != null) {
			return pf;
		}
		return createProcessfileOffensive(filename, description, adminuserId);
	}
	
	public static List<Processfile> getProcessfileWithLikeString(String search, long maxHits) {
		List<Processfile> out = new ArrayList<Processfile>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select processfile.id, processfile.filename, processfile.description, processfile.orderby, processfile.adminuserId from processfile where filename LIKE ? or description LIKE ? limit ?");
		    stmt.setString(1, search);
		    stmt.setString(2, search);
		    stmt.setLong(3, maxHits);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
				Processfile pf = new Processfile();
				pf.setId(rs.getLong(1));
				pf.setFilename(rs.getString(2));
		    	pf.setDescription(rs.getString(3));
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						pf.setOrderby((Integer) orderby);
					}
				}
				pf.setAdminuserId(rs.getLong(5));
				pf.setDirty(false);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
		
	}

	public static Processfile getProcessfileFromNameAndDescription(String filename, String description) {
		Processfile pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("select id, filename, description, orderby, adminuserId from processfile where filename = ? and description = ?");
		    stmt.setString(1, filename);
		    stmt.setString(2, description);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new Processfile();
				pf.setId(rs.getLong(1));
				pf.setFilename(rs.getString(2));
				pf.setDescription(rs.getString(3));
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						pf.setOrderby((Integer) orderby);
					}
				}
				pf.setAdminuserId(rs.getLong(5));
				pf.setDirty(false);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		//System.out.println("Hittade "+out+"!");
		return pf;
	}

	public static Processfile getProcessfileFromId(long id) {
		Processfile pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("select filename, description, orderby, adminuserId from processfile where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new Processfile();
				pf.setId(id);
				pf.setFilename(rs.getString(1));
				pf.setDescription(rs.getString(2));
				Object orderby = rs.getObject(3);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						pf.setOrderby((Integer) orderby);
					}
				}
				pf.setAdminuserId(rs.getLong(4));
				pf.setDirty(false);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return pf;
	}

	public static boolean setData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update processfile set data = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from processfile where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
		
}
