package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Session;

public class SessionDB {

	private static final Cache<Session> cache = new Cache<Session>(20);

	public static void clearCache() {
		cache.clear();
	}

/*
create table session (
  id bigserial primary key,
  start timestamp not null,
  finish timestamp not null,
  actorId bigint not null,
  foreign key ( actorId ) references actor (id)
);
 */

	public static Session getSessionFromId(long id) {
        Session out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("select start, finish, actorId from session where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Session();
				out.setStart(rs.getTimestamp(1));
				out.setFinish(rs.getTimestamp(2));
				out.setActorId(rs.getLong(3));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(out != null) {
        	cache.add(out, out.getId());
        }
		return out;
	}

	public static List<Session> getAllSessionsOfActor(long actorId) {
		List<Session> out = new ArrayList<Session>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("select id, start, finish from session where actorId = ?");
			stmt.setLong(1, actorId);
			rs = stmt.executeQuery();
			while(rs.next()) {
                Session sd = new Session();
				sd.setId(rs.getLong(1));
				sd.setStart(rs.getTimestamp(2));
				sd.setFinish(rs.getTimestamp(3));
				sd.setActorId(actorId);
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Session> getAllSessionsOfPerson(long personId) {
		List<Session> out = new ArrayList<Session>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("select session.id, session.start, session.finish, session.actorId from actor, session, agent where session.actorId = actor.id and actor.agentId = agent.id and agent.personId = ?");
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
                Session sd = new Session();
				sd.setId(rs.getLong(1));
				sd.setStart(rs.getTimestamp(2));
				sd.setFinish(rs.getTimestamp(3));
				sd.setActorId(rs.getLong(4));
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Session> getAllSessionsOfErrend(long errendId) {
		List<Session> out = new ArrayList<Session>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("select session.id, session.start, session.finish, session.actorId from actor, session where session.actorId = actor.id and actor.errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
                Session sd = new Session();
				sd.setId(rs.getLong(1));
				sd.setStart(rs.getTimestamp(2));
				sd.setFinish(rs.getTimestamp(3));
				sd.setActorId(rs.getLong(4));
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	// getAllSessionsOfErrend
	public static boolean createSession(Session s) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("insert into session ( start, finish, actorId ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);

            Date started = s.getStart();
            if(started == null) {
                started = new Date();
                s.setStart(started);
            }
            stmt.setTimestamp(1, new java.sql.Timestamp(started.getTime()));

            Date finished = s.getFinish();
            stmt.setTimestamp(2, finished != null ? new java.sql.Timestamp(finished.getTime()) : null);

			stmt.setLong(3, s.getActorId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				s.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(s != null) {
        	cache.add(s, s.getId());
        }
		return s != null;
	}

	public static boolean deleteSession(Session sd) {
		return deleteSession(sd.getId());
	}

	public static boolean deleteSession(long sessionId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from session where id = ?");
			stmt.setLong(1, sessionId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(sessionId);
		return true;
	}

	/*
            Date started = s.getStart();
            if(started == null) {
                started = new Date();
                s.setStart(started);
            }
            stmt.setTimestamp(1, new java.sql.Timestamp(started.getTime()));

            Date finished = s.getFinish();
            stmt.setTimestamp(2, finished != null ? new java.sql.Timestamp(finished.getTime()) : null);
	 */
	public static boolean updateSession(Session s) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("update session set start = ?, finish = ?, actorId = ? where id = ?");

            Date started = s.getStart();
            stmt.setTimestamp(1, started != null ? new java.sql.Timestamp(started.getTime()) : null);

            Date finished = s.getFinish();
            stmt.setTimestamp(2, finished != null ? new java.sql.Timestamp(finished.getTime()) : null);

            stmt.setLong(3, s.getActorId());
            stmt.setLong(4, s.getId());

			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(s != null) {
        	cache.add(s, s.getId());
        }
		return s != null;
	}

	public static List<Session> getAllSessions() {
		List<Session> out = new ArrayList<Session>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // start, finish, actorId
			stmt = conn.prepareStatement("select id, start, finish, actorId from session");
			rs = stmt.executeQuery();
			while(rs.next()) {
                Session pd = new Session();
				pd.setId(rs.getLong(1));
				pd.setStart(rs.getTimestamp(2));
				pd.setFinish(rs.getTimestamp(3));
				pd.setActorId(rs.getLong(4));
				cache.add(pd, pd.getId());
				out.add(pd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
