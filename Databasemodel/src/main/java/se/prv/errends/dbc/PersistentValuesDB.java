package se.prv.errends.dbc;

import java.util.List;

import general.reuse.database.parameters.dbc.PersistentValueDB;
import general.reuse.database.parameters.domain.PersistentValue;
import se.prv.errends.dbcreuse.Commons;

public class PersistentValuesDB {
	
    public static List<PersistentValue> getAllPersistentValues() throws Exception {
    	return PersistentValueDB.getAllPersistentValues(Commons.createPool());
    }
	
	public static boolean setStringValue(String key, String value) throws Exception {
		return PersistentValueDB.setStringValue(Commons.createPool(), key, value);
	}

	public static boolean setLongValue(String key, long value) throws Exception {
		return PersistentValueDB.setLongValue(Commons.createPool(), key, value);
	}
	
	public static boolean setIntegerValue(String key, int value) throws Exception {
		return PersistentValueDB.setIntegerValue(Commons.createPool(), key, value);
	}
	
	public static boolean setBooleanValue(String key, boolean value) throws Exception {
		return PersistentValueDB.setBooleanValue(Commons.createPool(), key, value);
	}
	
	public static String getStringValue(String key) throws Exception {
		return PersistentValueDB.getStringValue(Commons.createPool(), key);
	}

	public static Long getLongValue(String key) throws Exception {
		return PersistentValueDB.getLongValue(Commons.createPool(), key);
	}
	
	public static Integer getIntegerValue(String key) throws Exception {
		return PersistentValueDB.getIntegerValue(Commons.createPool(), key);
	}
	
	public static Boolean getBooleanValue(String key) throws Exception {
		return PersistentValueDB.getBooleanValue(Commons.createPool(), key);
	}
	
	public static boolean removeValue(String key) throws Exception {
		return PersistentValueDB.removeValue(Commons.createPool(), key);
	}

}
