package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.OrganizationUser;

public class OrganizationUserDB {

    private static final Cache<OrganizationUser> cache = new Cache<OrganizationUser>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table organizationuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(60) not null,
  active bool default true,
  superprivilegies bool default false,
  personId bigint not null,
  organizationId bigint not null,
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);
	 */

    public static OrganizationUser getOrganizationuserFromId(long id) {
    	OrganizationUser out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select username, password, active, superprivilegies, personId, organizationId from organizationuser where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new OrganizationUser();
                out.setUsername(rs.getString(1));
                out.setPassword(rs.getString(2));
                out.setActive(rs.getBoolean(3));
                out.setSuperprivilegies(rs.getBoolean(4));
                out.setPersonId(rs.getLong(5));
                out.setSuperprivilegies(rs.getBoolean(6));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static OrganizationUser getOrganizationuserFromOrganizationId(long organizationId) {
    	OrganizationUser out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, username, password, active, superprivilegies, personId from organizationuser where organizationId = ?");
            stmt.setLong(1, organizationId);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new OrganizationUser();
                out.setId(rs.getLong(1));
                out.setUsername(rs.getString(2));
                out.setPassword(rs.getString(3));
                out.setActive(rs.getBoolean(4));
                out.setSuperprivilegies(rs.getBoolean(5));
                out.setPersonId(rs.getLong(6));
                out.setOrganizationId(organizationId);
           }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static List<OrganizationUser> getOrganizationusersFromPersonId(long personId) {
    	List<OrganizationUser> out = new ArrayList<OrganizationUser>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, username, password, active, superprivilegies, organizationId from organizationuser where personId = ?");
            stmt.setLong(1, personId);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	OrganizationUser ou = new OrganizationUser();
            	ou.setId(rs.getLong(1));
            	ou.setUsername(rs.getString(2));
                ou.setPassword(rs.getString(3));
                ou.setActive(rs.getBoolean(4));
                ou.setSuperprivilegies(rs.getBoolean(5));
                ou.setOrganizationId(rs.getLong(6));
                ou.setPersonId(personId);
                cache.add(ou, ou.getId());
                out.add(ou);
           }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

    public static OrganizationUser getOrganizationuserFromUsername(String username) {
    	OrganizationUser out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // select username, password, active, organizationId from organizationuser where id = ?
            stmt = conn.prepareStatement("select id, password, active, superprivilegies, personId, organizationId from organizationuser where username = ?");
            stmt.setString(1, username);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new OrganizationUser();
                out.setId(rs.getLong(1));
                out.setPassword(rs.getString(2));
                out.setActive(rs.getBoolean(3));
                out.setSuperprivilegies(rs.getBoolean(4));
                out.setPersonId(rs.getLong(5));
                out.setOrganizationId(rs.getLong(6));
                out.setUsername(username);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static boolean createOrganizationuser(OrganizationUser au) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // select username, password, active, organizationId from organizationuser where id = ?
            stmt = conn.prepareStatement("insert into organizationuser ( username, password, active, superprivilegies, personId, organizationId ) values ( ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, au.getUsername());
            stmt.setString(2, au.getPassword());
            stmt.setBoolean(3, au.getActive());
            stmt.setBoolean(4, au.getSuperprivilegies());
            stmt.setLong(5, au.getPersonId());
            stmt.setLong(6, au.getOrganizationId());
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                au.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(au != null) {
        	cache.add(au, au.getId());
        }
        return au != null;
    }

    public static boolean deleteOrganizationuser(OrganizationUser au) {
        return deleteOrganizationuser(au.getId());
    }

    public static boolean deleteOrganizationuser(long organizationId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from organizationuser where id = ?");
            stmt.setLong(1, organizationId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(organizationId);
        return true;
    }

    public static boolean updateOrgaccountmanager(OrganizationUser au) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // username, password, active, organizationId : organizationuser
            stmt = conn.prepareStatement("update organizationuser set username = ?, password = ?, active = ?, superprivilegies = ?, personId = ?, organizationId = ? where id = ?");
            stmt.setString(1, au.getUsername());
            stmt.setString(2, au.getPassword());
            stmt.setBoolean(3, au.getActive());
            stmt.setBoolean(4, au.getSuperprivilegies());
            stmt.setLong(5, au.getPersonId());
            stmt.setLong(6, au.getOrganizationId());
            stmt.setLong(7, au.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(au != null) {
        	cache.add(au, au.getId());
        }
        return au != null;
    }

    public static List<OrganizationUser> getAllOrganizationUsers() {
        List<OrganizationUser> out = new ArrayList<OrganizationUser>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // username, password, active, organizationId : organizationuser
            stmt = conn.prepareStatement("select id, username, password, active, superprivilegies, personId, organizationId from organizationuser");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	OrganizationUser au = new OrganizationUser();
            	au.setId(rs.getLong(1));
            	au.setUsername(rs.getString(2));
            	au.setPassword(rs.getString(3));
               	au.setActive(rs.getBoolean(4));
               	au.setSuperprivilegies(rs.getBoolean(5));
                au.setPersonId(rs.getLong(6));
                au.setOrganizationId(rs.getLong(7));
                cache.add(au, au.getId());
                out.add(au);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
