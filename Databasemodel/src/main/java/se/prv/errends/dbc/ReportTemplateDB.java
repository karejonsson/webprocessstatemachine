package se.prv.errends.dbc;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.internal.Services;

public class ReportTemplateDB {

    /*
create table reporttemplate (
  id bigserial primary key,
  filename varchar(180) not null,
  reporttitle varchar(250) default null,
  description varchar(1000) default null,
  jasperreportdata bytea,
  parameterdata bytea,
  adminuserId bigint not null,
  unique(reportname, description),
  foreign key ( adminuserId ) references adminuser (id)
);
     */

	public static List<ReportTemplate> getAllReportTemplates() {
		List<ReportTemplate> out = new ArrayList<ReportTemplate>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, filename, reporttitle, description, adminuserId from reporttemplate");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ReportTemplate pf = new ReportTemplate();
		    	pf.setId(rs.getLong(1));
		    	pf.setFilename(rs.getString(2));
		    	pf.setReporttitle(rs.getString(3));
		    	pf.setDescription(rs.getString(4));
		    	pf.setAdminuserId(rs.getLong(5));
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<ReportTemplate> getAllReportTemplatesOfAdminuser(long adminuserId) {
		List<ReportTemplate> out = new ArrayList<ReportTemplate>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, filename, reporttitle, description from reporttemplate where adminuserId = ?");
		    stmt.setLong(1, adminuserId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ReportTemplate pf = new ReportTemplate();
		    	pf.setId(rs.getLong(1));
		    	pf.setFilename(rs.getString(2));
		    	pf.setReporttitle(rs.getString(3));
		    	pf.setDescription(rs.getString(4));
		    	pf.setAdminuserId(adminuserId);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteReportTemplate(ReportTemplate pf) {
	 	boolean outcome = deleteReportTemplate(pf.getId());
	 	return outcome;
	}

	public static boolean deleteReportTemplate(long processfileId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from reporttemplate where id = ?");
		    stmt.setLong(1, processfileId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateReportTemplate(ReportTemplate pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, reportname, description, adminuserId
		    stmt = conn.prepareStatement("update reporttemplate set filename = ?, reporttitle = ?, description = ?, adminuserId = ? where id = ?");
		    stmt.setString(1, pf.getFilename());
		    stmt.setString(2, pf.getReporttitle());
		    stmt.setString(3, pf.getDescription());
			stmt.setLong(4, pf.getAdminuserId());
		    stmt.setLong(5, pf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createReportTemplate(ReportTemplate pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createReportTemplate(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createReportTemplate(ReportTemplate pf, byte jasperreportdata[], byte parameterdata[]) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!createReportTemplate(pf, conn)) {
			    conn.rollback();
			    return false;
		    }
		    if(!setJasperReportData(pf.getId(), jasperreportdata, conn)) {
			    conn.rollback();
			    return false;
		    }
		    if(!setParameterData(pf.getId(), parameterdata, conn)) {
			    conn.rollback();
			    return false;
		    }
		    conn.commit();
		    return true;
		} 
		catch(Exception e) {
			e.printStackTrace();
		    if(conn != null) { 
		    	try {
		    		conn.rollback();
		    	} 
		    	catch (SQLException e1) {
		    		e1.printStackTrace();
		    	} 
		    }
		    return false;
		} 
		finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createReportTemplate(ReportTemplate pf, Connection conn) {
		PreparedStatement stmt = null;
		try { // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("insert into reporttemplate (filename, reporttitle, description, adminuserId) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, pf.getFilename());
		    stmt.setString(2, pf.getReporttitle());
		    stmt.setString(3, pf.getDescription());
			stmt.setLong(4, pf.getAdminuserId());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            pf.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static ReportTemplate createReportTemplateOffensive(String filename, String description, long adminuserId) {
		ReportTemplate pf = new ReportTemplate();
		pf.setFilename(filename);
		pf.setDescription(description);
		pf.setAdminuserId(adminuserId);
		if(createReportTemplate(pf)) {
			return pf;
		};
		return null;
	}

	public static ReportTemplate createReportTemplateDefensive(String filename, String description, long adminuserId) {
		ReportTemplate pf = getReportTemplateFromFilenameAndDescription(filename, description);
		if(pf != null) {
			return pf;
		}
		return createReportTemplateOffensive(filename, description, adminuserId);
	}
	
	public static ReportTemplate getReportTemplateFromFilenameAndDescription(String filename, String description) {
		ReportTemplate pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, filename, description, orderby, adminuserId
		    stmt = conn.prepareStatement("select id, reporttitle, adminuserId from reporttemplate where filename = ? and description = ?");
		    stmt.setString(1, filename);
		    stmt.setString(2, description);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new ReportTemplate();
				pf.setId(rs.getLong(1));
				pf.setReporttitle(rs.getString(2));
				pf.setDescription(rs.getString(3));
				pf.setAdminuserId(rs.getLong(4));
				pf.setFilename(filename);
				pf.setDescription(description);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		//System.out.println("Hittade "+out+"!");
		return pf;
	}

	public static ReportTemplate getReportTemplateFromId(long id) {
		ReportTemplate pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, reportname, description, adminuserId
		    stmt = conn.prepareStatement("select filename, reporttitle, description, adminuserId from reporttemplate where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new ReportTemplate();
				pf.setId(id);
				pf.setFilename(rs.getString(1));
				pf.setReporttitle(rs.getString(2));
				pf.setDescription(rs.getString(3));
				pf.setAdminuserId(rs.getLong(4));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return pf;
	}

	public static boolean setJasperReportData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setJasperReportData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setJasperReportData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update reporttemplate set jasperreportdata = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getJasperReportData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select jasperreportdata from reporttemplate where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
		
	public static boolean setParameterData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setParameterData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setParameterData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update reporttemplate set parameterdata = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getParameterData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select parameterdata from reporttemplate where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
		
}

