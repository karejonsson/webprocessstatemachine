package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;

public class RoledefDB {

	private static final Cache<Roledef> cache = new Cache<Roledef>(20);

	public static void clearCache() {
		cache.clear();
	}

	/*
	create table roledef (
	  id bigserial primary key,
	  rolename varchar(50) not null,
	  orderby int default null,
	  processdefId bigint not null,
	  foreign key ( processdefId ) references processdef (id)
	);
	 */

	public static Roledef getRoledefFromId(long id) {
		Roledef out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // rolename, orderby, processdefId
			stmt = conn.prepareStatement("select rolename, orderby, processdefId from roledef where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Roledef();
				out.setRolename(rs.getString(1));
                Object orderby = rs.getObject(2);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
				out.setProcessdefId(rs.getLong(3));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(out != null) {
        	cache.add(out, out.getId());
        }
		return out;
	}

	public static List<Roledef> getAllRoledefsOfProcessdef(long processdefId) {
		List<Roledef> out = new ArrayList<Roledef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // rolename, orderby, processdefId
			stmt = conn.prepareStatement("select id, rolename, orderby from roledef where processdefId = ?");
			stmt.setLong(1, processdefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Roledef rd = new Roledef();
				rd.setId(rs.getLong(1));
				rd.setRolename(rs.getString(2));
                Object orderby = rs.getObject(3);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        rd.setOrderby((Integer) orderby);
                	}
                }
				rd.setProcessdefId(processdefId);
				out.add(rd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createRoledef(Roledef rd) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // rolename, orderby, processdefId
			stmt = conn.prepareStatement("insert into roledef ( rolename, orderby, processdefId ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, rd.getRolename());
            Integer orderby = rd.getOrderby();
            if(orderby != null) {
                stmt.setInt(2, orderby);
            }
            else {
            	stmt.setNull(2, Types.NULL);
            }
			stmt.setLong(3, rd.getProcessdefId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				rd.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(rd != null) {
        	cache.add(rd, rd.getId());
        }
		return rd != null;
	}

	public static boolean deleteRoledef(Roledef sd) {
		return deleteRoledef(sd.getId());
	}

	public static boolean deleteRoledef(long roledefId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from roledef where id = ?");
			stmt.setLong(1, roledefId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(roledefId);
		return true;
	}

	public static boolean updateRoledef(Roledef rd) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // rolename, orderby, processdefId
			stmt = conn.prepareStatement("update roledef set rolename = ?, orderby = ?, processdefId = ? where id = ?");
			stmt.setString(1, rd.getRolename());
            Integer orderby = rd.getOrderby();
            if(orderby != null) {
                stmt.setInt(2, orderby);
            }
            else {
            	stmt.setNull(2, Types.NULL);
            }
			stmt.setLong(3, rd.getProcessdefId());
			stmt.setLong(4, rd.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(rd != null) {
        	cache.add(rd, rd.getId());
        }
		return rd != null;
	}

	public static List<Roledef> getAllRoledefs() {
		List<Roledef> out = new ArrayList<Roledef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // rolename, orderby, processdefId
			stmt = conn.prepareStatement("select id, rolename, orderby, processdefId from roledef");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Roledef rd = new Roledef();
				rd.setId(rs.getLong(1));
				rd.setRolename(rs.getString(2));
                Object orderby = rs.getObject(3);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        rd.setOrderby((Integer) orderby);
                	}
                }
				rd.setProcessdefId(rs.getLong(4));
				cache.add(rd, rd.getId());
				out.add(rd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

    public static List<Roledef> searchByName(String whatever) {
        List<Roledef> out = new ArrayList<Roledef>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, rolename, orderby, processdefId from roledef where rolename LIKE ?");
            stmt.setString(1, whatever);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Roledef rd = new Roledef();
                rd.setId(rs.getLong(1));
                rd.setRolename(rs.getString(2));
                if(rs.getObject(3) != null) {
                	rd.setOrderby(rs.getInt(3));
                }
                rd.setProcessdefId(rs.getLong(4));
               out.add(rd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        //System.out.println("Hittade "+out+"!");
        return out;
    }

}
