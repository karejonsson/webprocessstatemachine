package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.DocumentAttribute;

public class DocumentAttributeDB {

/*
create table documentattribute (
  id bigserial primary key,
  sessionId bigint not null,
  dossierdocattributedefId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdocattributedefId ) references dossierdocattributedef (id),
  foreign key ( documentId ) references document (id)
);
 */

	public static DocumentAttribute getDocumentAttributeFromId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select sessionId, dossierdocattributedefId, documentId from documentattribute where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				DocumentAttribute d = new DocumentAttribute();
				d.setSessionId(rs.getLong(1));
				d.setDossierdocattributedefId(rs.getLong(2));
				d.setDocumentId(rs.getLong(3));
				d.setId(id);
				return d;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static List<DocumentAttribute> getAllDocumentAttributesOfDocument(long documentId) {
		List<DocumentAttribute> out = new ArrayList<DocumentAttribute>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, sessionId, dossierdocattributedefId from documentattribute where documentId = ?");
			stmt.setLong(1, documentId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				DocumentAttribute d = new DocumentAttribute();
				d.setId(rs.getLong(1));
				d.setSessionId(rs.getLong(2));
				d.setDossierdocattributedefId(rs.getLong(3));
				d.setDocumentId(documentId);
				out.add(d);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean createDocumentAttribute(DocumentAttribute d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into documentattribute ( sessionId, dossierdocattributedefId, documentId ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, d.getSessionId());
			stmt.setLong(2, d.getDossierdocattributedefId());
			stmt.setLong(3, d.getDocumentId());
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				d.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return d != null;
	}
	
	public static boolean deleteDocumentAttribute(DocumentAttribute de) {
		return deleteDocumentAttribute(de.getId());
	}

	public static boolean deleteDocumentAttribute(long documentAttributeId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from documentattribute where id = ?");
			stmt.setLong(1, documentAttributeId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateDocumentAttribute(DocumentAttribute d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // filename, mimetype, active, origin, description, sessionId, dossierId
			stmt = conn.prepareStatement("update documentattribute set sessionId = ?, dossierdocattributedefId = ?, documentId = ? where id = ?");
			stmt.setLong(1, d.getSessionId());
			stmt.setLong(2, d.getDossierdocattributedefId());
			stmt.setLong(3, d.getDocumentId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return d != null;
	}

}

