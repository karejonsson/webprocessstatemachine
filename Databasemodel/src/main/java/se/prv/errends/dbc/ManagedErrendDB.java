package se.prv.errends.dbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.ManagedErrend;

public class ManagedErrendDB {

	/*
	create table managederrend (
	  id bigserial primary key,
	  errendId bigint not null,
	  processdefId bigint not null,
	  currentstateId bigint not null,
	  lasttransitionId bigint not null,
	  unique(errendId),
	  foreign key ( errendId ) references errend (id),
	  foreign key ( processdefId ) references processdef (id),
	  foreign key ( currentstateId ) references statedef (id),
	  foreign key ( lasttransitionId ) references transitiondef (id)
	);
	 */

    private static final Cache<ManagedErrend> cache = new Cache<ManagedErrend>(20);

    public static void clearCache() {
        cache.clear();
    }

	public static ManagedErrend getManagedErrendFromId(long id) {
		ManagedErrend out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId
			stmt = conn.prepareStatement("select errendId, processdefId, currentstateId, lasttransitionId from managederrend where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new ManagedErrend();
				out.setErrendId(rs.getLong(1));
				out.setProcessdefId(rs.getLong(2));
				out.setCurrentstateId(rs.getLong(3));
				Object lasttransitionId = rs.getObject(4);
				if(lasttransitionId != null) {
					if(lasttransitionId instanceof Integer || lasttransitionId instanceof Long) {
						out.setLasttransitionId((Long) lasttransitionId);
					}
				}
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(out != null) {
			cache.add(out, out.getId());
		}
		return out;
	}
	
    public static ManagedErrend getManagedErrendOfErrendId(long errendId) {
    	ManagedErrend out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
            stmt = conn.prepareStatement("select id, processdefId, currentstateId, lasttransitionId from managederrend where errendId = ?");
            stmt.setLong(1, errendId);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new ManagedErrend();
                out.setId(rs.getLong(1));
                out.setProcessdefId(rs.getLong(2));
                out.setCurrentstateId(rs.getLong(3));
				Object lasttransitionId = rs.getObject(4);
				if(lasttransitionId != null) {
					if(lasttransitionId instanceof Integer || lasttransitionId instanceof Long) {
						out.setLasttransitionId((Long) lasttransitionId);
					}
				}
                out.setErrendId(errendId);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

	public static List<ManagedErrend> getAllManagedErrendsOfProcessdef(long processdefId) {
		List<ManagedErrend> out = new ArrayList<ManagedErrend>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
			stmt = conn.prepareStatement("select id, errendId, currentstateId, lasttransitionId from managederrend where processdefId = ?");
			stmt.setLong(1, processdefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ManagedErrend t = new ManagedErrend();
				t.setId(rs.getLong(1));
				t.setErrendId(rs.getLong(2));
				t.setCurrentstateId(rs.getLong(3));
				Object lasttransitionId = rs.getObject(4);
				if(lasttransitionId != null) {
					if(lasttransitionId instanceof Integer || lasttransitionId instanceof Long) {
						t.setLasttransitionId((Long) lasttransitionId);
					}
				}
				t.setProcessdefId(processdefId);
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<ManagedErrend> getAllManagedErrendsCurrentlyInState(long stateId) {
		List<ManagedErrend> out = new ArrayList<ManagedErrend>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
			stmt = conn.prepareStatement("select id, errendId, processdefId, lasttransitionId from managederrend where currentstateId = ?");
			stmt.setLong(1, stateId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ManagedErrend t = new ManagedErrend();
				t.setId(rs.getLong(1));
				t.setErrendId(rs.getLong(2));
				t.setProcessdefId(rs.getLong(3));
				Object lasttransitionId = rs.getObject(4);
				if(lasttransitionId != null) {
					if(lasttransitionId instanceof Integer || lasttransitionId instanceof Long) {
						t.setLasttransitionId((Long) lasttransitionId);
					}
				}
				t.setCurrentstateId(stateId);
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createManagedErrend(ManagedErrend me) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
			stmt = conn.prepareStatement("insert into managederrend ( errendId, processdefId, currentstateId, lasttransitionId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, me.getErrendId());
			stmt.setLong(2, me.getProcessdefId());
			stmt.setLong(3, me.getCurrentstateId());
			Long lasttransitionId = me.getLasttransitionId();
			if(lasttransitionId != null) {
				stmt.setLong(4, lasttransitionId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				me.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(me != null) {
			cache.add(me, me.getId());
		}        Long transitiondefId = TransitiondefDB.getAllTransitiondefs().get(0).getId();

		return me != null;
	}

	public static boolean deleteManagedErrend(ManagedErrend me) {
		return deleteManagedErrend(me.getId());
	}

	public static boolean deleteManagedErrend(long managederrendId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from managederrend where id = ?");
			stmt.setLong(1, managederrendId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(managederrendId);
		return true;
	}

	public static boolean updateManagedErrend(ManagedErrend me) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
			stmt = conn.prepareStatement("update managederrend set errendId = ?, processdefId = ?, currentstateId = ?, lasttransitionId = ? where id = ?");
			stmt.setLong(1, me.getErrendId());
			stmt.setLong(2, me.getProcessdefId());
			stmt.setLong(3, me.getCurrentstateId());
			Long lasttransitionId = me.getLasttransitionId();
			if(lasttransitionId != null) {
				stmt.setLong(4, lasttransitionId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}
			stmt.setLong(5, me.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(me != null) {
			cache.add(me, me.getId());
		}
		return me != null;
	}

	public static List<ManagedErrend> getAllManagedErrends() {
		List<ManagedErrend> out = new ArrayList<ManagedErrend>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // errendId, processdefId, currentstateId, lasttransitionId : managederrend
			stmt = conn.prepareStatement("select id, errendId, processdefId, currentstateId, lasttransitionId from managederrend");
			rs = stmt.executeQuery();
			while(rs.next()) {
				ManagedErrend me = new ManagedErrend();
				me.setId(rs.getLong(1));
				me.setErrendId(rs.getLong(2));
				me.setProcessdefId(rs.getLong(3));
				me.setCurrentstateId(rs.getLong(4));
				Object lasttransitionId = rs.getObject(4);
				if(lasttransitionId != null) {
					if(lasttransitionId instanceof Integer || lasttransitionId instanceof Long) {
						me.setLasttransitionId((Long) lasttransitionId);
					}
				}
				cache.add(me, me.getId());
				out.add(me);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
