package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Agent;

public class AgentDB {

    private static final Cache<Agent> cache = new Cache<Agent>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table agent (
  id bigserial primary key,
  personId bigint not null,
  organizationId bigint not null,
  active bool default true,
  registerred timestamp not null,
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);
	 */

	public static List<Agent> getAllAgents() {
		List<Agent> out = new ArrayList<Agent>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, personId, organizationId, active, registerred from agent");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Agent oa = new Agent();
		    	oa.setId(rs.getLong(1));
		    	oa.setPersonId(rs.getLong(2));
				Object orgaccountId = rs.getObject(3);
				if(orgaccountId != null) {
					if(orgaccountId instanceof Long) {
						oa.setOrganizationId((Long) orgaccountId);
					}
				}
		    	oa.setActive(rs.getBoolean(4));
		    	oa.setRegisterred(rs.getTimestamp(5));
		    	out.add(oa);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<Agent> getAllAgentsOfPerson(long personId) {
		List<Agent> out = new ArrayList<Agent>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, organizationId, active, registerred from agent where personId = ?");
		    stmt.setLong(1, personId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Agent oa = new Agent();
		    	oa.setId(rs.getLong(1));
				Object orgaccountId = rs.getObject(2);
				if(orgaccountId != null) {
					if(orgaccountId instanceof Long) {
						oa.setOrganizationId((Long) orgaccountId);
					}
				}
		    	oa.setActive(rs.getBoolean(3));
		    	oa.setRegisterred(rs.getTimestamp(4));
		    	oa.setPersonId(personId);
		    	out.add(oa);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static List<Agent> getAllAgentsOfOrganization(long organizationId) {
		List<Agent> out = new ArrayList<Agent>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, personId, active, registerred from agent where organizationId = ?");
		    stmt.setLong(1, organizationId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Agent oa = new Agent();
		    	oa.setId(rs.getLong(1));
		    	oa.setPersonId(rs.getLong(2));
		    	oa.setActive(rs.getBoolean(3));
		    	oa.setRegisterred(rs.getTimestamp(4));
		    	oa.setOrganizationId(organizationId);
		    	out.add(oa);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteAgent(Agent pf) {
	 	boolean outcome = deleteAgent(pf.getId());
	 	return outcome;
	}

	public static boolean deleteAgent(long agentId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from agent where id = ?");
		    stmt.setLong(1, agentId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateAgent(Agent oa) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // personId, active, registerred from agent where orgaccountId
		    stmt = conn.prepareStatement("update agent set personId = ?, active = ?, registerred = ?, organizationId = ? where id = ?");
		    stmt.setLong(1, oa.getPersonId());
		    stmt.setBoolean(2, oa.isActive());
		    
            Date registerred = oa.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                oa.setRegisterred(registerred);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(registerred.getTime()));
		    stmt.setLong(4, oa.getOrganizationId());
			stmt.setLong(5, oa.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createAgent(Agent pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createAgent(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createAgent(Agent oa, Connection conn) {
		PreparedStatement stmt = null;
		try { // // personId, active, registerred from agent where orgaccountId
		    stmt = conn.prepareStatement("insert into agent ( personId, active, registerred, organizationId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setLong(1, oa.getPersonId());
		    stmt.setBoolean(2, oa.isActive());
            Date registerred = oa.getRegisterred();
            if(registerred == null) {
                registerred = new Date();
                oa.setRegisterred(registerred);
            }
            stmt.setTimestamp(3, new java.sql.Timestamp(registerred.getTime()));

			Long orgaccountId = oa.getOrganizationId();
			if(orgaccountId != null) {
				stmt.setLong(4, orgaccountId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}
		    
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            oa.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Agent getAgentFromId(long id) {
		Agent oa = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?
		    stmt = conn.prepareStatement("select personId, active, registerred, organizationId from agent where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	oa = new Agent();
				oa.setId(id);
				oa.setPersonId(rs.getLong(1));
				oa.setActive(rs.getBoolean(2));
				oa.setRegisterred(rs.getTimestamp(3));
				Object orgaccountId = rs.getObject(4);
				if(orgaccountId != null) {
					if(orgaccountId instanceof Long) {
						oa.setOrganizationId((Long) orgaccountId);
					}
				}
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return oa;
	}
	
}

