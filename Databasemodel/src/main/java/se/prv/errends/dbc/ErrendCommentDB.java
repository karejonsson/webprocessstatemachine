package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.ErrendComment;

public class ErrendCommentDB {

	private static final Cache<ErrendComment> cache = new Cache<ErrendComment>(20);

	public static void clearCache() {
		cache.clear();
	}

	/*
create table errendcomment (
  id bigserial primary key,
  title varchar(100) default null,
  comment varchar(30000) default null,
  sessionId bigint not null,
  errendId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( errendId ) references errend (id)
);
	);
	 */

	public static ErrendComment getErrendCommentFromId(long id) {
		ErrendComment out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("select title, comment, sessionId, errendId from errendcomment where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new ErrendComment();
				out.setTitle(rs.getString(1));
				out.setComment(rs.getString(2));
				out.setSessionId(rs.getLong(3));
				out.setErrendId(rs.getLong(4));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(out != null) {
			cache.add(out, out.getId());
		}
		return out;
	}

	public static List<ErrendComment> getAllErrendCommentsOfErrend(long errendId) {
		List<ErrendComment> out = new ArrayList<ErrendComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("select id, title, comment, sessionId from errendcomment where errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ErrendComment ec = new ErrendComment();
				ec.setId(rs.getLong(1));
				ec.setTitle(rs.getString(2));
				ec.setComment(rs.getString(3));
				ec.setSessionId(rs.getLong(4));
				ec.setErrendId(errendId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<ErrendComment> getAllErrendCommentsOfErrendAndActor(long errendId, long actorId) {
		List<ErrendComment> out = new ArrayList<ErrendComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("select errendcomment.id, errendcomment.title, errendcomment.comment, errendcomment.sessionId from errendcomment, session, actor where errendcomment.errendId = actor.errendId and errendcomment.errendId = ? and errendcomment.sessionId = session.id and session.actorId = ? and actor.id = session.actorId");
			stmt.setLong(1, errendId);
			stmt.setLong(2, actorId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ErrendComment ec = new ErrendComment();
				ec.setId(rs.getLong(1));
				ec.setTitle(rs.getString(2));
				ec.setComment(rs.getString(3));
				ec.setSessionId(rs.getLong(4));
				ec.setErrendId(errendId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<ErrendComment> getAllErrendCommentsOfSession(long sessionId) {
		List<ErrendComment> out = new ArrayList<ErrendComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("select id, title, comment, errendId from errendcomment where sessionId = ?");
			stmt.setLong(1, sessionId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ErrendComment ec = new ErrendComment();
				ec.setId(rs.getLong(1));
				ec.setTitle(rs.getString(2));
				ec.setComment(rs.getString(3));
				ec.setErrendId(rs.getLong(4));
				ec.setSessionId(sessionId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createErrendComment(ErrendComment ec) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("insert into errendcomment ( title, comment, sessionId, errendId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, ec.getTitle());
			stmt.setString(2, ec.getComment());
			stmt.setLong(3, ec.getSessionId());
			stmt.setLong(4, ec.getErrendId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				ec.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(ec != null) {
			cache.add(ec, ec.getId());
		}
		return ec != null;
	}

	public static boolean deleteErrendComment(ErrendComment ec) {
		return deleteErrendComment(ec.getId());
	}

	public static boolean deleteErrendComment(long errendcommentId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from errendcomment where id = ?");
			stmt.setLong(1, errendcommentId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(errendcommentId);
		return true;
	}

	public static boolean updateErrendComment(ErrendComment ec) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("update errendcomment set title = ?, comment = ?, sessionId = ?, errendId = ? where id = ?");
			stmt.setString(1, ec.getTitle());
			stmt.setString(2, ec.getComment());
			stmt.setLong(3, ec.getSessionId());
			stmt.setLong(4, ec.getErrendId());
			stmt.setLong(5, ec.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(ec != null) {
			cache.add(ec, ec.getId());
		}
		return ec != null;
	}

	public static List<ErrendComment> getAllErrendComments() {
		List<ErrendComment> out = new ArrayList<ErrendComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, errendId : errendcomment
			stmt = conn.prepareStatement("select id, title, comment, sessionId, errendId from errendcomment");
			rs = stmt.executeQuery();
			while(rs.next()) {
				ErrendComment ec = new ErrendComment();
				ec.setId(rs.getLong(1));
				ec.setTitle(rs.getString(2));
				ec.setComment(rs.getString(3));
				ec.setSessionId(rs.getLong(4));
				ec.setErrendId(rs.getLong(5));
				cache.add(ec, ec.getId());
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
