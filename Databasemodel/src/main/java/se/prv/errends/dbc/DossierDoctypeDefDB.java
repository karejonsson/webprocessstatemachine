package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.DossierDoctypeDef;

public class DossierDoctypeDefDB {

    private static final Cache<DossierDoctypeDef> cache = new Cache<DossierDoctypeDef>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table dossierdoctypedef {
  id bigserial primary key,
  dossierdoctypename varchar(50) not null,
  dossierdefId bigint not null,
  foreign key ( dossierdefId ) references dossierdef (id)
};
	 */

    public static DossierDoctypeDef getDossierDoctypeDefFromId(long id) {
    	DossierDoctypeDef out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select dossierdoctypename, dossierdefId from dossierdoctypedef where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new DossierDoctypeDef();
                out.setDossierdoctypename(rs.getString(1));
                out.setDossierdefId(rs.getLong(2));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
	public static List<DossierDoctypeDef> getAllDossierDoctypeDefsOfDossierDef(long processfileId) {
		List<DossierDoctypeDef> out = new ArrayList<DossierDoctypeDef>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // select processname, description, orderby, processfileId from adminuser where id = ?
		    stmt = conn.prepareStatement("select id, dossierdoctypename from dossierdoctypedef where dossierdefId = ?");
		    stmt.setLong(1, processfileId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	DossierDoctypeDef pd = new DossierDoctypeDef();
		    	pd.setId(rs.getLong(1));
		    	pd.setDossierdoctypename(rs.getString(2));
		    	pd.setDossierdefId(processfileId);
		    	out.add(pd);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createDossierDoctypeDef(DossierDoctypeDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("insert into dossierdoctypedef ( dossierdoctypename, dossierdefId ) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pd.getDossierdoctypename());
			stmt.setLong(2, pd.getDossierdefId());
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                pd.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static boolean deleteDossierDoctypeDef(DossierDoctypeDef pd) {
        return deleteDossierDoctypeDef(pd.getId());
    }

    public static boolean deleteDossierDoctypeDef(long dossierDoctypeDefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from dossierdoctypedef where id = ?");
            stmt.setLong(1, dossierDoctypeDefId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(dossierDoctypeDefId);
        return true;
    }

    public static boolean updateDossierDoctypeDef(DossierDoctypeDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("update dossierdoctypedef set dossierdoctypename = ?, dossierdefId = ? where id = ?");
            stmt.setString(1, pd.getDossierdoctypename());
			stmt.setLong(2, pd.getDossierdefId());
            stmt.setLong(3, pd.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static List<DossierDoctypeDef> getAllDossierDoctypeDefs() {
        List<DossierDoctypeDef> out = new ArrayList<DossierDoctypeDef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, dossierdoctypename, dossierdefId from dossierdoctypedef");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	DossierDoctypeDef pd = new DossierDoctypeDef();
            	pd.setId(rs.getLong(1));
            	pd.setDossierdoctypename(rs.getString(2));
            	pd.setDossierdefId(rs.getLong(3));
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
