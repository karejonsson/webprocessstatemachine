package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.FunctionDefReuse;

public class FunctionDefReuseDB {

    /*
create table functiondefreuse (
  id bigserial primary key,
  processdefId bigint not null,
  functiondefId bigint not null,
  foreign key ( processdefId ) references processdef (id),
  foreign key ( functiondefId ) references functiondef (id)
);
     */

	public static List<FunctionDefReuse> getAllFunctionDefReuses() {
		List<FunctionDefReuse> out = new ArrayList<FunctionDefReuse>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, processdefId, functiondefId from functiondefreuse");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	FunctionDefReuse pf = new FunctionDefReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setProcessdefId(rs.getLong(2));
		    	pf.setFunctionDefId(rs.getLong(3));
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<FunctionDefReuse> getAllFunctionDefReusesOfFunctionDef(long functiondefId) {
		List<FunctionDefReuse> out = new ArrayList<FunctionDefReuse>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, processdefId from functiondefreuse where functiondefId = ?");
		    stmt.setLong(1, functiondefId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	FunctionDefReuse pf = new FunctionDefReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setProcessdefId(rs.getLong(2));
		    	pf.setFunctionDefId(functiondefId);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static List<FunctionDefReuse> getAllFunctionDefReusesOfProcessdef(long processdefId) {
		List<FunctionDefReuse> out = new ArrayList<FunctionDefReuse>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, functiondefId from functiondefreuse where processdefId = ?");
		    stmt.setLong(1, processdefId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	FunctionDefReuse pf = new FunctionDefReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setFunctionDefId(rs.getLong(2));
		    	pf.setProcessdefId(processdefId);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteFunctionDefReuse(FunctionDefReuse pf) {
	 	boolean outcome = deleteFunctionDefReuse(pf.getId());
	 	return outcome;
	}

	public static boolean deleteFunctionDefReuse(long functiondefreuseId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from functiondefreuse where id = ?");
		    stmt.setLong(1, functiondefreuseId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateFunctionDefReuse(FunctionDefReuse pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); 
		    stmt = conn.prepareStatement("update functiondefreuse set functiondefId = ?, processdefId = ? where id = ?");
		    stmt.setLong(1, pf.getFunctionDefId());
		    stmt.setLong(2, pf.getProcessdefId());
			stmt.setLong(3, pf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createFunctionDefReuse(FunctionDefReuse pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createFunctionDefReuse(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createFunctionDefReuse(FunctionDefReuse fdr, Connection conn) {
		PreparedStatement stmt = null;
		try { // update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?
		    stmt = conn.prepareStatement("insert into functiondefreuse (functiondefId, processdefId) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setLong(1, fdr.getFunctionDefId());
		    stmt.setLong(2, fdr.getProcessdefId());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            fdr.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static FunctionDefReuse getFunctionDefReuseFromId(long id) {
		FunctionDefReuse pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?
		    stmt = conn.prepareStatement("select functiondefId, processdefId from functiondefreuse where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new FunctionDefReuse();
				pf.setId(id);
				pf.setFunctionDefId(rs.getLong(1));
				pf.setProcessdefId(rs.getLong(2));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return pf;
	}
	
}

