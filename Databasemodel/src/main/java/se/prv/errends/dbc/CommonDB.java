package se.prv.errends.dbc;

public class CommonDB {

    public static void clearCache() {
        ActorDB.clearCache();
        AdminuserDB.clearCache();
        ErrendCommentDB.clearCache();
        
        ErrendDB.clearCache();
        ManagedErrendDB.clearCache();
        PersonDB.clearCache();
        ProcessdefDB.clearCache();
        ProcessfileDB.clearCache();
        
        RoledefDB.clearCache();
        SessionDB.clearCache();
        StatedefDB.clearCache();
        TransitionCommentDB.clearCache();
        TransitionDB.clearCache();
        
        TransitiondefDB.clearCache();
        MessageDB.clearCache();
    }

}
