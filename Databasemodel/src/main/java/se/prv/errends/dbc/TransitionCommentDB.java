package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.TransitionComment;

public class TransitionCommentDB {

	private static final Cache<TransitionComment> cache = new Cache<TransitionComment>(20);

	public static void clearCache() {
		cache.clear();
	}

	public static TransitionComment getTransitionCommentFromId(long id) {
		TransitionComment out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("select title, comment, sessionId, transitionId from transitioncomment where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new TransitionComment();
				out.setTitle(rs.getString(1));
				out.setComment(rs.getString(2));
				out.setSessionId(rs.getLong(3));
				out.setTransitionId(rs.getLong(4));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(out != null) {
			cache.add(out, out.getId());
		}
		return out;
	}

	public static List<TransitionComment> getAllTransitionCommentsOfTransition(long transitionId) {
		List<TransitionComment> out = new ArrayList<TransitionComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("select id, title, comment, sessionId from transitioncomment where transitionId = ?");
			stmt.setLong(1, transitionId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				TransitionComment tc = new TransitionComment();
				tc.setId(rs.getLong(1));
				tc.setTitle(rs.getString(2));
				tc.setComment(rs.getString(3));
				tc.setSessionId(rs.getLong(4));
				tc.setTransitionId(transitionId);
				out.add(tc);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<TransitionComment> getAllTransitionCommentsOfSession(long sessionId) {
		List<TransitionComment> out = new ArrayList<TransitionComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("select id, title, comment, transitionId from transitioncomment where sessionId = ?");
			stmt.setLong(1, sessionId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				TransitionComment tc = new TransitionComment();
				tc.setId(rs.getLong(1));
				tc.setTitle(rs.getString(2));
				tc.setComment(rs.getString(3));
				tc.setTransitionId(rs.getLong(4));
				tc.setSessionId(sessionId);
				out.add(tc);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createTransitionComment(TransitionComment tc) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("insert into transitioncomment ( title, comment, sessionId, transitionId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, tc.getTitle());
			stmt.setString(2, tc.getComment());
			stmt.setLong(3, tc.getSessionId());
			stmt.setLong(4, tc.getTransitionId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				tc.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(tc != null) {
			cache.add(tc, tc.getId());
		}
		return tc != null;
	}

	public static boolean deleteTransitionComment(TransitionComment tc) {
		return deleteTransitionComment(tc.getId());
	}

	public static boolean deleteTransitionComment(long transitioncommentId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from transitioncomment where id = ?");
			stmt.setLong(1, transitioncommentId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(transitioncommentId);
		return true;
	}

	public static boolean updateTransitionComment(TransitionComment tc) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("update transitioncomment set title = ?, comment = ?, sessionId = ?, transitionId = ? where id = ?");
			stmt.setString(1, tc.getTitle());
			stmt.setString(2, tc.getComment());
			stmt.setLong(3, tc.getSessionId());
			stmt.setLong(4, tc.getTransitionId());
			stmt.setLong(5, tc.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(tc != null) {
			cache.add(tc, tc.getId());
		}
		return tc != null;
	}

	public static List<TransitionComment> getAllTransitionComments() {
		List<TransitionComment> out = new ArrayList<TransitionComment>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, comment, sessionId, transitionId : transitioncomment
			stmt = conn.prepareStatement("select id, title, comment, sessionId, transitionId from transitioncomment");
			rs = stmt.executeQuery();
			while(rs.next()) {
				TransitionComment tc = new TransitionComment();
				tc.setId(rs.getLong(1));
				tc.setTitle(rs.getString(2));
				tc.setComment(rs.getString(3));
				tc.setSessionId(rs.getLong(4));
				tc.setTransitionId(rs.getLong(5));
				cache.add(tc, tc.getId());
				out.add(tc);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
