package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.OrganisationRoleGroup;

public class OrganisationRoleGroupDB {
	
	/*
create table organisationrolegroup (
  id bigserial primary key,
  organisationId bigint not null,
  roledefId bigint not null,
  name varchar(30) default null,
  unique(organisationId, roledefId, name),
  foreign key ( organisationId ) references organization (id),
  foreign key ( roledefId ) references roledef (id)
);
	 */

	public static OrganisationRoleGroup getOrganisationRoleGroupFromId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select organisationId, roledefId, name from organisationrolegroup where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationRoleGroup org = new OrganisationRoleGroup();
				org.setOrganisationId(rs.getLong(1));
				org.setRoledefId(rs.getLong(2));
				org.setName(rs.getString(3));
				org.setId(id);
				return org;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}
	
	public static List<OrganisationRoleGroup> getAllOrganisationRoleGroupsOfOrganisation(long organisationId) {
		List<OrganisationRoleGroup> out = new ArrayList<OrganisationRoleGroup>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, roledefId, name from organisationrolegroup where organisationId = ?");
			stmt.setLong(1, organisationId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				OrganisationRoleGroup sd = new OrganisationRoleGroup();
				sd.setId(rs.getLong(1));
				sd.setRoledefId(rs.getLong(2));
				sd.setName(rs.getString(3));
				sd.setOrganisationId(organisationId);
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteOrganisationRoleGroup(OrganisationRoleGroup de) {
		return deleteOrganisationRoleGroup(de.getId());
	}

	public static boolean deleteOrganisationRoleGroup(long organisationRoleGroupId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from organisationrolegroup where id = ?");
			stmt.setLong(1, organisationRoleGroupId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createOrganisationRoleGroup(OrganisationRoleGroup de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into organisationrolegroup ( organisationId, roledefId, name ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, de.getOrganisationId());
			stmt.setLong(2, de.getRoledefId());
			stmt.setString(3, de.getName());
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				de.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}

	public static boolean updateOrganisationRoleGroup(OrganisationRoleGroup de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("update organisationrolegroup set organisationId = ?, roledefId = ?, name = ? where id = ?");
			stmt.setLong(1, de.getOrganisationId());
			stmt.setLong(2, de.getRoledefId());
			stmt.setString(3, de.getName());
			stmt.setLong(4, de.getId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}

}
