package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.ReportTemplateReuse;

public class ReportTemplateReuseDB {

    /*
create table reporttemplatereuse (
  id bigserial primary key,
  processdefId bigint not null,
  reporttemplateId bigint not null,
  foreign key ( processdefId ) references processdef (id),
  foreign key ( reporttemplateId ) references reporttemplate (id)
);
     */

	public static List<ReportTemplateReuse> getAllReportTemplateReuses() {
		List<ReportTemplateReuse> out = new ArrayList<ReportTemplateReuse>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, processdefId, reporttemplateId from reporttemplatereuse");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ReportTemplateReuse pf = new ReportTemplateReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setProcessdefId(rs.getLong(2));
		    	pf.setReporttemplateId(rs.getLong(3));
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<ReportTemplateReuse> getAllReportTemplateReusesOfReportTemplate(long reporttemplateId) {
		List<ReportTemplateReuse> out = new ArrayList<ReportTemplateReuse>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, processdefId from reporttemplatereuse where reporttemplateId = ?");
		    stmt.setLong(1, reporttemplateId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ReportTemplateReuse pf = new ReportTemplateReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setProcessdefId(rs.getLong(2));
		    	pf.setReporttemplateId(reporttemplateId);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static List<ReportTemplateReuse> getAllReportTemplateReusesOfProcessdef(long processdefId) {
		List<ReportTemplateReuse> out = new ArrayList<ReportTemplateReuse>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, reporttemplateId from reporttemplatereuse where processdefId = ?");
		    stmt.setLong(1, processdefId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ReportTemplateReuse pf = new ReportTemplateReuse();
		    	pf.setId(rs.getLong(1));
		    	pf.setReporttemplateId(rs.getLong(2));
		    	pf.setProcessdefId(processdefId);
		    	out.add(pf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean deleteReportTemplateReuse(ReportTemplateReuse pf) {
	 	boolean outcome = deleteReportTemplateReuse(pf.getId());
	 	return outcome;
	}

	public static boolean deleteReportTemplateReuse(long reporttemplatereuseId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from reporttemplatereuse where id = ?");
		    stmt.setLong(1, reporttemplatereuseId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateReportTemplateReuse(ReportTemplateReuse pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); 
		    stmt = conn.prepareStatement("update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?");
		    stmt.setLong(1, pf.getReporttemplateId());
		    stmt.setLong(2, pf.getProcessdefId());
			stmt.setLong(3, pf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createReportTemplateReuse(ReportTemplateReuse pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createReportTemplateReuse(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createReportTemplateReuse(ReportTemplateReuse pf, Connection conn) {
		PreparedStatement stmt = null;
		try { // update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?
		    stmt = conn.prepareStatement("insert into reporttemplatereuse (reporttemplateId, processdefId) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setLong(1, pf.getReporttemplateId());
		    stmt.setLong(2, pf.getProcessdefId());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            pf.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static ReportTemplateReuse getReportTemplateReuseFromId(long id) {
		ReportTemplateReuse pf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // update reporttemplatereuse set reporttemplateId = ?, processdefId = ? where id = ?
		    stmt = conn.prepareStatement("select reporttemplateId, processdefId from reporttemplatereuse where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	pf = new ReportTemplateReuse();
				pf.setId(id);
				pf.setReporttemplateId(rs.getLong(1));
				pf.setProcessdefId(rs.getLong(2));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return pf;
	}
	
}

