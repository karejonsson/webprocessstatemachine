package se.prv.errends.dbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Transition;

public class TransitionDB {

	private static final Cache<Transition> cache = new Cache<Transition>(20);

	public static void clearCache() {
		cache.clear();
	}

/*
create table transition (
  id bigserial primary key,
  transitiondefId bigint not null,
  sessionId bigint not null,
  predecessorId bigint,
  foreign key ( transitiondefId ) references transitiondef (id),
  foreign key ( sessionId ) references session (id),
  foreign key ( predecessorId ) references transition (id)
);
 */

	public static Transition getTransitionFromId(long id) {
		Transition out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("select transitiondefId, sessionId, predecessorId from transition where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Transition();
				out.setTransitiondefId(rs.getLong(1));
				out.setSessionId(rs.getLong(2));
				Object predecessorId = rs.getObject(3);
				if(predecessorId != null) {
					if(predecessorId instanceof Integer || predecessorId instanceof Long) {
						out.setPredecessorId((Long) predecessorId);
					}
				}
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(out != null) {
			cache.add(out, out.getId());
		}
		return out;
	}

	public static List<Transition> getAllTransitionsOfTransitiondef(long transitiondefId) {
		List<Transition> out = new ArrayList<Transition>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("select id, sessionId, predecessorId from transition where transitiondefId = ?");
			stmt.setLong(1, transitiondefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transition t = new Transition();
				t.setId(rs.getLong(1));
				t.setSessionId(rs.getLong(2));
				Object predecessorId = rs.getObject(3);
				if(predecessorId != null) {
					if(predecessorId instanceof Integer || predecessorId instanceof Long) {
						t.setPredecessorId((Long) predecessorId);
					}
				}
				t.setTransitiondefId(transitiondefId);
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Transition> getAllTransitionsOfSession(long sessionId) {
		List<Transition> out = new ArrayList<Transition>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("select id, transitiondefId, predecessorId from transition where sessionId = ?");
			stmt.setLong(1, sessionId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transition t = new Transition();
				t.setId(rs.getLong(1));
				t.setTransitiondefId(rs.getLong(2));
				Object predecessorId = rs.getObject(3);
				if(predecessorId != null) {
					if(predecessorId instanceof Integer || predecessorId instanceof Long) {
						t.setPredecessorId((Long) predecessorId);
					}
				}
				t.setSessionId(sessionId);
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Transition> getAllTransitionsOfErrend(long errendId) {
		List<Transition> out = new ArrayList<Transition>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("select id, transitiondefId, predecessorId from transition where sessionId = ?");
			stmt = conn.prepareStatement("select transition.id, transition.transitiondefId, transition.sessionId, transition.predecessorId from actor, session, transition where transition.sessionId = session.id and session.actorId = actor.id and actor.errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transition t = new Transition();
				t.setId(rs.getLong(1));
				t.setTransitiondefId(rs.getLong(2));
				t.setSessionId(rs.getLong(3));
				Object predecessorId = rs.getObject(4);
				if(predecessorId != null) {
					if(predecessorId instanceof Integer || predecessorId instanceof Long) {
						t.setPredecessorId((Long) predecessorId);
					}
				}
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	// getAllTransitionsOfErrend
	public static boolean createTransition(Transition t) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("insert into transition ( transitiondefId, sessionId, predecessorId ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, t.getTransitiondefId());
			stmt.setLong(2, t.getSessionId());
			Long predecessorId = t.getPredecessorId();
			if(predecessorId != null) {
				stmt.setLong(3, predecessorId);
			}
			else {
				stmt.setNull(3, Types.NULL);
			}

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				t.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(t != null) {
			cache.add(t, t.getId());
		}
		return t != null;
	}

	public static boolean deleteTransition(Transition t) {
		return deleteTransition(t.getId());
	}

	public static boolean deleteTransition(long transitionId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from transition where id = ?");
			stmt.setLong(1, transitionId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(transitionId);
		return true;
	}

	public static boolean updateTransition(Transition t) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("update transition set transitiondefId = ?, sessionId = ?, predecessorId = ? where id = ?");
			stmt.setLong(1, t.getTransitiondefId());
			stmt.setLong(2, t.getSessionId());
			Long predecessorId = t.getPredecessorId();
			if(predecessorId != null) {
				stmt.setLong(3, predecessorId);
			}
			else {
				stmt.setNull(3, Types.NULL);
			}
			stmt.setLong(4, t.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(t != null) {
			cache.add(t, t.getId());
		}
		return t != null;
	}

	public static List<Transition> getAllTransitions() {
		List<Transition> out = new ArrayList<Transition>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitiondefId, sessionId, predecessorId
			stmt = conn.prepareStatement("select id, transitiondefId, sessionId, predecessorId from transition");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transition t = new Transition();
				t.setId(rs.getLong(1));
				t.setTransitiondefId(rs.getLong(2));
				t.setSessionId(rs.getLong(3));
				Object predecessorId = rs.getObject(4);
				if(predecessorId != null) {
					if(predecessorId instanceof Integer || predecessorId instanceof Long) {
						t.setPredecessorId((Long) predecessorId);
					}
				}
				cache.add(t, t.getId());
				out.add(t);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
