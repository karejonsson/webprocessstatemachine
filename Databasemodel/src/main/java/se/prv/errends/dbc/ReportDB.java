package se.prv.errends.dbc;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Report;
import se.prv.errends.internal.Services;

public class ReportDB {

	private static final Cache<Report> cache = new Cache<Report>(20);

	public static void clearCache() {
		cache.clear();
	}

	/*
create table report (
  id bigserial primary key,
  reporttitle varchar(100) default null,
  data bytea,
  reporttemplatereuseId bigint not null,
  errendId bigint not null,
  actorId bigint not null,
  foreign key ( reporttemplatereuseId ) references reporttemplatereuse (id),
  foreign key ( errendId ) references errend (id),
  foreign key ( actorId ) references actor (id)
	 */

	public static Report getReportFromId(long id) {
		Report out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, sessionId, errendId : report
			stmt = conn.prepareStatement("select reporttitle, reporttemplatereuseId, errendId, actorId from report where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Report();
				out.setReporttitle(rs.getString(1));
				out.setReportTemplateReuseId(rs.getLong(2));
				out.setErrendId(rs.getLong(3));
				out.setActorId(rs.getLong(4));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(out != null) {
			cache.add(out, out.getId());
		}
		return out;
	}

	public static List<Report> getAllReportsOfErrend(long errendId) {
		List<Report> out = new ArrayList<Report>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, errendId : report
			stmt = conn.prepareStatement("select id, reporttitle, reporttemplatereuseId, actorId from report where errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Report ec = new Report();
				ec.setId(rs.getLong(1));
				ec.setReporttitle(rs.getString(2));
				ec.setReportTemplateReuseId(rs.getLong(3));
				ec.setActorId(rs.getLong(4));
				ec.setErrendId(errendId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}
	
	public static List<Report> getAllReportsOfActor(long actorId) {
		List<Report> out = new ArrayList<Report>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, errendId : report
			stmt = conn.prepareStatement("select id, reporttitle, reporttemplatereuseId, errendId from report where actorId = ?");
			stmt.setLong(1, actorId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Report ec = new Report();
				ec.setId(rs.getLong(1));
				ec.setReporttitle(rs.getString(2));
				ec.setReportTemplateReuseId(rs.getLong(3));
				ec.setErrendId(rs.getLong(4));
				ec.setActorId(actorId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Report> getAllReportsOfReportTemplateReuse(long reporttemplatereuseId) {
		List<Report> out = new ArrayList<Report>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, sessionId, errendId : report
			stmt = conn.prepareStatement("select id, reporttitle, errendId, actorId from report where reporttemplatereuseId = ?");
			stmt.setLong(1, reporttemplatereuseId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Report ec = new Report();
				ec.setId(rs.getLong(1));
				ec.setReporttitle(rs.getString(2));
				ec.setErrendId(rs.getLong(3));
				ec.setActorId(rs.getLong(4));
				ec.setReportTemplateReuseId(reporttemplatereuseId);
				out.add(ec);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createReport(Report ec) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, errendId, reporttemplatereuseId : report
			stmt = conn.prepareStatement("insert into report ( reporttitle, errendId, reporttemplatereuseId, actorId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, ec.getReporttitle());
			stmt.setLong(2, ec.getErrendId());
			stmt.setLong(3, ec.getReportTemplateReuseId());
			stmt.setLong(4, ec.getActorId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				ec.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(ec != null) {
			cache.add(ec, ec.getId());
		}
		return ec != null;
	}

	public static boolean deleteReport(Report ec) {
		return deleteReport(ec.getId());
	}

	public static boolean deleteReport(long reportId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from report where id = ?");
			stmt.setLong(1, reportId);
			stmt.executeUpdate();
			conn.commit();
		} 
		catch(Exception e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(reportId);
		return true;
	}

	public static boolean updateReport(Report ec) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, errendId : errendcomment
			stmt = conn.prepareStatement("update report set reporttitle = ?, errendId = ?, reporttemplatereuseId = ?, actorId = ? where id = ?");
			stmt.setString(1, ec.getReporttitle());
			stmt.setLong(2, ec.getErrendId());
			stmt.setLong(3, ec.getReportTemplateReuseId());
			stmt.setLong(4, ec.getActorId());
			stmt.setLong(5, ec.getId());
			stmt.executeUpdate();
			conn.commit();
		} 
		catch(Exception e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		if(ec != null) {
			cache.add(ec, ec.getId());
		}
		return ec != null;
	}

	public static List<Report> getAllReports() {
		List<Report> out = new ArrayList<Report>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // reporttitle, errendId : errendcomment
			stmt = conn.prepareStatement("select id, reporttitle, errendId, reporttemplatereuseId, actorId from report");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Report ec = new Report();
				ec.setId(rs.getLong(1));
				ec.setReporttitle(rs.getString(2));
				ec.setErrendId(rs.getLong(3));
				ec.setReportTemplateReuseId(rs.getLong(4));
				ec.setActorId(rs.getLong(5));
				cache.add(ec, ec.getId());
				out.add(ec);
			}
		} 
		catch(Exception e) {
			e.printStackTrace();
		} 
		finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static boolean setData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update report set data = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from report where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

}