package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Dossier;
import se.prv.errends.domain.Message;

public class DossierDB {

	/*
create table dossier (
  id bigserial primary key,
  name varchar(50) not null,
  description varchar(400) default null,
  sessionId bigint not null,
  dossierdefId bigint not null,
  errendId bigint default null,
  unique(errendId),
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdefId ) references dossierdef (id),
  foreign key ( errendId ) references errend (id)
);
	 */

	public static Dossier getDossierFromId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select name, description, sessionId, dossierdefId, errendId from dossier where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Dossier d = new Dossier();
				d.setId(id);
				d.setName(rs.getString(1));
				d.setDescription(rs.getString(2));
				d.setSessionId(rs.getLong(3));
				d.setDossierDefId(rs.getLong(4));
				d.setErrendId(rs.getLong(5));
				return d;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static Dossier getDossierOfErrendId(long errendId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select name, description, sessionId, dossierdefId, id from dossier where errendId = ?");
			stmt.setLong(1, errendId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Dossier d = new Dossier();
				d.setErrendId(errendId);
				d.setName(rs.getString(1));
				d.setDescription(rs.getString(2));
				d.setSessionId(rs.getLong(3));
				d.setDossierDefId(rs.getLong(4));
				d.setId(rs.getLong(5));
				return d;
			}
		} 
		catch(Exception e) {
			e.printStackTrace();
		} 
		finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static List<Dossier> getAllDossiersOfPerson(long personId) {
		List<Dossier> out = new ArrayList<Dossier>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select dossier.id, dossier.name, dossier.description, dossier.sessionId, dossier.dossierdefId, dossier.errendId from actor, session, agent, dossier where dossier.sessionId = session.id and session.actorId = actor.id and actor.agentId = agent.id and agent.personId = ?");
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Dossier d = new Dossier();
				d.setId(rs.getLong(1));
				d.setName(rs.getString(2));
				d.setDescription(rs.getString(3));
				d.setSessionId(rs.getLong(4));
				d.setDossierDefId(rs.getLong(5));
				if(rs.getObject(6) != null) {
					d.setErrendId(rs.getLong(6));
				}
				out.add(d);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean createDossier(Dossier d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into dossier ( name, description, sessionId, dossierdefId, errendId ) values ( ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, d.getName());
			stmt.setString(2, d.getDescription());
			stmt.setLong(3, d.getSessionId());
			stmt.setLong(4, d.getDossierDefId());
			Long errendId = d.getErrendId();
			if(errendId == null) {
				stmt.setNull(5, Types.BIGINT);
			}
			else {
				stmt.setLong(5, errendId);
			}
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				d.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return d != null;
	}
	
	public static boolean deleteDossier(Dossier d) {
		return deleteDossier(d.getId());
	}

	public static boolean deleteDossier(long dossierId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from dossier where id = ?");
			stmt.setLong(1, dossierId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateDossier(Dossier d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // name, description, sessionId, errendId
			stmt = conn.prepareStatement("update dossier set name = ?, description = ?, sessionId = ?, dossierdefId = ?, errendId = ? where id = ?");
			stmt.setString(1, d.getName());
			stmt.setString(2, d.getDescription());
			stmt.setLong(3, d.getSessionId());
			stmt.setLong(4, d.getDossierDefId());
			Long errendId = d.getErrendId();
			if(errendId == null) {
				stmt.setNull(5, Types.BIGINT);
			}
			else {
				stmt.setLong(5, errendId);
			}
			stmt.setLong(6, d.getId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

}
