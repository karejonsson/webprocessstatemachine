package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.FunctionDef;

public class FunctionDefDB {

    private static final Cache<FunctionDef> cache = new Cache<FunctionDef>(20);

    public static void clearCache() {
        cache.clear();
    }

/*
create table functiondef (
  id bigserial primary key,
  functionname varchar(100) not null,
  typename varchar(10) default null,
  source varchar(30000) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);
 */

    public static FunctionDef getFunctionDefFromId(long id) {
    	FunctionDef out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, script, orderby, processfileId
            stmt = conn.prepareStatement("select functionname, typename, source, orderby, processfileId from functiondef where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new FunctionDef();
                out.setFunctionname(rs.getString(1));
                out.setTypename(rs.getString(2));
                out.setSource(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(5);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						out.setProcessfileId((Long) processfileId);
					}
				}
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
    public static FunctionDef getFunctionDefFromName(String name) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, typename, source, orderby, processfileId from functiondef where functionname = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	FunctionDef out = new FunctionDef();
                out.setId(rs.getLong(1));
                out.setTypename(rs.getString(2));
                out.setSource(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(5);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						out.setProcessfileId((Long) processfileId);
					}
				}
                out.setFunctionname(name);
                return out;
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return null;
    }
    
	public static List<FunctionDef> getAllFunctionDefsOfProcessfile(long processfileId) {
		List<FunctionDef> out = new ArrayList<FunctionDef>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // select processname, description, orderby, processfileId from adminuser where id = ?
		    stmt = conn.prepareStatement("select id, functionname, typename, source, orderby from functiondef where processfileId = ?");
		    stmt.setLong(1, processfileId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	FunctionDef pd = new FunctionDef();
		    	pd.setId(rs.getLong(1));
		    	pd.setFunctionname(rs.getString(2));
		    	pd.setTypename(rs.getString(3));
		    	pd.setSource(rs.getString(4));
                Object orderby = rs.getObject(5);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
		    	pd.setProcessfileId(processfileId);
		    	out.add(pd);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createFunctionDef(FunctionDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("insert into functiondef ( functionname, typename, source, orderby, processfileId ) values ( ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pd.getFunctionname());
            stmt.setString(2, pd.getTypename());
            stmt.setString(3, pd.getSource());
            Integer orderby = pd.getOrderby();
            if(orderby != null) {
                stmt.setInt(4, orderby);
            }
            else {
            	stmt.setNull(4, Types.NULL);
            }

			Long processfileId = pd.getProcessfileId();
			if(processfileId != null) {
				stmt.setLong(5, processfileId);
			}
			else {
				stmt.setNull(5, Types.NULL);
			}
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                pd.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static boolean deleteFunctionDef(FunctionDef pd) {
        return deleteFunctionDef(pd.getId());
    }

    public static boolean deleteFunctionDef(long processdefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from functiondef where id = ?");
            stmt.setLong(1, processdefId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(processdefId);
        return true;
    }

    public static boolean updateFunctionDef(FunctionDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("update functiondef set functionname = ?, typename = ?, source = ?, orderby = ?, processfileId = ? where id = ?");
            stmt.setString(1, pd.getFunctionname());
            stmt.setString(2, pd.getTypename());
            stmt.setString(3, pd.getSource());
            Integer orderby = pd.getOrderby();
            if(orderby != null) {
                stmt.setInt(4, orderby);
            }
            else {
            	stmt.setNull(4, Types.NULL);
            }

			Long processfileId = pd.getProcessfileId();
			if(processfileId != null) {
				stmt.setLong(5, processfileId);
			}
			else {
				stmt.setNull(5, Types.NULL);
			}
            stmt.setLong(6, pd.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static List<FunctionDef> getAllFunctionDefs() {
        List<FunctionDef> out = new ArrayList<FunctionDef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, functionname, typename, source, orderby, processfileId from functiondef");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	FunctionDef pd = new FunctionDef();
            	pd.setId(rs.getLong(1));
            	pd.setFunctionname(rs.getString(2));
            	pd.setTypename(rs.getString(3));
            	pd.setSource(rs.getString(4));
                Object orderby = rs.getObject(5);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(6);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						pd.setProcessfileId((Long) processfileId);
					}
				}
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }
    
    public static List<FunctionDef> getAllFunctionDefsByName(String functionname) {
        List<FunctionDef> out = new ArrayList<FunctionDef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, typename, source, orderby, processfileId from functiondef where functionname = ?");
            stmt.setString(1, functionname);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	FunctionDef pd = new FunctionDef();
            	pd.setId(rs.getLong(1));
            	pd.setTypename(rs.getString(2));
            	pd.setSource(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(5);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						pd.setProcessfileId((Long) processfileId);
					}
				}
            	pd.setFunctionname(functionname);
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

    public static FunctionDef getReusedFunctionDefOfProcessDefByName(String name, long processdefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select functiondef.id, functiondef.typename, functiondef.source, functiondef.orderby, functiondef.processfileId from functiondef, functiondefreuse, processdef where functiondef.id = functiondefreuse.functiondefId and functiondef.functionname = ? and functiondefreuse.processdefId = ?");
            stmt.setString(1, name);
            stmt.setLong(2, processdefId);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	FunctionDef pd = new FunctionDef();
            	pd.setId(rs.getLong(1));
            	pd.setFunctionname(name);
            	pd.setTypename(rs.getString(2));
            	pd.setSource(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
				Object functionfileId = rs.getObject(5);
				if(functionfileId != null) {
					if(functionfileId instanceof Integer || functionfileId instanceof Long) {
						pd.setProcessfileId((Long) functionfileId);
					}
				}
                cache.add(pd, pd.getId());
                return pd;
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return null;
    }

    public static List<FunctionDef> getReusedFunctionDefsOfProcessDef(long processdefId) {
        List<FunctionDef> out = new ArrayList<FunctionDef>();
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select functiondef.id, functiondef.typename, functiondef.source, functiondef.orderby, functiondef.processfileId, functiondef.functionname from functiondef, functiondefreuse where functiondef.id = functiondefreuse.functiondefId and functiondefreuse.processdefId = ?");
            stmt.setLong(1, processdefId);
            rs = stmt.executeQuery();
            while(rs.next()) {
            	FunctionDef pd = new FunctionDef();
            	pd.setId(rs.getLong(1));
            	pd.setTypename(rs.getString(2));
            	pd.setSource(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
				Object functionfileId = rs.getObject(5);
				if(functionfileId != null) {
					if(functionfileId instanceof Integer || functionfileId instanceof Long) {
						pd.setProcessfileId((Long) functionfileId);
					}
				}
            	pd.setFunctionname(rs.getString(6));
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
