package se.prv.errends.dbc;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Document;
import se.prv.errends.internal.Services;

public class DocumentDB {

	/*
create table document (
  id bigserial primary key,
  filename varchar(180) not null,
  mimetype varchar(180) not null,
  active bool default true not null,
  origin varchar(600) not null,
  description varchar(1200) default null,
  dateOfReception timestamp not null,
  data bytea,
  sessionId bigint not null,
  dossierdoctypedefId bigint default null,
  dossierId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdoctypedefId ) references dossierdoctypedef (id),
  foreign key ( dossierId ) references dossier (id)
);
	 */

	public static Document getDocumentFromId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select filename, mimetype, active, origin, description, dateOfReception, sessionId, dossierdoctypedefId, dossierId from document where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Document d = new Document();
				d.setFilename(rs.getString(1));
				d.setMimetype(rs.getString(2));
				d.setActive(rs.getBoolean(3));
				d.setOrigin(rs.getString(4));
				d.setDescription(rs.getString(5));
				d.setDateOfReception(rs.getTimestamp(6));
				d.setSessionId(rs.getLong(7));
				if(rs.getObject(8) != null) {
					d.setDossierDoctypeDefId(rs.getLong(8));
				}
				d.setDossierId(rs.getLong(9));
				d.setId(id);
				return d;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static List<Document> getAllDocumentsOfDossier(long dossierId) {
		List<Document> out = new ArrayList<Document>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, filename, mimetype, active, origin, description, dateOfReception, sessionId, dossierdoctypedefId from document where dossierId = ?");
			stmt.setLong(1, dossierId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Document d = new Document();
				d.setId(rs.getLong(1));
				d.setFilename(rs.getString(2));
				d.setMimetype(rs.getString(3));
				d.setActive(rs.getBoolean(4));
				d.setOrigin(rs.getString(5));
				d.setDescription(rs.getString(6));
				d.setDateOfReception(rs.getTimestamp(7));
				d.setSessionId(rs.getLong(8));
				if(rs.getObject(9) != null) {
					d.setDossierDoctypeDefId(rs.getLong(9));
				}
				d.setDossierId(dossierId);
				out.add(d);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean createDocument(Document d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into document ( filename, mimetype, active, origin, description, dateOfReception, sessionId, dossierdoctypedefId, dossierId ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, d.getFilename());
			stmt.setString(2, d.getMimetype());
			stmt.setBoolean(3, d.isActive());
			stmt.setString(4, d.getOrigin());
			stmt.setString(5, d.getDescription());		
			if(d.getDateOfReception() == null) {
				d.setDateOfReception(new Date());
			}
			stmt.setTimestamp(6, new java.sql.Timestamp(d.getDateOfReception().getTime()));
			stmt.setLong(7, d.getSessionId());
			Long dossierdoctypedefId = d.getDossierDoctypeDefId();
			if(dossierdoctypedefId == null) {
				stmt.setNull(8, Types.BIGINT);
			}
			else {
				stmt.setLong(8, dossierdoctypedefId);
			}
			stmt.setLong(9, d.getDossierId());		
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				d.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return d != null;
	}
	
	public static boolean deleteDocument(Document de) {
		return deleteDocument(de.getId());
	}

	public static boolean deleteDocument(long documentId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from document where id = ?");
			stmt.setLong(1, documentId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateDocument(Document d) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // filename, mimetype, active, origin, description, sessionId, dossierId
			stmt = conn.prepareStatement("update document set filename = ?, mimetype = ?, active = ?, origin = ?, description = ?, dateOfReception = ?, sessionId = ?, dossierdoctypedefId = ?, dossierId = ? where id = ?");
			stmt.setString(1, d.getFilename());
			stmt.setString(2, d.getMimetype());
			stmt.setBoolean(3, d.isActive());
			stmt.setString(4, d.getOrigin());
			stmt.setString(5, d.getDescription());

			if(d.getDateOfReception() == null) {
				d.setDateOfReception(new Date());
			}
			stmt.setTimestamp(6, new java.sql.Timestamp(d.getDateOfReception().getTime()));

			stmt.setLong(7, d.getSessionId());
			Long dossierdoctypedefId = d.getDossierDoctypeDefId();
			if(dossierdoctypedefId == null) {
				stmt.setNull(8, Types.BIGINT);
			}
			else {
				stmt.setLong(8, dossierdoctypedefId);
			}
			stmt.setLong(9, d.getDossierId());
			stmt.setLong(10, d.getId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return d != null;
	}

	public static boolean setData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update document set data = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from document where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

}
