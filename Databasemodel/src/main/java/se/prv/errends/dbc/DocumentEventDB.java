package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.DocumentEvent;

public class DocumentEventDB {

	/*
create table documentevent (
  id bigserial primary key,
  description varchar(1200) default null,
  code int not null,
  systeminfo varchar(800) not null,
  timeofevent timestamp not null,
  sessionId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( documentId ) references document (id)
);
	 */

	public static DocumentEvent getDocumentEventOfId(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select documentId, description, code, systeminfo, timeofevent, sessionId from documentevent where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				DocumentEvent de = new DocumentEvent();
				de.setDocumentId(rs.getLong(1));
				de.setDescription(rs.getString(2));
				de.setCode(rs.getInt(3));
				de.setSysteminfo(rs.getString(4));
				de.setTimeofevent(rs.getTimestamp(5));
				de.setSessionId(rs.getLong(6));
				de.setId(id);
				return de;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static List<DocumentEvent> getAllDocumentEventsOfDocument(long documentId) {
		List<DocumentEvent> out = new ArrayList<DocumentEvent>();
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("select id, description, code, systeminfo, timeofevent, sessionId from documentevent where documentId = ?");
			stmt.setLong(1, documentId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				DocumentEvent sd = new DocumentEvent();
				sd.setId(rs.getLong(1));
				sd.setDescription(rs.getString(2));
				sd.setCode(rs.getInt(3));
				sd.setSysteminfo(rs.getString(4));
				sd.setTimeofevent(rs.getTimestamp(5));
				sd.setSessionId(rs.getLong(6));
				sd.setDocumentId(documentId);
				out.add(sd);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean createDocumentEvent(DocumentEvent de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("insert into documentevent ( description, code, systeminfo, timeofevent, sessionId, documentId ) values ( ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, de.getDescription());
			stmt.setInt(2, de.getCode());
			stmt.setString(3, de.getSysteminfo());
			if(de.getTimeofevent() == null) {
				de.setTimeofevent(new Date());
			}
			stmt.setTimestamp(4, new Timestamp(de.getTimeofevent().getTime()));
			stmt.setLong(5,  de.getSessionId());		
			stmt.setLong(6,  de.getDocumentId());		
			stmt.executeUpdate();
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				de.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}
	
	public static boolean deleteDocumentEvent(DocumentEvent de) {
		return deleteDocumentEvent(de.getId());
	}

	public static boolean deleteDocumentEvent(long documentEventId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from documentevent where id = ?");
			stmt.setLong(1, documentEventId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateDocumentEvent(DocumentEvent de) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); 
			stmt = conn.prepareStatement("update documentevent set description = ?, code = ?, systeminfo = ?, timeofevent = ?, sessionId = ?, documentId = ? where id = ?");
			stmt.setString(1, de.getDescription());
			stmt.setInt(2, de.getCode());
			stmt.setString(3, de.getSysteminfo());
			if(de.getTimeofevent() == null) {
				de.setTimeofevent(new Date());
			}
			stmt.setTimestamp(4, new Timestamp(de.getTimeofevent().getTime()));
			stmt.setLong(5, de.getSessionId());
			stmt.setLong(6, de.getDocumentId());
			stmt.setLong(7, de.getId());
 			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return de != null;
	}

}
