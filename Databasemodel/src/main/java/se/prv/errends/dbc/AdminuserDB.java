package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Adminuser;

public class AdminuserDB {

    private static final Cache<Adminuser> cache = new Cache<Adminuser>(20);

    public static void clearCache() {
        cache.clear();
    }

    /*
create table adminuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(40) not null,
  active bool default true,
  superprivilegies bool default false,
  orderby int default null,
  personId bigint not null,
  foreign key ( personId ) references person (id)
);
	 */

    public static Adminuser getAdminuserFromId(long id) {
    	Adminuser out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select username, password, active, superprivilegies, orderby, personId from adminuser where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Adminuser();
                out.setUsername(rs.getString(1));
                out.setPassword(rs.getString(2));
                out.setActive(rs.getBoolean(3));
                out.setSuperprivilegies(rs.getBoolean(4));
                Object orderby = rs.getObject(5);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
                out.setPersonId(rs.getLong(6));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static Adminuser getAdminuserFromPersonId(long personId) {
    	Adminuser out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, username, password, active, superprivilegies, orderby from adminuser where personId = ?");
            stmt.setLong(1, personId);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Adminuser();
                out.setId(rs.getLong(1));
                out.setUsername(rs.getString(2));
                out.setPassword(rs.getString(3));
                out.setActive(rs.getBoolean(4));
                out.setSuperprivilegies(rs.getBoolean(5));
                Object orderby = rs.getObject(6);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
                out.setPersonId(personId);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static Adminuser getAdminuserFromUsername(String username) {
    	Adminuser out = null;
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, personId, password, active, superprivilegies, orderby from adminuser where username = ?");
            stmt.setString(1, username);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Adminuser();
                out.setId(rs.getLong(1));
                out.setPersonId(rs.getLong(2));
                out.setPassword(rs.getString(3));
                out.setActive(rs.getBoolean(4));
                out.setSuperprivilegies(rs.getBoolean(5));
                Object orderby = rs.getObject(6);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
                out.setUsername(username);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }

    public static boolean createAdminuser(Adminuser au) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("insert into adminuser ( username, password, active, superprivilegies, orderby, personId ) values ( ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, au.getUsername());
            stmt.setString(2, au.getPassword());
            stmt.setBoolean(3, au.isActive());
            stmt.setBoolean(4, au.isSuperprivilegies());
            Integer orderby = au.getOrderby();
            if(orderby != null) {
                stmt.setInt(5, au.getOrderby());
            }
            else {
            	stmt.setNull(5, Types.NULL);
            }
            stmt.setLong(6, au.getPersonId());
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                au.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(au != null) {
        	cache.add(au, au.getId());
        }
        return au != null;
    }

    public static boolean deleteAdminuser(Adminuser au) {
        return deleteAdminuser(au.getId());
    }

    public static boolean deleteAdminuser(long adminuserId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from adminuser where id = ?");
            stmt.setLong(1, adminuserId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(adminuserId);
        return true;
    }

    public static boolean updateAdminuser(Adminuser au) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();// username, password, active, superprivilegies, orderby, personId
            stmt = conn.prepareStatement("update adminuser set username = ?, password = ?, active = ?, superprivilegies = ?, orderby = ?, personId = ? where id = ?");
            stmt.setString(1, au.getUsername());
            stmt.setString(2, au.getPassword());
            stmt.setBoolean(3, au.isActive());
            stmt.setBoolean(4, au.isSuperprivilegies());
            Integer orderby = au.getOrderby();
            if(orderby != null) {
                stmt.setInt(5, orderby);
            }
            else {
            	stmt.setNull(5, Types.NULL);
            }
            stmt.setLong(6, au.getPersonId());
            stmt.setLong(7, au.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(au != null) {
        	cache.add(au, au.getId());
        }
        return au != null;
    }

    public static List<Adminuser> getAllAdminusers() {
        List<Adminuser> out = new ArrayList<Adminuser>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, username, password, active, superprivilegies, orderby, personId from adminuser");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Adminuser au = new Adminuser();
            	au.setId(rs.getLong(1));
            	au.setUsername(rs.getString(2));
            	au.setPassword(rs.getString(3));
            	au.setActive(rs.getBoolean(4));
            	au.setSuperprivilegies(rs.getBoolean(5));
                Object orderby = rs.getObject(6);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        au.setOrderby((Integer) orderby);
                	}
                }
            	au.setPersonId(rs.getLong(7));
                cache.add(au, au.getId());
                out.add(au);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
