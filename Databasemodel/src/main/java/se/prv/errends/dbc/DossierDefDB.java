package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.DossierDef;

public class DossierDefDB {

    private static final Cache<DossierDef> cache = new Cache<DossierDef>(20);

    public static void clearCache() {
        cache.clear();
    }

	/*
create table dossierdef (
  id bigserial primary key,
  dossiertypename varchar(50) not null,
  description varchar(250) default null,
  processfileId bigint default null,
  foreign key ( processfileId ) references processfile (id)
);
	 */

    public static DossierDef getDossierDefFromId(long id) {
    	DossierDef out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select dossiertypename, description, processfileId from dossierdef where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new DossierDef();
                out.setDossiertypename(rs.getString(1));
                out.setDescription(rs.getString(2));
                if(rs.getObject(3) != null) {
                    out.setProcessfileId(rs.getLong(3));
                }
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
	public static List<DossierDef> getAllDossierDefsOfProcessfile(long processfileId) {
		List<DossierDef> out = new ArrayList<DossierDef>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // select processname, description, orderby, processfileId from adminuser where id = ?
		    stmt = conn.prepareStatement("select id, dossiertypename, description from dossierdef where processfileId = ?");
		    stmt.setLong(1, processfileId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	DossierDef pd = new DossierDef();
		    	pd.setId(rs.getLong(1));
		    	pd.setDossiertypename(rs.getString(2));
		    	pd.setDescription(rs.getString(3));
		    	pd.setProcessfileId(processfileId);
		    	out.add(pd);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createDossierDef(DossierDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("insert into dossierdef ( dossiertypename, description, processfileId ) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pd.getDossiertypename());
            stmt.setString(2, pd.getDescription());
			Long processfileId = pd.getProcessfileId();
			if(processfileId == null) {
				stmt.setNull(3, Types.BIGINT);				
			}
			else {
				stmt.setLong(3, processfileId);
			}
            int affectedRows = stmt.executeUpdate();
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                pd.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static boolean deleteDossierDef(DossierDef pd) {
        return deleteDossierDef(pd.getId());
    }

    public static boolean deleteDossierDef(long dossierDefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from dossierdef where id = ?");
            stmt.setLong(1, dossierDefId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(dossierDefId);
        return true;
    }

    public static boolean updateDossierDef(DossierDef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("update dossierdef set dossiertypename = ?, description = ?, processfileId = ? where id = ?");
            stmt.setString(1, pd.getDossiertypename());
            stmt.setString(2, pd.getDescription());
			Long processfileId = pd.getProcessfileId();
			if(processfileId == null) {
				stmt.setNull(3, Types.BIGINT);				
			}
			else {
				stmt.setLong(3, processfileId);
			}
            stmt.setLong(4, pd.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static List<DossierDef> getAllDossierDefs() {
        List<DossierDef> out = new ArrayList<DossierDef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, dossiertypename, description, processfileId from dossierdef");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	DossierDef pd = new DossierDef();
            	pd.setId(rs.getLong(1));
            	pd.setDossiertypename(rs.getString(2));
            	pd.setDescription(rs.getString(3));
            	if(rs.getObject(4) != null) {
	            	pd.setProcessfileId(rs.getLong(4));
	            }
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
