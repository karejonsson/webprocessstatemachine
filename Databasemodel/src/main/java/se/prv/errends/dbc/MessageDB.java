package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Message;

public class MessageDB {

	private static final Cache<Message> cache = new Cache<Message>(20);

	public static void clearCache() {
		cache.clear();
	}

/*
create table message (
  id bigserial primary key,
  title varchar(80) default null,
  writtenmessage varchar(2000) default null,
  unread bool default true not null,
  send timestamp not null,
  sourceActorId bigint,
  targetActorId bigint,
  foreign key ( sourceActorId ) references actor (id),
  foreign key ( targetActorId ) references actor (id)
);
 */

	public static Message getMessageFromId(long id) {
		Message out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, unread, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("select title, writtenmessage, unread, send, sourceActorId, targetActorId from message where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Message();
				out.setTitle(rs.getString(1));
				out.setWrittenmessage(rs.getString(2));
				out.setUnread(rs.getBoolean(3));
				out.setSend(rs.getTimestamp(4));

				Object sourceId = rs.getObject(5);
				if(sourceId != null) {
					if(sourceId instanceof Integer || sourceId instanceof Long) {
						out.setSourceActorId((Long) sourceId);
					}
				}

				Object targetId = rs.getObject(6);
				if(targetId != null) {
					if(targetId instanceof Integer || targetId instanceof Long) {
						out.setTargetActorId((Long) targetId);
					}
				}

				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(out != null) {
        	cache.add(out, out.getId());
        }
		return out;
	}

	public static List<Message> getAllMessagesOfPerson(long personId) {
		List<Message> out = new ArrayList<Message>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("select message.id, message.title, message.writtenmessage, message.unread, message.send, message.sourceActorId, message.targetActorId from message, actor, agent where (message.sourceActorId = actor.id or message.targetActorId = actor.id) and actor.agentId = agent.id and agent.personId = ?");
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Message m = new Message();
				m.setId(rs.getLong(1));
				m.setTitle(rs.getString(2));
				m.setWrittenmessage(rs.getString(3));
				m.setUnread(rs.getBoolean(4));
				m.setSend(rs.getTimestamp(5));
				
				Object sourceId = rs.getObject(6);
				if(sourceId != null) {
					if(sourceId instanceof Integer || sourceId instanceof Long) {
						m.setSourceActorId((Long) sourceId);
					}
				}

				Object targetId = rs.getObject(7);
				if(targetId != null) {
					if(targetId instanceof Integer || targetId instanceof Long) {
						m.setTargetActorId((Long) targetId);
					}
				}

				out.add(m);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Message> getAllUnreadMessagesOfPerson(long personId) {
		List<Message> out = new ArrayList<Message>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("select message.id, message.title, message.writtenmessage, message.send, message.sourceActorId, message.targetActorId from message, actor, agent where message.unread = true and message.targetActorId = actor.id and actor.agentId = agent.id and agent.personId = ?");
			stmt.setLong(1, personId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Message m = new Message();
				m.setId(rs.getLong(1));
				m.setTitle(rs.getString(2));
				m.setWrittenmessage(rs.getString(3));
				m.setSend(rs.getTimestamp(4));
				
				Object sourceId = rs.getObject(5);
				if(sourceId != null) {
					if(sourceId instanceof Integer || sourceId instanceof Long) {
						m.setSourceActorId((Long) sourceId);
					}
				}

				Object targetId = rs.getObject(6);
				if(targetId != null) {
					if(targetId instanceof Integer || targetId instanceof Long) {
						m.setTargetActorId((Long) targetId);
					}
				}

				m.setUnread(true);
				out.add(m);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Message> getAllMessagesOfActor(long actorId) {
		List<Message> out = new ArrayList<Message>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("select id, title, writtenmessage, unread, send, sourceActorId, targetActorId from message where (sourceActorId = ? or targetActorId = ?)");
			stmt.setLong(1, actorId);
			stmt.setLong(2, actorId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Message m = new Message();
				m.setId(rs.getLong(1));
				m.setTitle(rs.getString(2));
				m.setWrittenmessage(rs.getString(3));
				m.setUnread(rs.getBoolean(4));
				m.setSend(rs.getTimestamp(5));
				
				Object sourceId = rs.getObject(6);
				if(sourceId != null) {
					if(sourceId instanceof Integer || sourceId instanceof Long) {
						m.setSourceActorId((Long) sourceId);
					}
				}

				Object targetId = rs.getObject(7);
				if(targetId != null) {
					if(targetId instanceof Integer || targetId instanceof Long) {
						m.setTargetActorId((Long) targetId);
					}
				}

				out.add(m);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createMessage(Message m) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("insert into message ( title, writtenmessage, unread, send, sourceActorId, targetActorId ) values ( ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);

			stmt.setString(1,  m.getTitle());
			stmt.setString(2,  m.getWrittenmessage());
			stmt.setBoolean(3,  m.isUnread());
            Date send = m.getSend();
            if(send == null) {
            	send = new Date();
                m.setSend(send);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(send.getTime()));
            
            Long id = m.getSourceActorId();
            if(id == null) {
                stmt.setNull(5, Types.NULL);
            }
            else {
            	stmt.setLong(5, id);
            }
            id = m.getTargetActorId();
            if(id == null) {
                stmt.setNull(6, Types.NULL);
            }
            else {
            	stmt.setLong(6, id);
            }

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				m.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(m != null) {
        	cache.add(m, m.getId());
        }
		return m != null;
	}

	public static boolean deleteMessage(Message m) {
		return deleteMessage(m.getId());
	}

	public static boolean deleteMessage(long messageId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from message where id = ?");
			stmt.setLong(1, messageId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(messageId);
		return true;
	}

	public static boolean updateMessage(Message s) {
		if(s.getSourceActorId() == null && s.getTargetActorId() == null) {
			deleteMessage(s);
			return true;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // title, writtenmessage, send, sourceActorId, targetActorId : message
			stmt = conn.prepareStatement("update message set title = ?, writtenmessage = ?, unread = ?, send = ?, sourceActorId = ?, targetActorId = ? where id = ?");

			stmt.setString(1, s.getTitle());
			stmt.setString(2, s.getWrittenmessage());
			stmt.setBoolean(3, s.isUnread());
            Date send = s.getSend();
            stmt.setTimestamp(4, send != null ? new java.sql.Timestamp(send.getTime()) : null);

            Long id = s.getSourceActorId();
            if(id == null) {
                stmt.setNull(5, Types.NULL);
            }
            else {
            	stmt.setLong(5, id);
            }
            id = s.getTargetActorId();
            if(id == null) {
                stmt.setNull(6, Types.NULL);
            }
            else {
            	stmt.setLong(6, id);
            }
            stmt.setLong(7, s.getId());

			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(s != null) {
        	cache.add(s, s.getId());
        }
		return s != null;
	}

}
