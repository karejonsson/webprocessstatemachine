package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Processdef;

public class ProcessdefDB {

    private static final Cache<Processdef> cache = new Cache<Processdef>(20);

    public static void clearCache() {
        cache.clear();
    }

/*
create table processdef (
  id bigserial primary key,
  processname varchar(50) not null,
  description varchar(250) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);
 */

    public static Processdef getProcessdefFromId(long id) {
    	Processdef out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select processname, description, orderby, processfileId from processdef where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Processdef();
                out.setProcessname(rs.getString(1));
                out.setDescription(rs.getString(2));
                Object orderby = rs.getObject(3);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(4);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						out.setProcessfileId((Long) processfileId);
					}
				}
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
	public static List<Processdef> getAllProcessdefsOfProcessfile(long processfileId) {
		List<Processdef> out = new ArrayList<Processdef>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // select processname, description, orderby, processfileId from adminuser where id = ?
		    stmt = conn.prepareStatement("select id, processname, description, orderby from processdef where processfileId = ?");
		    stmt.setLong(1, processfileId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Processdef pd = new Processdef();
		    	pd.setId(rs.getLong(1));
		    	pd.setProcessname(rs.getString(2));
		    	pd.setDescription(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
		    	pd.setProcessfileId(processfileId);
		    	out.add(pd);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createProcessdef(Processdef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("insert into processdef ( processname, description, orderby, processfileId ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pd.getProcessname());
            stmt.setString(2, pd.getDescription());
            Integer orderby = pd.getOrderby();
            if(orderby != null) {
                stmt.setInt(3, orderby);
            }
            else {
            	stmt.setNull(3, Types.NULL);
            }

			Long processfileId = pd.getProcessfileId();
			if(processfileId != null) {
				stmt.setLong(4, processfileId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                pd.setId(generatedKeys.getLong(1));
            }
            catch(Exception e) {
                //System.out.println("Fel 1");
                e.printStackTrace();
            }
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static boolean deleteProcessdef(Processdef pd) {
        return deleteProcessdef(pd.getId());
    }

    public static boolean deleteProcessdef(long processdefId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from processdef where id = ?");
            stmt.setLong(1, processdefId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(processdefId);
        return true;
    }

    public static boolean updateProcessdef(Processdef pd) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("update processdef set processname = ?, description = ?, orderby = ?, processfileId = ? where id = ?");
            stmt.setString(1, pd.getProcessname());
            stmt.setString(2, pd.getDescription());
            Integer orderby = pd.getOrderby();
            if(orderby != null) {
                stmt.setInt(3, orderby);
            }
            else {
            	stmt.setNull(3, Types.NULL);
            }

			Long processfileId = pd.getProcessfileId();
			if(processfileId != null) {
				stmt.setLong(4, processfileId);
			}
			else {
				stmt.setNull(4, Types.NULL);
			}
            stmt.setLong(5, pd.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(pd != null) {
        	cache.add(pd, pd.getId());
        }
        return pd != null;
    }

    public static List<Processdef> getAllProcessdefs() {
        List<Processdef> out = new ArrayList<Processdef>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // processname, description, orderby, processfileId
            stmt = conn.prepareStatement("select id, processname, description, orderby, processfileId from processdef");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Processdef pd = new Processdef();
            	pd.setId(rs.getLong(1));
            	pd.setProcessname(rs.getString(2));
            	pd.setDescription(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        pd.setOrderby((Integer) orderby);
                	}
                }
				Object processfileId = rs.getObject(5);
				if(processfileId != null) {
					if(processfileId instanceof Integer || processfileId instanceof Long) {
						pd.setProcessfileId((Long) processfileId);
					}
				}
                cache.add(pd, pd.getId());
                out.add(pd);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}
