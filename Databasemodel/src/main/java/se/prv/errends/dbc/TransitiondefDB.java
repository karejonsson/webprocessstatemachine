package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Transitiondef;

public class TransitiondefDB {

	private static final Cache<Transitiondef> cache = new Cache<Transitiondef>(20);

	public static void clearCache() {
		cache.clear();
	}

	/*
	create table transitiondef (
	  id bigserial primary key,
	  transitionname varchar(50) not null,
	  layout varchar(30000) default null,
	  orderby int default null,
	  sourcetransitiondefId bigint not null,
	  targettransitiondefId bigint not null,
	  foreign key ( sourcestatedefId ) references transitiondef (id),
	  foreign key ( targetstatedefId ) references transitiondef (id)
	);
	 */

	public static Transitiondef getTransitiondefFromId(long id) {
		Transitiondef out = cache.get(id);
		if(out != null) {
			return out;
		}
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("select transitionname, layout, orderby, sourcestatedefId, targetstatedefId from transitiondef where id = ?");
			stmt.setLong(1, id);
			rs = stmt.executeQuery();
			while(rs.next()) {
				out = new Transitiondef();
				out.setTransitionname(rs.getString(1));
				out.setLayout(rs.getString(2));
                Object orderby = rs.getObject(3);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        out.setOrderby((Integer) orderby);
                	}
                }
				out.setSourcestatedefId(rs.getLong(4));
				out.setTargetstatedefId(rs.getLong(5));
				out.setId(id);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(out != null) {
        	cache.add(out, out.getId());
        }
		return out;
	}

	public static List<Transitiondef> getAllSourceTransitiondefsOfStatedef(long sourcestatedefId) {
		List<Transitiondef> out = new ArrayList<Transitiondef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("select id, transitionname, layout, orderby, targetstatedefId from transitiondef where sourcestatedefId = ?");
			stmt.setLong(1, sourcestatedefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transitiondef td = new Transitiondef();
				td.setId(rs.getLong(1));
				td.setTransitionname(rs.getString(2));
				td.setLayout(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        td.setOrderby((Integer) orderby);
                	}
                }
				td.setTargetstatedefId(rs.getLong(5));
				td.setSourcestatedefId(sourcestatedefId);
				out.add(td);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Transitiondef> getAllTargetTransitiondefsOfStatedef(long targetstatedefId) {
		List<Transitiondef> out = new ArrayList<Transitiondef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("select id, transitionname, layout, orderby, targetstatedefId from transitiondef where targetstatedefId = ?");
			stmt.setLong(1, targetstatedefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transitiondef td = new Transitiondef();
				td.setId(rs.getLong(1));
				td.setTransitionname(rs.getString(2));
				td.setLayout(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        td.setOrderby((Integer) orderby);
                	}
                }
				td.setSourcestatedefId(rs.getLong(5));
				td.setTargetstatedefId(targetstatedefId);
				out.add(td);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static List<Transitiondef> getAllTransitiondefsOfProcessdef(long processdefId) {
		List<Transitiondef> out = new ArrayList<Transitiondef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("select transitiondef.id, transitiondef.transitionname, transitiondef.layout, transitiondef.orderby, transitiondef.sourcestatedefId, transitiondef.targetstatedefId from transitiondef, statedef where transitiondef.sourcestatedefId = statedef.id and statedef.processdefId = ?");
			stmt.setLong(1, processdefId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transitiondef td = new Transitiondef();
				td.setId(rs.getLong(1));
				td.setTransitionname(rs.getString(2));
				td.setLayout(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        td.setOrderby((Integer) orderby);
                	}
                }
				td.setSourcestatedefId(rs.getLong(5));
				td.setTargetstatedefId(rs.getLong(6));
				out.add(td);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}

		return out;
	}

	public static boolean createTransitiondef(Transitiondef td) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("insert into transitiondef ( transitionname, layout, orderby, sourcestatedefId, targetstatedefId ) values ( ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, td.getTransitionname());
			stmt.setString(2, td.getLayout());
            Integer orderby = td.getOrderby();
            if(orderby != null) {
                stmt.setInt(3, orderby);
            }
            else {
            	stmt.setNull(3, Types.NULL);
            }
			stmt.setLong(4, td.getSourcestatedefId());
			stmt.setLong(5, td.getTargetstatedefId());

			int affectedRows = stmt.executeUpdate();
			//System.out.println("Skrivna rader "+affectedRows);
			try {
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				generatedKeys.next();
				td.setId(generatedKeys.getLong(1));
			}
			catch(Exception e) {
				//System.out.println("Fel 1");
				e.printStackTrace();
			}
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(td != null) {
        	cache.add(td, td.getId());
        }
		return td != null;
	}

	public static boolean deleteTransitiondef(Transitiondef td) {
		return deleteTransitiondef(td.getId());
	}

	public static boolean deleteTransitiondef(long transitiondefId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection();
			stmt = conn.prepareStatement("delete from transitiondef where id = ?");
			stmt.setLong(1, transitiondefId);
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		cache.remove(transitiondefId);
		return true;
	}

	public static boolean updateTransitiondef(Transitiondef td) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("update transitiondef set transitionname = ?, layout = ?, orderby = ?, sourcestatedefId = ?, targetstatedefId = ? where id = ?");
			stmt.setString(1, td.getTransitionname());
			stmt.setString(2, td.getLayout());
            Integer orderby = td.getOrderby();
            if(orderby != null) {
                stmt.setInt(3, orderby);
            }
            else {
            	stmt.setNull(3, Types.NULL);
            }
			stmt.setLong(4, td.getSourcestatedefId());
			stmt.setLong(5, td.getTargetstatedefId());
			stmt.setLong(6, td.getId());
			stmt.executeUpdate();
			conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
        if(td != null) {
        	cache.add(td, td.getId());
        }
		return td != null;
	}

	public static List<Transitiondef> getAllTransitiondefs() {
		List<Transitiondef> out = new ArrayList<Transitiondef>();

		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
			conn = pool.reserveConnection(); // transitionname, layout, orderby, sourcestatedefId, targetstatedefId
			stmt = conn.prepareStatement("select id, transitionname, layout, orderby, sourcestatedefId, targetstatedefId from transitiondef");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Transitiondef td = new Transitiondef();
				td.setId(rs.getLong(1));
				td.setTransitionname(rs.getString(2));
				td.setLayout(rs.getString(3));
                Object orderby = rs.getObject(4);
                if(orderby != null) {
                	if(orderby instanceof Integer || orderby instanceof Long) {
                        td.setOrderby((Integer) orderby);
                	}
                }
				td.setSourcestatedefId(rs.getLong(5));
				td.setTargetstatedefId(rs.getLong(6));
				cache.add(td, td.getId());
				out.add(td);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (Exception e) {};
			try { if (stmt != null) stmt.close(); } catch (Exception e) {};
			if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
