package se.prv.errends.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbcreuse.Cache;
import se.prv.errends.dbcreuse.Commons;
import se.prv.errends.domain.Errend;

public class ErrendDB {

    private static final Cache<Errend> cache = new Cache<Errend>(20);

    public static void clearCache() {
        cache.clear();
    }

    /*
create table errend (
  id bigserial primary key,
  created timestamp not null,
  initiated bool default false,
  active bool default true,
  orderby int default null,
  adminusername varchar(40),
  personname varchar(40),
  organizationId bigint not null,
  foreign key ( organizationId ) references organization (id)
);
     */

    public static Errend getErrendFromId(long id) {
    	Errend out = cache.get(id);
        if(out != null) {
            return out;
        }
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select created, initiated, active, orderby, adminusername, personname, organizationId from errend where id = ?");
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            while(rs.next()) {
                out = new Errend();
                out.setCreated(rs.getTimestamp(1));
                out.setInitiated(rs.getBoolean(2));
                out.setActive(rs.getBoolean(3));
                if(rs.getObject(4) != null) {
                	out.setOrderby(rs.getInt(4));
                }
				out.setAdminusername(rs.getString(5));
				out.setPersonname(rs.getString(6));
                out.setOrganizationId(rs.getLong(7));
                out.setId(id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(out != null) {
        	cache.add(out, out.getId());
        }
        return out;
    }
    
	public static List<Errend> getAllErrendsOfAgent(long organizationId) {
		List<Errend> out = new ArrayList<Errend>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, created, initiated, active, orderby, adminusername, personname from errend where organizationId = ?");
		    stmt.setLong(1, organizationId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Errend e = new Errend();
		    	e.setId(rs.getLong(1));
		    	e.setCreated(rs.getTimestamp(2));
		    	e.setInitiated(rs.getBoolean(3));
		    	e.setActive(rs.getBoolean(4));
				Object orderby = rs.getObject(5);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						e.setOrderby((Integer) orderby);
					}
				}
				e.setAdminusername(rs.getString(6));
				e.setPersonname(rs.getString(7));
		    	e.setOrganizationId(organizationId);
		    	out.add(e);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

	public static List<Errend> getAllNotInitiatedErrends() {
		List<Errend> out = new ArrayList<Errend>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, created, active, orderby, adminusername, personname, organizationId from errend where initiated = false");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Errend e = new Errend();
		    	e.setId(rs.getLong(1));
		    	e.setCreated(rs.getTimestamp(2));
		    	e.setInitiated(false);
		    	e.setActive(rs.getBoolean(3));
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						e.setOrderby((Integer) orderby);
					}
				}
				e.setAdminusername(rs.getString(5));
				e.setPersonname(rs.getString(6));
		    	e.setOrganizationId(rs.getLong(7));
		    	out.add(e);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

	public static List<Errend> getAllErrendsOfActiveState(boolean state) {
		List<Errend> out = new ArrayList<Errend>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, created, initiated, orderby, adminusername, personname, organizationId from errend where active = ?");
            stmt.setBoolean(1, state);
            rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Errend e = new Errend();
		    	e.setId(rs.getLong(1));
		    	e.setCreated(rs.getTimestamp(2));
		    	e.setInitiated(rs.getBoolean(3));
		    	e.setActive(state);
				Object orderby = rs.getObject(4);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						e.setOrderby((Integer) orderby);
					}
				}
				e.setAdminusername(rs.getString(5));
				e.setPersonname(rs.getString(6));
		    	e.setOrganizationId(rs.getLong(7));
		    	out.add(e);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

    public static boolean createErrend(Errend e) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("insert into errend ( created, initiated, active, orderby, adminusername, personname, organizationId ) values ( ?, ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);

		    Date created = e.getCreated();
		    if(created == null) {
		    	created = new Date();
		    	e.setCreated(created);
		    }
		    stmt.setTimestamp(1, new java.sql.Timestamp(created.getTime()));

		    stmt.setBoolean(2, e.isInitiated());
		    stmt.setBoolean(3, e.isActive());
            Integer orderby = e.getOrderby();
            if(orderby != null) {
                stmt.setInt(4, orderby);
            }
            else {
            	stmt.setNull(4, Types.NULL);
            }
            stmt.setString(5, e.getAdminusername());
            stmt.setString(6,  e.getPersonname());
            stmt.setLong(7, e.getOrganizationId());
            
            int affectedRows = stmt.executeUpdate();
            //System.out.println("Skrivna rader "+affectedRows);
            try {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                generatedKeys.next();
                e.setId(generatedKeys.getLong(1));
            }
            catch(Exception ex) {
                //System.out.println("Fel 1");
                ex.printStackTrace();
            }
            conn.commit();
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception ex) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(e != null) {
        	cache.add(e, e.getId());
        }
        return e != null;
    }

    public static boolean deleteErrend(Errend e) {
        return deleteErrend(e.getId());
    }

    public static boolean deleteErrend(long errendId) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("delete from errend where id = ?");
            stmt.setLong(1, errendId);
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        cache.remove(errendId);
        return true;
    }

    public static boolean updateErrend(Errend e) {
        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection();// created, active, orderby, personId
            stmt = conn.prepareStatement("update errend set created = ?, initiated = ?, active = ?, orderby = ?, adminusername = ?, personname = ?, organizationId = ? where id = ?");

		    Date created = e.getCreated();
		    if(created == null) {
		    	created = new Date();
		    	e.setCreated(created);
		    }
		    stmt.setTimestamp(1, new java.sql.Timestamp(created.getTime()));
            
            stmt.setBoolean(2, e.isInitiated());
            stmt.setBoolean(3, e.isActive());
            Integer orderby = e.getOrderby();
            if(orderby != null) {
                stmt.setInt(4, orderby);
            }
            else {
            	stmt.setNull(4, Types.NULL);
            }
            stmt.setString(5, e.getAdminusername());
            stmt.setString(6, e.getPersonname());
            stmt.setLong(7, e.getOrganizationId());
            stmt.setLong(8, e.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception ex) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        if(e != null) {
        	cache.add(e, e.getId());
        }
        return e != null;
    }

    public static List<Errend> getAllErrends() {
        List<Errend> out = new ArrayList<Errend>();

        JDBCConnectionPool pool = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            pool = Commons.createPool();
            conn = pool.reserveConnection(); // created, active, orderby, personId
            stmt = conn.prepareStatement("select id, created, initiated, active, orderby, adminusername, personname, organizationId from errend");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	Errend e = new Errend();
            	e.setId(rs.getLong(1));
            	e.setCreated(rs.getTimestamp(2));
            	e.setInitiated(rs.getBoolean(3));
            	e.setActive(rs.getBoolean(4));
				Object orderby = rs.getObject(5);
				if(orderby != null) {
					if(orderby instanceof Integer || orderby instanceof Long) {
						e.setOrderby((Integer) orderby);
					}
				}
				e.setAdminusername(rs.getString(6));
				e.setPersonname(rs.getString(7));
            	e.setOrganizationId(rs.getLong(8));
            	cache.add(e, e.getId());
                out.add(e);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

}

