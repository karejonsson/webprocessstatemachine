package se.prv.errends.internal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import se.prv.errends.reuse.cli.DateFunctions;
//import se.dom.rinfo.reuse.strings.StringReoccurring;

public class Services {

	public static byte[] getByteArrayFromStream(InputStream is) throws IOException {
		if (is == null) {
			return new byte[0];
		}
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}
	
	public static void putToFile(File f, byte bytesToPut[]) throws IOException {
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(bytesToPut);
		fos.close();
	}

	/*
	public static String calculateChecksum(byte[] array) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(array);
		return (StringReoccurring.bytesToHex(thedigest)).toLowerCase();
	}
	*/
	
}
