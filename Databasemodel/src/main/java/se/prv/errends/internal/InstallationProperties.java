package se.prv.errends.internal;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {
	
	private static final String pathToConfigFile_property = "prv.errends.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/prverrends.properties";

	private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);
	 
	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}
	
	public static final String jdcb_driver_class_property = "prv.errends.jdbc.driverclass";
	public static final String jdcb_url_property = "prv.errends.jdbc.url";
	public static final String jdcb_username_property = "prv.errends.jdbc.username";
	public static final String jdcb_password_property = "prv.errends.jdcb.password";
	
	public static void main(String args[]) throws Exception {
		System.out.println("Driver klass "+getString(jdcb_driver_class_property, "inte rätt"));
	}

}
