package se.prv.errends.reuse.cli;

public class PropertyNames {

	public static final String smtpserver = "smtpserver";
	public static final String smtpauth = "smtpauth";
	public static final String systemdefaultemailaddress = "systemdefaultemailaddress";
	public static final String systemdefaultemailpassword = "systemdefaultemailpassword";
	public static final String smtpport = "smtpport";
	public static final String starttlsenable = "starttlsenable";
	public static final String blockmail = "blockmail";
	public static final String testmailreceiver = "testmailreceiver";
	public static final String smtpauthenticate = "smtpauthenticate";
	public static final String cron_schema_respite = "cron.schema.respite";
	public static final String cron_schema_appoint = "cron.schema.appoint";
	public static final String youhaveamessagetitle = "youhaveamessagetitle";
	public static final String youhaveamessagemessage = "youhaveamessagemessage";
	public static final String youhavebeenappointedtitle = "youhavebeenappointedtitle";
	public static final String youhavebeenappointedmessage = "youhavebeenappointedmessage";
	public static final String appointmentsnecessarymessage = "appointmentsnecessarymessage";
	public static final String appointmentsnecessarytitle = "appointmentsnecessarytitle";
	public static final String installation = "installation";

	public static final String date_format = "date.format";
	public static final String time_format = "time.format";
	public static final String datetime_format = "datetime.format";
	public static final String admintomellantitle = "admintomellantitle";
	public static final String admintomellanmessage = "admintomellanmessage";

}
