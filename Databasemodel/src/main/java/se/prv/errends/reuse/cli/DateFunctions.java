package se.prv.errends.reuse.cli;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import general.reuse.properties.CommandLineProperties;

public class DateFunctions {

	public static final String defaultFormatter = "yyyyMMdd_HHmm";

    public static String getDateStringFilename(Properties props) throws ParseException {
    	String formatter = props != null ? props.getProperty("dateformatter.filename", defaultFormatter) : defaultFormatter;
    	SimpleDateFormat sdf = new SimpleDateFormat(formatter);
    	Calendar calendar = Calendar.getInstance();
    	return sdf.format(calendar.getTime());
    }

    public static String getDateStringFilename(CommandLineProperties clp) throws ParseException {
    	String formatter = clp != null ? clp.getString("dateformatter.filename", defaultFormatter) : defaultFormatter;
    	SimpleDateFormat sdf = new SimpleDateFormat(formatter);
    	Calendar calendar = Calendar.getInstance();
    	return sdf.format(calendar.getTime());
    }

    public static String getDateStringReadable(Properties props) throws ParseException {
    	String formatter = props != null ? props.getProperty("dateformatter.readable", defaultFormatter) : defaultFormatter;
    	SimpleDateFormat sdf = new SimpleDateFormat(formatter);
    	Calendar calendar = Calendar.getInstance();
    	return sdf.format(calendar.getTime());
    }

    public static String getDateStringReadable(CommandLineProperties clp) throws ParseException {
    	String formatter = clp != null ? clp.getString("dateformatter.readable", defaultFormatter) : defaultFormatter;
    	SimpleDateFormat sdf = new SimpleDateFormat(formatter);
    	Calendar calendar = Calendar.getInstance();
    	return sdf.format(calendar.getTime());
    }

    public static String getDateStringReadable(CommandLineProperties clp, Date date) throws ParseException {
    	String formatter = clp != null ? clp.getString("dateformatter.readable", defaultFormatter) : defaultFormatter;
    	SimpleDateFormat sdf = new SimpleDateFormat(formatter);
    	return sdf.format(date);
    }

}
