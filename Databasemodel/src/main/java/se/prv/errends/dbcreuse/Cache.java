package se.prv.errends.dbcreuse;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Cache<K> {
	
	private Vector<Long> list = new Vector<Long>();
	private Map<Long, K> cache = new HashMap<Long, K>();
	
	private int size = 0;

	public Cache(int size) {
		this.size = size;
	}
	
	public K get(Long id) {
		return cache.get(id);
	}
	
	public void add(K obj, Long id) {
		int index = list.indexOf(id);
		if(index != -1) {
			list.remove(index);
			list.add(id);
			cache.put(id,  obj);			
		}
		else {
			while(list.size() >= size) {
				Long oldest = list.remove(0);
				cache.remove(oldest);
			}
			list.add(id);
			cache.put(id,  obj);			
		}
	}
	
	public void remove(Long id) {
		int index = list.indexOf(id);
		if(index == -1) {
			return;
		}
		list.remove(index);
		cache.remove(id);
	}

	public void clear() {
		list.clear();
		cache.clear();
	}
	
}
