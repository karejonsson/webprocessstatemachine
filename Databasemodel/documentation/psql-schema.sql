create table person (
  id bigserial primary key,
  firstname varchar(100) not null,
  familyname varchar(150) not null,
  swedishpnr varchar(12) not null,
  email varchar(150) not null,
  active bool default true,
  registerred timestamp not null,
  unique(swedishpnr)
);

create table organization (
  id bigserial primary key,
  orgname varchar(100) not null,
  orgnr varchar(14) not null,
  active bool default true,
  registerred timestamp not null,
  unique(orgnr)
);

create table agent (
  id bigserial primary key,
  personId bigint not null,
  organizationId bigint not null,
  active bool default true,
  registerred timestamp not null,
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);

create table adminuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(60) not null,
  active bool default true,
  superprivilegies bool default false,
  orderby int default null,
  personId bigint not null,
  unique(personId),
  foreign key ( personId ) references person (id)
);

create table organizationuser (
  id bigserial primary key,
  username varchar(30) not null,
  password varchar(60) not null,
  active bool default true,
  superprivilegies bool default false,
  personId bigint not null,
  organizationId bigint not null,
  foreign key ( personId ) references person (id),
  foreign key ( organizationId ) references organization (id)
);

create table errend (
  id bigserial primary key,
  created timestamp not null,
  initiated bool default false,
  active bool default true,
  orderby int default null,
  adminusername varchar(40),
  personname varchar(40),
  organizationId bigint not null,
  foreign key ( organizationId ) references organization (id)
);

create table processfile (
  id bigserial primary key,
  filename varchar(50) not null,
  description varchar(250) default null,
  data bytea,
  orderby int default null,
  adminuserId bigint not null,
  unique(filename, description),
  foreign key ( adminuserId ) references adminuser (id)
);

create table processdef (
  id bigserial primary key,
  processname varchar(50) not null,
  description varchar(250) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);

create table statedef (
  id bigserial primary key,
  statename varchar(50) not null,
  layout varchar(30000) default null,
  orderby int default null,
  processdefId bigint not null,
  foreign key ( processdefId ) references processdef (id)
);

create table transitiondef (
  id bigserial primary key,
  transitionname varchar(50) not null,
  layout varchar(30000) default null,
  orderby int default null,
  sourcestatedefId bigint not null,
  targetstatedefId bigint not null,
  foreign key ( sourcestatedefId ) references statedef (id),
  foreign key ( targetstatedefId ) references statedef (id)
);

create table roledef (
  id bigserial primary key,
  rolename varchar(50) not null,
  orderby int default null,
  processdefId bigint not null,
  foreign key ( processdefId ) references processdef (id)
);

create table actor (
  id bigserial primary key,
  orderby int default null,
  roledefId bigint default null,
  agentId bigint not null,
  errendId bigint default null,
  unique(roledefId, agentId, errendId),
  foreign key ( agentId ) references agent (id),
  foreign key ( roledefId ) references roledef (id),
  foreign key ( errendId ) references errend (id)
);

create table session (
  id bigserial primary key,
  start timestamp not null,
  finish timestamp,
  actorId bigint not null,
  foreign key ( actorId ) references actor (id)
);

create table transition (
  id bigserial primary key,
  transitiondefId bigint not null,
  sessionId bigint not null,
  predecessorId bigint,
  foreign key ( transitiondefId ) references transitiondef (id),
  foreign key ( sessionId ) references session (id),
  foreign key ( predecessorId ) references transition (id)
);

create table managederrend (
  id bigserial primary key,
  errendId bigint not null,
  processdefId bigint not null,
  currentstateId bigint not null,
  lasttransitionId bigint,
  unique(errendId),
  foreign key ( errendId ) references errend (id),
  foreign key ( processdefId ) references processdef (id),
  foreign key ( currentstateId ) references statedef (id),
  foreign key ( lasttransitionId ) references transition (id)
);

create table transitioncomment (
  id bigserial primary key,
  title varchar(100) default null,
  comment varchar(30000) default null,
  sessionId bigint not null,
  transitionId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( transitionId ) references transition (id)
);

create table errendcomment (
  id bigserial primary key,
  title varchar(100) default null,
  comment varchar(30000) default null,
  sessionId bigint not null,
  errendId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( errendId ) references errend (id)
);

create table persistentvalues (
  id bigserial primary key,
  key varchar(100) not null,
  value varchar(300) not null,
  unique(key)
);

create table message (
  id bigserial primary key,
  title varchar(80) default null,
  writtenmessage varchar(2000) default null,
  unread bool default true not null,
  send timestamp not null,
  sourceActorId bigint,
  targetActorId bigint,
  foreign key ( sourceActorId ) references actor (id),
  foreign key ( targetActorId ) references actor (id)
);

create table stateexpiration (
  id bigserial primary key,
  managederrendId bigint not null,
  transitiondefId bigint not null,
  expirey timestamp not null,
  foreign key ( managederrendId ) references managederrend (id),
  foreign key ( transitiondefId ) references transitiondef (id)
);

create table reporttemplate (
  id bigserial primary key,
  filename varchar(180) not null,
  reporttitle varchar(250) default null,
  description varchar(1000) default null,
  jasperreportdata bytea,
  parameterdata bytea,
  adminuserId bigint not null,
  unique(reporttitle, description),
  foreign key ( adminuserId ) references adminuser (id)
);

create table reporttemplatereuse (
  id bigserial primary key,
  processdefId bigint not null,
  reporttemplateId bigint not null,
  foreign key ( processdefId ) references processdef (id),
  foreign key ( reporttemplateId ) references reporttemplate (id)
);

create table report (
  id bigserial primary key,
  reporttitle varchar(100) default null,
  data bytea,
  reporttemplatereuseId bigint not null,
  errendId bigint not null,
  actorId bigint not null,
  foreign key ( reporttemplatereuseId ) references reporttemplatereuse (id),
  foreign key ( errendId ) references errend (id),
  foreign key ( actorId ) references actor (id)
);

create table functiondef (
  id bigserial primary key,
  functionname varchar(100) not null,
  typename varchar(10) default null,
  source varchar(30000) default null,
  orderby int default null,
  processfileId bigint,
  foreign key ( processfileId ) references processfile (id)
);

create table functiondefreuse (
  id bigserial primary key,
  processdefId bigint not null,
  functiondefId bigint not null,
  unique(processdefId, functiondefId),
  foreign key ( processdefId ) references processdef (id),
  foreign key ( functiondefId ) references functiondef (id)
);

create table organisationrolegroup (
  id bigserial primary key,
  organisationId bigint not null,
  roledefId bigint not null,
  name varchar(30) default null,
  unique(organisationId, roledefId, name),
  foreign key ( organisationId ) references organization (id),
  foreign key ( roledefId ) references roledef (id)
);

create table organisationgroupmember (
  id bigserial primary key,
  agentId bigint not null,
  rolegroupId bigint not null,
  unique(agentId, rolegroupId),
  foreign key ( agentId ) references agent (id),
  foreign key ( rolegroupId ) references organisationrolegroup (id)
);

create table dossierdef (
  id bigserial primary key,
  dossiertypename varchar(50) not null,
  description varchar(250) default null,
  processfileId bigint default null,
  foreign key ( processfileId ) references processfile (id)
);

create table dossierdoctypedef (
  id bigserial primary key,
  dossierdoctypename varchar(50) not null,
  dossierdefId bigint not null,
  foreign key ( dossierdefId ) references dossierdef (id)
);

create table dossierdocattributedef (
  id bigserial primary key,
  dossierdocattributename varchar(50) not null,
  dossierdefId bigint not null,
  foreign key ( dossierdefId ) references dossierdef (id)
);

create table dossier (
  id bigserial primary key,
  name varchar(50) not null,
  description varchar(1200) default null,
  sessionId bigint not null,
  dossierdefId bigint not null,
  errendId bigint default null,
  unique(errendId),
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdefId ) references dossierdef (id),
  foreign key ( errendId ) references errend (id)
);

create table dossierevent (
  id bigserial primary key,
  description varchar(1200) not null,
  code int not null,
  systeminfo varchar(800) not null,
  timeofevent timestamp not null,
  sessionId bigint not null,
  dossierId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierId ) references dossier (id)
);

create table document (
  id bigserial primary key,
  filename varchar(180) not null,
  mimetype varchar(180) not null,
  active bool default true not null,
  origin varchar(600) not null,
  description varchar(1200) default null,
  dateOfReception timestamp not null,
  data bytea,
  sessionId bigint not null,
  dossierdoctypedefId bigint default null,
  dossierId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdoctypedefId ) references dossierdoctypedef (id),
  foreign key ( dossierId ) references dossier (id)
);

create table documentevent (
  id bigserial primary key,
  description varchar(1200) default null,
  code int not null,
  systeminfo varchar(800) not null,
  timeofevent timestamp not null,
  sessionId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( documentId ) references document (id)
);

create table documentattribute (
  id bigserial primary key,
  sessionId bigint not null,
  dossierdocattributedefId bigint not null,
  documentId bigint not null,
  foreign key ( sessionId ) references session (id),
  foreign key ( dossierdocattributedefId ) references dossierdocattributedef (id),
  foreign key ( documentId ) references document (id)
);


