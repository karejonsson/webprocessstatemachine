insert into person (firstname, familyname, swedishpnr, email, active, registerred) values
('Super', 'User', '000001010008', 'not@applicable.se', true, '20170927');

insert into adminuser (username, password, superprivilegies, personId) values
('super', 'e542050f239e0b8f53e70464bd9665e1', true, 1);

insert into persistentvalues (key, value) values ('smtpserver', 'mailrelay.prv.se');
insert into persistentvalues (key, value) values ('smtpauth', 'false');
insert into persistentvalues (key, value) values ('systemdefaultemailaddress', 'support.process@prv.se');
insert into persistentvalues (key, value) values ('systemdefaultemailpassword', 'abc123');
insert into persistentvalues (key, value) values ('smtpport', '25');
insert into persistentvalues (key, value) values ('starttlsenable', 'true');
insert into persistentvalues (key, value) values ('blockmail', 'true');
insert into persistentvalues (key, value) values ('testmailreceiver', 'kare.jonsson@prv.se');
insert into persistentvalues (key, value) values ('installation', 'Identifiering för denna installation');
insert into persistentvalues (key, value) values ('smtpauthenticate', 'true');
insert into persistentvalues (key, value) values ('cron.schema.respite', '0 0 0/2 * * ?');
insert into persistentvalues (key, value) values ('cron.schema.appoint', '0 0 0/2 * * ?');

insert into persistentvalues (key, value) values ('youhaveamessagetitle', 'you-have-a-message-title');
insert into persistentvalues (key, value) values ('youhaveamessagemessage', 'you-have-a-message-message');
insert into persistentvalues (key, value) values ('youhavebeenappointedtitle', 'you-have-been-appointed-title');
insert into persistentvalues (key, value) values ('youhavebeenappointedmessage', 'you-have-been-appointed-message');

insert into persistentvalues (key, value) values ('appointmentsnecessarymessage', 'appointments-necessary-message');
insert into persistentvalues (key, value) values ('appointmentsnecessarytitle', 'appointments-necessary-title');

insert into persistentvalues (key, value) values ('date.format', 'yyyy-MM-dd');
insert into persistentvalues (key, value) values ('time.format', 'HH:mm:SS');
insert into persistentvalues (key, value) values ('datetime.format', 'yyyy-MM-dd HH:mm:SS');

insert into functiondef (functionname, typename, source) values ('you-have-been-appointed-message', 'string', 'string-producer you-have-been-appointed-message
require-current actor-instance superactor
require-context actor-instance reciever
require-current installation-identifier installationid
groovy-evaluable """"""
return """Hej """+reciever.agent.person.firstname+"""

Du har fått rollen """+reciever.roledef.rolename+""" i ett ärende med benämningarna.

PRVs namn: """+reciever.errend.adminusername+"""
Ansökandes namn: """+reciever.errend.personname+"""

Tilldelningen har gjorts av """+superactor.agent.person.firstname+""" """+superactor.agent.person.familyname+""".
Brevet har automatgenererats från systemet """+installationid+"""
"""; 
""""""
');

insert into functiondef (functionname, typename, source) values ('you-have-been-appointed-title', 'string', 'string-producer you-have-been-appointed-title
groovy-evaluable """"""
return """Tilldelning av roll i ärende"""; 
""""""
');

insert into functiondef (functionname, typename, source) values ('you-have-a-message-message', 'string', 'string-producer you-have-a-message-message
require-current actor-instance sender
require-current message-instance message
require-context actor-instance reciever
require-current installation-identifier installationid
groovy-evaluable """"""
def err = reciever.errend != null ? reciever.errend : null;
String role = "Meddelandet skickas utan specificerad roll.";
if(reciever.roledefId != null) {
  role = """Din roll är """+reciever.roledef.rolename+"""."""; 
}
String body = "Du har fått ett internt meddelande";
if(err != null) {
  body = """Du har fått ett meddelande rörande ett ärende med benämningarna

PRVs namn: """+reciever.errend.adminusername+"""
Ansökandes namn: """+reciever.errend.personname;
}
return """Hej """+reciever.agent.person.firstname+"""

"""+body+"""

"""+role+"""

Meddelandet har skickats av """+sender.agent.person.firstname+""" """+sender.agent.person.familyname+""".
Meddelandets titel är """+message.title+""".
Brevet har automatgenererats från systemet """+installationid+""" 
"""; 
""""""
');

insert into functiondef (functionname, typename, source) values ('you-have-a-message-title', 'string', 'string-producer you-have-a-message-title
groovy-evaluable """"""
return """Internt meddelande att läsa"""; 
""""""
');

insert into functiondef (functionname, typename, source) values ('appointments-necessary-title', 'string', 'string-producer appointments-necessary-title
groovy-evaluable """"""
return """Tilldelning av roller i ärende behövs"""; 
""""""
');


insert into functiondef (functionname, typename, source) values ('appointments-necessary-message', 'string', 'string-producer appointments-necessary-message
require-prepared all-not-initiated-errends errends
require-current superuser-instance superactor
require-current installation-identifier installationid
groovy-evaluable """"""
def list = new StringBuffer()
for(errend in errends) {
   list.append(errend.personname+" "+errend.organization.orgname+" "+errend.organization.orgnr+"\n")
}
return """Hej """+superactor.agent.person.firstname+"""

Det finns """+errends.size+""" ärende"""+(errends.size != 1 ? "n" : "" )+""" som behöver initieras. De är 

"""+list+"""
Brevet har automatgenererats från systemet """+installationid+"""
"""; 
""""""
');


