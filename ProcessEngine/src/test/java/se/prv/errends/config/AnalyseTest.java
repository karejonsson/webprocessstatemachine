package se.prv.errends.config;

import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;

import org.junit.After;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.processengine.reuse.StringFileManipulation;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.dbc.AdminuserTest;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Processfile;

public class AnalyseTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    @Test
    public void parseConfigs() throws Exception {
        ClassLoader cl = AnalyseTest.class.getClassLoader();
        InputStream is = cl.getResourceAsStream("simple1.graphml");
        AdminuserTest.recursiveSetup();
        Adminuser au = AdminuserDB.getAllAdminusers().get(0);

        Processfile pf = new Processfile();
        pf.setDescription("En beskrivning av denna fil");
        pf.setAdminuserId(au.getId());
        pf.setFilename("simple1.graphml");
        pf.save();

        byte[] data = StringFileManipulation.readFullyToBytearray(is);
        ProcessfileDB.setData(pf.getId(), data);

        CommonDB.clearCache();

        DBInitialisation.initializeProcessNodes(pf, "Some description", "Some process name");
    }

}
