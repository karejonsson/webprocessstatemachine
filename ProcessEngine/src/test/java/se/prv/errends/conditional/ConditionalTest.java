package se.prv.errends.conditional;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.AdminuserTest;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.initialize.DBInitializationTest;
import se.prv.errends.initialize.InitializationReporter;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class ConditionalTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    @Test
    public void test() throws Exception {
        ClassLoader cl = ConditionalTest.class.getClassLoader();
        InputStream is = cl.getResourceAsStream("conditional1.graphml");

        AdminuserTest.recursiveSetup();
        Adminuser au = AdminuserDB.getAllAdminusers().get(0);

        Processfile pf = new Processfile();
        pf.setDescription("En beskrivning av denna fil");
        pf.setAdminuserId(au.getId());
        pf.setFilename("conditional1.graphml");
        pf.save();

        byte[] data = StringFileManipulation.readFullyToBytearray(is);
        ProcessfileDB.setData(pf.getId(), data);
		
    	InitializationReporter ir = new InitializationReporter();
    	DBInitialisation.inititializeProcessNodes(pf, "Some description", "Some process name", ir);
    	
    	System.out.println("Report :"+ir.getReport());

    	List<String> booleanFs = ir.getBooleanFunctions();
    	List<String> stringFs = ir.getStringFunctions();

    	System.out.println("Boolean functions ("+booleanFs.size()+")");
    	for(String bf : booleanFs) {
    		System.out.println(bf);
    	}
    	System.out.println("String functions ("+stringFs.size()+")");
    	for(String sf : stringFs) {
    		System.out.println(sf);
    	}
    	
    }

}
