package se.prv.errends.action;

import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.mgmt.EmailManagement;
import se.prv.errends.reuse.cli.PropertyNames;

public class EmailActionPerformerTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws Exception {
		CommonDB.clearCache();
		pool = TestUtilities.setupPool();

		PersistentValuesDB.setStringValue(PropertyNames.smtpserver, "mailrelay.prv.se");
		PersistentValuesDB.setStringValue(PropertyNames.smtpauth, "false");
		PersistentValuesDB.setStringValue(PropertyNames.systemdefaultemailaddress, "support.process@prv.se");
		PersistentValuesDB.setStringValue(PropertyNames.systemdefaultemailpassword, "abc123");
		PersistentValuesDB.setStringValue(PropertyNames.smtpport, "25");
		PersistentValuesDB.setStringValue(PropertyNames.starttlsenable, "false");
		PersistentValuesDB.setStringValue(PropertyNames.blockmail, "true");
		PersistentValuesDB.setStringValue(PropertyNames.testmailreceiver, "kare.jonsson@prv.se");
		PersistentValuesDB.setStringValue(PropertyNames.smtpauthenticate, "false");
		PersistentValuesDB.setStringValue(PropertyNames.cron_schema_respite, "0 0 0/2 * * ?");
		PersistentValuesDB.setStringValue(PropertyNames.cron_schema_appoint, "0 0 0/2 * * ?");
		PersistentValuesDB.setStringValue(PropertyNames.time_format, "HH:mm:SS");
		PersistentValuesDB.setStringValue(PropertyNames.datetime_format, "yyyy-MM-dd hh:mm:ss");
		PersistentValuesDB.setStringValue(PropertyNames.date_format, "yyyy-MM-dd");
	}
	
	@After
	public void tearDown() throws Exception {
		TestUtilities.destroyPool(pool);
		CommonDB.clearCache();
	}

	@Test
	public void sendTestMail() throws Exception {
		Person p = new Person();
		p.setActive(true);
		p.setEmail("kare.jonsson@prv.se");
		p.setFamilyname("Jonsson");
		p.setFirstname("Kåre");
		p.setSwedishpnr("196905010150");
		p.setRegisterred(new Date(System.currentTimeMillis()));

		Agent oa = new Agent();
		oa.setPerson(p);
		Actor actor = new Actor();
		actor.setAgent(oa);
		String message = EmailManagement.createMessageToSendByInlineAtUserTransition(actor, null, "return \"Hej\";");
		System.out.println("1: "+message);
		Assert.assertTrue(message.equals("Hej"));
		
		message = EmailManagement.createMessageToSendByInlineAtUserTransition(actor, null, "return \"Hej \"+actor.agent.person.firstname;");
		System.out.println("2: "+message);
		Assert.assertTrue(message.equals("Hej Kåre"));
	}
	
}
