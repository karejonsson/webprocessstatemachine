package se.prv.errends.workdays;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Set;

import de.jollyday.Holiday;
import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;

public class TestSomeCalculations {
	
	public static void main(String args[]) {
		LocalDate ld = LocalDate.of(2018, 4, 26);
		for(int i = 1 ; i < 10 ; i++) {
			System.out.println(""+i+" dagar senare "+add(i, ld)+", överhoppade "+skipped(i, ld));
		}
		
	}

	public static LocalDate add(int workdays) {
		return add(workdays, LocalDate.now());
	}

	public static LocalDate add(int workdays, LocalDate startdate) {
		HolidayManager m = HolidayManager.getInstance(HolidayCalendar.SWEDEN);
		int y = startdate.getYear(); 
		Set<Holiday> holidays = m.getHolidays(y);
		holidays.addAll(m.getHolidays(y+1));
		holidays.addAll(m.getHolidays(y+2));
		return add(startdate, workdays, holidays);
	}

	public static LocalDate add(int workdays, Set<Holiday> holidays) {
		return add(LocalDate.now(), workdays, holidays);
	}

	public static LocalDate add(LocalDate date, int workdays, Set<Holiday> holidays) {
	    if (workdays < 1) {
	        return date;
	    }

	    LocalDate result = date;
	    int addedDays = 0;
	    while (addedDays < workdays) {
	        result = result.plusDays(1);
	        if(!isWeekend(result) && !isWithin(result, holidays)) {
	            ++addedDays;
	        }
	    }

	    return result;
	}

	public static int skipped(int workdays) {
		return skipped(workdays, LocalDate.now());
	}

	public static int skipped(int workdays, LocalDate startdate) {
		HolidayManager m = HolidayManager.getInstance(HolidayCalendar.SWEDEN);
		int y = startdate.getYear(); 
		Set<Holiday> holidays = m.getHolidays(y);
		holidays.addAll(m.getHolidays(y+1));
		holidays.addAll(m.getHolidays(y+2));
		return skipped(startdate, workdays, holidays);
	}
	
	public static int skipped(int workdays, Set<Holiday> holidays) {
		return skipped(LocalDate.now(), workdays, holidays);
	}

	public static int skipped(LocalDate date, int workdays, Set<Holiday> holidays) {
	    if (workdays < 1) {
	        return 0;
	    }

	    LocalDate result = date;
	    int addedDays = 0;
	    int skipped = 0;
	    while (addedDays < workdays) {
	        result = result.plusDays(1);
	        if(!isWeekend(result) && !isWithin(result, holidays)) {
	            ++addedDays;
	        }
	        else {
	        	skipped++;
	        }
	    }

	    return skipped;
	}
	
	public static boolean isWeekend(LocalDate date) {
        if (date.getDayOfWeek() == DayOfWeek.SATURDAY ||
        		date.getDayOfWeek() == DayOfWeek.SUNDAY) {
	            return true;
	        }
		return false;
	}
	
	public static boolean isWithin(LocalDate date, Set<Holiday> holidays) {
		for(Holiday holiday : holidays) {
			if(holiday.getDate().equals(date)) {
				return true;
			}
		}
		return false;
	}
	
}
