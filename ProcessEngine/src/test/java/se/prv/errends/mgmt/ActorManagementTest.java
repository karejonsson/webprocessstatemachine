package se.prv.errends.mgmt;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.AdminuserTest;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Person;

public class ActorManagementTest {

    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        CommonDB.clearCache();
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
        CommonDB.clearCache();
    }

    @Test
    public void errendlessActor() throws Exception {
    	AdminuserTest.recursiveSetup();
    	Person p = PersonDB.getAllActivePersons().get(0);
    	Assert.assertTrue(ActorDB.getAllActors().size() == 0);
    }
    
}
