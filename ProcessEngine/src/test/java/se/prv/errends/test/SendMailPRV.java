package se.prv.errends.test;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailPRV {

	public static boolean sendMail(String from, String to, String server, String subject, String htmlMsg) throws Exception {

		try {
			if (to.length() == 0) return true;

			Properties props = new Properties();
			props.put("mail.smtp.host", server);

			java.util.StringTokenizer tok = new java.util.StringTokenizer(to,";");
			InternetAddress[] arr = new InternetAddress[tok.countTokens()];
			int i = 0;
			while (tok.hasMoreTokens()) arr[i++] = new InternetAddress(tok.nextToken());

			javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, null);

			MimeMessage mimeMsg = new MimeMessage(mailSession);
			mimeMsg.setFrom(new InternetAddress(from));
			mimeMsg.setRecipients(javax.mail.Message.RecipientType.TO, arr);
			mimeMsg.setSubject(subject);
			mimeMsg.setSentDate(new Date());
			mimeMsg.setContent(htmlMsg,"text/html");
			mimeMsg.saveChanges();

			Transport.send(mimeMsg);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void main(String[] args) throws Exception {
		sendMail("kare.jonsson@prv.se", "kare.jonsson@prv.se", "mailrelay.prv.se", "subject", "message");
	}

}
