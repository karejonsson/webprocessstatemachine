package se.prv.errends.initialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.config.AnalyseTest;
import se.prv.errends.dbc.AdminuserTest;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class DBInitializationTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
		CommonDB.clearCache();
		pool = TestUtilities.setupPool();
	}

	@After
	public void tearDown() throws Exception {
		TestUtilities.destroyPool(pool);
		CommonDB.clearCache();
	}
	
	private static String getResourceContent(String filesymbol) throws UnsupportedEncodingException {
		ClassLoader cl = DBInitializationTest.class.getClassLoader();
		InputStream is = cl.getResourceAsStream("producers/"+filesymbol+".producer");
		byte[] internalMacros = null;
		try {
			internalMacros = StringFileManipulation.readFullyToBytearray(is);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return new String(internalMacros, "UTF-8");
	}
	
	private static void setupFunctionDef(String functionname, String filenamesymbol) throws UnsupportedEncodingException {
		FunctionDef fd = new FunctionDef();
		fd.setFunctionname(functionname);
		fd.setTypename("string");
		fd.setSource(getResourceContent(filenamesymbol));
		fd.save();
	}

	public static void initializeData() throws Exception {
		AdminuserTest.recursiveSetup();
		
		PersistentValuesDB.setStringValue("smtpserver", "mailrelay.prv.se");
		PersistentValuesDB.setStringValue("smtpauth", "false");
		PersistentValuesDB.setStringValue("systemdefaultemailaddress", "support.process@prv.se");
		PersistentValuesDB.setStringValue("systemdefaultemailpassword", "abc123");
		PersistentValuesDB.setStringValue("smtpport", "25");
		PersistentValuesDB.setStringValue("starttlsenable", "true");
		PersistentValuesDB.setStringValue("blockmail", "false");
		PersistentValuesDB.setStringValue("testmailreceiver", "kare.jonsson@prv.se");
		PersistentValuesDB.setStringValue("smtpauthenticate", "true");

		PersistentValuesDB.setStringValue("youhaveamessagetitle", "you-have-a-message-title");
		setupFunctionDef("you-have-a-message-title", "youhaveamessagetitle");
		
		PersistentValuesDB.setStringValue("youhaveamessagemessage", "you-have-a-message-message");
		setupFunctionDef("you-have-a-message-message", "youhaveamessagemessage");
		
		PersistentValuesDB.setStringValue("youhavebeenappointedtitle", "you-have-been-appointed-title");
		setupFunctionDef("you-have-been-appointed-title", "youhavebeenappointedtitle");
		
		PersistentValuesDB.setStringValue("youhavebeenappointedmessage", "you-have-been-appointed-message");
		setupFunctionDef("you-have-been-appointed-message", "youhavebeenappointedmessage");
		
		PersistentValuesDB.setStringValue("date.format", "yyyy-MM-dd");
		PersistentValuesDB.setStringValue("time.format", "HH:mm:SS");
		PersistentValuesDB.setStringValue("datetime.format", "yyyy-MM-dd HH:mm:SS");

		Person p = new Person();
		p.setActive(true);
		p.setEmail("kare.jonsson@prv.se");
		p.setFamilyname("Jonsson");
		p.setFirstname("Kåre");
		p.setSwedishpnr("196905010150");
		p.setRegisterred(new Date(System.currentTimeMillis()));
		p.save();

		Adminuser au = new Adminuser();
		au.setActive(true);
		au.setPassword(Adminuser.calculateChecksum("super"));
		au.setPersonId(p.getId());
		au.setSuperprivilegies(true);
		au.setUsername("kare");
		au.save();

		ClassLoader cl = AnalyseTest.class.getClassLoader();
		InputStream is = cl.getResourceAsStream("simple1.graphml");

		Processfile pf = new Processfile();
		pf.setDescription("En beskrivning av denna fil");
		pf.setAdminuserId(au.getId());
		pf.setFilename("simple1.graphml");
		ProcessfileDB.createProcessfile(pf);
		byte[] data = StringFileManipulation.readFullyToBytearray(is);
		ProcessfileDB.setData(pf.getId(), data);
	}
	
	@Test
	public void deleteProcessdef() throws Exception {
		initializeData();
		Processfile pf = ProcessfileDB.getAllProcessfiles().get(0);
		DBInitialisation.initializeProcessNodes(pf, "Some description", "Some process name");
		CommonDB.clearCache();
		Processdef pd = ProcessdefDB.getAllProcessdefs().get(0);
		pd.deleteRecursive();
	}

}
