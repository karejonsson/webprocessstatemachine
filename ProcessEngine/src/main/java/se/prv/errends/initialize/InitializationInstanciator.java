package se.prv.errends.initialize;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public class InitializationInstanciator implements InitializationCallbacks {

	@Override
	public Processdef getProcessdef(Processfile pf, String description, String processname) {
		Processdef pd = new Processdef();
		pd.setProcessfile(pf);
		pd.setDescription(description);
		pd.setProcessname(processname);
		pd.save();
		return pd;
	}

	@Override
	public Statedef getStatedef(Long id, String longText, String shortText) {
		Statedef sd = new Statedef();
		sd.setProcessdefId(id);
		sd.setLayout(longText);
		sd.setStatename(shortText);
		sd.save();
		return sd;
	}

	@Override
	public NodeConfiguration createNodeConfig(Statedef sd, String nodeid) {
		try {
			String directions = sd.getLayout();
	        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(directions.getBytes()));
	        return PrvErrendNodeConfigParser.parseNodeConfig(isr, "Node " + nodeid);
		}
		catch(Exception e) {
			return null;
		}
	}

	@Override
	public Transitiondef getTransitiondef(Long sourcestatedefId, Long targetstatedefId, String layout, String transitionname) {
		Transitiondef td = new Transitiondef();
		td.setSourcestatedefId(sourcestatedefId);
		td.setTargetstatedefId(targetstatedefId);
		td.setLayout(layout);
		td.setTransitionname(transitionname);
		td.save();
		return td;
	}

	@Override
	public EdgeConfiguration createEdgeConfig(Transitiondef td, String edgeid) {
		try {
			String directions = td.getLayout();
	        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(directions.getBytes()));
	        return PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Edge "+edgeid);
		}
		catch(Exception e) {
			return null;
		}
	}

	@Override
	public Roledef createRoledef(Long id, String role) {
        Roledef rd = new Roledef();
        rd.setProcessdefId(id);
        rd.setRolename(role);
        rd.save();
        return rd;
	}

	@Override
	public void notifyNoLayoutDefined(Statedef sd, String nodeid) {
		// TODO Auto-generated method stub
		
	}

}
