package se.prv.errends.initialize;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public class InitializationReporter implements InitializationCallbacks {
	
	private StringBuffer report = new StringBuffer();
	private int errorCount = 0;

	@Override
	public Processdef getProcessdef(Processfile pf, String description, String processname) {
		Processdef pd = new Processdef();
		pd.setProcessfile(pf);
		pd.setDescription(description);
		pd.setProcessname(processname);
		pd.setId(1l);
		report.append("Processfile for process \""+processname+"\" with description \""+description+"\" created\n");
		return pd;
	}
	
	private long sdid = 0l;
	
	@Override
	public Statedef getStatedef(Long id, String longText, String shortText) {
		Statedef sd = new Statedef();
		sd.setProcessdefId(id);
		sd.setLayout(longText);
		sd.setStatename(shortText);
		sd.setId(sdid++);
		report.append("State definition named \""+shortText+"\" created. Numeric id is "+sd.getId()+", textual id is "+shortText+"\n");
		return sd;
	}

	@Override
	public NodeConfiguration createNodeConfig(Statedef sd, String nodeid) {
		//System.out.println("Reporter createNodeConfig");
		try {
			String directions = sd.getLayout();
	        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(directions.getBytes()));
			NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "Node " + nodeid);
			report.append("State definition named \""+sd.getStatename()+"\" has parseable layout specification.\n"+
			        "-> State id is "+sd.getId()+"\n");
			String function = nc.getExplanationFunctionName();
			if(!stringFunctions.contains(function)) {
				stringFunctions.add(function);
			}
			return nc;
		}
		catch(Exception e) {
			e.printStackTrace();
			report.append("State definition named \""+sd.getStatename()+"\" has NON parseable layout specification. --- ERROR\n");
			errorCount++;
			return null;
		}
	}
	
	private long tdid = 0l;
	
	@Override
	public Transitiondef getTransitiondef(Long sourcestatedefId, Long targetstatedefId, String layout, String transitionname) {
		Transitiondef td = new Transitiondef();
		td.setSourcestatedefId(sourcestatedefId);
		td.setTargetstatedefId(targetstatedefId);
		td.setLayout(layout);
		td.setTransitionname(transitionname);
		td.setId(tdid++);
		report.append("Transitiondefinition named \""+transitionname+"\" created. Numeric id is "+td.getId()+"\n");
		return td;
	}
	
	private List<String> stringFunctions = new ArrayList<String>();
	
	public List<String> getStringFunctions() {
		return stringFunctions;
	}
	
	private List<String> booleanFunctions = new ArrayList<String>();
	
	public List<String> getBooleanFunctions() {
		return booleanFunctions;
	}

	@Override
	public EdgeConfiguration createEdgeConfig(Transitiondef td, String edgeid) {
		try {
			String directions = td.getLayout();
	        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(directions.getBytes()));
			EdgeConfiguration ec = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Edge "+edgeid);
			report.append("Transition defintion named \""+td.getTransitionname()+"\" has parseable specification.\n"+
			        "-> Source state id is "+td.getSourcestatedefId()+", target state id is "+td.getTargetstatedefId()+"\n");

			List<String> functions = ec.getStringFunctions(); 
			for(String function : functions) {
				if(!stringFunctions.contains(function)) {
					stringFunctions.add(function);
				}
			}
			functions = ec.getBooleanFunctions();
			for(String function : functions) {
				if(!booleanFunctions.contains(function)) {
					booleanFunctions.add(function);
				}
			}			
			return ec;
		}
		catch(Exception e) {
			e.printStackTrace();
			report.append("Transition definition named \""+td.getTransitionname()+"\" has NON parseable specification. --- ERROR\n");
			errorCount++;
			return null;
		}
	}

	private long rdid = 0l;

	@Override
	public Roledef createRoledef(Long id, String role) {
        Roledef rd = new Roledef();
        rd.setProcessdefId(id);
        rd.setRolename(role);
        rd.setId(rdid++);
		report.append("Role definition named \""+role+"\" created\n");
       return rd;
	}
	
	public String getReport() {
		if(errorCount == 0) {
			report.append("There are no errors in the file.\n");
		}
		else {
			report.append("There are "+errorCount+" errors in the file.\n");
		}
		if(stringFunctions.size() == 0) {
			report.append("There are no referred string producing functions in the file.\n");
		}
		else {
			report.append("The following "+stringFunctions.size()+" string producing function/s are referred in the file.\n");
			for(String stringFunction : stringFunctions) {
				report.append(stringFunction+"\n");
			}
		}
		if(booleanFunctions.size() == 0) {
			report.append("There are no referred boolean producing functions in the file.\n");
		}
		else {
			report.append("The following "+booleanFunctions.size()+" boolean producing function/s are referred in the file.\n");
			for(String booleanFunction : booleanFunctions) {
				report.append(booleanFunction+"\n");
			}
		}
		return report.toString();
	}

	@Override
	public void notifyNoLayoutDefined(Statedef sd, String nodeid) {
		report.append("State \""+sd.getStatename()+"\" with id "+nodeid+" has no layout specification\n");
	}

}
