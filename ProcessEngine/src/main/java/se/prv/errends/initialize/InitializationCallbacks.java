package se.prv.errends.initialize;

import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public interface InitializationCallbacks {

	Processdef getProcessdef(Processfile pf, String description, String processname);

	Statedef getStatedef(Long id, String longText, String shortText);

	NodeConfiguration createNodeConfig(Statedef sd, String nodeid);

	Transitiondef getTransitiondef(Long sourcestatedefId, Long targetstatedefId, String layout, String transitionname);

	EdgeConfiguration createEdgeConfig(Transitiondef td, String edgeid);

	Roledef createRoledef(Long id, String role);

	void notifyNoLayoutDefined(Statedef sd, String nodeid);

}
