package se.prv.errends.initialize;

import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.dossierconfig.parse.PrvDossierConfigParser;
import se.prv.errends.dossierconfig.struct.DossierConfiguration;
import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierDocattributeDef;
import se.prv.errends.domain.DossierDoctypeDef;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Roledef;
import se.prv.readgraphml.GraphmlEdge;
import se.prv.readgraphml.GraphmlGraph;
import se.prv.readgraphml.GraphmlNode;
import se.prv.readgraphml.ReadGraphmlSAX;

import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

import java.io.InputStreamReader;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;


import java.io.ByteArrayInputStream;

public class DBInitialisation {

    public static Processdef initializeProcessNodes(Processfile pf, String description, String processname) throws Exception {
    	return inititializeProcessNodes(pf, description, processname, new InitializationInstanciator());
    }
	
    public static DossierDef initializeDossierConfigNodes(Processfile pf, String description, String processname) throws Exception {
    	return inititializeDossierConfigNodes(pf, description, processname);
    }

    public static String getInitiationReport(Processfile pf, String description, String processname) throws Exception {
    	InitializationReporter ir = new InitializationReporter();
    	inititializeProcessNodes(pf, description, processname, ir);
    	return ir.getReport();
    }
	
    public static Processdef inititializeProcessNodes(Processfile pf, String description, String processname, InitializationCallbacks ic) throws Exception {
        byte[] data = pf.getData();
        GraphmlGraph gg = ReadGraphmlSAX.getGraph(new ByteArrayInputStream(data));

        Processdef pd = ic.getProcessdef(pf, description, processname);
        		
        Map<String, Long> m = new HashMap<String, Long>();
        List<String> roles = new ArrayList<String>();

        for(String nodeid : gg.getNodeKeyset()) {
            GraphmlNode n = gg.getNode(nodeid);
            Statedef sd = ic.getStatedef(pd.getId(), n.getLongText(), n.getShortText());
            m.put(nodeid, sd.getId());
            //System.out.println("Node mapping "+nodeid+" -> "+sd.getId());
            String directions = n.getLongText();
            if(directions != null) {
                //InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(directions.getBytes()));
                //NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "Node " + nodeid);
            	NodeConfiguration nc = ic.createNodeConfig(sd, nodeid);
            	if(nc != null) {
	                List<String> rolenames = nc.getAllRolenames();
	                for(String role : rolenames) {
	                    if (!roles.contains(role)) {
	                        roles.add(role);
	                    }
	                }
            	}
            }
            else {
            	ic.notifyNoLayoutDefined(sd, nodeid);
            }
        }
        
        for(int i = 0 ; i < gg.getEdgeAmount() ; i++) {
        	GraphmlEdge e = gg.getEdge(i);
        	Transitiondef td = ic.getTransitiondef(m.get(e.getSourceId()), m.get(e.getTargetId()), e.getLongText(), e.getShortText());
            //System.out.println("Edge source mapping "+e.getSourceId()+" -> "+m.get(e.getSourceId()));
            //System.out.println("Edge target mapping "+e.getTargetId()+" -> "+m.get(e.getTargetId()));
            String directions = e.getLongText();
            if(directions != null) {
            	EdgeConfiguration ec = ic.createEdgeConfig(td, e.getId());
                //System.out.println("Parsing: "+directions);
            	if(ec != null) {
	                List<String> rolenames = ec.getRoles();
	                for(String role : rolenames) {
	                    if (!roles.contains(role)) {
	                        roles.add(role);
	                    }
	                }
            	}
            }
        }

        for(String role : roles) {
        	ic.createRoledef(pd.getId(), role);
            //System.out.println("Roll "+role);
        }

        return pd;
    }

    public static DossierDef inititializeDossierConfigNodes(Processfile pf, String description, String dossiertypename) throws Exception {
    	byte[] data = pf.getData();
        DossierConfiguration dc = PrvDossierConfigParser.parseDossierConfig(new InputStreamReader(new ByteArrayInputStream(data)), "Processfile data "+pf.getId());

        DossierDef dd = new DossierDef();
        dd.setDescription(description);
        dd.setDossiertypename(dossiertypename);
        dd.setProcessfileId(pf.getId());
        dd.save();
        
        List<String> types = dc.getTypes();
        for(String type : types) {
        	DossierDoctypeDef ddd = new DossierDoctypeDef();
        	ddd.setDossierdefId(dd.getId());
        	ddd.setDossierdoctypename(type);
        	ddd.save();
        }
        
        List<String> attributes = dc.getAttributes();
        for(String attribute : attributes) {
        	DossierDocattributeDef ddd = new DossierDocattributeDef();
        	ddd.setDossierdefId(dd.getId());
        	ddd.setDossierdocattributename(attribute);
        	ddd.save();
        }
        
    	return dd;
    }

}
