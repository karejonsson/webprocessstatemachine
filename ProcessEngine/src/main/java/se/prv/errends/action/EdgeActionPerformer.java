package se.prv.errends.action;

import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.struct.EdgeActionVisitor;
import se.prv.errends.edgeconfig.struct.EmailnotificationAction;
import se.prv.errends.edgeconfig.struct.TransitionAction;
import se.prv.errends.mgmt.EmailManagement;
import se.prv.errends.reuse.cli.PropertyNames;

public class EdgeActionPerformer implements EdgeActionVisitor {
	
	private List<String> roles = null;
	private Session session;
	private TransitionAction transaction;
	private Transitiondef transitiondef = null;
	
	public EdgeActionPerformer(List<String> roles, Session session, TransitionAction transaction, Transitiondef transitiondef) {
		this.roles = roles;
		this.session = session;
		this.transaction = transaction;
		this.transitiondef = transitiondef;
	}
	
	private Actor getActorOfErrendAndRole(String rolename, List<Actor> errendActors) {
		for(Actor errendActor : errendActors) {
			String r = errendActor.getRoledef().getRolename();
			//System.out.println("Jämför <"+r+"> med <"+rolename+">");
			if(errendActor.getRoledef().getRolename().equals(rolename)) {
				//System.out.println("TRÄFF");
				return errendActor;
			}
		}
		//System.out.println("Det blev null som svar");
		return null;
	}
	
	@Override
	public void visit(EmailnotificationAction emailnotificationAction) {
		System.out.println("EdgeActionPerformer.visit "+emailnotificationAction);
		Errend errend = session.getActor().getErrend();
		System.out.println("EdgeActionPerformer.visit errend "+errend);
		List<Actor> errendActors = ActorDB.getAllActorsOfErrendId(errend.getId());
		List<String> roles = emailnotificationAction.getRoles();
		for(String role : roles) {
			System.out.println("EdgeActionPerformer.visit EmailnotificationAction ------Roll "+role);
			Actor receiver = getActorOfErrendAndRole(role, errendActors);
			if(receiver == null) {
				continue;
			}
			if(emailnotificationAction.useInline()) {
				String groovyscript_messageproducer = emailnotificationAction.getValue(EmailnotificationAction.inlineMessageKey);
				String title = emailnotificationAction.getValue(EmailnotificationAction.inlineTitleKey);
				System.out.println("EdgeActionPerformer.visit EmailnotificationAction inline title="+title);
				System.out.println("EdgeActionPerformer.visit EmailnotificationAction inline groovyscript_messageproducer="+groovyscript_messageproducer);

				
				String message = EmailManagement.createMessageToSendByInlineAtUserTransition(receiver, transitiondef, groovyscript_messageproducer);
				System.out.println("EdgeActionPerformer.visit meddelande\n"+message);
				EmailManagement.sendEmailWithPrecalculatedTitleAndMessage(receiver.getAgent(), title, message);
			}
			if(emailnotificationAction.useFunction()) {
				String functionnameMessage = emailnotificationAction.getValue(EmailnotificationAction.functionMessageKey);
				String functionnameTitle = emailnotificationAction.getValue(EmailnotificationAction.functionTitleKey);
				System.out.println("EdgeActionPerformer.visit EmailnotificationAction function functionnameTitle="+functionnameTitle);
				System.out.println("EdgeActionPerformer.visit EmailnotificationAction function functionnameMessage="+functionnameMessage);

				String installationidentifier = null;
				try {
					installationidentifier = PersistentValuesDB.getStringValue(PropertyNames.installation);
				} catch (Exception e) {
					installationidentifier = "installationens identifierare kunde inte bestämmas";
					e.printStackTrace();
				}
				
				String message = EmailManagement.createTextByFunctionAtUserTransition(receiver, transitiondef, functionnameMessage, session.getActor(), installationidentifier);
				String title = EmailManagement.createTextByFunctionAtUserTransition(receiver, transitiondef, functionnameTitle, session.getActor(), installationidentifier);
				System.out.println("EdgeActionPerformer.visit meddelande\n"+message);
				EmailManagement.sendEmailWithPrecalculatedTitleAndMessage(receiver.getAgent(), title, message);
			}
		}
		System.out.println("EdgeActionPerformer.visit slut");
	}

}
