package se.prv.errends.mgmt;

import java.util.ArrayList;
import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.reuse.cli.PropertyNames;

public class ActorManagement {

	public static Actor getErrendlessActor(Person p) {		
		Agent accountlessAgent = AgentManagement.getDefaultAgentOfPerson(p);
		return getErrendlessActor(accountlessAgent);
	}
	
	public static Actor getErrendlessActor(Agent agent) {		
		List<Actor> actors = ActorDB.getAllActorsOfAgentId(agent.getId());

		for(Actor actor : actors) {
			if(actor.getErrendId() == null) {
				return actor;
			}
		}
		Actor out = new Actor();
		out.setAgent(agent);
		out.save();
		return out;
	}
	
	private static Actor get(List<Actor> actors, Agent oa, Roledef rd) {
		//System.out.println("---------------------------------------------------");
		//System.out.println("Actormanagement get Rolldefid = "+rd.getId()+" Personid = "+p.getId());
		for(Actor actor : actors) {
			//System.out.println("Jämför med "+actor);
 			if(actor.getAgentId().equals(oa.getId()) && actor.getRoledefId().equals(rd.getId())) {
 				//System.out.println("TRÄFF!! "+actor);
				return actor;
			}
		}
		//System.out.println("Ingen träff");
		return null;
	}
	
	public static String saveErrendSetupReturnUserText(Errend errend, List<Roledef> roledefs, String adminusername, List<AgentWithRoledef> personsWithRoledef, Actor current) throws Exception {
		List<Actor> actors = ActorDB.getAllActorsOfErrendId(errend.getId());
		
		List<Actor> actorsToSave = new ArrayList<Actor>();
		List<Long> roledefIdstaken = new ArrayList<Long>();
		
		for(AgentWithRoledef pwr : personsWithRoledef) {
			Actor actor = get(actors, pwr.getAgent(), pwr.getRoledef());
			if(actor == null) {
				actor = new Actor();
				actor.setRoledefId(pwr.getRoledef().getId());
				actor.setErrendId(errend.getId());
				actor.setAgentId(pwr.getAgent().getId());
				actorsToSave.add(actor);
				//System.out.println("Sparar aktör för att skriva ner senare "+actor);
			}
			roledefIdstaken.add(pwr.getRoledef().getId());
		}
		
		boolean allRolesaAssigned = true;
		
		StringBuffer undoneAssigments = null;
		
		for(Roledef roledef : roledefs) {
			if(!roledefIdstaken.contains(roledef.getId())) {
				allRolesaAssigned = false;
				if(undoneAssigments == null) {
					undoneAssigments = new StringBuffer();
					undoneAssigments.append("Det finns roller som inte tilldelats.\n");
				}
				undoneAssigments.append("Rollen "+roledef.getRolename()+" har ej tilldelats.\n");
			}
		}

		errend.setAdminusername(adminusername);
		errend.setInitiated(allRolesaAssigned);
		errend.save();
		StringBuffer doneAssigments = null;

		for(Actor actor : actorsToSave) {
			if(doneAssigments == null) {
				doneAssigments = new StringBuffer();
				doneAssigments.append("Tilldelningar har gjorts.\n");
			}
			doneAssigments.append("Rollen "+actor.getRoledef().getRolename()+" har tilldelats.\n");
			
			//System.out.println("Sparar "+actor);
			actor.save();
			EmailManagement.sendMessageFromFunctionByProperties(actor, PropertyNames.youhavebeenappointedtitle, PropertyNames.youhavebeenappointedmessage, current, null);

			//System.out.println("Sparade "+actor);
		}
		if(undoneAssigments == null) {
			undoneAssigments = new StringBuffer();
			undoneAssigments.append("Ärendet är initierat");
		}
		return undoneAssigments.toString()+"\n"+doneAssigments.toString();
	}
	
	public static boolean isSameActor(Actor a1, Actor a2) {
		return a1.getId().equals(a2.getId());
	}


}
