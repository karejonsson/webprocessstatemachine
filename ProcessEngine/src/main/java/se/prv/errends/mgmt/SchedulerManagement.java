package se.prv.errends.mgmt;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.reuse.cli.PropertyNames;

public class SchedulerManagement {
	
	public final static SchedulationJobWrapper[] jobs = new SchedulationJobWrapper[] {
			new SchedulationJobWrapper(RespiteSchedulationJob.class, "1", PropertyNames.cron_schema_respite, "Frister"),
			new SchedulationJobWrapper(AppointSchedulationJob.class, "2", PropertyNames.cron_schema_appoint, "Rolltilldelningar"),
			
			
			//new SchedulationJobWrapper(SchedulationJobTest1.class, "1", PropertyNames.cron_schema_respite, "Frister"),
			//new SchedulationJobWrapper(SchedulationJobTest2.class, "2", PropertyNames.cron_schema_appoint, "Rolltilldelningar") 
	};

	private static Scheduler scheduler = null;
	private static String errorMessage = null;
	
	public static boolean isActive() {
		return scheduler != null && errorMessage == null;
	}

	public static void initialize() {
		System.out.println("SchedulerManagement.initialize");
		System.out.println("--------------------------------------------------------------------------------------------------");
		if(isActive()) {
			//System.out.println("SchedulerManagement.initialize redan aktiv, gör inget.");
			return;
		}
		
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (SchedulerException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			errorMessage = sw.toString();
			e.printStackTrace();
			return;
		}
		
		for(SchedulationJobWrapper wrapper : jobs) {

			String cronSchedule = null;
			try {
				cronSchedule = PersistentValuesDB.getStringValue(wrapper.getCronSchemaProperty());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			if(cronSchedule == null || cronSchedule.trim().length() < 3) {
				errorMessage = "Det finns ingen parameter satt för schedulering.";
				System.out.println(errorMessage);
				return;
			}
	
			// define the job and tie it to our MyJob class
			JobDetail job = JobBuilder.newJob(wrapper.getJobClass())
					.withIdentity(wrapper.getJobSymbol(), wrapper.getGroupSymbol())
					.build();
			
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(wrapper.getTriggerSymbol(), wrapper.getGroupSymbol())
					.startNow()
					.withSchedule(CronScheduleBuilder.cronSchedule(cronSchedule))
					.build();
	
			try {
				scheduler.scheduleJob(job, trigger);
			} catch (SchedulerException e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				errorMessage = sw.toString();
				e.printStackTrace();
				return;
			}
		
		}

		try {
			scheduler.start();
		} catch (SchedulerException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			errorMessage = sw.toString();
			e.printStackTrace();
			return;
		}

	}

	public static boolean stop() throws SchedulerException {
		//System.out.println("SchedulerManagement.stop aktiv="+isActive());
		if(isActive()) {
			//System.out.println("SchedulerManagement.stop stoppar");
			scheduler.shutdown();
			scheduler = null;
			errorMessage = null;
			return true;
		}
		//System.out.println("SchedulerManagement.stop redan stoppad");
		errorMessage = null;
		return false;
	}

}
