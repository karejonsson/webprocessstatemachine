package se.prv.errends.mgmt;

import org.quartz.Job;

public class SchedulationJobWrapper {
	
	private Class<? extends ManageableJob> jobClazz;
	private String symbol;
	private String cronSchemaProperty;
	private String theme = null;
	
	public SchedulationJobWrapper(Class<? extends ManageableJob> jobClazz, String symbol, String cronSchemaProperty, String theme) {
		this.jobClazz = jobClazz;
		this.symbol = symbol;
		this.cronSchemaProperty = cronSchemaProperty;
		this.theme = theme;
	}
	
	public String getGroupSymbol() {
		return "group"+symbol;
	}

	public String getJobSymbol() {
		return "job"+symbol;
	}

	public String getTriggerSymbol() {
		return "trigger"+symbol;
	}
	
	public String getCronSchemaProperty() {
		return cronSchemaProperty;
	}
	
	public Class<? extends ManageableJob> getJobClass() {
		return jobClazz;
	}

	public String getTheme() {
		return theme;
	}
	
}
