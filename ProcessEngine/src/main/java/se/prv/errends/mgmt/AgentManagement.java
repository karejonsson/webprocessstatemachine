package se.prv.errends.mgmt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;

public class AgentManagement {
	
	public static Agent getDefaultAgentOfPerson(Person p) {
		List<Agent> agents = AgentDB.getAllAgentsOfPerson(p.getId());

		for(Agent agent : agents) {
			if(agent.getOrganisation().getOrgnr().equals(p.getSwedishpnr())) {
				return agent;
			}
		}
		
		Organisation org = OrganizationDB.getOrganizationFromOrgNr(p.getSwedishpnr());
		if(org == null) {
			org = new Organisation();
			org.setOrgnr(p.getSwedishpnr());
			org.setOrgname("Defaultorganisation");
			org.setActive(true);
			org.setRegisterred(new Date(System.currentTimeMillis()));
			org.save();
		}

		Agent out = new Agent();
		out.setPerson(p);
		out.setRegisterred(new Date(System.currentTimeMillis()));
		out.setActive(true);
		out.setOrganization(org);
		out.save();
		return out;
	}

	public static List<Agent> getAllowedAgents(Person person) {
		ArrayList<Agent> out = new ArrayList<Agent>();
		out.add(getDefaultAgentOfPerson(person));
		return out;
	}

	public static List<Agent> getAllowedAgents(Organisation organization) {
		List<Agent> out = organization.getReferringAgents();
		return out;
	}

}
