package se.prv.errends.mgmt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.StateExpiration;

public class RespiteSchedulationJob implements ManageableJob {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		execute();
	}

	public void execute() throws JobExecutionException {
		System.out.println("RespiteSchedulationJob.execute");
		System.out.println("--------------------------------------------------------------------------------------------------");
		
		List<StateExpiration> expired = StateExpirationDB.getAllStateExpirationsExpired();
		System.out.println("Hittade "+expired.size());
		
		List<Long> errends = getDifferentErrendIds(expired);
		for(Long eId : errends) {
			List<StateExpiration> errendsExpirations = getExpirationsOfErrendId(expired, eId);
			StateExpiration se = getOldestExpiration(errendsExpirations);
			try {
				handleExpiration(se);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private List<Long> getDifferentErrendIds(List<StateExpiration> expired) {
		List<Long> out = new ArrayList<Long>();
		if(expired == null) {
			return out;
		}
		if(expired.size() == 0) {
			return out;
		}
		for(StateExpiration se : expired) {
			Long eId = se.getManagedErrend().getErrendId();
			if(!out.contains(eId)) {
				out.add(eId);
			}
		}
		return out;
	}
	
	private List<StateExpiration> getExpirationsOfErrendId(List<StateExpiration> expired, long errendId) {
		List<StateExpiration> out = new ArrayList<StateExpiration>();
		if(expired == null) {
			return out;
		}
		if(expired.size() == 0) {
			return out;
		}
		for(StateExpiration se : expired) {
			if(se.getManagedErrend().getErrendId().equals(errendId)) {
				out.add(se);
			}
		}
		return out;
	}
	
	private StateExpiration getOldestExpiration(List<StateExpiration> expired) {
		if(expired == null) {
			return null;
		}
		if(expired.size() == 0) {
			return null;
		}
		StateExpiration candidate = expired.get(0);
		for(int idx = 1 ; idx < expired.size() ; idx++) {
			StateExpiration other = expired.get(idx);
			if(other.getExpirey().getTime() < candidate.getExpirey().getTime()) {
				candidate = other;
			}
		}
		return candidate;
	}
	
	private void handleExpiration(StateExpiration se) throws Exception {
		//System.out.println("RespiteSchedulation.handleExpiration StateExpiration="+se);
		if(se == null) {
			return;
		}
		//System.out.println("RespiteSchedulation.handleExpiration ManagedErrend="+se.getManagedErrend());
		ErrendManagement em = ErrendManagement.getErrendManagerForSystemDeamon(se.getManagedErrend());
		Session sess = new Session();
		sess.setActor(em.getActor());
		sess.setStart(new Date());
		sess.save();
		em.performTransition(se.getTransitiondef().getTransitionname(), sess);
		sess.setFinish(new Date());
		sess.save();
	}

}
