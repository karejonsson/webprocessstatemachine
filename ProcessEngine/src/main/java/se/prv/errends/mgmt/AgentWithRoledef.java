package se.prv.errends.mgmt;

import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Roledef;

public class AgentWithRoledef {
	private Agent agent = null;
	private Roledef roledef = null;
	public AgentWithRoledef(Agent agent, Roledef roledef) {
		this.agent = agent;
		this.roledef = roledef;
	}
	
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	public Roledef getRoledef() {
		return roledef;
	}
	public void setRoledef(Roledef roledef) {
		this.roledef = roledef;
	}
}
