package se.prv.errends.mgmt;

import org.quartz.Job;
import org.quartz.JobExecutionException;

public interface ManageableJob extends Job {
	
	void execute() throws JobExecutionException;

}
