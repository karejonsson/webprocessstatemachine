package se.prv.errends.mgmt;

import java.util.ArrayList;
import java.util.List;

import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public class StatedefManaged {
	private Statedef statedef = null;
	private NodeConfiguration nc = null;
	public String toString() {
		return "{ StatedefManaged: statedef "+statedef+" }";
	}
	public Statedef getStatedef() {
		return statedef;
	}
	public void setStatedef(Statedef statedef) {
		this.statedef = statedef;
	}
	public NodeConfiguration getNc() {
		return nc;
	}
	public void setNc(NodeConfiguration nc) {
		this.nc = nc;
	}
	public String getStatename() {
		return statedef.getStatename();
	}
	private List<TransitiondefManaged> sourceTransitiondefs = null;
	public List<TransitiondefManaged> getSourceTransitiondefs() throws Exception {
		if(sourceTransitiondefs != null) {
			return sourceTransitiondefs;
		}
		sourceTransitiondefs = new ArrayList<TransitiondefManaged>();
		List<Transitiondef> tds = statedef.getSourceTransitiondefs();
		for(Transitiondef td : tds) {
			sourceTransitiondefs.add(new TransitiondefManaged(td));
		}
		return sourceTransitiondefs;
	}
	
	public static List<String> getAllBooleanFunctionDefNames(List<StatedefManaged> states) {
		List<String> out = new ArrayList<String>();
		if(states == null) {
			return out;
		}
		for(StatedefManaged state : states) {
			List<TransitiondefManaged> transitions;
			try {
				transitions = state.getSourceTransitiondefs();
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			for(TransitiondefManaged transition : transitions) {
				List<String> fs = transition.getEc().getBooleanFunctions();
				for(String f : fs) {
					if(f == null) {
						continue;
					}
					if(!out.contains(f)) {
						out.add(f);
					}
				}
			}
		}
		return out;
	}
	
	public static List<String> getAllStringFunctionDefNames(List<StatedefManaged> states) {
		List<String> out = new ArrayList<String>();
		if(states == null) {
			return out;
		}
		for(StatedefManaged state : states) {
			List<TransitiondefManaged> transitions;
			try {
				transitions = state.getSourceTransitiondefs();
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			for(TransitiondefManaged transition : transitions) {
				List<String> fs = transition.getEc().getStringFunctions(); 
				for(String f : fs) {
					if(f == null) {
						continue;
					}
					if(!out.contains(f)) {
						out.add(f);
					}
				}
			}
		}
		return out;
	}

}
