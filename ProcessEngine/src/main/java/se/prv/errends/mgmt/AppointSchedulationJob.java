package se.prv.errends.mgmt;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.evaluation.GeneralProducerVisitor;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;
import se.prv.errends.reuse.cli.PropertyNames;

public class AppointSchedulationJob implements ManageableJob {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		execute();
	}

	public void execute() throws JobExecutionException {
		System.out.println("AppointSchedulationJob.execute");
		System.out.println("--------------------------------------------------------------------------------------------------");
		
		List<Errend> errends = ErrendDB.getAllNotInitiatedErrends();
		if(errends == null || errends.size() == 0) {
			return;
		}
		
		String titleFunctionname = null;
		try {
			titleFunctionname = PersistentValuesDB.getStringValue(PropertyNames.appointmentsnecessarytitle);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		FunctionDef titleFunction = FunctionDefDB.getFunctionDefFromName(titleFunctionname);
		System.out.println("AppointSchedulationJob TITLE: "+titleFunctionname+" -> "+titleFunction);
		ProducerConfiguration titleProducer = getProducerConfiguration(titleFunction);
		
		String messageFunctionname = null;
		try {
			messageFunctionname = PersistentValuesDB.getStringValue(PropertyNames.appointmentsnecessarymessage);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		String installationidentifier = null;
		try {
			installationidentifier = PersistentValuesDB.getStringValue(PropertyNames.installation);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		FunctionDef messageFunction = FunctionDefDB.getFunctionDefFromName(messageFunctionname);
		System.out.println("AppointSchedulationJob TITLE: "+messageFunctionname+" -> "+messageFunction);
		ProducerConfiguration messageProducer = getProducerConfiguration(messageFunction);

		List<Adminuser> admins = AdminuserDB.getAllAdminusers();
		Adminuser superAdmin = AdminuserDB.getAdminuserFromUsername("super");
		Actor currentActor = ActorManagement.getErrendlessActor(superAdmin.getPerson());
		GeneralProducerVisitor visitor = new GeneralProducerVisitor(currentActor, currentActor, installationidentifier, errends);
		for(Adminuser admin : admins) {
			if(!admin.isActive()) {
				continue;
			}
			Actor contextActor = ActorManagement.getErrendlessActor(admin.getPerson());
			visitor.setContext(contextActor);
			
			visitor.reset();
			titleProducer.accept(visitor);
			String title = visitor.getOut().toString();
			System.out.println("AppointSchedulationJob Title "+title);
			
			visitor.reset();
			messageProducer.accept(visitor);
			String message = visitor.getOut().toString();
			System.out.println("AppointSchedulationJob Message "+message);
			
			EmailManagement.sendMessageFromFunctionByProperties(contextActor, title, message);
		}
		
	}
	
	private ProducerConfiguration getProducerConfiguration(FunctionDef fd) {
		ByteArrayInputStream bais;
		try {
			bais = new ByteArrayInputStream(fd.getSource().getBytes("UTF-8"));
		} 
		catch (UnsupportedEncodingException e1) {
			System.err.println("UnsupportedEncodingException for "+fd);
			e1.printStackTrace();
			return null;
		}
		catch (Exception e1) {
			System.err.println("Exception for "+fd);
			e1.printStackTrace();
			return null;
		}
		InputStreamReader isr = new InputStreamReader(bais);
		ProducerConfiguration config = null;
		try {
			config = PrvErrendProducersConfigParser.parseProducerConfig(isr, "FunctionDef "+fd.getId()).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return config;
	}

}
