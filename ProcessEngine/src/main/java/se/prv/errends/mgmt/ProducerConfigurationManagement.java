package se.prv.errends.mgmt;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import se.prv.errends.domain.FunctionDef;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class ProducerConfigurationManagement {

	public static ProducerConfiguration getProducerConfigurationFromFunctionDef(FunctionDef fd) {
		String functionDefinition = fd.getSource();
		List<ProducerConfiguration> producers = null;
		try {
			InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(functionDefinition.getBytes("UTF-8")));
	    	producers = PrvErrendProducersConfigParser.parseProducerConfig(isr, "functiondef.id="+fd.getId());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(producers == null) {
			System.err.println("EmailManagement.getProducerConfigurationFromFunctionDef Producenten innehöll inga definitioner eller var det inte parsebart. functiondef.id="+fd.getId());
			return null;
		}

		if(producers.size() != 1) {
			System.err.println("EmailManagement.getProducerConfigurationFromFunctionDef Producenten innehöll flera definitioner. functiondef.id="+fd.getId());
			System.err.println("");
			return null;
		}
		
		System.out.println("EmailManagement.getProducerConfigurationFromFunctionDef till slutet");
		return producers.get(0);
	}
 	

}
