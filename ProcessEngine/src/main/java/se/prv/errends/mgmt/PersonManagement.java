package se.prv.errends.mgmt;

import se.prv.errends.domain.Person;

public class PersonManagement {

	public static boolean isPnr(String pnr) {
		String formatted = Person.format(pnr);
		if(formatted == null) {
			return false;
		}
		return formatted.length() >= 9 && formatted.length() <= 12;
	}
	
	public static String getCorrectedPnr(String pnr) {
		if(!isPnr(pnr)) {
			return null;
		}
		String formatted = Person.format(pnr);
		if(formatted.length() == 9) {
			try {
				return Person.makeCompletionWithChecknumber(formatted);
			} catch (Exception e) {
				return null;
			}
		}
		if(formatted.length() == 10) {
			try {
				return Person.fix(formatted);
			} catch (Exception e) {
				return null;
			}
		}
		if(formatted.length() == 11) {
			try {
				return Person.makeCompletionWithChecknumber(formatted);
			} catch (Exception e) {
				return null;
			}
		}
		if(formatted.length() == 12) {
			try {
				return Person.fix(formatted);
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
	
    public static boolean personalDataComplete(Person person) {
    	if(person == null) {
    		return false;
    	}
    	if(person.getFamilyname() == null) {
    		return false;
    	}
    	if(person.getFamilyname().length() < 2) {
    		return false;
    	}
    	if(person.getFirstname() == null) {
    		return false;
    	}
    	if(person.getFirstname().length() < 2) {
    		return false;
    	}
    	if(person.getEmail() == null) {
    		return false;
    	}
    	if(person.getEmail().length() < 6) {
    		return false;
    	}
    	if(!Person.acceptableEmail(person.getEmail())) {
    		return false;
    	}
    	return true;
    }


}
