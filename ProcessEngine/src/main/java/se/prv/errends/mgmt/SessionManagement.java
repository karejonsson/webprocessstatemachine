package se.prv.errends.mgmt;

import java.util.Date;
import java.util.List;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;

public class SessionManagement {
	
	public static Session getErrendlessSession(Person p) {
		Actor errendlessActor = ActorManagement.getErrendlessActor(p);
		Session out = new Session();
		out.setActor(errendlessActor);
		out.setStart(new Date(System.currentTimeMillis()));
		out.save();
		return out;
	}
	
	public static Session switchAsNecessary(Session previousSession, Actor actor) {
		Session session = null;
		if(ActorManagement.isSameActor(previousSession.getActor(), actor)) {
			return previousSession;
		}
		previousSession.setFinish(new Date(System.currentTimeMillis()));
		previousSession.save();
		session = new Session();
		session.setActor(actor);
		session.setStart(new Date(System.currentTimeMillis()));
		session.save();
		return session;
	}

}
