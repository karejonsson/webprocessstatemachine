package se.prv.errends.mgmt;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import se.prv.errends.action.EdgeActionPerformer;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.RoledefDB;
import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.dbc.StatedefDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.ManagedErrend;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.StateExpiration;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transition;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.struct.AllowTransition;
import se.prv.errends.edgeconfig.struct.EdgeAction;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.edgeconfig.struct.StatetimeExpiration;
import se.prv.errends.edgeconfig.struct.TransitionAction;
import se.prv.errends.evaluation.GeneralProducerVisitor;
import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;
import se.prv.errends.reuse.cli.PropertyNames;

public class ErrendManagement {
	
	public static String toString(List list) {
		if(list == null) {
			return "NULL-list";
		}
		if(list.size() == 0) {
			return "[]";
		}
		if(list.size() == 1) {
			return "["+list.get(0)+"]";
		}
		StringBuffer out = new StringBuffer();
		out.append("["+list.get(0));
		for(int i = 1 ; i < list.size() ; i++) {
			out.append(", "+list.get(i));
		}
		return out.toString()+"]";
	}
	
	public static ArrayList<StatedefManaged> getStatedefManaged(Processdef pd) throws Exception {
		List<Statedef> statedefs = pd.getReferringStatedefs();
		//System.out.println("ErrendManagement.getStatedefManaged statedefs="+toString(statedefs));

		ArrayList<StatedefManaged> sms = new ArrayList<StatedefManaged>();
		//StatedefManaged entry = null;
		for(Statedef sd : statedefs) {
			StatedefManaged sm = new StatedefManaged();
			sm.setStatedef(sd);
	        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(sd.getLayout().getBytes("UTF-8")));
	        NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "Statedef layout id "+sd.getId());
			sm.setNc(nc);
			sms.add(sm);
		}
		return sms;
	}
	
	private static StatedefManaged getInitialStatedefManaged(ArrayList<StatedefManaged> statedefs) {
		StatedefManaged entry = null;
		for(StatedefManaged sd : statedefs) {
			if(sd.getNc().isAnonymousStartValid()) {
				if(entry != null) {
					return null;
				}
				entry = sd;
			}
		}
		return entry;
	}
	private static StatedefManaged getCurrentStatedefManaged(ArrayList<StatedefManaged> statedefs, Statedef current) {
		//StatedefManaged entry = null;
		for(StatedefManaged statedef : statedefs) {
			if(statedef.getStatedef().getId().equals(current.getId())) {
				return statedef;
			}
			//System.out.println("ErrendManagement.getCurrentStatedefManaged olika "+statedef.getStatedef().getId()+" != "+current.getId());
			//System.out.println("ErrendManagement.getCurrentStatedefManaged olika "+statedef.getStatedef().getId().getClass().getName()+" != "+current.getId().getClass().getName());
		}
		return null;
	}
	
	public static ErrendManagement getErrendFromDBStructure(Actor actor) throws Exception {
		ManagedErrend me = actor.getErrend().getManagedErrend();
		Processdef pd = me.getProcessdef();
		
		ArrayList<StatedefManaged> sms = null;
		try {
			sms = getStatedefManaged(pd);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ErrendManagement("Interna fel i ärendetypens definition");
		}
		StatedefManaged sm = getCurrentStatedefManaged(sms, me.getCurrentStatedef()); 
		return new ErrendManagement(actor, sms, sm);
	}
	
	public static ErrendManagement getErrendManagerForSystemDeamon(ManagedErrend me) throws Exception {
		Processdef pd = me.getProcessdef();
		//System.out.println("ErrendManagement.getErrendManagerForSystemDeamon pd="+pd);
		
		ArrayList<StatedefManaged> sms = null;
		try {
			sms = getStatedefManaged(pd);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ErrendManagement("Interna fel i ärendetypens definition");
		}
		//System.out.println("ErrendManagement.getErrendManagerForSystemDeamon slutlig sms="+toString(sms));
		Statedef sd = me.getCurrentStatedef();
		//System.out.println("ErrendManagement.getErrendManagerForSystemDeamon sd="+sd);
		StatedefManaged sm = getCurrentStatedefManaged(sms, sd); 
		Actor actor = getActorForSuperDeamon(me);
		//System.out.println("ROLL "+actor.getRoledef().getRolename());
		//System.out.println("PERSON "+actor.getAgent().getPerson());
		
		return new ErrendManagement(actor, sms, sm);
	}
	
	private static Actor getActorForSuperDeamon(ManagedErrend me) {
		Adminuser au = AdminuserDB.getAdminuserFromUsername("super");
		
		Agent accountlessAgent = AgentManagement.getDefaultAgentOfPerson(au.getPerson());
		
		List<Actor> availableActors = ActorDB.getAllActorsOfErrendIdAndAgentId(me.getErrendId(), accountlessAgent.getPersonId());
		if(availableActors.size() == 1) {
			return availableActors.get(0);
		}
		Actor actor = new Actor();
		actor.setErrendId(me.getErrendId());
		actor.setAgentId(accountlessAgent.getId());
		actor.setRoledef(getErrendsDeamonRole(me.getProcessdefId()));
		actor.save();
		return actor;
	}
	
	private static Roledef getErrendsDeamonRole(Long processdefId) {
		List<Roledef> roledefs = RoledefDB.getAllRoledefsOfProcessdef(processdefId);
		for(Roledef rd : roledefs) {
			if(rd.getRolename().equals("deamon")) {
				return rd;
			}
		}
		Roledef rd = new Roledef();
		rd.setProcessdefId(processdefId);
		rd.setRolename("deamon");
		rd.save();
		return rd;
	}
	
	public static ErrendManagement retreiveErrendFromDBStructure(Agent oa, Processdef pd, String personname) {
		ArrayList<StatedefManaged> sms = null;
		try {
			sms = getStatedefManaged(pd);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ErrendManagement("Interna fel i ärendetypens definition");
		}
		StatedefManaged sm = getInitialStatedefManaged(sms);
		if(sm == null) {
			return new ErrendManagement("Måste initieras av Administratör");
		}
		Actor newActor = createErrendNodesRetrieveActor(oa, personname, sm, pd);
		return new ErrendManagement(newActor, sms, sm);
	}
	
	public static ErrendManagement createErrendFromDBStructureAndInitialize(Agent oa, Processdef pd, String personname) throws Exception {
		ArrayList<StatedefManaged> sms = null;
		try {
			sms = getStatedefManaged(pd);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ErrendManagement("Interna fel i ärendetypens definition");
		}
		StatedefManaged sm = getInitialStatedefManaged(sms);
		if(sm == null) {
			return new ErrendManagement("Måste initieras av Administratör");
		}
		Actor newActor = createErrendNodesRetrieveActor(oa, personname, sm, pd);
		ErrendManagement out = new ErrendManagement(newActor, sms, sm);
		out.setupExpirationsOnInitiation();
		return out;
	}
	
	private static Actor createErrendNodesRetrieveActor(Agent oa, String personname, StatedefManaged initialState, Processdef pd) {
		Errend e = new Errend();
		e.setActive(true);
		e.setOrganization(oa.getOrganisation());
		e.setCreated(new Date(System.currentTimeMillis()));
		e.setPersonname(personname);
		e.save();
		
		ManagedErrend me = new ManagedErrend();
		me.setErrend(e);
		me.setCurrentState(initialState.getStatedef());
		me.setProcessdef(pd);
		me.save();
		
		Actor actor = new Actor();
		actor.setErrend(e);
		actor.setAgent(oa);
		
		String initialRole = initialState.getNc().getAnonymousStartRole();
		
		List<Roledef> roledefs = pd.getRoledefs();
		for(Roledef roledef : roledefs) {
			if(roledef.getRolename().equals(initialRole)) {
				actor.setRoledef(roledef);
			}
		}
		actor.save();
		
		return actor;
	}
	
	private String errorMessage = null;
	
	private ErrendManagement(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public boolean isError() {
		return errorMessage != null;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	private Actor actor = null;
	private ArrayList<StatedefManaged> sms = null;
	private StatedefManaged currentState = null;
	private Errend errend = null;
	
	private ErrendManagement(Actor actor, ArrayList<StatedefManaged> sms, StatedefManaged currentState) {
		this.actor = actor;
		this.sms = sms;
		this.currentState = currentState;
		errend = actor.getErrend();
		/*
		ManagedErrend me = errend.getManagedErrend();
		try {
			setupExpirationsOnInitiation(me, currentState);
		} catch (Exception e) {
			errorMessage = "Kunde inte initiera tillståndet";
			e.printStackTrace();
		}
		*/
	}
	
	public Actor getActor() {
		return actor;
	}
		
	public Errend getErrend() {
		return errend;
	}
	
	public String getCurrentStatename() {
		Statedef sd = currentState.getStatedef();
		return sd.getStatename();
	}
	
	public StatedefManaged getCurrentState() {
		return currentState;
	}
	
	/*
	public List<String> getErrendActions() throws Exception {
		List<TransitiondefManaged> transitiondefs = currentState.getSourceTransitiondefs();
		List<String> out = new ArrayList<String>();
		String rolename = actor.getRoledef().getRolename();
		for(TransitiondefManaged td : transitiondefs) {
			if(td.allowsTransitionOnRolename(rolename)) {
				out.add(td.getTransitionname());
			}
		}
		return out;
	}
	*/
	
	public List<TransitiondefManaged> getErrendActions() throws Exception {
		List<TransitiondefManaged> transitiondefs = currentState.getSourceTransitiondefs();
		List<TransitiondefManaged> out = new ArrayList<TransitiondefManaged>();
		String rolename = actor.getRoledef().getRolename();
		for(TransitiondefManaged td : transitiondefs) {
			if(td.allowsTransitionOnRolename(rolename)) {
				out.add(td);
			}
		}
		return out;
	}
	
	private StatedefManaged getManaged(Statedef sd) {
		for(StatedefManaged sdm : sms) {
			if(sdm.getStatedef().getId().equals(sd.getId())) {
				return sdm;
			}
		}
		return null;
	}
	
	private void makeNodesForTransition(TransitiondefManaged td, Session session) throws Exception {
		System.out.println("ErrendManagement.makeNodesForTransition");
		Transitiondef transitiondef = td.getTransitiondef();
		Transition transition = new Transition();
		transition.setTransitiondefId(transitiondef.getId());
		ManagedErrend me = errend.getManagedErrend();
		Transition precessor = me.getLastTransition();
		transition.setPrececessor(precessor);
		transition.setSession(session);
		transition.save();
		Statedef sd = td.getTransitiondef().getTargetstatedef();
		StatedefManaged sdf = getManaged(sd);
		setupExpirationsOnTransition(me, sdf);
		me.setLastTransition(transition);
		me.setCurrentState(sd);
		me.save();
		if(sdf.getNc().isSpontaneousEnd()) {
			errend.setActive(false);
			errend.save();
		}
	}
	
	private void setupExpirationsOnTransition(ManagedErrend me, StatedefManaged sdf) throws Exception {
		// Time to remove old expirations and set the new ones
		if(me.getCurrentstateId().equals(sdf.getStatedef().getId())) {
			System.out.println("ErrendManagement.setupExpirationsOnTransition Till samma tillstånd");
			// To same state
			// 1. Remove only the transitions pointing to same state
			List<StateExpiration> _expirations = StateExpirationDB.getAllStateExpirationsOfManagedErrend(me.getId());
			for(StateExpiration expiration : _expirations) {
				if(expiration.getTransitiondef().getTargetstatedefId().equals(sdf.getStatedef().getId())) {
					expiration.delete();
					System.out.println("ErrendManagement.setupExpirationsOnTransition Tog bort "+expiration);
				}
				else {
					System.out.println("ErrendManagement.setupExpirationsOnTransition Behöll "+expiration);
				}
			}
			// 2. Create only for the transitions pointing to same state
			List<TransitiondefManaged> tdms = sdf.getSourceTransitiondefs();
			for(TransitiondefManaged tdm : tdms) {
				if(!tdm.getTransitiondef().getTargetstatedef().getId().equals(sdf.getStatedef().getId())) {
					// Transition points to other state. Skip it.
					System.out.println("ErrendManagement.setupExpirationsOnTransition pekar till annat "+tdm.getTransitiondef());
					continue;
				}
				// Transition points to same state
				EdgeConfiguration ecf = tdm.getEc();
				List<StatetimeExpiration> expirations = ecf.getStatetimeExpirations();
				for(StatetimeExpiration ste : expirations) {
					long millisToExpiration = ste.getMillisToExpiration();
					StateExpiration se = new StateExpiration();
					se.setExpirey(new Date(System.currentTimeMillis()+millisToExpiration));
					se.setManagederrendId(me.getId());
					se.setTransitiondefId(tdm.getTransitiondef().getId());
					se.save();
				}
			}
		}
		else {
			// To other state. Remove all, initiate all
			System.out.println("Till annat tillstånd");
			me.deleteExpirations();
			List<TransitiondefManaged> tdms = sdf.getSourceTransitiondefs();
			for(TransitiondefManaged tdm : tdms) {
				EdgeConfiguration ecf = tdm.getEc();
				List<StatetimeExpiration> expirations = ecf.getStatetimeExpirations();
				for(StatetimeExpiration ste : expirations) {
					long millisToExpiration = ste.getMillisToExpiration();
					StateExpiration se = new StateExpiration();
					se.setExpirey(new Date(System.currentTimeMillis()+millisToExpiration));
					se.setManagederrendId(me.getId());
					se.setTransitiondefId(tdm.getTransitiondef().getId());
					se.save();
				}
			}
		}
	}
	
	public void setupExpirationsOnInitiation() throws Exception {
		ManagedErrend me = actor.getErrend().getManagedErrend();
		//StatedefManaged sdf = currentState;
		//System.out.println("Till annat tillstånd");
		me.deleteExpirations();
		List<TransitiondefManaged> tdms = currentState.getSourceTransitiondefs();
		for(TransitiondefManaged tdm : tdms) {
			EdgeConfiguration ecf = tdm.getEc();
			List<StatetimeExpiration> expirations = ecf.getStatetimeExpirations();
			for(StatetimeExpiration ste : expirations) {
				long millisToExpiration = ste.getMillisToExpiration();
				StateExpiration se = new StateExpiration();
				se.setExpirey(new Date(System.currentTimeMillis()+millisToExpiration));
				se.setManagederrendId(me.getId());
				se.setTransitiondefId(tdm.getTransitiondef().getId());
				se.save();
			}
		}
	}
	
	public boolean performTransition(String transitionname, Session session) throws Exception {
		//System.out.println("ErrendManagement.performTransition transitionname="+transitionname);
		List<TransitiondefManaged> transitiondefs = currentState.getSourceTransitiondefs();
		for(TransitiondefManaged tdm : transitiondefs) {
			System.out.println("ErrendManagement.performTransition "+tdm.getTransitionname()+" : "+transitionname);
			if(tdm.getTransitionname().equals(transitionname)) {
				System.out.println("ErrendManagement.performTransition "+"JA transitionname "+transitionname);
				String rolename = session.getActor().getRoledef().getRolename();
				if(tdm.allowsTransitionOnRolename(rolename)) {
					System.out.println("ErrendManagement.performTransition "+"JA + JA role "+rolename);
					if(allowsTransitionInParticularState(tdm, rolename, session)) {
						System.out.println("ErrendManagement.performTransition "+"JA + JA + JA particular state");
						return commitTransitionDoActions(session, tdm);
					}
					else {
						System.out.println("ErrendManagement.performTransition "+"JA + JA + NEJ particular state");
					}
				}
				else {
					System.out.println("ErrendManagement.performTransition "+"JA + NEJ role "+rolename);
				}
			}
			else {
				System.out.println("ErrendManagement.performTransition "+"NEJ transitionname "+transitionname);
			}
		}
		throw new Exception("Övergången lyckades inte. Tillstånd "+currentState.getStatename()+" med övergången "+transitionname);
	}
	
	private boolean allowsTransitionInParticularState(TransitiondefManaged tdm, String rolename, Session session) {
		if(rolename.equals("deamon")) {
			return true;
		}
		EdgeConfiguration ec = tdm.getEc();
		AllowTransition at = ec.getFirstAllowedTransitionOnRole(rolename);
		if(at == null) {
			return false;
		}
		String functionname = at.getFunction();
		if(functionname == null) {
			return true;
		}
		
		String installationidentifier = null;
		try {
			installationidentifier = PersistentValuesDB.getStringValue(PropertyNames.installation);
		} catch (Exception e) {
			installationidentifier = "installationens identifierare kunde inte bestämmas";
			e.printStackTrace();
		}

		
		Processdef pd = actor.getErrend().getManagedErrend().getProcessdef();
		FunctionDef fd = pd.getFunctionDef(functionname);
		String functionDefinition = fd.getSource();
		List<ProducerConfiguration> producers = null;
		try {
			InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(functionDefinition.getBytes("UTF-8")));
	    	producers = PrvErrendProducersConfigParser.parseProducerConfig(isr , "Processdef "+pd.getId()+", FunctionDef "+fd.getId());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(producers == null) {
			return false;
		}

		for(ProducerConfiguration producer : producers) {
			GeneralProducerVisitor visitor = new GeneralProducerVisitor(actor, session.getActor(), tdm.getTransitiondef(), installationidentifier);
			producer.accept(visitor);
			Boolean out = (Boolean) visitor.getOut();
			if(out) {
				return true;
			}
		}

		return true;
	}
	
	private boolean commitTransitionDoActions(Session session, TransitiondefManaged tdm) throws Exception {
		System.out.println("ErrendManagement.commitTransitionDoActions action="+tdm.getTransitionname());
		EdgeConfiguration ec = tdm.getEc(); 
		//Transitiondef transdef = tdm.getTransitiondef();
		String rolename = session.getActor().getRoledef().getRolename();
		System.out.println("ErrendManagement.commitTransitionDoActions rollnamn "+rolename);
		List<TransitionAction> tactions = ec.getApplyingTransitionActions(rolename);
		System.out.println("ErrendManagement.commitTransitionDoActions fick "+tactions.size()+" tillämpbara övergångs-uppgifter");
		for(TransitionAction taction : tactions) {
			List<String> roles = taction.getRoles();
			List<EdgeAction> eas = taction.getActions();
			for(EdgeAction ea : eas) {
				EdgeActionPerformer eap = new EdgeActionPerformer(roles, session, taction, tdm.getTransitiondef());
				ea.accept(eap);
			}
		}
		System.out.println("ErrendManagement.commitTransitionDoActions efter snurror");
		makeNodesForTransition(tdm, session);

		Transitiondef td = tdm.getTransitiondef();
		System.out.println("Source "+td.getSourcestatedefId()+", target "+td.getTargetstatedefId());
		Statedef next = StatedefDB.getStatedefFromId(td.getTargetstatedefId());
		currentState = getManaged(next);
		System.out.println("ErrendManagement namn på tillstånd efter "+getCurrentStatename());
		return true;
	}

}
