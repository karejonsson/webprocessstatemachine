package se.prv.errends.mgmt;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;

public class TransitiondefManaged {
	
	private Transitiondef transition = null;
	private EdgeConfiguration ec = null;
	
	public String toString() {
		return "{ TransitiondefManaged: transition "+transition+" }";
	}
	
	public TransitiondefManaged(Transitiondef td) throws Exception {
		this.transition = td;
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(td.getLayout().getBytes("UTF-8")));
        ec = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Transitiondef config id = "+td.getId());
	}

	public String getTransitionname() {
		return transition.getTransitionname();
	}
	
	public boolean allowsTransitionOnRolename(String rolename) {
		return ec.allowsTransitionOnRolename(rolename);
	}

	public Transitiondef getTransitiondef() {
		return transition;
	}
	
	public EdgeConfiguration getEc() {
		return ec;
	}
	
}
