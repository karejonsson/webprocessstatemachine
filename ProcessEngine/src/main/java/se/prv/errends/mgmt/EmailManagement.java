package se.prv.errends.mgmt;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import general.reuse.codeloading.DynamicClassLoader;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import se.prv.errends.action.EdgeActionPerformer;
import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.evaluation.GeneralProducerVisitor;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;
import se.prv.errends.reuse.cli.PropertyNames;

public class EmailManagement {

	// Called when GUI-user sends written message through the internal message handler.
	// Called when a role in an errend has been appointed to the agent.
	public static boolean sendMessageFromFunctionByProperties(Actor context, String titleFunctionProperty, String messageFunctionProperty, Actor current, se.prv.errends.domain.Message internalMessage) {
		System.out.println("EmailManagement.sendMessageFromFunctionByProperties titleFunctionProperty="+titleFunctionProperty);
		System.out.println("EmailManagement.sendMessageFromFunctionByProperties messageFunctionProperty="+messageFunctionProperty);
		System.out.println("EmailManagement.sendMessageFromFunctionByProperties current="+current);
				
		if(!context.getAgent().isActive()) {
			return false;
		}
		Person person = context.getAgent().getPerson();
		if(!person.isActive()) {
			return false;
		}
		String message = null;
		String title = null;
		try {
			String messageFunction = PersistentValuesDB.getStringValue(messageFunctionProperty);
			message = createTextByFunctionAtMessageSent(context, messageFunction, current, internalMessage);
			String titleFunction = PersistentValuesDB.getStringValue(titleFunctionProperty);
			title = createTextByFunctionAtMessageSent(context, titleFunction, current, internalMessage);			
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println("EmailManagement.sendMessageFromFunctionByProperties "+person+", titleproperty="+titleFunctionProperty+", messageproperty="+messageFunctionProperty);
		return sendMessageFromFunctionByProperties(context, title, message);
	}
	
	public static boolean sendMessageFromFunctionByProperties(Actor context, String title, String message) {
		if(!context.getAgent().isActive()) {
			return false;
		}
		Person person = context.getAgent().getPerson();
		if(!person.isActive()) {
			return false;
		}
		System.out.println("EmailManagement.sendMessageFromFunctionByProperties "+person);
		try {
			Boolean blockmail = PersistentValuesDB.getBooleanValue(PropertyNames.blockmail);
			if(blockmail == null || blockmail == true) {
				System.out.println("Skickar inte mejlet.");
				return true;
			}
			//System.out.println("EdgeActionPerformer.send "+person);
			String smptserver = PersistentValuesDB.getStringValue(PropertyNames.smtpserver);
			String systemdefaultemailaddress = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailaddress);
			String systemdefaultemailpassword = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailpassword);
			int smtpport = PersistentValuesDB.getIntegerValue(PropertyNames.smtpport);
			boolean smtpauthenticate = PersistentValuesDB.getBooleanValue(PropertyNames.smtpauthenticate);
			Boolean smtpauth = PersistentValuesDB.getBooleanValue(PropertyNames.smtpauth);
			sendMail(person.getEmail(), systemdefaultemailaddress, systemdefaultemailpassword, title, message, smptserver, smtpauth, smtpport, smtpauthenticate);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	// Called from EdgeAction
	public static boolean sendEmailWithPrecalculatedTitleAndMessage(Agent agent, String title, String message) {
		System.out.println("EmailManagement.sendEmailWithTitleAndMessage "+agent+", title="+title+", message="+message);
		if(!agent.isActive()) {
			System.out.println("EmailManagemnent.sendEmailWithTitleAndMessage: Ej aktiv agent.");
			return false;
		}
		Person person = agent.getPerson();
		if(!person.isActive()) {
			System.out.println("EmailManagemnent.sendEmailWithTitleAndMessage: Ej aktiv person.");
			return false;
		}
		try {
			Boolean blockmail = PersistentValuesDB.getBooleanValue(PropertyNames.blockmail);
			if(blockmail == null || blockmail == true) {
				System.out.println("EmailManagemnent.sendEmailWithTitleAndMessage: Skickar inte mejlet.");
				return true;
			}
			System.out.println("EdgeActionPerformer.send "+person);
			String smptserver = PersistentValuesDB.getStringValue(PropertyNames.smtpserver);
			String systemdefaultemailaddress = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailaddress);
			String systemdefaultemailpassword = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailpassword);
			int smtpport = PersistentValuesDB.getIntegerValue(PropertyNames.smtpport);
			boolean smtpauthenticate = PersistentValuesDB.getBooleanValue(PropertyNames.smtpauthenticate);
			Boolean smtpauth = PersistentValuesDB.getBooleanValue(PropertyNames.smtpauth);
			sendMail(person.getEmail(), systemdefaultemailaddress, systemdefaultemailpassword, title, message, smptserver, smtpauth, smtpport, smtpauthenticate);
		}
		catch(Exception e) {
			System.out.println("EmailManagement.sendEmailWithTitleAndMessage slut false");
			e.printStackTrace();
			return false;
		}
		System.out.println("EmailManagement.sendEmailWithTitleAndMessage slut true");
		return true;
	}
	
	public static String createMessageToSendByInlineAtUserTransition(Actor receiver, Transitiondef transdef, String groovyscript_messageproducer) {
		if(!receiver.getAgent().isActive()) {
			System.err.println("EmailManagement.createMessageToSendByInlineAtUserTransition Ombudet ej aktivt. id="+receiver.getAgent().getId());
			return "";
		}
		if(!receiver.getAgent().getPerson().isActive()) {
			System.err.println("EmailManagement.createMessageToSendByInlineAtUserTransitionPersonen ej aktiv. id="+receiver.getAgent().getPerson().getId());
			return "";
		}
		Binding bind = new Binding();
		bind.setProperty("actor", receiver);
		bind.setProperty("transition", transdef);
		//DynamicClassLoader reusedClassLoader = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
		DynamicClassLoader reusedClassLoader = new DynamicClassLoader(EdgeActionPerformer.class.getClassLoader());
		GroovyShell shell = new GroovyShell(reusedClassLoader, bind);
		Object message = shell.evaluate(groovyscript_messageproducer, EdgeActionPerformer.class.getSimpleName(), "");
		if(message == null) { 
			System.err.println("EmailManagement.createMessageToSendByInlineAtUserTransition ingen text producerades");
			return "";
		}
		return message.toString();
	}

	public static String createTextByFunctionAtMessageSent(Actor actorContext, String functionname, Actor actorCurrent, se.prv.errends.domain.Message internalMessage) {
		FunctionDef fd = null;
		Processdef pd = null;
		try {
			pd = actorContext.getErrend().getManagedErrend().getProcessdef();
			fd = pd.getFunctionDef(functionname);
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
		if(fd == null) {
			System.out.println("E-EmailManagement.createTextByFunctionAtMessageSent ingen återanvänd funktion hittades. functionname="+functionname+((pd != null) ? ", processdef.id = "+pd.getId() : ", pd == null"));
			List<FunctionDef> defs = FunctionDefDB.getAllFunctionDefsByName(functionname);
			if(defs.size() > 0) {
				fd = defs.get(0);
			}
		}
		if(fd == null) {
			System.err.println("EmailManagement.createTextByFunctionAtMessageSent Inget sätt att bestämma funktion för meddelandet. functionname="+functionname);
			return "";
		}
		String installationidentifier = null;
		try {
			installationidentifier = PersistentValuesDB.getStringValue(PropertyNames.installation);
		} catch (Exception e) {
			installationidentifier = "installationens identifierare kunde inte bestämmas";
			e.printStackTrace();
		}
		System.out.println("EmailManagement.createTextByFunctionAtMessageSent till slutet");
		return createTextByFunctionAtMessageSent(actorContext, fd, actorCurrent, internalMessage, installationidentifier);
	}
	
	public static String createTextByFunctionAtMessageSent(Actor actorContext, FunctionDef fd, Actor actorCurrent, se.prv.errends.domain.Message internalMessage, String installationidentifier) {
		GeneralProducerVisitor visitor = new GeneralProducerVisitor(actorContext, actorCurrent, internalMessage, installationidentifier);
		return createTextByFunction(actorContext, fd, visitor, "FunctionDef "+fd.getId());
	}
	
	public static String createTextByFunctionAtUserTransition(Actor actorContext, Transitiondef transdef, String functionname, Actor actorCurrent, String installationidentifier) {
		GeneralProducerVisitor visitor = new GeneralProducerVisitor(actorContext, actorCurrent, transdef, installationidentifier);
		Processdef pd = actorContext.getErrend().getManagedErrend().getProcessdef();
		FunctionDef fd = pd.getFunctionDef(functionname);
		System.out.println("EmailManagement.createTextByFunctionAtUserTransition till slutet");
		return createTextByFunction(actorContext, fd, visitor, "Processdef "+pd.getId()+" -> FunctionDef "+fd.getId());
	}
	
	public static String createTextByFunction(Actor actorContext, FunctionDef fd, GeneralProducerVisitor visitor, String parserMessage) {
		if(!actorContext.getAgent().isActive()) {
			System.err.println("EmailManagement.createTextByFunction Ombudet ej aktivt. id="+actorContext.getAgent().getId());
			return "";
		}
		if(!actorContext.getAgent().getPerson().isActive()) {
			System.err.println("EmailManagement.createTextByFunction Personen ej aktiv. id="+actorContext.getAgent().getPerson().getId());
			return "";
		}
		ProducerConfiguration producer = ProducerConfigurationManagement.getProducerConfigurationFromFunctionDef(fd);
		producer.accept(visitor);
		
		System.out.println("EmailManagement.createTextByFunction till slutet");
		return visitor.getOut().toString();
	}
	
	public static String createTextByFunction(Actor actorContext, ProducerConfiguration producer, GeneralProducerVisitor visitor) {
		producer.accept(visitor);		
		return visitor.getOut().toString();
	}
	
	// When sending test e-mail
	public static boolean sendTestMail() {
		try {
			String title = "Testbrev";
			String smptserver = PersistentValuesDB.getStringValue(PropertyNames.smtpserver);
			String systemdefaultemailaddress = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailaddress);
			String systemdefaultemailpassword = PersistentValuesDB.getStringValue(PropertyNames.systemdefaultemailpassword);
			int smtpport = PersistentValuesDB.getIntegerValue(PropertyNames.smtpport);
			boolean starttlsenable = PersistentValuesDB.getBooleanValue(PropertyNames.starttlsenable);
			Boolean blockmail = PersistentValuesDB.getBooleanValue(PropertyNames.blockmail);
			Boolean smtpauth = PersistentValuesDB.getBooleanValue(PropertyNames.smtpauth);
			String testmailreceiver = PersistentValuesDB.getStringValue(PropertyNames.testmailreceiver);
			String mailMessage = 
					"testmailreceiver : "+testmailreceiver+
					"\nsmptserver : "+smptserver+
					"\nsystemdefaultemailaddress : "+systemdefaultemailaddress+
					"\nsystemdefaultemailpassword : "+systemdefaultemailpassword+
					"\nsmtpport : "+smtpport+
					"\nstarttlsenable : "+starttlsenable+
					"\nsmtpauth : "+smtpauth+
					"\nblockmail : "+blockmail+", skickning hade blockerats utifrån "+(blockmail == null || blockmail == true);
			sendMail(testmailreceiver, systemdefaultemailaddress, systemdefaultemailpassword, title, mailMessage, smptserver, smtpauth, smtpport, starttlsenable);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
    private static Properties getProperties(String starttlsenable, String auth, String host, String port) {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", starttlsenable);
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        return props;
    }
	
	private static void sendMail(
			String receiver, 
			String sender,
			String senderpwd,
			String title, 
			String messageText, 
			String smtpserver, 
			boolean smtpauth, 
			int smtpport, 
			boolean starttlsenable) {
		
		System.out.println("receiver : "+receiver);
		System.out.println("sender : "+sender);
		System.out.println("senderpwd : "+senderpwd);
		System.out.println("title : "+title);
		System.out.println("messageText :\n----------------------------\n"+messageText+"\n----------------------------");
		System.out.println("smtpserver : "+smtpserver);
		System.out.println("smtpauth : "+smtpauth);
		System.out.println("smtpport : "+smtpport);
		System.out.println("starttlsenable : "+starttlsenable);
		
		javax.mail.Session session = javax.mail.Session.getInstance(getProperties(""+starttlsenable, ""+smtpauth, smtpserver, ""+smtpport),
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(sender, senderpwd);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver));
			message.setSubject(title);
			message.setText(messageText);

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		System.out.println("E-posten har skickats.");
	}

}
