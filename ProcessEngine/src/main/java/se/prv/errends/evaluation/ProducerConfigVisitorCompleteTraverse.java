package se.prv.errends.evaluation;

import se.prv.errends.producerconfig.struct.ProducerConfigVisitor;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public abstract class ProducerConfigVisitorCompleteTraverse implements ProducerConfigVisitor {
	
	public abstract void attention(ProducerConfiguration pc);

	@Override
	public void visit(ProducerConfiguration pc) {
		attention(pc);
		for(int idx = 0 ; idx < pc.getNoPreparedFunctions() ; idx++) {
			pc.getPreparedFunctions(idx).accept(this);
		}
		for(int idx = 0 ; idx < pc.getNoCurrentFunctions() ; idx++) {
			pc.getCurrentFunctions(idx).accept(this);
		}
		for(int idx = 0 ; idx < pc.getNoContextFunctions() ; idx++) {
			pc.getContextFunctions(idx).accept(this);
		}
		pc.getGroovyEvaluable().accept(this);
	}

}
