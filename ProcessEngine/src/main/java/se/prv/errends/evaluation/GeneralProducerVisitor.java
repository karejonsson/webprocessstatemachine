package se.prv.errends.evaluation;

import java.util.List;

import general.reuse.codeloading.DynamicClassLoader;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import se.prv.errends.action.EdgeActionPerformer;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Message;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.producerconfig.struct.ActorContextFunction;
import se.prv.errends.producerconfig.struct.ActorCurrentFunction;
import se.prv.errends.producerconfig.struct.AllNotInitiatedErrendsFunction;
import se.prv.errends.producerconfig.struct.GroovyBooleanEvaluable;
import se.prv.errends.producerconfig.struct.GroovyStringEvaluable;
import se.prv.errends.producerconfig.struct.InstallationIdentifierCurrentFunction;
import se.prv.errends.producerconfig.struct.MessageCurrentFunction;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;
import se.prv.errends.producerconfig.struct.SuperuserCurrentFunction;
import se.prv.errends.producerconfig.struct.TransitionCurrentFunction;

public class GeneralProducerVisitor extends ProducerConfigVisitorCompleteTraverse {
	
	private Binding bind = new Binding();
	
	private Actor context;
	private Actor current;
	private Transitiondef transdef;
	private String name;
	private Message message;
	private String identifier;
	private List<Errend> errends;
	
	public GeneralProducerVisitor(Actor context, Actor current, String identifier) {
		this(context, current, (Transitiondef) null, identifier, null);
	}

	public GeneralProducerVisitor(Actor context, Actor current, String identifier, List<Errend> errends) {
		this(context, current, (Transitiondef) null, identifier, errends);
	}

	public GeneralProducerVisitor(Actor context, Actor current, Transitiondef transdef, String identifier) {
		this(context, current, transdef, identifier, null);
	}

	public GeneralProducerVisitor(Actor context, Actor current, Transitiondef transdef, String identifier, List<Errend> errends) {
		this.context = context;
		this.current = current;
		this.transdef = transdef;
		this.identifier = identifier;
		this.errends = errends;
	}

	public GeneralProducerVisitor(Actor context, Actor current, Message message, String identifier) {
		this(context, current, message, identifier, null);
	}
	
	public GeneralProducerVisitor(Actor context, Actor current, Message message, String identifier, List<Errend> errends) {
		this.context = context;
		this.current = current;
		this.message = message;
		this.identifier = identifier;
		this.errends = errends;
	}
	
	public void reset() {
		System.out.println("GeneralProducerVisitor - reset");
		bind = new Binding();
		out = null;
	}

	@Override
	public void attention(ProducerConfiguration pc) {
		System.out.println("GeneralProducerVisitor - attention "+pc.getName());
		name = pc.getName();
	}

	@Override
	public void visit(ActorCurrentFunction aif) {
		System.out.println("GeneralProducerVisitor - ActorCurrentFunction "+aif.getName());
		bind.setProperty(aif.getName(), current);
	}

	@Override
	public void visit(ActorContextFunction aif) {
		System.out.println("GeneralProducerVisitor - ActorContextFunction "+aif.getName());
		bind.setProperty(aif.getName(), context);
	}

	@Override
	public void visit(TransitionCurrentFunction tcf) {
		System.out.println("GeneralProducerVisitor - TransitionCurrentFunction "+tcf.getName());
		bind.setProperty(tcf.getName(), transdef);
	}

	@Override
	public void visit(SuperuserCurrentFunction suif) {
		System.out.println("GeneralProducerVisitor - SuperuserCurrentFunction "+suif.getName());
		bind.setProperty(suif.getName(), current);
	}

	@Override
	public void visit(AllNotInitiatedErrendsFunction anief) {
		System.out.println("GeneralProducerVisitor - AllNotInitiatedErrendsFunction");
		if(errends == null) {
			errends = ErrendDB.getAllNotInitiatedErrends();
		}
		bind.setProperty(anief.getName(), errends);
	}

	@Override
	public void visit(MessageCurrentFunction mcf) {
		System.out.println("GeneralProducerVisitor - MessageCurrentFunction "+mcf.getName());
		bind.setProperty(mcf.getName(), message);
	}

	@Override
	public void visit(GroovyBooleanEvaluable gbe) {
		System.out.println("GeneralProducerVisitor - GroovyBooleanEvaluable type "+gbe.typename);
		DynamicClassLoader reusedClassLoader = new DynamicClassLoader(EdgeActionPerformer.class.getClassLoader());
		GroovyShell shell = new GroovyShell(reusedClassLoader, bind);
		out = shell.evaluate(gbe.getScript(), name, "");
		if(out == null) {
			out = Boolean.FALSE;
		}
	}

	@Override
	public void visit(GroovyStringEvaluable gse) {
		System.out.println("GeneralProducerVisitor - GroovyStringEvaluable type "+gse.typename);
		DynamicClassLoader reusedClassLoader = new DynamicClassLoader(EdgeActionPerformer.class.getClassLoader());
		GroovyShell shell = new GroovyShell(reusedClassLoader, bind);
		out = shell.evaluate(gse.getScript(), name.replaceAll("-", ""), "");
		if(out == null) {
			out = "";
		}
	}
	
	@Override
	public void visit(InstallationIdentifierCurrentFunction iicf) {
		System.out.println("GeneralProducerVisitor - InstallationIdentifierCurrentFunction type "+iicf.getName());
		bind.setProperty(iicf.getName(), identifier);
	}

	
	private Object out = null;
	
	public Object getOut() {
		System.out.println("GeneralProducerVisitor - getOut");
		return out;
	}

	public void setContext(Actor context) {
		System.out.println("GeneralProducerVisitor - setContext "+context);
		this.context = context;
	}

}
