package se.prv.errends.mainweb.reporttest;

// https://gist.github.com/rponte/5044469

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class GenerateSimplePdfReportWithJasperReports {

	public static void main(String[] args) {
		
		try {
		
			String reportName = "/home/kare/ws/jasper/Blank_A4_Kåre_test";
			//Map<String, Object> parameters = new HashMap<String, Object>();
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("UPPERTEXT", "En harrang i övre textfältet");
			m.put("MIDDLETEXT", "En harrang i det mittre textfältet\nsom sträcker sig över tre rader minst\n"
					+ "men som också har en rad som är så lång att vi kommer på se vad rapportverktyget gör när den måste hantera det själv genom att troligtvis bryta raden men i övrigt visa vad den har för strategi att falla tillbaka på.");
			m.put("LOWERTEXT", "En harrang i lägre textfältet");

			// compiles jrxml
			JasperCompileManager.compileReportToFile(reportName + ".jrxml");
			// fills compiled report with parameters and a connection
			JasperPrint print = JasperFillManager.fillReport(reportName + ".jasper", m);
			// exports report to pdf
			JRExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, new FileOutputStream(reportName + ".pdf")); // your output goes here
			
			exporter.exportReport();

		} catch (Exception e) {
			throw new RuntimeException("It's not possible to generate the pdf report.", e);
		} finally {
			// it's your responsibility to close the connection, don't forget it!
		}
		
}
}
