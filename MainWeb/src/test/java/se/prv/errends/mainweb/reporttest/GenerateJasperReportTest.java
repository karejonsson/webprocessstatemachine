package se.prv.errends.mainweb.reporttest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class GenerateJasperReportTest {

	public static final String filename = "First";

	@Before
	public void setup() throws JRException, IOException {
		String project = System.getProperty("user.dir");
		String reportResources = project+"/src/test/resources/reports";
		File tmpdir = new File(reportResources+"/tmp");
		
		if(!tmpdir.exists()) {
			tmpdir.mkdirs();
		}
		
		File input = new File(reportResources+"/"+filename+".jrxml");
		Assert.assertTrue(input.exists());
		
		File output = new File(tmpdir, filename+".jasper");
		if(output.exists()) {
			output.delete();
		}
		Assert.assertTrue(!output.exists());
		
		String wholeJRXMLWithPlaceholder = StringFileManipulation.readFullyToString(new FileInputStream(input));
		String wholeJRXMLFixedForImage = wholeJRXMLWithPlaceholder.replaceAll("RESOURCEREPORTFOLDER", reportResources);
		
		System.out.println(wholeJRXMLFixedForImage);
		
		ByteArrayInputStream sourceJRXML = new ByteArrayInputStream(wholeJRXMLFixedForImage.getBytes(StandardCharsets.UTF_8.name()));
		JasperCompileManager.compileReportToStream(sourceJRXML, new FileOutputStream(reportResources+"/tmp/"+filename+".jasper"));
	}
	
	@After
	public void tearDown() {
		String project = System.getProperty("user.dir");
		String reportResources = project+"/src/test/resources/reports";
		File tmpdir = new File(reportResources+"/tmp");
		tmpdir.delete();
	}
	
	@Test
	public void createTemplate() throws JRException, IOException {
		String project = System.getProperty("user.dir");
		String reportResources = project+"/src/test/resources/reports";
		File jasper = new File(reportResources+"/tmp/"+filename+".jasper");
		Assert.assertTrue(jasper.exists());	
		
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasper);
		JRParameter[] pars = jasperReport.getParameters();
		for(JRParameter par : pars) {
			if(!par.isSystemDefined()) {
				System.out.println(par.getName());
			}
		}
		Assert.assertTrue(hasUserParameter(pars, "UPPERTEXT"));	
		Assert.assertTrue(hasUserParameter(pars, "MIDDLETEXT"));	
	}
	
	private boolean hasUserParameter(JRParameter[] pars, String parametername) {
		for(JRParameter par : pars) {
			if(!par.isSystemDefined() && par.getName().equals(parametername)) {
				return true;
			}
		}
		return false;
	}
	
	@Test
	public void generatePDF() throws JRException, FileNotFoundException {
		Map parameters = new HashMap();
		parameters.put("UPPERTEXT", "En harrang i övre textfältet");
		parameters.put("MIDDLETEXT", "En harrang i det mittre textfältet\nsom sträcker sig över tre rader minst\n"
				+ "men som också har en rad som är så lång att vi kommer på se vad rapportverktyget gör när den måste hantera det själv genom att troligtvis bryta raden men i övrigt visa vad den har för strategi att falla tillbaka på.");

		String project = System.getProperty("user.dir");
		String reportResources = project+"/src/test/resources/reports";
		File jasper = new File(reportResources+"/tmp/"+filename+".jasper");

		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasper);

		JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());

		// Export pdf file
		JasperExportManager.exportReportToPdfStream(jprint, new FileOutputStream(reportResources+"/tmp/"+filename+".pdf"));
		File pdf = new File(reportResources+"/tmp/"+filename+".pdf");
		Assert.assertTrue(pdf.exists());		
	}

}
