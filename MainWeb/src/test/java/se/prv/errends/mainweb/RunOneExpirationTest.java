package se.prv.errends.mainweb;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.errends.config.AnalyseTest;
import se.prv.errends.dbc.AdminuserTest;
import se.prv.errends.dbc.CommonDB;
import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.AgentTest;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.dbc.PersistentValuesTest;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.dbcreuse.TestUtilities;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.SchedulerManagement;
import se.prv.errends.processengine.reuse.StringFileManipulation;
import se.prv.errends.reuse.cli.PropertyNames;

public class RunOneExpirationTest {

	private JDBCConnectionPool pool = null;

	@Before
	public void setup() throws IOException, SQLException {
	    CommonDB.clearCache();
	    pool = TestUtilities.setupPool();
	}
	
	@After
	public void tearDown() throws Exception {
	    TestUtilities.destroyPool(pool);
	    CommonDB.clearCache();
	}
	
	public void setupStatemachineWithExpiration() throws Exception {
		//AdminuserTest.recursiveSetup();
		
		PersistentValuesDB.setStringValue(PropertyNames.cron_schema_respite, "*/2 * * * * ?");

		Person p1 = new Person();
		p1.setActive(true);
		p1.setEmail("kare.jonsson@prv.se");
		p1.setFamilyname("Johansson");
		p1.setFirstname("Kåre");
		p1.setSwedishpnr(Person.makeCompletionWithChecknumber("690601015"));
		p1.setRegisterred(new Date(System.currentTimeMillis()));
		p1.save();
		
		Person p2 = new Person();
		p2.setActive(true);
		p2.setEmail("kare.jonsson@prv.se");
		p2.setFamilyname("Jonsson");
		p2.setFirstname("Kåre");
		p2.setSwedishpnr("196905010150");
		p2.setRegisterred(new Date(System.currentTimeMillis()));
		p2.save();
		
		AgentTest.recursiveSetup(p1);
		
		Adminuser au = new Adminuser();
		au.setActive(true);
		au.setPassword(Adminuser.calculateChecksum("super"));
		au.setPersonId(p2.getId());
		au.setSuperprivilegies(true);
		au.setUsername("super");
		au.save();

		ClassLoader cl = RunOneExpirationTest.class.getClassLoader();
		InputStream is = cl.getResourceAsStream("expiration.graphml");

		Processfile pf = new Processfile();
		pf.setDescription("Så lite det går med utgången frist");
		pf.setAdminuserId(au.getId());
		pf.setFilename("expiration.graphml");
		ProcessfileDB.createProcessfile(pf);
		byte[] data = StringFileManipulation.readFullyToBytearray(is);
		ProcessfileDB.setData(pf.getId(), data);
		
		DBInitialisation.initializeProcessNodes(pf, "Some description", "Some process name");
	}
	
	@Test
	public void expiration() throws Exception {
		setupStatemachineWithExpiration();
		PersistentValuesTest.setupData();

		Long eId = null;
		{
			Person p = PersonDB.getAllActivePersons().get(0);
			Agent oa = AgentDB.getAllAgents().get(0);
			Processdef pd = ProcessdefDB.getAllProcessdefs().get(0);
			ErrendManagement em = ErrendManagement.createErrendFromDBStructureAndInitialize(oa, pd, "Evighetsmaskin");
			em.getErrend().setAdminusername("PCT/SE2017/001");
			if(em.isError()) {
				System.out.println("Fel :"+em.getErrorMessage());
				Assert.assertTrue(false);
			}
			
			eId = em.getErrend().getId();
		}
		
		SchedulerManagement.initialize();

		Assert.assertTrue(ErrendDB.getErrendFromId(eId).getManagedErrend().getCurrentStatedef().getStatename().equals("Början"));
		Assert.assertTrue(StateExpirationDB.getAllStateExpirationsExpired().size() == 0);
		System.out.println("FELET 1 : "+StateExpirationDB.getAllStateExpirations().size());
		Assert.assertTrue(StateExpirationDB.getAllStateExpirations().size() == 1);
		Thread.sleep(3000);
		System.out.println("FELET 2 : "+StateExpirationDB.getAllStateExpirationsExpired().size());
		Assert.assertTrue(StateExpirationDB.getAllStateExpirationsExpired().size() == 0);
		System.out.println("FELET 3 : "+StateExpirationDB.getAllStateExpirations().size());
		Assert.assertTrue(StateExpirationDB.getAllStateExpirations().size() == 0);
		Assert.assertTrue(ErrendDB.getErrendFromId(eId).getManagedErrend().getCurrentStatedef().getStatename().equals("Slut"));
	}
	
}
