package se.prv.errends.mainweb.reporttest;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

// https://www.tutorialspoint.com/jasper_reports/jasper_report_parameters.htm

public class JasperReportFill {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws JRException {
		String jrxml = "/home/kare/JaspersoftWorkspace/MyReports/Blank_A4_Kåre_test.jrxml";//"/home/kare/ws/jasper/2/jasper_report_template.jrxml";
		//String jasper = jrxml.replaceAll("jrxml", "jasper");
		//String pdf = jrxml.replaceAll("jrxml", "pdf");

		DataBeanList DataBeanList = new DataBeanList();
		ArrayList<DataBean> dataList = DataBeanList.getDataBeanList();

		JRBeanCollectionDataSource beanColDataSource =
				new JRBeanCollectionDataSource(dataList, true);

		Map parameters = new HashMap();
		/**
		 * Passing ReportTitle and Author as parameters
		 */
		parameters.put("UPPERTEXT", "En harrang i övre textfältet");
		parameters.put("MIDDLETEXT", "En harrang i det mittre textfältet\nsom sträcker sig över tre rader minst\n"
				+ "men som också har en rad som är så lång att vi kommer på se vad rapportverktyget gör när den måste hantera det själv genom att troligtvis bryta raden men i övrigt visa vad den har för strategi att falla tillbaka på.");

		//parameters.put("ReportTitle", "List of Contacts");
		//parameters.put("Author", "Prepared By Manisha");

		try {
			JasperReport jasper = JasperCompileManager.compileReport(jrxml);
			
			//JasperExportManager.
			

			// Generate jasper print
			JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasper, parameters, new JREmptyDataSource());
			//JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasper, parameters, beanColDataSource);//new JREmptyDataSource());

			// Export pdf file
			ByteArrayOutputStream pdf = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jprint, pdf);

			
			//JasperFillManager.fillReportToFile(jasper, parameters, beanColDataSource);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}
}



