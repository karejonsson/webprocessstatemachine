package se.prv.errends.mainweb.reporttest;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class Jasper {

	public static final String jrxml = "/home/kare/ws/jasper/Blank_A4_Kåre_test.jrxml";
	public static final String jasper = jrxml.replaceAll("jrxml", "jasper");
	public static final String outfile = "/home/kare/ws/jasper/x.pdf"; 

	public static void main(String args[]) {

		Map<String, Object> m = new HashMap<String, Object>();
		m.put("UPPERTEXT", "En harrang i övre textfältet");
		m.put("MIDDLETEXT", "En harrang i det mittre textfältet\nsom sträcker sig över tre rader minst\n"
				+ "men som också har en rad som är så lång att vi kommer på se vad rapportverktyget gör när den måste hantera det själv genom att troligtvis bryta raden men i övrigt visa vad den har för strategi att falla tillbaka på.");
		m.put("LOWERTEXT", "En harrang i lägre textfältet");
 
		try {
			//String jrxmlFileName = jrxml;
			//String jasperFileName = jrxml.replaceAll("jrxml", "jasper");

			JasperCompileManager.compileReportToFile(jrxml, jasper);
			
			// Generate jasper print
			JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasper, m);

			// Export pdf file
			JasperExportManager.exportReportToPdfFile(jprint, outfile);

			System.out.println("Done exporting reports to pdf. Now in "+outfile);

		} catch (Exception e) {
			System.out.print("Exception" + e);
			e.printStackTrace();
		}

	}

}
