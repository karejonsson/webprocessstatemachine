package se.prv.errends.mainweb.person;

import java.util.Date;
import java.util.List;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.MessageDB;
import se.prv.errends.dbc.PersonDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mainweb.ui.PersonUI;
import se.prv.errends.mgmt.PersonManagement;
import se.prv.errends.mgmt.SessionManagement;

public class PersonLoginPage extends VerticalLayout implements View {
	private static final long serialVersionUID = 1L;

	public PersonLoginPage() {
		Panel panel = new Panel("Login");
		panel.setSizeUndefined();
		addComponent(panel);

		
		FormLayout content = new FormLayout();
		TextField username = new TextField("Personnummer");
		content.addComponent(username);
		//username.setValue("19700101031");

		Button send = new Button("Bank ID");
		send.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				if(credentialsConfirmed(username.getValue().trim())) {
					Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
					if(MessageDB.getAllUnreadMessagesOfPerson(person.getId()).size() != 0) {
						((PersonUI) UI.getCurrent()).setup(new MessagesView());
					}
					else {
						List<Actor> actors = ActorDB.getAllActiveActorsOfPersonId(person.getId());
						((PersonUI) UI.getCurrent()).setup(new ActorView(actors));
					}
				}
				else {
					Notification.show("Felaktiga behörigheter", Notification.Type.ERROR_MESSAGE);
				}
			}
		});
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	private boolean credentialsConfirmed(String pnr) {
		if(PersonManagement.isPnr(pnr)) {
			return setup(pnr);
		}
		return false;
	}
	
	private boolean setup(String pnr) {
		String fixedPnr = PersonManagement.getCorrectedPnr(pnr);
		if(fixedPnr == null) {
			return false;
		}
		Person person = PersonDB.getPersonFromSwedishPnr(fixedPnr);
		if(person == null) {
			person = new Person();
			try {
				person.setSwedishpnr(fixedPnr);
				person.setFirstname("");
				person.setFamilyname("");
				person.setEmail("a@b.c");
			} catch (Exception e) {
				return false;
			}
			person.setActive(true);
			person.setRegisterred(new Date(System.currentTimeMillis()));
			person.save();
		}
		Session sess = SessionManagement.getErrendlessSession(person);
		UI.getCurrent().getSession().setAttribute(CommonUI.sessionlookup, sess);
		UI.getCurrent().getSession().setAttribute(CommonUI.personlookup, person);
		
		return true;
	}

}
