package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;

public class TransitiondefFormEdit extends Window {
	
	private Runnable onClose = null;
	private Transitiondef td = null;
	
	private TextField statename = null;
	private TextArea layout = null;


	public TransitiondefFormEdit(Transitiondef sd, Runnable onClose) {
		this.td = sd;
		this.onClose = onClose;
		setModal(true);

		FormLayout content = new FormLayout();
		content.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");
		content.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.8)+"px");
		
		statename = new TextField("Namn");
		statename.setValue(sd.getTransitionname() != null ? sd.getTransitionname() : "");
		content.addComponent(statename);

		layout = new TextArea("Specifikation");
		layout.setValue(sd.getLayout() != null ? sd.getLayout() : "");
		layout.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		layout.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.6)+"px");
		content.addComponent(layout);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Avbryt");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancelBtn);
		
		Button verifyBtn = new Button("Verifiera");
		verifyBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				verifyLayout();
			}
			
		});
		buttons.addComponent(verifyBtn);
		
		Button saveBtn = new Button("Uppdatera");
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
			
		});
		buttons.addComponent(saveBtn);
		content.addComponent(buttons);
		setContent(content);
	}

	protected void verifyLayout() {
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(layout.getValue().getBytes()));
        try {
        	EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Node");
			Notification.show("Rätt", Notification.Type.HUMANIZED_MESSAGE);

        }
        catch(Exception e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
	}

	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void save() {
		String name = statename.getValue().trim();
		td.setTransitionname(name);
		String spec = layout.getValue();
		td.setLayout(spec);
		td.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
