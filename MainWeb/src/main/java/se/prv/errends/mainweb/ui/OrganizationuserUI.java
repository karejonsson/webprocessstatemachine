package se.prv.errends.mainweb.ui;

import java.util.Date;
import java.util.List;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.MenuBar.MenuItem;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.adminuser.ProcessdefView;
import se.prv.errends.mainweb.adminuser.VerifyConfigurationSyntax;
import se.prv.errends.mainweb.orguser.OrganizationsAgentView;
import se.prv.errends.mainweb.orguser.OrganizationsGroupView;
import se.prv.errends.mainweb.orguser.OrganizationUserLoginPage;
import se.prv.errends.mainweb.person.ActorView;
import se.prv.errends.mainweb.person.PersonFormEdit;
import se.prv.errends.mgmt.PersonManagement;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class OrganizationuserUI extends SimplifiedContentUI implements CommonUI {
	
	private VaadinRequest request;

	public VaadinRequest getRequest() {
		return request;
	}

	@Override
	protected void init(VaadinRequest request) {
		this.request = request;
		setContent(new OrganizationUserLoginPage());
	}
	
	private MenuItem system = null;
	private MenuItem personmenu = null;
	private OrganizationUser orguser = null;
	private Person person = null;
	
	public OrganizationuserUI() {
	}
	
	public void setup(Component c) {
		orguser = (OrganizationUser) getSession().getAttribute(CommonUI.orguserlookup);
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		super.setup(c);
	}	
	
    public void setupGeneralMenubar() {
		if(system == null) {
			system = menubar.addItem("System", null);
			MenuItem errendtypes = system.addItem("Ärendetyper", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ProcessdefView());
	            }  
	        });
			MenuItem syntaxcheck = system.addItem("Syntaxkontroll", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new VerifyConfigurationSyntax());
	            }  
	        });
			MenuItem version = system.addItem("Version", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	displayCurrentVersion();
	            }  
	        });
			MenuItem logout = system.addItem("Logga ut", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	logout();
	            }  
	        });
		}
    }

    private void logout() {
    	Session sess = (Session) getSession().getAttribute(CommonUI.sessionlookup);
    	sess.setFinish(new Date(System.currentTimeMillis()));
    	sess.save();
      	Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath()+"/org");
     	//Page.getCurrent().setLocation( "/org" );
    	VaadinSession.getCurrent().close();
    }
    
    private void displayCurrentVersion() {
    	String version = "";
    	try {
    		version = StringFileManipulation.readFullyToString(getClass().getClassLoader().getResourceAsStream("version.txt"));
    	}
    	catch(Exception e) {
    	}
    	String datetime = "";
    	try {
    		datetime = StringFileManipulation.readFullyToString(getClass().getClassLoader().getResourceAsStream("datetime.txt"));
    	}
    	catch(Exception e) {
    	}
    	Notification.show(version+"\n"+datetime, Type.HUMANIZED_MESSAGE); 	
    }
    
    public void setupParticularMenubar() {
		if(personmenu == null) {
			//Person p = (Person) getSession().getAttribute(personlookup);
			personmenu = menubar.addItem("Användare", null);
			MenuItem agents = personmenu.addItem("Ombud", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new OrganizationsAgentView(orguser.getOrganization(), null));
	            }  
	        });
			MenuItem roleGroups = personmenu.addItem("Rollgrupper", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new OrganizationsGroupView(orguser.getOrganization(), null));
	            }  
	        });
			MenuItem actors = personmenu.addItem("Aktörer", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
					List<Actor> actors = ActorDB.getAllActiveActorsOfOrganizationId(orguser.getOrganizationId());
	            	setWorkContent(new ActorView(actors));
	            }  
	        });
			MenuItem personinfo = personmenu.addItem("Personuppgifter", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		    		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new PersonFormEdit(person, null));
	            }  
	        });
		}
    }

}
