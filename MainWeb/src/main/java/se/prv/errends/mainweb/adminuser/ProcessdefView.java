package se.prv.errends.mainweb.adminuser;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.mainweb.adminuser.AskForAgentAndNameForm.NotifyAboutAgentAndName;
import se.prv.errends.mainweb.adminuser.processeditor.ExportedFunctionDefLink;
import se.prv.errends.mainweb.adminuser.processeditor.ExportedProcessdefLink;
import se.prv.errends.mainweb.adminuser.processeditor.ProcessEditorMain;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.AgentManagement;
import se.prv.errends.mgmt.ErrendManagement;

public class ProcessdefView extends Panel {
	
	private List<Processdef> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Processdef> grid = null;
	
	private Adminuser adminuser = null;
	private Person person = null;
	private Organisation organization = null;
	private OrganizationUser orguser = null;

	public ProcessdefView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		orguser = (OrganizationUser) UI.getCurrent().getSession().getAttribute(CommonUI.orguserlookup);
		organization = orguser != null ? orguser.getOrganization() : null; 
		
		rows = ProcessdefDB.getAllProcessdefs();

		grid = new Grid<>();

        grid.addColumn(Processdef::getProcessname).setCaption("Ärendetyp").setEditorComponent(new TextField(), Processdef::setProcessname).setEditable(adminuser != null);
		grid.getEditor().setEnabled(true);

		if(adminuser != null) {
			grid.addComponentColumn(row -> {
				HorizontalLayout hl = new HorizontalLayout();
				hl.addComponent(new ExportedProcessdefLink(row));
				hl.addComponent(new ExportedFunctionDefLink(row));
				return hl;
			}).setCaption("Exportera");
		}

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
			  /*
			  { 
			      Button createErrendBtn = new Button("Skapa ärende");
			      createErrendBtn.setEnabled(person != null);
			      createErrendBtn.addClickListener(click -> createErrend(row));
			      out.addComponent(createErrendBtn);
			  }
			  */
		      Button describeBtn = new Button("Beskriv");
		      describeBtn.setEnabled(person != null);
		      describeBtn.addClickListener(click -> describeErrend(row));
		      out.addComponent(describeBtn);
		      if(adminuser != null) {
			      Button deleteBtn = new Button("Radera");
			      deleteBtn.addClickListener(click -> delete(row));
			      out.addComponent(deleteBtn);
			      if(adminuser.isSuperprivilegies()) {
				      Button deleteRecursivelyBtn = new Button("Radera rekursivt");
				      deleteRecursivelyBtn.addClickListener(click -> deleteRecursively(row));
				      out.addComponent(deleteRecursivelyBtn);
			      }
			      Button editErrendBtn = new Button("Redigera");
			      editErrendBtn.addClickListener(click -> editErrend(row));
			      out.addComponent(editErrendBtn);
		      }
		      return out;
		}).setCaption("Hantering");
		
		grid.getEditor().setEnabled(adminuser != null);
		
		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		if(adminuser != null) {
			HorizontalLayout buttons = new HorizontalLayout();
			Button saveBtn = new Button("Spara");
			saveBtn.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					save();
				}
			});		
			buttons.addComponent(saveBtn);		
			Button newProcessdefBtn = new Button("Ny");
			newProcessdefBtn.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					newProcessdef();
				}
			});		
			buttons.addComponent(newProcessdefBtn);		
			layout.addComponent(buttons);
		}
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void delete(Processdef pf) {
		pf.deleteRecursive();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
		//System.out.println("delete "+pf);
	}
	
	private void deleteRecursively(Processdef pf) {
		pf.deleteRecursiveWithErrends();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
	}
	
	/*
	private void createErrend(Processdef pf) {
		if(organization == null) {
			if(adminuser == null) {
				// Person user
				System.out.println("ProcessdefView.createErrend: Person-användare");
				List<Agent> theOnly = new ArrayList<Agent>();
				theOnly.add(AgentManagement.getDefaultAgentOfPerson(person));
				createErrend(pf, theOnly);
			}
			else {
				// Admin user
				System.out.println("ProcessdefView.createErrend: System-admin-användare");
				createErrend(pf, AgentDB.getAllAgents());
			}
			
		}
		else {
			// Organization user
			System.out.println("ProcessdefView.createErrend: Organisations-användare");
			createErrend(pf, AgentManagement.getAllowedAgents(organization));
		}
	}
	*/
	/*
	private void createErrend(Processdef pf, List<Agent> allowedAgents) {
		AskForAgentAndNameForm aafnf = new AskForAgentAndNameForm(
				allowedAgents,
				new NotifyAboutAgentAndName() {
					@Override
					public void agentAndName(Agent agent, String name) {
						createErrend(pf, agent, name);
					}
				}, 
				new Runnable() {
					@Override
					public void run() {
					}
				});
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(aafnf);
	}
	*/
	
	private void createErrend(Processdef pd, Agent agent, String name) {
		try {
			ErrendManagement em = ErrendManagement.createErrendFromDBStructureAndInitialize(agent, pd, name);
			if(em.isError()) {
				Notification.show(em.getErrorMessage(), Notification.Type.ERROR_MESSAGE);
				return;
			}
			Notification.show("Ärende "+name+" skapat", Notification.Type.ASSISTIVE_NOTIFICATION);
		}
		catch(Exception e) {
			e.printStackTrace();
			Notification.show("Ärende "+name+" kunde inte skapas", Notification.Type.ERROR_MESSAGE);
		}
	}
	
	private void describeErrend(Processdef pf) {
		ShowLongText slt = new ShowLongText("Analys av processen "+pf.getProcessname(), pf.getDescription());
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(slt);
	}
	
	public void save() {
		for(Processdef pf : rows) {
			pf.save();
		}
	}
	
	public void notifyAboutNewProcessdef(Processdef pf) {
		if(pf != null) {
			rows.add(pf);
		}
		grid.getDataProvider().refreshAll();
	}
	
	private ProcessdefFormNew npff = null;
	
	public void newProcessdef() {
		//System.out.println("ProcessdefView.newProcessdef");
		npff = new ProcessdefFormNew(null, new Runnable() {
			@Override
			public void run() {
				Processdef pd = npff.getProcessdef();
				ProcessdefView.this.notifyAboutNewProcessdef(pd);
				((AdminuserUI) UI.getCurrent()).setup(ProcessdefView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	public void editErrend(Processdef row) {
		((AdminuserUI) UI.getCurrent()).setWorkContent(new ProcessEditorMain(row,
				new Runnable() {
					@Override
					public void run() {
						((AdminuserUI) UI.getCurrent()).setup(ProcessdefView.this);
					}
		}));
	}
	
}
