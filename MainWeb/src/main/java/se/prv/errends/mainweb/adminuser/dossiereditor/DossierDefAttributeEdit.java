package se.prv.errends.mainweb.adminuser.dossiereditor;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierDocattributeDef;

public class DossierDefAttributeEdit extends Window {
	
	private Runnable onClose = null;
	private DossierDocattributeDef dossierdocattributedef = null;
	private TextField name = null;
	
	public DossierDefAttributeEdit(DossierDocattributeDef dossierdocattributedef, Runnable onClose) {
		this.onClose = onClose;
		this.dossierdocattributedef = dossierdocattributedef;
		setModal(true);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		name = new TextField("Namn");
		name.setValue(dossierdocattributedef.getDossierdocattributename() != null ? dossierdocattributedef.getDossierdocattributename() : "");
		content.addComponent(name);
		
		HorizontalLayout buttons = new HorizontalLayout();
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(e -> cancel());
		buttons.addComponent(cancel);
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(e -> finish());	
		buttons.addComponent(finishBtn);
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		DossierDef dossierdef = dossierdocattributedef.getDossierDef();
		dossierdef.save();
		dossierdocattributedef.setDossierdef(dossierdef);
		dossierdocattributedef.setDossierdocattributename(name.getValue());
		dossierdocattributedef.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
