package se.prv.errends.mainweb.adminuser.processeditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.ReportTemplateDB;
import se.prv.errends.dbc.ReportTemplateReuseDB;
import se.prv.errends.dbc.TransitiondefDB;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.FunctionDefReuse;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.ReportTemplateReuse;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.StatedefManaged;
import se.prv.errends.producerconfig.struct.GroovyBooleanEvaluable;
import se.prv.errends.producerconfig.struct.GroovyStringEvaluable;

public class ProcessEditorMain extends Panel {

	private List<Statedef> staterows = null;
	private VerticalLayout statelayout = new VerticalLayout();
	private Grid<Statedef> stategrid = null;

	private List<TransitiondefRowHolder> transitionrows = null;
	private VerticalLayout transitionlayout = new VerticalLayout();
	private Grid<TransitiondefRowHolder> transitiongrid = null;

	private List<FunctionRowHolder> stringfunctionrows = null;
	private List<FunctionRowHolder> booleanfunctionrows = null;
	private VerticalLayout functionslayout = new VerticalLayout();
	private VerticalLayout stringfunctionslayout = new VerticalLayout();
	private VerticalLayout booleanfunctionslayout = new VerticalLayout();
	private Grid<FunctionRowHolder> stringfunctiongrid = null;
	private Grid<FunctionRowHolder> booleanfunctiongrid = null;

	private List<ReportTemplateReuseRowHolder> reporttemplaterows = null;
	private VerticalLayout reporttemplatelayout = new VerticalLayout();
	private Grid<ReportTemplateReuseRowHolder> reporttemplategrid = null;
	private List<ReportTemplate> reporttemplates = null;

	private Map<Long, Statedef> lookupStatedef = new HashMap<Long, Statedef>();
	private Map<Long, ReportTemplate> lookupReportTemplate = new HashMap<Long, ReportTemplate>();

	private Processdef pd;
	private Runnable onClose;
	private TabSheet tabsheet = new TabSheet();
	private VerticalLayout layout = new VerticalLayout();

	public ProcessEditorMain(Processdef pd, Runnable onClose) {
		this.pd = pd;
		this.onClose = onClose;

		setupStateTab();
		setupTransitionTab();
		setupFunctionsTab();
		setupReportTab();

		tabsheet.addTab(statelayout, "Tillstånd");
		tabsheet.addTab(transitionlayout, "Övergångar");
		tabsheet.addTab(functionslayout, "Funktioner");
		tabsheet.addTab(reporttemplatelayout, "Rapporter");

		HorizontalLayout buttons = new HorizontalLayout();
		Button cancel = new Button("Klar");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
		});
		buttons.addComponent(cancel);

		layout.addComponent(buttons);
		layout.addComponent(tabsheet);
		setContent(layout);
	}

	private void setupStateTab() {
		staterows = pd.getAllStatedefs();
		for(Statedef sd : staterows) {
			lookupStatedef.put(sd.getId(), sd);
		}
		stategrid = new Grid<>();
		stategrid.getEditor().setEnabled(true);
		stategrid.setItems(staterows);
		stategrid.setSelectionMode(SelectionMode.NONE);
		stategrid.setSizeFull();

		stategrid.addComponentColumn(row -> {
			return new Label(row.getStatename());
		}).setCaption("Namn");

		stategrid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button describeBtn = new Button("Beskriv");
			describeBtn.addClickListener(click -> describeState(row));
			out.addComponent(describeBtn);
			Button editBtn = new Button("Redigera");
			editBtn.addClickListener(click -> editState(row));
			out.addComponent(editBtn);
			Button deleteBtn = new Button("Radera");
			deleteBtn.addClickListener(click -> deleteState(row));
			out.addComponent(deleteBtn);
			Button deleteRecursivelyBtn = new Button("Radera rekursivt");
			deleteRecursivelyBtn.addClickListener(click -> deleteStateRecursively(row));
			out.addComponent(deleteRecursivelyBtn);
			return out;
		}).setCaption("Hantering");

		HorizontalLayout statebuttons = new HorizontalLayout();
		Button statesave = new Button("Spara");
		statesave.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				saveStates();
			}
		});
		statebuttons.addComponent(statesave);
		Button newStateBtn = new Button("Nytt");
		newStateBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				newState();
			}
		});
		statebuttons.addComponent(newStateBtn);

		statelayout.addComponent(statebuttons);
		statelayout.addComponent(stategrid);
	}

	private void setupReportTab() {
		reporttemplates = ReportTemplateDB.getAllReportTemplates();
		lookupReportTemplate = new HashMap<Long, ReportTemplate>();
		for(ReportTemplate rt : reporttemplates) {
			lookupReportTemplate.put(rt.getId(), rt);
		}

		List<ReportTemplateReuse> reusedReportTemplates = ReportTemplateReuseDB.getAllReportTemplateReusesOfProcessdef(pd.getId());

		reporttemplaterows = new ArrayList<ReportTemplateReuseRowHolder>();

		for(ReportTemplateReuse rtr : reusedReportTemplates) {
			reporttemplaterows.add(new ReportTemplateReuseRowHolder(rtr, reporttemplates, lookupReportTemplate));
		}

		reporttemplategrid = new Grid<>();
		reporttemplategrid.getEditor().setEnabled(true);
		reporttemplategrid.setItems(reporttemplaterows);
		reporttemplategrid.setSelectionMode(SelectionMode.NONE);
		reporttemplategrid.setSizeFull();

		reporttemplategrid.addComponentColumn(row -> {
			return row.getReportTemplateComponent();
		}).setCaption("Rapport").setExpandRatio(2);

		reporttemplategrid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button deleteBtn = new Button("Radera");
			deleteBtn.addClickListener(click -> deleteReportTemplateReuse(row));
			out.addComponent(deleteBtn);
			return out;
		}).setCaption("Hantering");

		HorizontalLayout reporttemplatebuttons = new HorizontalLayout();
		Button reporttemplatesaveBtn = new Button("Spara");
		reporttemplatesaveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				saveReportTemplateReuses();
			}
		});
		reporttemplatebuttons.addComponent(reporttemplatesaveBtn);
		Button newReportTemplateReuseBtn = new Button("Ny");
		newReportTemplateReuseBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				newReportTemplateReuse();
			}
		});
		reporttemplatebuttons.addComponent(newReportTemplateReuseBtn);

		reporttemplatelayout.addComponent(reporttemplatebuttons);
		reporttemplatelayout.addComponent(reporttemplategrid);
	}

	private void setupTransitionTab() {
		List<Transitiondef> transitions = TransitiondefDB.getAllTransitiondefsOfProcessdef(pd.getId());
		transitionrows = new ArrayList<TransitiondefRowHolder>();
		for(Transitiondef td : transitions) {
			transitionrows.add(new TransitiondefRowHolder(td, lookupStatedef));
		}
		transitiongrid = new Grid<>();
		transitiongrid.getEditor().setEnabled(true);
		transitiongrid.setItems(transitionrows);
		transitiongrid.setSelectionMode(SelectionMode.NONE);
		transitiongrid.setSizeFull();

		transitiongrid.addComponentColumn(row -> {
			return row.getSourceStateComponent();
		}).setCaption("Från");
		transitiongrid.addComponentColumn(row -> {
			return new Label(row.getTransitionname());
		}).setCaption("Namn");
		transitiongrid.addComponentColumn(row -> {
			return row.getTargetStateComponent();
		}).setCaption("Till");

		transitiongrid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button describeBtn = new Button("Beskriv");
			describeBtn.addClickListener(click -> describeTransition(row));
			out.addComponent(describeBtn);
			Button editBtn = new Button("Redigera");
			editBtn.addClickListener(click -> editTransition(row));
			out.addComponent(editBtn);
			Button deleteBtn = new Button("Radera");
			deleteBtn.addClickListener(click -> deleteTransition(row));
			out.addComponent(deleteBtn);
			return out;
		}).setCaption("Hantering");

		HorizontalLayout transitionbuttons = new HorizontalLayout();
		Button transitionsaveBtn = new Button("Spara");
		transitionsaveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				saveTransitions();
			}
		});
		transitionbuttons.addComponent(transitionsaveBtn);
		Button newTransitionBtn = new Button("Ny");
		newTransitionBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				newTransition();
			}
		});
		transitionbuttons.addComponent(newTransitionBtn);

		transitionlayout.addComponent(transitionbuttons);
		transitionlayout.addComponent(transitiongrid);
	}
	
	private void setupFunctionsTab() {
		List<FunctionDefReuse> currentlyReusedFunctions = pd.getFunctionDefReuses();
		List<FunctionDef> allFunctions = FunctionDefDB.getAllFunctionDefs();
		
		List<StatedefManaged> states = null;
		try {
			states = ErrendManagement.getStatedefManaged(pd);
		} catch (Exception e) {
			// TODO Something sensible
			e.printStackTrace();
		}
		
    	List<String> booleanFs = StatedefManaged.getAllBooleanFunctionDefNames(states);
    	List<String> stringFs = StatedefManaged.getAllStringFunctionDefNames(states);

    	// String part begin
    	stringfunctionrows = new ArrayList<FunctionRowHolder>();
		for(String stringF : stringFs) {
			stringfunctionrows.add(new FunctionRowHolder(GroovyStringEvaluable.typename, stringF, currentlyReusedFunctions, allFunctions));
		}
		stringfunctiongrid = new Grid<>();
		stringfunctiongrid.getEditor().setEnabled(true);
		stringfunctiongrid.setItems(stringfunctionrows);
		stringfunctiongrid.setSelectionMode(SelectionMode.NONE);
		stringfunctiongrid.setSizeFull();

		stringfunctiongrid.addComponentColumn(row -> {
			return row.getNameComponent();
		}).setCaption("Refererande symbol");
		stringfunctiongrid.addComponentColumn(row -> {
			return row.getStatusComponent();
		}).setCaption("Status");
		stringfunctiongrid.addComponentColumn(row -> {
			return row.getOptionsComponent();
		}).setCaption("Alternativ");

		stringfunctiongrid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button editBtn = new Button("Redigera");
			editBtn.addClickListener(click -> editStringFunction(row));
			out.addComponent(editBtn);
			return out;
		}).setCaption("Hantering");

		HorizontalLayout stringfunctionbuttons = new HorizontalLayout();
		Button stringfunctionBtn = new Button("Spara");
		stringfunctionBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				saveStringFunctions();
			}
		});
		stringfunctionbuttons.addComponent(stringfunctionBtn);
		
		stringfunctionslayout.addComponent(stringfunctionbuttons);
		stringfunctionslayout.addComponent(stringfunctiongrid);
    	// String part end

    	// Boolean part begin
		booleanfunctionrows = new ArrayList<FunctionRowHolder>();
		for(String booleanF : booleanFs) {
			booleanfunctionrows.add(new FunctionRowHolder(GroovyBooleanEvaluable.typename, booleanF, currentlyReusedFunctions, allFunctions));
		}
		booleanfunctiongrid = new Grid<>();
		booleanfunctiongrid.getEditor().setEnabled(true);
		booleanfunctiongrid.setItems(booleanfunctionrows);
		booleanfunctiongrid.setSelectionMode(SelectionMode.NONE);
		booleanfunctiongrid.setSizeFull();

		booleanfunctiongrid.addComponentColumn(row -> {
			return row.getNameComponent();
		}).setCaption("Refererande symbol");
		booleanfunctiongrid.addComponentColumn(row -> {
			return row.getStatusComponent();
		}).setCaption("Status");
		booleanfunctiongrid.addComponentColumn(row -> {
			return row.getOptionsComponent();
		}).setCaption("Alternativ");

		booleanfunctiongrid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button editBtn = new Button("Redigera");
			editBtn.addClickListener(click -> editBooleanFunction(row));
			out.addComponent(editBtn);
			return out;
			// 
		}).setCaption("Hantering");

		HorizontalLayout booleanfunctionbuttons = new HorizontalLayout();
		Button booleanfunctionBtn = new Button("Spara");
		booleanfunctionBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				saveBooleanFunctions();
			}
		});
		booleanfunctionbuttons.addComponent(booleanfunctionBtn);
		
		booleanfunctionslayout.addComponent(booleanfunctionbuttons);
		booleanfunctionslayout.addComponent(booleanfunctiongrid);
    	// Boolean part end
		
		TabSheet tabsheet = new TabSheet();
		tabsheet.addTab(stringfunctionslayout, "Textuella");
		tabsheet.addTab(booleanfunctionslayout, "Booleska");
		functionslayout.addComponent(tabsheet);
	}
	
	private void saveBooleanFunctions() {
		List<FunctionDefReuse> reuses = pd.getFunctionDefReuses();
		for(FunctionDefReuse reuse : reuses) {
			FunctionDef fd = reuse.getFunctionDef();
			if(!fd.getTypename().equals(GroovyBooleanEvaluable.typename)) {
				continue;
			}
			reuse.delete();
		}
		for(FunctionRowHolder booleanfunctionrow : booleanfunctionrows) {
			FunctionDef fd = booleanfunctionrow.getCurrentFunctionDef();
			if(fd != null) {
				fd.save();
				FunctionDefReuse reuse = new FunctionDefReuse();
				reuse.setFunctionDef(fd);
				reuse.setProcessdef(pd);
				reuse.save();				
			}
		}
		List<FunctionDefReuse> currentlyReusedFunctions = pd.getFunctionDefReuses();
		List<FunctionDef> allFunctions = FunctionDefDB.getAllFunctionDefs();
		
		for(FunctionRowHolder booleanfunctionrow : booleanfunctionrows) {
			booleanfunctionrow.initialize(currentlyReusedFunctions, allFunctions);
		}
		booleanfunctiongrid.getDataProvider().refreshAll();
	}

	private void saveStringFunctions() {
		List<FunctionDefReuse> reuses = pd.getFunctionDefReuses();
		for(FunctionDefReuse reuse : reuses) {
			FunctionDef fd = reuse.getFunctionDef();
			if(!fd.getTypename().equals(GroovyStringEvaluable.typename)) {
				continue;
			}
			reuse.delete();
		}
		for(FunctionRowHolder stringfunctionrow : stringfunctionrows) {
			FunctionDef fd = stringfunctionrow.getCurrentFunctionDef();
			if(fd != null) {
				fd.save();
				FunctionDefReuse reuse = new FunctionDefReuse();
				reuse.setFunctionDef(fd);
				reuse.setProcessdef(pd);
				reuse.save();				
			}
		}

		List<FunctionDefReuse> currentlyReusedFunctions = pd.getFunctionDefReuses();
		List<FunctionDef> allFunctions = FunctionDefDB.getAllFunctionDefs();
		
		for(FunctionRowHolder stringfunctionrow : stringfunctionrows) {
			stringfunctionrow.initialize(currentlyReusedFunctions, allFunctions);
		}
		stringfunctiongrid.getDataProvider().refreshAll();
		System.out.println("ProcessEditorMain.saveStringFunctions till slutet");
	}

	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		//close();
	}

	private void describeState(Statedef row) {
		String description = row.getLayout();
		if(description == null) {
			Notification.show("Har ej beskrivning", Notification.Type.HUMANIZED_MESSAGE);
			return;
		}
		String name = row.getStatename();
		if(name == null) {
			name = "";
		}
		((AdminuserUI) UI.getCurrent()).openSubwindow(new ShowLongText("Tillstånd "+name, description));
		return;
	}

	private void describeTransition(TransitiondefRowHolder row) {
		String description = row.getLayout();
		if(description == null) {
			Notification.show("Har ej beskrivning", Notification.Type.HUMANIZED_MESSAGE);
			return;
		}
		String name = row.getTransitionname();
		if(name == null) {
			name = "";
		}
		((AdminuserUI) UI.getCurrent()).openSubwindow(new ShowLongText("Övergång "+name, description));
		return;
	}	

	public void saveTransitions() {
		for(TransitiondefRowHolder trh :  transitionrows) {
			String errorMessage = trh.getErrormessageForSave();
			if(errorMessage != null) {
				Notification.show(errorMessage, Notification.Type.ERROR_MESSAGE);
			}
		}
		for(TransitiondefRowHolder trh :  transitionrows) {
			trh.save();
		}
	}

	public void saveReportTemplateReuses() {
		for(ReportTemplateReuseRowHolder trh :  reporttemplaterows) {
			String errorMessage = trh.getErrormessageForSave();
			if(errorMessage != null) {
				Notification.show(errorMessage, Notification.Type.ERROR_MESSAGE);
			}
		}
		for(ReportTemplateReuseRowHolder trh :  reporttemplaterows) {
			trh.save();
		}
	}

	public void newTransition() {
		Transitiondef td = new Transitiondef();
		transitionrows.add(new TransitiondefRowHolder(td, lookupStatedef));
		transitiongrid.getDataProvider().refreshAll();		
	}

	public void newReportTemplateReuse() {
		ReportTemplateReuse rtr = new ReportTemplateReuse();
		rtr.setProcessdef(pd);
		reporttemplaterows.add(new ReportTemplateReuseRowHolder(rtr, reporttemplates, lookupReportTemplate));
		reporttemplategrid.getDataProvider().refreshAll();		
	}

	public void editTransition(TransitiondefRowHolder tdrh) {
		((AdminuserUI) UI.getCurrent()).openSubwindow(new TransitiondefFormEdit(tdrh.getTransitiondef(), new Runnable() {
			@Override
			public void run() {
				transitiongrid.getDataProvider().refreshAll();
			}
		}));
	}

	public void deleteTransition(TransitiondefRowHolder td) {
		td.smoothDelete();
		transitionrows.remove(td);
		transitiongrid.getDataProvider().refreshAll();	
	}

	public void deleteReportTemplateReuse(ReportTemplateReuseRowHolder rtrrh) {
		rtrrh.smoothDelete();
		reporttemplaterows.remove(rtrrh);
		reporttemplategrid.getDataProvider().refreshAll();	
	}

	public void saveStates() {
		System.out.println("ProcessEditorMain.saveStates");
		for(Statedef sd : staterows) {
			if(!sd.isPersisted()) {
				System.out.println("Tillstånd "+sd.getStatename()+" var ej persisterat.");
				sd.save();
				lookupStatedef.put(sd.getId(), sd);
				System.out.println("Nu finns "+lookupStatedef.keySet().size()+" nycklar.");
			}
			else {
				System.out.println("Tillstånd "+sd.getStatename()+" är persisterat.");
				sd.save();
			}
		}
		System.out.println("Slutligen "+lookupStatedef.keySet().size()+" nycklar.");
	}

	public void newState() {
		Statedef sd = new Statedef();
		sd.setProcessdef(pd);
		staterows.add(sd);
		stategrid.getDataProvider().refreshAll();
		alertAboutNewState(sd); 
	}

	public void editState(Statedef sd) {
		((AdminuserUI) UI.getCurrent()).openSubwindow(new StatedefFormEdit(sd, new Runnable() {
			@Override
			public void run() {
				stategrid.getDataProvider().refreshAll();
				alertAboutChangedState(sd);
			}
		}));
	}
	
	public void deleteState(Statedef sd) {
		if(sd.isPersisted()) {
			sd.delete();
		}
		staterows.remove(sd);
		stategrid.getDataProvider().refreshAll();
		alertAboutRemovedState(sd);
	}

	public void deleteStateRecursively(Statedef sd) {
		sd.deleteRecursive();
		staterows.remove(sd);
		stategrid.getDataProvider().refreshAll();		
	}

	private void alertAboutRemovedState(Statedef sd) {
		for(TransitiondefRowHolder trh :  transitionrows) {
			trh.alertStatedefRemoved(sd);
		}
	}

	private void alertAboutChangedState(Statedef sd) {
		for(TransitiondefRowHolder trh :  transitionrows) {
			trh.alertStatedefChanged(sd);
		}
	}

	private void alertAboutNewState(Statedef sd) {
		for(TransitiondefRowHolder trh :  transitionrows) {
			trh.alertNewStatedef(sd);
		}
	}
	
	public void editStringFunction(FunctionRowHolder frh) {
		FunctionDef fd = frh.getCurrentFunctionDef();
		if(fd == null) {
			fd = new FunctionDef();
		}
		final FunctionDef finalFD = fd;
		String name = frh.getName();
		((AdminuserUI) UI.getCurrent()).openSubwindow(new FunctionFormEdit(fd, name, new Runnable() {
			@Override
			public void run() {
				stringfunctiongrid.getDataProvider().refreshAll();
				alertAboutChangedStringFunction(frh, finalFD);
			}
		}));
	}

	private void alertAboutChangedStringFunction(FunctionRowHolder frh, FunctionDef fd) {
		frh.setFunctionDef(fd);
		frh.getOptionsComponent();
	}

	public void editBooleanFunction(FunctionRowHolder frh) {
		FunctionDef fd = frh.getCurrentFunctionDef();
		if(fd == null) {
			fd = new FunctionDef();
		}
		final FunctionDef finalFD = fd;
		String name = frh.getName();
		((AdminuserUI) UI.getCurrent()).openSubwindow(new FunctionFormEdit(fd, name, new Runnable() {
			@Override
			public void run() {
				booleanfunctiongrid.getDataProvider().refreshAll();
				alertAboutChangedBooleanFunction(frh, finalFD);
			}
		}));
	}

	private void alertAboutChangedBooleanFunction(FunctionRowHolder frh, FunctionDef fd) {
		frh.setFunctionDef(fd);
		frh.getOptionsComponent();
	}

}
