package se.prv.errends.mainweb.adminuser.dossiereditor;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierDocattributeDef;
import se.prv.errends.domain.DossierDoctypeDef;

public class DossierDefView extends Panel {
	
	private TabSheet tabsheet = new TabSheet();
	
	private Grid<DossierDocattributeDef> attributesGrid = null;
	private List<DossierDocattributeDef> attributesRows = null;

	private Grid<DossierDoctypeDef> typesGrid = null;
	private List<DossierDoctypeDef> typesRows = null;
	
	private DossierDef dossierdef = null;

	private Runnable onClose = null;

	public DossierDefView(DossierDef dossierdef, Runnable onClose) {
		this.dossierdef = dossierdef;
		this.onClose = onClose;
		
		attributesRows = dossierdef.getReferringDossierDocattributeDef();
		typesRows = dossierdef.getReferringDossierDoctypeDefs();
		
		tabsheet.setWidth("100%");
		tabsheet.addTab(getAttributeEditingComponent(), "Attribut");
		tabsheet.addTab(getTypeEditingComponent(), "Types");

		setContent(tabsheet);	
	}
	
	private Component getAttributeEditingComponent() {
		attributesGrid = new Grid<>();
		attributesGrid.setWidth("100%");
		attributesGrid.getEditor().setEnabled(false);
		attributesGrid.setItems(attributesRows);
		attributesGrid.setSelectionMode(SelectionMode.SINGLE);
		attributesGrid.setSizeFull();

        attributesGrid.addComponentColumn(row -> {
        	return new Label(row.getDossierdocattributename());
        }).setCaption("Namn"); 
        attributesGrid.addComponentColumn(row -> {
        	HorizontalLayout buttons = new HorizontalLayout();
        	
            Button removeBtn = new Button("Radera");
            buttons.addComponent(removeBtn);
            removeBtn.addClickListener(e -> removeAttributeAction(row));
            
            Button editBtn = new Button("Redigera");
            buttons.addComponent(editBtn);
            editBtn.addClickListener(e -> editAttributeAction(row));

            return buttons;
        }).setCaption("Aktiv");
        
        VerticalLayout out = new VerticalLayout();
        
        HorizontalLayout buttons = new HorizontalLayout();
        Button newDocument = new Button("Nytt attribut");
        buttons.addComponent(newDocument);
        newDocument.addClickListener(e -> newAttributeAction());
        
        if(onClose != null) {
            Button backBtn = new Button("Åter");
            buttons.addComponent(backBtn);
            backBtn.addClickListener(e -> backAction());
        }
        out.addComponent(buttons);
        
        out.addComponent(attributesGrid);
        return out;
	}

	private void removeAttributeAction(DossierDocattributeDef row) {
		row.delete();
		attributesRows.remove(row);
		attributesGrid.getDataProvider().refreshAll();
	}

	private void newAttributeAction() {
		final DossierDocattributeDef dossierdocattributedef = new DossierDocattributeDef();
		dossierdocattributedef.setDossierdef(dossierdef);
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefAttributeEdit(dossierdocattributedef, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefView.this);
	    		if(dossierdocattributedef.getId() != null) {
	    			attributesRows.add(dossierdocattributedef);
		    		attributesGrid.getDataProvider().refreshAll();
	    		}
			}
		}));
	}

	private void editAttributeAction(DossierDocattributeDef dossierdoctypedef) {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefAttributeEdit(dossierdoctypedef, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefView.this);
	    		attributesGrid.getDataProvider().refreshAll();
			}
		}));
	}
	
	private Component getTypeEditingComponent() {
		typesGrid = new Grid<>();
		typesGrid.setWidth("100%");
		typesGrid.getEditor().setEnabled(false);
		typesGrid.setItems(typesRows);
		typesGrid.setSelectionMode(SelectionMode.SINGLE);
		typesGrid.setSizeFull();

		typesGrid.addComponentColumn(row -> {
        	return new Label(row.getDossierdoctypename());
        }).setCaption("Namn"); 
		typesGrid.addComponentColumn(row -> {
        	HorizontalLayout buttons = new HorizontalLayout();
        	
            Button removeBtn = new Button("Radera");
            buttons.addComponent(removeBtn);
            removeBtn.addClickListener(e -> removeTypeAction(row));
            
            Button editBtn = new Button("Redigera");
            buttons.addComponent(editBtn);
            editBtn.addClickListener(e -> editTypeAction(row));

            return buttons;
        }).setCaption("Aktiv");
        
        VerticalLayout out = new VerticalLayout();
        
        HorizontalLayout buttons = new HorizontalLayout();
        Button newDocument = new Button("Ny typ");
        buttons.addComponent(newDocument);
        newDocument.addClickListener(e -> newTypeAction());
        
        if(onClose != null) {
            Button backBtn = new Button("Åter");
            buttons.addComponent(backBtn);
            backBtn.addClickListener(e -> backAction());
        }
        out.addComponent(buttons);
        
        out.addComponent(typesGrid);
        return out;
	}

	private void removeTypeAction(DossierDoctypeDef row) {
		row.delete();
		typesRows.remove(row);
		typesGrid.getDataProvider().refreshAll();
	}

	private void newTypeAction() {
		final DossierDoctypeDef dossierdoctypedef = new DossierDoctypeDef();
		dossierdoctypedef.setDossierdefId(dossierdef.getId());
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefTypeEdit(dossierdoctypedef, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefView.this);
	    		if(dossierdoctypedef.getId() != null) {
	    			typesRows.add(dossierdoctypedef);
	    			typesGrid.getDataProvider().refreshAll();
	    		}
			}
		}));
	}

	private void editTypeAction(DossierDoctypeDef dossierdoctypedef) {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefTypeEdit(dossierdoctypedef, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefView.this);
	    		typesGrid.getDataProvider().refreshAll();
			}
		}));
	}

	private void backAction() {
		onClose.run();
	}

}
