package se.prv.errends.mainweb.ui;

import java.util.Date;
import java.util.List;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.adminuser.ProcessdefView;
import se.prv.errends.mainweb.person.ActorView;
import se.prv.errends.mainweb.person.AllowedActorsView;
import se.prv.errends.mainweb.person.DossierListView;
import se.prv.errends.mainweb.person.MessagesView;
import se.prv.errends.mainweb.person.PersonFormEdit;
import se.prv.errends.mainweb.person.PersonLoginPage;
import se.prv.errends.mainweb.person.SessionView;

import com.vaadin.ui.MenuBar.MenuItem;

public class PersonUI extends SimplifiedContentUI implements CommonUI {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VaadinRequest request;

	public VaadinRequest getRequest() {
		return request;
	}

	@Override
	protected void init(VaadinRequest request) {
		this.request = request;
		setContent(new PersonLoginPage());
	}
	
	private MenuItem system = null;
	private MenuItem personmenu = null;
	private Person person = null;
	
	public PersonUI() {
	}
	
	public void setup(Component c) {
		person = (Person) getSession().getAttribute(CommonUI.personlookup);
		super.setup(c);
	}	
	
    public void setupGeneralMenubar() {
		if(system == null) {
			Person p = (Person) getSession().getAttribute(CommonUI.personlookup);
			system = menubar.addItem("System", null);
			MenuItem errendtypes = system.addItem("Ärendetyper", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483013525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ProcessdefView());
	            }  
	        });
			MenuItem logout = system.addItem("Logga ut", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	logout();
	        		//setContent(new PersonLoginPage());
	            }  
	        });
		}
    }
    
    private void logout() {
    	Session sess = (Session) getSession().getAttribute(CommonUI.sessionlookup);
    	sess.setFinish(new Date(System.currentTimeMillis()));
    	sess.save();
     	Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath()+"/");
    	VaadinSession.getCurrent().close();
    }
    
    private boolean personalDataComplete() {
    	if(person == null) {
    		return false;
    	}
    	if(person.getFamilyname() == null) {
    		return false;
    	}
    	if(person.getFamilyname().length() < 2) {
    		return false;
    	}
    	if(person.getFirstname() == null) {
    		return false;
    	}
    	if(person.getFirstname().length() < 2) {
    		return false;
    	}
    	if(person.getEmail() == null) {
    		return false;
    	}
    	if(person.getEmail().length() < 6) {
    		return false;
    	}
    	if(!Person.acceptableEmail(person.getEmail())) {
    		return false;
    	}
    	return true;
    }
    
    public void setupParticularMenubar() {
		if(personmenu == null) {
			//Person p = (Person) getSession().getAttribute(personlookup);
			personmenu = menubar.addItem("Användare", null);
			MenuItem personinfo = personmenu.addItem("Personuppgifter", new MenuBar.Command() {
				private static final long serialVersionUID = 3725756735100541361L;
				public void menuSelected(MenuItem selectedItem) {
	            	Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		    		((PersonUI) UI.getCurrent()).openSubwindow(new PersonFormEdit(person, null));
	            }  
	        });
			MenuItem processfiles = personmenu.addItem("Aktörer", new MenuBar.Command() {
				private static final long serialVersionUID = -6984623122566081432L;
				public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
					Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
					List<Actor> actors = ActorDB.getAllActiveActorsOfPersonId(person.getId());
	            	setWorkContent(new ActorView(actors));
	            }  
	        });
			MenuItem allowedErrends = personmenu.addItem("Tillgängliga ärenden", new MenuBar.Command() {
				private static final long serialVersionUID = -9025131162751761954L;
				public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new AllowedActorsView());
	            }  
	        });
			
			MenuItem messages = personmenu.addItem("Meddelanden", new MenuBar.Command() {
				private static final long serialVersionUID = 2253648242513080087L;
				public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	System.out.println(""+person);
	            	setWorkContent(new MessagesView());
	            }  
	        });
			
			
			MenuItem sessions = personmenu.addItem("Sessioner", new MenuBar.Command() {
				private static final long serialVersionUID = -1725402416738126819L;
				public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new SessionView());
	            }  
	        });
			MenuItem dossiers = personmenu.addItem("Akter", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!personalDataComplete()) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new DossierListView());
	            }  
	        });
		}
    }

}
