package se.prv.errends.mainweb.person;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.ui.CommonUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;

public class PersonFormEdit extends Window {
	
	private Runnable onClose = null;
	private Person person = null;
	private TextField firstname = null;
	private TextField familyname = null;
	private TextField email = null;
	private Label registerred = null;
	private TextField swedishPnr = null;
	private CheckBox active = null;
	
	public PersonFormEdit(Person person, Runnable onClose) {
		this.onClose = onClose;
		this.person = person;
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		firstname = new TextField("Förnamn");
		firstname.setValue(person.getFirstname() != null ? person.getFirstname() : "");
		content.addComponent(firstname);
		
		familyname = new TextField("Efternamn");
		familyname.setValue(person.getFamilyname() != null ? person.getFamilyname() : "");
		content.addComponent(familyname);
		
		email = new TextField("E-post");
		email.setValue(person.getEmail() != null ? person.getEmail() : "");
		content.addComponent(email);
		
		registerred = new Label("Registrerad");
		registerred.setValue(Adminuser.presentDate(person.getRegisterred()));
		content.addComponent(registerred);
		
		swedishPnr = new TextField("Personnummer");
		swedishPnr.setValue(person.getSwedishpnr() != null ? person.getSwedishpnr() : "");
		content.addComponent(swedishPnr);
		
		if(UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup) != null) {
			active = new CheckBox("Aktiv");
			active.setValue(person.isActive());
			content.addComponent(active);
		}
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		String em = email.getValue().trim();
		if(!Person.acceptableEmail(em)) {
			Notification.show("Felaktig e-postadress", Notification.Type.ERROR_MESSAGE);
			return;
		}
		person.setFirstname(firstname.getValue());
		person.setFamilyname(familyname.getValue());
		try {
			person.setEmail(em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			person.setSwedishpnr(swedishPnr.getValue());
		} catch (Exception e) {
			Notification.show("Personnumret ogiltigt", Notification.Type.ERROR_MESSAGE);
		}
		if(active != null) {
			person.setActive(active.getValue());
		}
		person.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
