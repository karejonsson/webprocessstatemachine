package se.prv.errends.mainweb.orguser;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Organisation;

public class OrganizationFormEdit extends Window {
	
	private Runnable onClose = null;
	private Organisation organization = null;
	private TextField orgname = null;
	private TextField orgnr = null;
	private CheckBox active = null;
	
	public OrganizationFormEdit(Organisation organization, Runnable onClose) {
		this.onClose = onClose;
		this.organization = organization;
		setModal(true);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		orgname = new TextField("Organisationsnamn");
		orgname.setValue(organization.getOrgname() != null ? organization.getOrgname() : "");
		content.addComponent(orgname);
		
		orgnr = new TextField("Organisationsnummer");
		orgnr.setValue(organization.getOrgnr() != null ? organization.getOrgnr() : "");
		content.addComponent(orgnr);
		
		active = new CheckBox("Aktiv");
		active.setValue(organization.getActive());
		content.addComponent(active);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		organization.setOrgname(orgname.getValue());
		organization.setOrgnr(orgnr.getValue());
		organization.setActive(active.getValue());
		organization.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
