package se.prv.errends.mainweb.adminuser;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Processfile;
import se.prv.errends.mainweb.ui.CommonUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ProcessfileFormNew extends Window {
	
	private Runnable onClose = null;
	
	private TextField description = null;
	private Upload upload = null;
	private ByteArrayOutputStream bais = null;
	private ImageUploader receiver = null;
	private String filename;
	private String mimetype;
	private ProcessfileView pfv = null;
	
	public ProcessfileFormNew(ProcessfileView pfv, Runnable onClose) {
		this.pfv = pfv;
		this.onClose = onClose;
		setModal(true);
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		description = new TextField("Beskrivning");
		content.addComponent(description);
		
		receiver = new ImageUploader();
		upload = new Upload("Processfil", receiver);
		upload.addSucceededListener(receiver);
		upload.setImmediateMode(false);
		
		content.addComponent(upload);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		//System.out.println("NewProcessfileForm.finish");
		upload.startUpload();
	}
	
	private void uploadDone() {
		Processfile pf = new Processfile();
		pf.setFilename(filename);
		pf.setDescription(description.getValue());
		Adminuser au = (Adminuser) getSession().getAttribute(CommonUI.adminuserlookup);
		pf.setAdminuserId(au.getId());
		pf.save();
		pf.setData(bais.toByteArray());
		pfv.notifyAboutNewProcessfile(pf);

		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	class ImageUploader implements Receiver, SucceededListener {

	    public OutputStream receiveUpload(String filename,
	                                      String mimeType) {
	    	ProcessfileFormNew.this.filename = filename;
	    	ProcessfileFormNew.this.mimetype = mimeType;
	        bais = new ByteArrayOutputStream();
	    	return bais;
	    }

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			uploadDone();
		}
	};
	
}
