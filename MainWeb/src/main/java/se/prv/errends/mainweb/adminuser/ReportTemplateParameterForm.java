package se.prv.errends.mainweb.adminuser;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import se.prv.errends.domain.Processfile;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.parametertemplateconfig.struct.ParameterTemplate;
import se.prv.errends.parametertemplateconfig.struct.ParameterTemplateHandling;

public class ReportTemplateParameterForm extends Window {
	
	private Runnable onClose = null;
	private ReportTemplate rt = null;
	private JasperReport jasperReport = null;
	private List<String> parameternames = new ArrayList<String>();
	private List<ParameterTemplate> parameters = null; 

	private Grid<ParameterTemplate> grid = null;

	private VerticalLayout layout = new VerticalLayout();

	public ReportTemplateParameterForm(ReportTemplate rt, Runnable onClose) {
		this.rt = rt;
		this.onClose = onClose;
		try {
			jasperReport = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(rt.getJasperReportData()));
		}
		catch(Exception e) {
			e.printStackTrace();
			Notification.show("Parametermallen kan inte redigeras", Notification.Type.ERROR_MESSAGE);
			if(onClose != null) {
				onClose.run();
			}
			close();
			return;
		}
		JRParameter[] pars = jasperReport.getParameters();
		for(JRParameter par : pars) {
			if(!par.isSystemDefined() && !par.getName().startsWith("INTERNAL_")) {
				parameternames.add(par.getName());
			}
		}
		byte[] existingParameters = rt.getParameterData();
		try {
			parameters = ParameterTemplateHandling.createParameterList(existingParameters, "ReportTemplate parameters id="+rt.getId());
		} 
		catch (Exception e) {
			Notification.show("Parametermallen kan inte återskapas", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
			if(onClose != null) {
				onClose.run();
			}
			close();
			return;
		}
		if(parameters == null) {
			parameters = new ArrayList<ParameterTemplate>();
		}
		setupParameters();
		
		grid = new Grid<>();

        grid.addColumn(ParameterTemplate::getName).setCaption("Parameternamn");
        grid.addColumn(ParameterTemplate::getMinlenS).setCaption("Minlängd").setEditorComponent(new TextField(), ParameterTemplate::setMinlenS);
        grid.addColumn(ParameterTemplate::getMaxlenS).setCaption("Maxlängd").setEditorComponent(new TextField(), ParameterTemplate::setMaxlenS);
        grid.addColumn(ParameterTemplate::getDescription).setCaption("Beskrivning").setEditorComponent(new TextField(), ParameterTemplate::setDescription);
		grid.getEditor().setEnabled(true);

		grid.setItems(parameters);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		
		buttons.addComponent(saveBtn);
		
		Button backBtn = new Button("Åter");
		backBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				back();
			}
		});
		
		buttons.addComponent(backBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");
		
		setContent(layout);
	}
	
	public void save() {
		try {
			byte[] pars = ParameterTemplateHandling.serializeParameterListToBytes(parameters);
			rt.setParameterData(pars);
			Notification.show("Parametrar sparade", Notification.Type.HUMANIZED_MESSAGE);
		} catch (UnsupportedEncodingException e) {
			Notification.show("Sparande av parametrar misslyckades", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	public void back() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
		
	private ParameterTemplate getParameterTemplate(String name) {
		for(ParameterTemplate parameter : parameters) {
			if(parameter.getName().equals(name)) {
				return parameter;
			}
		}
		return null;
	}

	public void setupParameters() {
		for(String pn : parameternames) {
			ParameterTemplate pt = getParameterTemplate(pn);
			if(pt == null) {
				parameters.add(new ParameterTemplate(null, null, pn, null));
			}
		}
		List<ParameterTemplate> toRemove = new ArrayList<ParameterTemplate>();
		for(ParameterTemplate parameter : parameters) {
			if(!parameternames.contains(parameter.getName())) {
				toRemove.add(parameter);
			}
		}
		for(ParameterTemplate redundantParameter : toRemove) {
			parameters.remove(redundantParameter);
		}
	}

}
