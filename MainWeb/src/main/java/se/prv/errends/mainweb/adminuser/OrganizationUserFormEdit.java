package se.prv.errends.mainweb.adminuser;

import java.security.NoSuchAlgorithmException;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.orguser.OrganizationFormEdit;
import se.prv.errends.mainweb.person.PersonFormEdit;
import se.prv.errends.mainweb.person.SearchPersonForm;
import se.prv.errends.mainweb.ui.AdminuserUI;

public class OrganizationUserFormEdit extends Window {
	
	private Runnable onClose = null;
	private OrganizationUser orguser = null;
	private TextField username = null;
	private TextField password = null;
	private Label personField = null;
	private Label organizationField = null;
	private CheckBox active = null;
	private CheckBox superprivilegies = null;
	private Person person = null;
	private Organisation organization = null;
	
	public OrganizationUserFormEdit(OrganizationUser orguser, Runnable onClose) {
		this.onClose = onClose;
		this.orguser = orguser;
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		username = new TextField("Användarnamn");
		username.setValue(orguser != null ? (orguser.getUsername() == null ? "" : orguser.getUsername()): "");
		content.addComponent(username);
		
		password = new TextField("Lösenord");
		password.setValue("");
		content.addComponent(password); 

		active = new CheckBox("Aktiv");
		active.setValue(orguser != null ? orguser.getActive() : true);
		content.addComponent(active);

		superprivilegies = new CheckBox("Super");
		superprivilegies.setValue(orguser != null ? orguser.getSuperprivilegies() : true);
		content.addComponent(superprivilegies);

		personField = new Label();
		String personFieldDefault = "Person ej vald";
		if(orguser != null && orguser.getPersonId() != null) {
			Person p = orguser.getPerson();
			personFieldDefault = p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname();
		}
		personField.setValue(personFieldDefault);
		content.addComponent(personField);
		
		organizationField = new Label();
		String organizationFieldDefault = "Organisation ej vald";
		if(orguser != null && orguser.getOrganizationId() != null) {
			Organisation org = orguser.getOrganization();
			organizationFieldDefault = org.getOrgname()+" / "+org.getOrgnr();
		}
		organizationField.setValue(organizationFieldDefault);
		content.addComponent(organizationField);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button choseOrganizationBtn = new Button("Välj organisation");
		choseOrganizationBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Organisation org = new Organisation();
				((AdminuserUI) UI.getCurrent()).openSubwindow(new SearchOrganizationForm(
						new SearchOrganizationForm.NotifyAboutPick() {
							@Override
							public void picked(Organisation org) {
								organization = org;
								organizationField.setValue(org.getOrgname()+" / "+org.getOrgnr());
							}
						}, 
						new Runnable() {
					@Override
					public void run() {
					}
				}));
			}
			
		});
		buttons.addComponent(choseOrganizationBtn);
		
		Button createOrganizationBtn = new Button("Skapa organisation");
		createOrganizationBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Organisation org = new Organisation();
				((AdminuserUI) UI.getCurrent()).openSubwindow(new OrganizationFormEdit(org, new Runnable() {
					@Override
					public void run() {
						organization = org;
						organizationField.setValue(org.getOrgname()+" / "+org.getOrgnr());
					}
				}));
			}
			
		});
		buttons.addComponent(createOrganizationBtn);
		
		Button chosePersonBtn = new Button("Välj person");
		chosePersonBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				((AdminuserUI) UI.getCurrent()).openSubwindow(new SearchPersonForm(
						new SearchPersonForm.NotifyAboutPersonPick() {
							@Override
							public void picked(Person p) {
								person = p;
								personField.setValue(p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname());
							}
						},
						null
						));
			}
			
		});
		buttons.addComponent(chosePersonBtn);
		
		Button createPersonBtn = new Button("Skapa person");
		createPersonBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Person p = new Person();
				((AdminuserUI) UI.getCurrent()).openSubwindow(new PersonFormEdit(p, new Runnable() {
					@Override
					public void run() {
						person = p;
						personField.setValue(p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname());
					}
				}));
			}
			
		});
		buttons.addComponent(createPersonBtn);
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		if(orguser == null) {
			orguser = new OrganizationUser();
		}
		if(username.getValue() != null && username.getValue().length() < 4) {
			Notification.show("Användarnamnet för kort", Notification.Type.ERROR_MESSAGE);
			return;
		}
		orguser.setUsername(username.getValue());
		orguser.setActive(active.getValue());
		try {
			orguser.setPassword(Adminuser.calculateChecksum(password.getValue()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		orguser.setPerson(person);
		orguser.setOrganization(organization);
		orguser.setSuperprivilegies(superprivilegies.getValue());
		orguser.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
