package se.prv.errends.mainweb.errend;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.parametertemplateconfig.struct.ParameterTemplate;
import se.prv.errends.parametertemplateconfig.struct.ParameterTemplateHandling;
import se.prv.errends.parametervalue.struct.ParameterValue;
import se.prv.errends.parametervalue.struct.ParameterValueHandling;

public class ReportFormEdit extends Window {
	
	private Runnable onClose = null;
	private ReportsInErrendView reiv = null;
	private Report report = null;
	private Session session = null;
	private Map<String, ParameterTemplate> lookupTemplate = new HashMap<String, ParameterTemplate>();
	private Map<String, ParameterValue> lookupValue = new HashMap<String, ParameterValue>();
	private List<ParameterValue> pvals = null;

	private Map<String, TextArea> lookupEditor = new HashMap<String, TextArea>();

	public ReportFormEdit(ReportsInErrendView reiv, Report report, Runnable onClose) throws Exception {
		this.reiv = reiv;
		this.onClose = onClose;
		this.report = report;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		setModal(true);
		
		ReportTemplate rt = report.getReportTemplateReuse().getReportTemplate();
		
		List<ParameterTemplate> ptemps = null;
		try {
			ptemps = ParameterTemplateHandling.createParameterList(rt.getParameterData(), "ReportTemplate params id="+rt.getId());
		} catch (Exception e) {
			e.printStackTrace();
			if(onClose != null) {
				onClose.run();
			}
			close();
			throw new Exception("Mallparametrarna kunde inte läsas ut");
		}
		
		for(ParameterTemplate ptemp : ptemps) {
			lookupTemplate.put(ptemp.getName(), ptemp);
		}
		
		JasperReport jasperReport = null;
		try {
			jasperReport = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(rt.getJasperReportData()));
		}
		catch(Exception e) {
			e.printStackTrace();
			if(onClose != null) {
				onClose.run();
			}
			close();
			throw new Exception("Parametermallen kan inte redigeras");
		}
		JRParameter[] pars = jasperReport.getParameters();
		for(JRParameter par : pars) {
			if(!par.isSystemDefined() && !par.getName().startsWith("INTERNAL_")) {
				if(lookupTemplate.get(par.getName()) == null) {
					if(onClose != null) {
						onClose.run();
					}
					close();
					throw new Exception("Parametern "+par.getName()+" saknas. Är mallen initierad?");
				}
			}
		}
		
		try {
			pvals = ParameterValueHandling.createParameterList(report.getData(), "Report data id="+report.getId());
		} catch (Exception e) {
			e.printStackTrace();
			if(onClose != null) {
				onClose.run();
			}
			close();
			throw new Exception("Parametrarna kunde inte läsas ut");
		}
		if(pvals == null) {
			pvals = new ArrayList<ParameterValue>();
		}
		
		for(ParameterValue pval : pvals) {
			lookupValue.put(pval.getName(), pval);
		}

		for(ParameterTemplate ptemp : ptemps) {
			String name = ptemp.getName();
			if(lookupValue.get(name) == null) {
				ParameterValue pv = new ParameterValue(name, "");
				pvals.add(pv);
				lookupValue.put(name, pv);
			}
		}
		
		FormLayout content = new FormLayout();
		
		for(ParameterValue pval : pvals) {
			TextArea editor = new TextArea(pval.getName());
			lookupEditor.put(pval.getName(), editor);
			editor.setValue(pval.getValue());
			editor.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
			editor.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.2)+"px");
			content.addComponent(editor);
		}
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Åter");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancelBtn);
		
		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				save();
			} 
			
		});
		buttons.addComponent(saveBtn);
		
		Button checkBtn = new Button("Kontroll");
		checkBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				check();
			}
			
		});
		buttons.addComponent(checkBtn);
		
		content.addComponent(buttons);
		setContent(content);
	}
	
	private String getLimitErrorMessage() {
		for(ParameterValue pval : pvals) {
			String fieldname = pval.getName();
			TextArea ta = lookupEditor.get(fieldname);
			ParameterTemplate ptemp = lookupTemplate.get(fieldname);
			String val = ta.getValue();

			if(ptemp.getMinlen() == null && val == null) {
			}
			if(ptemp.getMinlen() != null && val == null) {
				return "Fältet "+fieldname+" får inte vara tomt. Minst "+ptemp.getMinlen()+" tecken.";
			}
			if(ptemp.getMinlen() == null && val != null) {
			}
			if(ptemp.getMinlen() != null && val != null) {
				if(val.length() < ptemp.getMinlen()) {
					return "Fältet "+fieldname+" skall ha minst "+ptemp.getMinlen()+" tecken. Nu är det "+val.length()+".";
				}
			}
			if(ptemp.getMaxlen() == null && val == null) {
			}
			if(ptemp.getMaxlen() != null && val == null) {
				return "Fältet "+fieldname+" får inte vara tomt. Maximalt "+ptemp.getMaxlen()+" tecken.";
			}
			if(ptemp.getMaxlen() == null && val != null) {
			}
			if(ptemp.getMaxlen() != null && val != null) {
				if(val.length() > ptemp.getMaxlen()) {
					return "Fältet "+fieldname+" skall ha maximalt "+ptemp.getMaxlen()+" tecken. Nu är det "+val.length()+".";
				}
			}
		}
		return null;
	}

	private void check() {
		String lem = getLimitErrorMessage();
		if(lem != null) {
			Notification.show(lem, Notification.Type.ERROR_MESSAGE);
			return;
		}
		Notification.show("parametrarna uppfyller kraven", Notification.Type.HUMANIZED_MESSAGE);
	}

	protected void save() {
		String lem = getLimitErrorMessage();
		if(lem != null) {
			Notification.show(lem, Notification.Type.ERROR_MESSAGE);
			return;
		}
		for(ParameterValue pval : pvals) {
			String fieldname = pval.getName();
			TextArea ta = lookupEditor.get(fieldname);
			pval.setValue(ta.getValue());
		}
		byte[] parameterData = null;
		try {
			parameterData = ParameterValueHandling.serializeParameterListToBytes(pvals);
		} catch (UnsupportedEncodingException e) {
			Notification.show("Parametrarna kunde inte serialiseras", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
		report.setData(parameterData);
		Notification.show("Parametrarna sparades", Notification.Type.HUMANIZED_MESSAGE);
	}

	protected void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
