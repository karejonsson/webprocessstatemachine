package se.prv.errends.mainweb.adminuser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class AdminuserView extends Panel {
	
	private List<Adminuser> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Adminuser> grid = null;
	private boolean sessionIsOfSuper = false;
	private Adminuser current = null;
	
	public AdminuserView(Adminuser current) {
		this.current = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = AdminuserDB.getAllAdminusers();
		try {
			if(current == null) {
				//System.out.println("Superstatus - null "+sessionIsOfSuper);
				sessionIsOfSuper = false;
			}
			sessionIsOfSuper = current.isSuperprivilegies();
		}
		catch(Exception e) {
			//System.out.println("Superstatus - fel "+sessionIsOfSuper);
			sessionIsOfSuper = false;
			e.printStackTrace();
		}
		
		//System.out.println("Superstatus - slut "+sessionIsOfSuper);

		grid = new Grid<>();

        grid.addColumn(Adminuser::getUsername).setCaption("Användarnamn").setEditorComponent(new TextField(), Adminuser::setUsername);
        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Aktiv");
        	box.setValue(row.isActive());
        	box.addValueChangeListener(event ->  {
        		//System.out.println("AKTIV: Klick till "+box.getValue()+" på "+row.getUsername());
        		row.setActive(box.getValue());
        	});
            return box;
        });

        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Super");
        	box.setEnabled(sessionIsOfSuper);
        	box.setValue(row.isSuperprivilegies());
        	box.addValueChangeListener(event -> { 
        		//System.out.println("SUPER: Klick till "+box.getValue()+" på "+row.getUsername());
        		row.setSuperprivilegies(box.getValue());
        	});
            return box;
        });

		grid.getEditor().setEnabled(sessionIsOfSuper);

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button changeCredentialsBtn = new Button("Byt lösen");
		      changeCredentialsBtn.setEnabled(current.getId().equals(row.getId()) || sessionIsOfSuper);
		      changeCredentialsBtn.addClickListener(click -> changeCredentials(row));
		      out.addComponent(changeCredentialsBtn);
		      return out;
		}).setCaption("Hantering");
		
		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		saveBtn.setEnabled(sessionIsOfSuper);
		buttons.addComponent(saveBtn);
		
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newAdministrator();
			}
		});
		newBtn.setEnabled(sessionIsOfSuper);		
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void changeCredentials(Adminuser au) {
		AdminuserChangeCredentialsForm npff = new AdminuserChangeCredentialsForm(this, au, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(AdminuserView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	public void save() {
		for(Adminuser au : rows) {
			System.out.println("Sparar "+au.getUsername());
			au.save();
		}
	}
	
	public void newAdministrator() {
		Adminuser adminuser = new Adminuser();
		AdminuserFormEdit afe = new AdminuserFormEdit(adminuser, new Runnable() {
			@Override
			public void run() {
				notifyAboutNewAdminuser(adminuser);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(afe);
		/*
		ProcessfileFormNew npff = new ProcessfileFormNew(this, new Runnable() {
			@Override
			public void run() {
				((MainwebUI) UI.getCurrent()).setupForAdmin(ProcessFileView.this);
			}
		});
		((MainwebUI) UI.getCurrent()).openSubwindow(npff);
		*/
	}
	
	public void notifyAboutNewAdminuser(Adminuser adminuser) {
		rows.add(adminuser);
		grid.getDataProvider().refreshAll();
	}

}
