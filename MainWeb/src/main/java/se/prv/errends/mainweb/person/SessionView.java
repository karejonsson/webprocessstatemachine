package se.prv.errends.mainweb.person;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.SessionDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;

public class SessionView extends Panel {
	
	private List<Session> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Session> grid = null;
	private Person person = null;

	public SessionView() {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		rows = SessionDB.getAllSessionsOfPerson(person.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Errend e = row.getActor().getErrend();
        	if(e == null) {
        		return new Label("Ärendelös session");
        	}
        	else {
            	CheckBox box = new CheckBox();
            	box.setValue(e.isActive());
            	return box;
        	}
        }).setCaption("Aktivstatus"); 
        grid.addComponentColumn(row -> {
        	Date date = row.getStart();
        	Label out = new Label(Adminuser.presentDate(date));
        	return out;
        }).setCaption("Skapat"); 
        grid.addComponentColumn(row -> {
        	Date date = row.getFinish();
        	Label out = new Label(Adminuser.presentDate(date));
        	return out;
        }).setCaption("Avslutat"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.getActor().getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getRolename());
        	}
        }).setCaption("Rollnamn"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.getActor().getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getProcessdef().getProcessname());
        	}
        }).setCaption("Ärendetyp"); 
		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
}
