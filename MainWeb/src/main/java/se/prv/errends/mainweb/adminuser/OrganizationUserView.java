package se.prv.errends.mainweb.adminuser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.OrganizationUserDB;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.mainweb.ui.AdminuserUI;

public class OrganizationUserView extends Panel {
	
	private List<OrganizationUser> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<OrganizationUser> grid = null;
	
	public OrganizationUserView() {
		rows = OrganizationUserDB.getAllOrganizationUsers();

		grid = new Grid<>();

        grid.addColumn(OrganizationUser::getUsername).setCaption("Användarnamn").setEditorComponent(new TextField(), OrganizationUser::setUsername);
        grid.addComponentColumn(row -> { 
        	try {
        		return new Label(row.getOrganization().getOrgname());
            }
        	catch(Exception e) {
        		return new Label("--");
        	}
        }).setCaption("Organisation");
        grid.addComponentColumn(row -> { 
        	try {
        		return new Label(row.getOrganization().getOrgnr());
            }
        	catch(Exception e) {
        		return new Label("--");
        	}
        }).setCaption("Organisationsnr");
        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Aktiv");
        	box.setValue(row.getActive());
        	box.addValueChangeListener(event ->  {
        		row.setActive(box.getValue());
        	});
            return box;
        }).setCaption("Aktivstatus");

        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Super");
        	box.setValue(row.getSuperprivilegies());
        	box.addValueChangeListener(event ->  {
        		row.setActive(box.getValue());
        	});
            return box;
        }).setCaption("Superprivilegier");

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button changeCredentialsBtn = new Button("Byt lösen");
		      changeCredentialsBtn.addClickListener(click -> changeCredentials(row));
		      out.addComponent(changeCredentialsBtn);
		      return out;
		}).setCaption("Hantering");
		
		grid.getEditor().setEnabled(true);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		buttons.addComponent(saveBtn);
		
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newOrganizationUser();
			}
		});
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void changeCredentials(OrganizationUser au) {
		OrganizationUserChangeCredentialsForm npff = new OrganizationUserChangeCredentialsForm(this, au, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(OrganizationUserView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	public void save() {
		for(OrganizationUser orguser : rows) {
			orguser.save();
		}
	}
	
	public void newOrganizationUser() {
		OrganizationUser orguser = new OrganizationUser();
		//orguser.save();
		rows.add(orguser);
		notifyAboutChangeOfOrganizationUser(orguser);
		OrganizationUserFormEdit afe = new OrganizationUserFormEdit(orguser, new Runnable() {
			@Override
			public void run() {
				notifyAboutChangeOfOrganizationUser(orguser);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(afe);
	}
	
	public void notifyAboutChangeOfOrganizationUser(OrganizationUser orguser) {
		grid.getDataProvider().refreshAll();
	}

}
