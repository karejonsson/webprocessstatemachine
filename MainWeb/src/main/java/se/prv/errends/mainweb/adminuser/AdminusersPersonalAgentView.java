package se.prv.errends.mainweb.adminuser;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification.Type;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;

public class AdminusersPersonalAgentView extends Panel {
	
	private List<Agent> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Agent> grid = null;
	private Session session = null;
	private Person subjectedPerson = null;

	public AdminusersPersonalAgentView(Person person) {
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		subjectedPerson = person;

		rows = subjectedPerson.getReferringAgents();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getEmail());
        }).setCaption("E-post"); 
        grid.addComponentColumn(row -> {
        	return new Label(Adminuser.presentDate(row.getRegisterred()));
        }).setCaption("Registrerad"); 
        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Aktiv");
        	box.setValue(row.isActive());
        	box.addValueChangeListener(event ->  {
        		row.setActive(box.getValue());
        	});
            return box;
        }).setCaption("Aktivstatus"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getFirstname());
        }).setCaption("Förnamn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getFamilyname());
        }).setCaption("Efterrnamn"); 
		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button deleteBtn = new Button("Radera");
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      return out;
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button newAgentBtn = new Button("Ny");
		newAgentBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				newAgent();
			}
			
		});
		buttons.addComponent(newAgentBtn);

		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void newAgent() {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(
				new SearchOrganizationForm(new SearchOrganizationForm.NotifyAboutPick() {
					@Override
					public void picked(Organisation org) {
						Agent agent = new Agent();
						agent.setOrganization(org);
						agent.setPerson(subjectedPerson);
						agent.setRegisterred(new Date(System.currentTimeMillis()));
						agent.setActive(true);
						agent.save();
						rows.add(agent);
						grid.getDataProvider().refreshAll();
					}
				}, null));
	}
	
	private void delete(Agent agent) {
		// Saknas det inte ett borttagande från tabellen här?
		try {
			agent.deleteRecursively();
		}
		catch(Exception e) {
			Notification.show("Aktören kan ej raderas pga befintliga engagemang", Type.ERROR_MESSAGE);
			return;
		}
	}
	
}
