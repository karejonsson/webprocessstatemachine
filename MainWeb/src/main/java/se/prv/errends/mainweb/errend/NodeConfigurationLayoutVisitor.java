package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;

import se.prv.errends.domain.Errend;
import se.prv.errends.nodeconfig.struct.NodeLayoutVisitor;
import se.prv.errends.nodeconfig.struct.NodeView;
import se.prv.errends.nodeconfig.struct.GuiFunctions;

public class NodeConfigurationLayoutVisitor implements NodeLayoutVisitor {
	
	private Errend errend = null;
	private ErrendInStateView eisv = null;
	
	public NodeConfigurationLayoutVisitor(Errend errend, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
	}
	
	@Override
	public void visit(GuiFunctions tabbedLayout) {
		TabSheet tabsheet = new TabSheet();
		List<NodeView> nodeviews = tabbedLayout.getNodeViews();
		for(NodeView nodeview : nodeviews) {
			NodeConfigurationViewVisitor visitor = new NodeConfigurationViewVisitor(errend, eisv);
			nodeview.accept(visitor);
			Component tab = visitor.getComponent();
			if(tab != null) {
				tabsheet.addTab(tab, nodeview.getName());
			}
		}
		out = tabsheet;
	}
	
	private Component out = null;
	
	public Component getComponent() {
		return out;
	}

}
