package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.TransitionDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Transition;

public class HistoryInErrendView extends Panel {
	
	private List<Transition> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Transition> grid = null;

	private Errend errend = null;
	//private Session session = null;
	private ErrendInStateView eisv = null;
	
	public HistoryInErrendView(Errend errend, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
		//session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.authenticated);
		rows = TransitionDB.getAllTransitionsOfErrend(errend.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Label out = new Label(Adminuser.presentDate(row.getSession().getStart()));
            return out;
        }).setCaption("Start");
        grid.addComponentColumn(row -> {
        	Label out = null;
        	try {
               	out = new Label(row.getTransitiondef().getSourcestatedef().getStatename());
                        	}
        	catch(Exception e) {
        		out = new Label("--");
        	}
            return out;
        }).setCaption("Från");
        grid.addComponentColumn(row -> {
        	Label out = null;
        	try {
               	out = new Label(row.getTransitiondef().getTransitionname());
                        	}
        	catch(Exception e) {
        		out = new Label("--");
        	}
            return out;
        }).setCaption("Operation");
        grid.addComponentColumn(row -> {
        	Label out = null;
        	try {
               	out = new Label(row.getTransitiondef().getTargetstatedef().getStatename());
                        	}
        	catch(Exception e) {
        		out = new Label("--");
        	}
            return out;
        }).setCaption("Till");
        grid.addComponentColumn(row -> {
        	Actor actor = row.getSession().getActor();
        	Agent agent = actor.getAgent();
        	Person p = agent.getPerson();
        	Label out = new Label(agent.getOrganisation().getOrgname()+" / "+p.getFirstname()+" "+p.getFamilyname());
            return out;
        }).setCaption("Ombud");
        grid.addComponentColumn(row -> {
        	Label out = new Label(row.getSession().getActor().getRoledef().getRolename());
            return out;
        }).setCaption("Roll");

		grid.getEditor().setEnabled(true);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		setContent(layout);
	}

	public Errend getErrend() {
		return errend;
	}
	
}
