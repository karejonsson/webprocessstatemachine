package se.prv.errends.mainweb.adminuser;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.FunctionDefReuse;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class FunctionsFileimportForm extends Window {
	
	private Runnable onClose = null;
	
	private Processfile pf = null;
	private ComboBox<ProcessdefListItem> processdefChoice = null; 
	
	public FunctionsFileimportForm(Processfile pf, Runnable onClose) {
		this.pf = pf;
		this.onClose = onClose;
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		processdefChoice = new ComboBox<ProcessdefListItem>("Koppla till processtyp");
		List<Processdef> pds = ProcessdefDB.getAllProcessdefs();
		List<ProcessdefListItem> items = new ArrayList<ProcessdefListItem>();
		for(Processdef pd : pds) {
			ProcessdefListItem pdli = new ProcessdefListItem(pd);
			items.add(pdli);
		}
		processdefChoice.setItems(items);
		content.addComponent(processdefChoice);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		if(pf != null) {
			Button control = new Button("Kontroll");
			control.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void buttonClick(ClickEvent event) {
					control();
				}
				
			});
			buttons.addComponent(control);
		}

		Button create = new Button("Skapa");
		create.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				create();
			}
			
		});
		buttons.addComponent(create);

		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void control() {
		try {
			byte[] data = pf.getData();
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			InputStreamReader isr = new InputStreamReader(bais);
	    	List<ProducerConfiguration> producers = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Processfile "+pf.getId());
			Notification.show("OK! "+producers.size()+" funktioner", Notification.Type.HUMANIZED_MESSAGE);
		} catch (Exception e) {
			Notification.show("Kontroll fallerade", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void create() {
		Optional<ProcessdefListItem> op = processdefChoice.getSelectedItem();
		Processdef pd = null;
		try {
			pd = op.get().getProcessdef();
		}
		catch(Exception e) {
		}

		if(pd == null) {
			System.out.println("Ingen processdefinition vald");
		}
		else {
			System.out.println("Processdefinitionen "+pd.getProcessname()+" vald");
		}
		
		byte[] data = pf.getData();
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		InputStreamReader isr = new InputStreamReader(bais);
		List<ProducerConfiguration> producers = null;
    	try {
			producers = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Processfile "+pf.getId());
		} catch (Exception e) {
			Notification.show("Inläsning fallerade\nFilen är ej parsebar", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}

    	for(ProducerConfiguration producer : producers) {
    		FunctionDef fd = new FunctionDef();
    		fd.setProcessfile(pf);
    		fd.setFunctionname(producer.getName());
    		fd.setTypename(producer.getTypename());
    		fd.setSource(producer.toStringReproduce());
    		fd.save();
    		if(pd != null) {
    			FunctionDefReuse fdr = new FunctionDefReuse();
    			fdr.setFunctionDef(fd);
    			fdr.setProcessdef(pd);
    			fdr.save();
    		}
    	}

		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private static class ProcessdefListItem {
		private Processdef pd = null;
		public ProcessdefListItem(Processdef pd) {
			this.pd = pd;
		}
		public String toString() {
			return pd.getProcessname()+"/"+pd.getProcessfile().getDescription();
		}
		public Processdef getProcessdef() {
			return pd;
		}
	}

}
