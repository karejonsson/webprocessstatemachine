package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ReportDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.nodeconfig.struct.ReportView;

public class ReportsInErrendView extends Panel {

	private List<Report> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Report> grid = null;

	private Errend errend = null;
	private Session session = null;
	private ErrendInStateView eisv = null;
	private ReportView reportView = null;

	public ReportsInErrendView(Errend errend, ErrendInStateView eisv, ReportView reportView) {
		this.errend = errend;
		this.eisv = eisv;
		this.reportView = reportView;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		rows = ReportDB.getAllReportsOfErrend(errend.getId());

		grid = new Grid<>();

		grid.addComponentColumn(row -> {
			return new Label(row.getReportTemplateReuse().getReportTemplate().getReporttitle());
		}).setCaption("Mallens titel").setExpandRatio(1);

		/*
		JavaScript.getCurrent().addViewChangeListener(new ViewChangeListener() {

			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {
				System.out.println("ViewChangeListener . "+event);
				return false;
			}

		});
		 */
		/*
		 .addAttachListener(new AttachListener() {


			@Override
			public void attach(AttachEvent event) {
				System.out.println("ReportsInErrendView 1.X "+event);
			}

		});
		 */
		grid.addComponentColumn(row -> {
			if(download()) {
				return new ReportLink(row, new Runnable() {

					@Override
					public void run() {
						//System.out.println("ReportsInErrendView 1");
						//JavaScript.getCurrent().execute("alert(\"Hej\");");

						//ReportsInErrendView.this
						//System.out.println("ReportsInErrendView 1.1");
						//Notification.show("FEL", Type.ERROR_MESSAGE);
						//setContent(new ShowLongText("Fel vid rapportgenerering", "blaha"));
						//UI.getCurrent().addWindow(new ShowLongText("Fel vid rapportgenerering", "blaha"));
						//System.out.println("ReportsInErrendView 2");
						//UI.getCurrent().doRefresh(((CommonUI) UI.getCurrent()).getRequest());
						//UI.getCurrent().
						//System.out.println("ReportsInErrendView 3");
					}

				});
			}
			else {
				return new Label(ReportLink.getName(row));
			}
		}).setCaption("Egen titel").setExpandRatio(1);

		grid.addComponentColumn(row -> {
			Actor actor = row.getActor();
			Agent agent = actor.getAgent();
			Person p = agent.getPerson();
			Organisation organization = agent.getOrganisation();
			String orgpart = "";
			if(organization != null) {
				orgpart = organization.getOrgname()+" / ";
			}
			String rolepart = "";
			Roledef roledef = actor.getRoledef();
			if(roledef != null) {
				rolepart = roledef+" / ";
			}
			Label out = new Label(orgpart+rolepart+p.getFirstname()+" "+p.getFamilyname());
			return out;
		}).setCaption("Mottagare").setExpandRatio(1);

		grid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button editBtn = new Button("Redigera");
			editBtn.addClickListener(click -> editReport(row));
			out.addComponent(editBtn);
			Button deleteBtn = new Button("Radera");
			deleteBtn.addClickListener(click -> deleteReport(row));
			out.addComponent(deleteBtn);
			return out;
		}).setCaption("Hantering").setExpandRatio(1);

		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();

		HorizontalLayout buttons = new HorizontalLayout();
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newReport();
			}
		});
		buttons.addComponent(newBtn);

		layout.addComponent(buttons);

		layout.addComponent(grid);

		setContent(layout);
	}

	private boolean download() {
		return evalBooleanProperty("download", true);
	}

	private boolean evalBooleanProperty(String key, boolean defaultResponse) {
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: "+key+", default "+defaultResponse);
		if(!reportView.hasKey(key)) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 1 "+defaultResponse);
			return defaultResponse;
		}
		String value = reportView.getValue(key);
		if(value == null) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 2 "+defaultResponse);
			return defaultResponse;
		}
		if(value.toLowerCase().contains("n")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 3 false");
			return false;
		}
		if(value.toLowerCase().contains("y") || value.toLowerCase().contains("j")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 true");
			return true;
		}
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 "+defaultResponse);
		return defaultResponse;
	}

	public Errend getErrend() {
		return errend;
	}

	private Object editReport(Report row) {
		ReportFormEdit rfe = null;
		try {
			//System.out.println("ReportsInErrendView 1");
			rfe = new ReportFormEdit(this, row, new Runnable() {
				@Override
				public void run() {
					//close();
					//System.out.println("ReportsInErrendView 2");
					((SimplifiedContentUI) UI.getCurrent()).setWorkContent(eisv);
					//System.out.println("ReportsInErrendView 3");
				}
			});
			//System.out.println("ReportsInErrendView 4");
			((SimplifiedContentUI) UI.getCurrent()).openSubwindow(rfe);
			//System.out.println("ReportsInErrendView 5");
		} catch (Exception e) {
			Notification.show("Redigering misslyckades\n"+e.getMessage(), Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return null;
	}

	private void deleteReport(Report row) {
		rows.remove(row);
		row.delete();
		grid.getDataProvider().refreshAll();
	}

	private void newReport() {
		ReportFormNew npff = new ReportFormNew(this, new Runnable() {
			@Override
			public void run() {
				((SimplifiedContentUI) UI.getCurrent()).setWorkContent(eisv);
			}
		});
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(npff);
	}

	public void notifyAboutNewReport(Report pf) {
		rows.add(pf);
		grid.getDataProvider().refreshAll();
	}

}
