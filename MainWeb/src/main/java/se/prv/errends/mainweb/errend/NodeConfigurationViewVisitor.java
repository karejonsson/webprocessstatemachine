package se.prv.errends.mainweb.errend;

import com.vaadin.ui.Component;

import se.prv.errends.domain.Errend;
import se.prv.errends.nodeconfig.struct.CommentView;
import se.prv.errends.nodeconfig.struct.DossierRules;
import se.prv.errends.nodeconfig.struct.ErrendstateView;
import se.prv.errends.nodeconfig.struct.HistoryView;
import se.prv.errends.nodeconfig.struct.NodeViewVisitor;
import se.prv.errends.nodeconfig.struct.ParticipantView;
import se.prv.errends.nodeconfig.struct.ReportView;
import se.prv.errends.nodeconfig.struct.SessionView;

public class NodeConfigurationViewVisitor implements NodeViewVisitor {
	
	private Errend errend = null;
	private ErrendInStateView eisv = null;
	private Component out = null;
	
	
	public NodeConfigurationViewVisitor(Errend errend, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
	}

	@Override
	public void visit(SessionView sessionView) {
		out = new SessionsInErrendView(errend, eisv);
	}

	@Override
	public void visit(HistoryView sessionView) {
		out = new HistoryInErrendView(errend, eisv);
	}

	@Override
	public void visit(ErrendstateView sessionView) {
		out = new StateOfErrendView(errend, eisv);
	}

	@Override
	public void visit(ParticipantView participantView) {
		out = new ParticipantsInErrendView(errend, participantView, eisv);
	}

	public Component getComponent() {
		return out;
	}

	@Override
	public void visit(ReportView reportView) {
		out = new ReportsInErrendView(errend, eisv, reportView);
	}

	@Override
	public void visit(CommentView reportView) {
		out = new CommentsInErrendView(errend, eisv, reportView);
	}

}
