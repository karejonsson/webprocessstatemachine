package se.prv.errends.mainweb.errend;

import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import se.prv.errends.domain.Errend;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.StatedefManaged;

public class StateOfErrendView extends Panel {

	private VerticalLayout layout = new VerticalLayout();
	private Errend errend = null;
	private ErrendManagement em = null;
	private ErrendInStateView eisv = null;

	public StateOfErrendView(Errend errend, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
		
		em = (ErrendManagement) UI.getCurrent().getSession().getAttribute(CommonUI.currenterrend);
		
		TextField adminusername = new TextField("Formellt ärendenamn");
		adminusername.setEnabled(false);
		String errendAdminusername = errend.getAdminusername();
		if(errendAdminusername == null) {
			errendAdminusername = "--";
		}
		adminusername.setValue(errendAdminusername);		
		layout.addComponent(adminusername);
		
		TextField personname = new TextField("Eget ärendenamn");
		personname.setEnabled(false);
		String errendPersonname = errend.getPersonname();
		if(errendPersonname == null) {
			errendPersonname = "--";
		}
		personname.setValue(errendPersonname);		
		layout.addComponent(personname);
		
		TextField state = new TextField("Tillstånd");
		state.setEnabled(false);
		StatedefManaged sdm = em.getCurrentState();
		state.setValue(sdm.getStatename());	
		layout.addComponent(state);
		
		setContent(layout);
	}

}
