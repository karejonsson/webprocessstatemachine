package se.prv.errends.mainweb.person;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.RoledefDB;
import se.prv.errends.domain.Roledef;

public class ChoseRoledefForm extends Window {
	
	private Runnable onClose = null;
	private Roledef roledef = null;
	private TextField whatever = null;
	private TextField name = null;
	private Grid<Roledef> grid = null;
	private NotifyAboutRoledefPick notify = null;
	
	public ChoseRoledefForm(NotifyAboutRoledefPick notify, Runnable onClose) {
		super("Välj roll");
		this.onClose = onClose;
		this.notify = notify;
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		name = new TextField("Eget namn");
		content.addComponent(name);

		whatever = new TextField("Fritext");
		content.addComponent(whatever);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button searchBtn = new Button("Sök");
		searchBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				search();
			}
			
		});
		buttons.addComponent(searchBtn);	
		content.addComponent(buttons);
		
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getRolename());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getProcessdef().getProcessname());
        }).setCaption("Process"); 
        if(notify != null) {
	        grid.addComponentColumn(row -> {
	        	Button pick = new Button("Välj");
	    		pick.addClickListener(new ClickListener() {
	    			private static final long serialVersionUID = 1L;
	    			@Override
	    			public void buttonClick(ClickEvent event) {
	    				picked(row, name.getValue());
	    			}
	    		});
	        	return pick;
	        }).setCaption("Välj"); 
        }
        
		grid.getEditor().setEnabled(false);

		content.addComponent(grid);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void search() {
		String name = whatever.getValue();
		if(name == null || name.trim().length() < 1) {
			name = "%";
		}
		else {
			if(!name.contains("*")) {
				name = "%"+name.trim()+"%";
			}
			else {
				name = name.trim().replace("*", "%");
			}
		}
		//System.out.println("SearchPersonForm.search: Verklig sökning "+name);
		List<Roledef> hits = RoledefDB.searchByName(name);
		grid.setItems(hits);
	}
	
	public interface NotifyAboutRoledefPick {
		public void picked(Roledef rd, String name);
	}
	
	private void picked(Roledef rd, String name) {
		if(notify != null) {
			notify.picked(rd, name);
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
