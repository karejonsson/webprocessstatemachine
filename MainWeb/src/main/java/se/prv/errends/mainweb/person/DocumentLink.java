package se.prv.errends.mainweb.person;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Link;

import se.prv.errends.domain.Document;

public class DocumentLink extends Link {
	
	private static final long serialVersionUID = -861816214400624227L;
	private Document df;

	public DocumentLink(Document df) {
		super();
		this.df = df;
		setCaption(df.getFilename());
		setDescription("Hämta fil till din dator");
		setTargetName("_blank");
	}

	@Override
	public void attach() {
		super.attach(); // Must call.

		StreamResource.StreamSource source = new StreamResource.StreamSource() {
			public InputStream getStream() {
				return new ByteArrayInputStream(df.getData());
			}
		};
		StreamResource resource = new StreamResource (source, df.getFilename());
		resource.setMIMEType(df.getMimetype());
		resource.setCacheTime(0);
		setResource(resource);
	}
}
