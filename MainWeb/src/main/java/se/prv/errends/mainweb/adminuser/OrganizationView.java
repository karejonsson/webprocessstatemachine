package se.prv.errends.mainweb.adminuser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.OrganizationDB;
import se.prv.errends.domain.Organisation;
import se.prv.errends.mainweb.orguser.OrganizationFormEdit;
import se.prv.errends.mainweb.orguser.OrganizationsAgentView;
import se.prv.errends.mainweb.ui.AdminuserUI;

public class OrganizationView extends Panel {
	
	private List<Organisation> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Organisation> grid = null;
	
	public OrganizationView() {
		rows = OrganizationDB.getAllOrganizations();

		grid = new Grid<>();

        grid.addColumn(Organisation::getOrgname).setCaption("Namn").setEditorComponent(new TextField(), Organisation::setOrgname);
        grid.addColumn(Organisation::getOrgnr).setCaption("Nummer").setEditorComponent(new TextField(), Organisation::setOrgnr);
        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Aktiv");
        	box.setValue(row.getActive());
        	box.addValueChangeListener(event ->  {
        		row.setActive(box.getValue());
        	});
            return box;
        }).setCaption("Aktivstatus");

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button changeCredentialsBtn = new Button("Redigera");
		      changeCredentialsBtn.addClickListener(click -> editOrganization(row));
		      out.addComponent(changeCredentialsBtn);
		      Button agentsBtn = new Button("Ombud");
		      agentsBtn.addClickListener(click -> agents(row));
		      out.addComponent(agentsBtn);
		      return out;
		}).setCaption("Hantering");
		
		grid.getEditor().setEnabled(true);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		buttons.addComponent(saveBtn);
		
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newOrganization();
			}
		});
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void editOrganization(Organisation org) {
		OrganizationFormEdit ofe = new OrganizationFormEdit(org, new Runnable() {
			@Override
			public void run() {
				notifyAboutChangeOfOrganization();
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(ofe);
	}
	
	private void agents(Organisation org) {
		OrganizationsAgentView ofe = new OrganizationsAgentView(org, null);
		((AdminuserUI) UI.getCurrent()).setWorkContent(ofe);
	}
	
	public void save() {
		for(Organisation org : rows) {
			org.save();
		}
	}
	
	public void newOrganization() {
		Organisation org = new Organisation();
		org.setOrgname("");
		org.setOrgnr("");
		org.save();
		rows.add(org);
		notifyAboutChangeOfOrganization();
		editOrganization(org);
	}
	
	public void notifyAboutChangeOfOrganization() {
		grid.getDataProvider().refreshAll();
	}

}
