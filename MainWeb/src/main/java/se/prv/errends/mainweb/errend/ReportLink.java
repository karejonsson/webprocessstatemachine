package se.prv.errends.mainweb.errend;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.server.DownloadStream;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.ReportTemplateReuse;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.parametervalue.struct.ParameterValue;
import se.prv.errends.parametervalue.struct.ParameterValueHandling;
import se.prv.errends.reuse.cli.PropertyNames;

public class ReportLink extends Link {
	
	private Report report;
	private Runnable onError = null;

	public ReportLink(Report report, Runnable onError) {
		super();
		this.report = report;
		this.onError = onError;
		setCaption(getName(report));
		setDescription("Hämta genererad rapport");
		setTargetName("_blank");
	}
	
	public static String getName(Report report) {
		String title = report.getReporttitle();
		if(title == null || title.trim().length() == 0) {
			title = report.getReportTemplateReuse().getReportTemplate().getReporttitle();
		}
		return title;
	}
	
	public static String getDownloadFilename(Report report) {
		String title = report.getReporttitle();
		if(title == null || title.trim().length() == 0) {
			title = report.getReportTemplateReuse().getReportTemplate().getReporttitle();
		}
		String aun = report.getErrend().getAdminusername();
		if(aun == null) {
			aun = "";
		}
		String pn = report.getErrend().getPersonname();
		if(pn == null) {
			pn = "";
		}
		title = title+aun+"_"+pn;
		title = title.trim().replaceAll(" ", "_");
		return title;
	}

	@Override
	public void attach() {
		super.attach(); // Must call.

		StreamResource.StreamSource source = new StreamResource.StreamSource() {
			public InputStream getStream() {
				JasperReport jasperReport = null;
				ReportTemplateReuse rtr = report.getReportTemplateReuse();
				ReportTemplate rt = rtr.getReportTemplate();
				byte[] compilerJasperReport = rt.getJasperReportData();
				try {
					jasperReport = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(compilerJasperReport));
				}
				catch(Exception e) {
					e.printStackTrace();
					Notification.show("Rapporten kan inte läsas fram", Notification.Type.ERROR_MESSAGE);
					return null;
				}
				
				byte[] parametersRaw = report.getData();
				List<ParameterValue> params;
				try {
					params = ParameterValueHandling.createParameterList(parametersRaw, "Report id="+report.getId());
				} catch (Exception e) {
					e.printStackTrace();
					Notification.show("Rapportens fältinformation kan inte utläsas", Notification.Type.ERROR_MESSAGE);
					return null;
				}

				Map<String, Object> parameters = new HashMap<String, Object>();
				for(ParameterValue pv : params) {
					parameters.put(pv.getName(), pv.getValue());
				}

				parameters.put("INTERNAL_FIRSTNAME", report.getActor().getAgent().getPerson().getFirstname());
				parameters.put("INTERNAL_FAMILYNAME", report.getActor().getAgent().getPerson().getFamilyname());
				parameters.put("INTERNAL_SWEDISHPNR", report.getActor().getAgent().getPerson().getSwedishpnr());
				parameters.put("INTERNAL_EMAIL", report.getActor().getAgent().getPerson().getEmail());
				parameters.put("INTERNAL_ORGNAME", report.getActor().getAgent().getOrganisation().getOrgname());
				parameters.put("INTERNAL_ORGNR", report.getActor().getAgent().getOrganisation().getOrgnr());
				parameters.put("INTERNAL_ROLENAME", report.getActor().getRoledef().getRolename());

				parameters.put("INTERNAL_PERSONALNAME", report.getErrend().getPersonname());
				parameters.put("INTERNAL_ADMINISTRATIONSNAME", report.getErrend().getAdminusername());
				
				String dateformat;
				try {
					dateformat = PersistentValuesDB.getStringValue(PropertyNames.date_format);
				} catch (Exception e1) {
					dateformat = "yyyy-MM-dd";
					e1.printStackTrace();
				}
				String timeformat;
				try {
					timeformat = PersistentValuesDB.getStringValue(PropertyNames.time_format);
				} catch (Exception e1) {
					timeformat = "HH:mm:SS";
					e1.printStackTrace();
				}
				String datetimeformat;
				try {
					datetimeformat = PersistentValuesDB.getStringValue(PropertyNames.datetime_format);
				} catch (Exception e1) {
					datetimeformat = "yyyy-MM-dd HH:mm:SS";
					e1.printStackTrace();
				}

				Date dateCreated = report.getErrend().getCreated();
								
				parameters.put("INTERNAL_DATEINITIATED", new SimpleDateFormat(dateformat).format(dateCreated));
				parameters.put("INTERNAL_DATETIMEINITIATED", new SimpleDateFormat(datetimeformat).format(dateCreated));
				parameters.put("INTERNAL_TIMEINITIATED", new SimpleDateFormat(timeformat).format(dateCreated));
				
				JRParameter[] pars = jasperReport.getParameters();
				for(JRParameter par : pars) {
					if(!par.isSystemDefined() && !par.getName().startsWith("INTERNAL_")) {
						if(parameters.get(par.getName()) == null) {
							if(onError != null) {
								onError.run();
							}
							//ReportLink.this.getErrorHandler().error(null);
							//Notification.show("Parametern "+par.getName()+" saknas. Är rapporten redigerad? Är mallen initierad?", Notification.Type.ERROR_MESSAGE);
							//System.out.println("Parametern "+par.getName()+" saknas. Är rapporten redigerad? Är mallen initierad?");
							//(new Throwable()).printStackTrace();
							//setTargetName(null);
							//throw new Exception("Parametern "+par.getName()+" saknas. Är rapporten redigerad? Är mallen initierad?");
							return null;
						}
					}
				}

				
				// Generate jasper print
				JasperPrint jprint;
				try {
					jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
				}
				catch(JRException jre) {
					jre.printStackTrace();
					Notification.show("Rapportens fältinformation är inte korrekt", Notification.Type.ERROR_MESSAGE);
					return null;
				}
				
				// Export pdf file
				ByteArrayOutputStream pdf = new ByteArrayOutputStream();
				try {
					JasperExportManager.exportReportToPdfStream(jprint, pdf);
				} catch (JRException e) {
					e.printStackTrace();
					Notification.show("Rapportens kunde inte genereras", Notification.Type.ERROR_MESSAGE);
					return null;
				}
				return new ByteArrayInputStream(pdf.toByteArray());
			}
		};
		String title = getDownloadFilename(report);
		StreamResource resource = new StreamResource (source, title+".pdf") {
			public DownloadStream getStream() {
				DownloadStream out = super.getStream();
				InputStream is = out.getStream();
				if(is == null) {
					//UI.getCurrent().requestRepaint();
					// Nu är det fel
					//String cap = ReportLink.this.getCaption();
					//ReportLink.this.setCaption(cap+" fel");
					//String msg = "Rapportens kunde inte genereras.\nKontrollera att mallen är initierad och rapporten redigerad.";
					//System.out.println("MSG: "+msg);
					// ((CommonUI) UI.getCurrent()).openSubwindow(new ShowLongText("Fel vid rapportgenerering", msg)); // Händer inte 
					//Notification.show(msg, Notification.Type.ERROR_MESSAGE);
				}
				return out;
			}
		};
		resource.setMIMEType("application/pdf");
		resource.setCacheTime(0);
		setResource(resource);
	}
	
}