package se.prv.errends.mainweb.person;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.HorizontalLayout;

import se.prv.errends.dbc.DossierDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Dossier;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.mainweb.adminuser.AskForAgentAndNameForm;
import se.prv.errends.mainweb.adminuser.AskForAgentAndNameForm.NotifyAboutAgentAndName;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.nodeconfig.struct.DefaultTypesafeDossierRules;

public class DossierListView extends Panel {
	
	private VerticalLayout layout = new VerticalLayout();
	private List<Dossier> rows = null;
	private Grid<Dossier> grid = null;
	private Person person = null;

	public DossierListView() {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		rows = DossierDB.getAllDossiersOfPerson(person.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Long eid = row.getErrendId();
        	if(eid == null) {
        		return new Label("Ärendelös");
        	}
        	else {
        		return new Label("I ärende");
        	}
        }).setCaption("Status"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getName());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getDescription());
        }).setCaption("Beskrivning"); 
        grid.addComponentColumn(row -> {
        	HorizontalLayout hl = new HorizontalLayout();
        	Long eId = row.getErrendId();
        	if(eId == null) {
        		{
                	Button handleBtn = new Button("Hantera");
                	hl.addComponent(handleBtn);
                	handleBtn.addClickListener(e -> handleDossier(row));
        		}
        		{
	            	Button deleteBtn = new Button("Radera");
	            	hl.addComponent(deleteBtn);
	            	deleteBtn.addClickListener(e -> deleteDossier(row));
        		}
        		{
	            	Button createErrendBtn = new Button("Skapa ärende");
	            	hl.addComponent(createErrendBtn);
	            	createErrendBtn.addClickListener(e -> createErrend(row));
        		}
        		{
                	Button editBtn = new Button("Redigera");
                	hl.addComponent(editBtn);
                	editBtn.addClickListener(e -> editDossier(row));
        		}
        		/*
        		{
                	Button showBtn = new Button("Visa");
                	hl.addComponent(showBtn);
                	showBtn.addClickListener(e -> System.out.println(row.toString()));
        		}
        		*/
        	}
        	else {
        		/*
        		{
                	Button showBtn = new Button("Visa");
                	hl.addComponent(showBtn);
                	showBtn.addClickListener(e -> System.out.println(row.toString()));
        		}
        		*/
        	}
        	return hl;
        }).setCaption("Hantering"); 
        
		grid.getEditor().setEnabled(false);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		Button newDossier = new Button("Ny akt");
		newDossier.addClickListener(e -> newDossier());
		layout.addComponent(newDossier);
		layout.addComponent(grid);
		setContent(layout);
	}

	private void createErrend(Dossier dossier) {
		List<Agent> allAgents = person.getReferringAgents();
		List<Agent> allowedAgents = new ArrayList<Agent>();
		allAgents.forEach(e -> { if(e.isActive()) { allowedAgents.add(e); } } );
		List<Processdef> allowedProcessDefs = ProcessdefDB.getAllProcessdefs();

		AskForAgentAndNameForm aafnf = new AskForAgentAndNameForm(
				allowedProcessDefs,
				allowedAgents,
				new NotifyAboutAgentAndName() {
					@Override
					public void agentAndName(Processdef pf, Agent agent, String name) {
						createErrend(pf, agent, name, dossier);
					}
				}, 
				new Runnable() {
					@Override
					public void run() {
						grid.getDataProvider().refreshAll();
					}
				});
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(aafnf);
	}
		
	private void createErrend(Processdef pd, Agent agent, String name, Dossier dossier) {
		try {
			ErrendManagement em = ErrendManagement.createErrendFromDBStructureAndInitialize(agent, pd, name);
			if(em.isError()) {
				Notification.show(em.getErrorMessage(), Notification.Type.ERROR_MESSAGE);
				return;
			}
			Errend errend = em.getErrend();
			dossier.setErrend(errend);
			dossier.save();
			Notification.show("Ärende "+name+" skapat", Notification.Type.ASSISTIVE_NOTIFICATION);
		}
		catch(Exception e) {
			e.printStackTrace();
			Notification.show("Ärende "+name+" kunde inte skapas", Notification.Type.ERROR_MESSAGE);
		}
	}
	

	private void editDossier(Dossier row) {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierFormEdit(row, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierListView.this);
	    		if(row.getId() != null) {
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("EDIT "+row);
	}

	private void newDossier() {
		final Dossier dossier = new Dossier();
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierFormEdit(dossier, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(DossierListView.this);
	    		if(dossier.getId() != null) {
		    		rows.add(dossier);
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("NEW ");
	}

	private void deleteDossier(Dossier row) {
		rows.remove(row);
		row.deleteRecursive();
		grid.getDataProvider().refreshAll();
		System.out.println("DELETE "+row);
	}

	private void handleDossier(Dossier row) {
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new DossierView(row, new DefaultTypesafeDossierRules(true), new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(DossierListView.this);
	    		if(row.getId() != null) {
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("HANDLE "+row);
	}

}
