package se.prv.errends.mainweb.adminuser.processeditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.FunctionDefReuse;

public class FunctionRowHolder {
	
	private String typename = null;
	private String functionname = null;
	private List<FunctionDefReuse> currentlyReusedFunctions = null;
	private List<FunctionDef> allFunctions = null;
	
	private Map<FunctionDef, FunctionDefReuse> upwards = null;
	private Map<String, List<FunctionDef>> namemapReused = null;
	private Map<String, List<FunctionDef>> namemapAll = null;
	private List<Long> reusedFunctionIds = null;
	private List<FunctionDefListItem> items = null;
	
	public FunctionRowHolder(String typename, String functionname, List<FunctionDefReuse> currentlyReusedFunctions, List<FunctionDef> allFunctions) {
		this.typename = typename;
		this.functionname = functionname;
		initialize(currentlyReusedFunctions, allFunctions);
	}
	
	void initialize(List<FunctionDefReuse> currentlyReusedFunctions, List<FunctionDef> allFunctions) {
		this.currentlyReusedFunctions = currentlyReusedFunctions;
		this.allFunctions = allFunctions;
		upwards = new HashMap<FunctionDef, FunctionDefReuse>();
		namemapReused = new HashMap<String, List<FunctionDef>>();
		namemapAll = new HashMap<String, List<FunctionDef>>();
		reusedFunctionIds = new ArrayList<Long>();
		items = new ArrayList<FunctionDefListItem>();

		for(FunctionDefReuse fdr : currentlyReusedFunctions) {
			upwards.put(fdr.getFunctionDef(), fdr);
			reusedFunctionIds.add(fdr.getFunctionDefId());
			String name = fdr.getFunctionDef().getFunctionname();
			List<FunctionDef> list = namemapReused.get(name);
			if(list == null) {
				list = new ArrayList<FunctionDef>();
				namemapReused.put(name, list);
			}
			list.add(fdr.getFunctionDef());
		}
		for(FunctionDef fd : allFunctions) {
			String name = fd.getFunctionname();
			List<FunctionDef> list = namemapAll.get(name);
			if(list == null) {
				list = new ArrayList<FunctionDef>();
				namemapAll.put(name, list);
			}
			list.add(fd);
		}
		optionsComponent = null;
		getOptionsComponent();
		statusComponent = null;
		getStatusComponent();
	}
	
	public void setFunctionDef(FunctionDef fd) {
		System.out.println("FunctionRowHolder.setFunctionDef "+fd);
		if(fd.getId() == null) {
			System.out.println("FunctionRowHolder.setFunctionDef id NULL");
			fd.save();
			FunctionDefListItem newFunction = new FunctionDefListItem(fd, "Ny funktion");
			items.add(newFunction);
			optionsComponent.setItems(items);
			optionsComponent.setSelectedItem(newFunction);
		}
		else {
			System.out.println("FunctionRowHolder.setFunctionDef id EJ null");
			for(FunctionDefListItem item : items) {
				if(item.getFunctionDef().getId().equals(fd.getId())) {
					System.out.println("FunctionRowHolder.setFunctionDef hittade "+item);
					optionsComponent.setSelectedItem(item);
					return;
				}
			}
			System.out.println("FunctionRowHolder.setFunctionDef hittade inte "+fd);
			FunctionDefListItem newItem = new FunctionDefListItem(fd, "Ny funktion");
			items.add(newItem);
			optionsComponent.setSelectedItem(newItem);
		}
	}

	public String getName() {
		return functionname;
	}
	
	private Label nameComponent = null;
	
	public Component getNameComponent() {
		if(nameComponent != null) {
			return nameComponent;
		}
		nameComponent = new Label(functionname);
		return nameComponent;
	}
	
	private Label statusComponent = null;
	
	public Component getStatusComponent() {
		if(statusComponent != null) {
			return statusComponent;
		}
		statusComponent = new Label(statusMessage);
		getOptionsComponent();
		updateChosenFunctionMessage();
		return statusComponent;
	}
	
	private String statusMessage = "Status obestämd";
	
	private void setStatusMessage(String message) {
		statusMessage = message;
		if(statusComponent != null) {
			statusComponent.setValue(statusMessage);
		}
	}
	
	private ComboBox<FunctionDefListItem> optionsComponent = null;
	
	private List<FunctionDefListItem> getCorrectlyNamedCurrentlyReused(List<Long> avoid) {
		List<FunctionDefListItem> items = new ArrayList<FunctionDefListItem>();
		for(FunctionDefReuse fdr : currentlyReusedFunctions) {
			if(fdr.getFunctionDef().getFunctionname().equals(functionname)) {
				if(!avoid.contains(fdr.getFunctionDefId())) {
					items.add(new FunctionDefListItem(fdr));
					avoid.add(fdr.getFunctionDefId());
				}
			}
		}
		return items;
	}
	
	private List<FunctionDefListItem> getCorrectlyNamedNotReused(List<Long> avoid) {
		List<FunctionDefListItem> items = new ArrayList<FunctionDefListItem>();
		for(FunctionDef fd : allFunctions) {
			if(fd.getFunctionname().equals(functionname)) {
				if(!avoid.contains(fd.getId())) {
					items.add(new FunctionDefListItem(fd, "Från annan process"));
					avoid.add(fd.getId());
				}
			}
		}
		return items;
	}
	
	public Component getOptionsComponent() {
		if(optionsComponent != null) {
			return optionsComponent;
		}
		optionsComponent = new ComboBox<FunctionDefListItem>();
		optionsComponent.setSizeFull();
		
		List<Long> avoid = new ArrayList<Long>();
		items.addAll(getCorrectlyNamedCurrentlyReused(avoid));
		items.addAll(getCorrectlyNamedNotReused(avoid));
		//items.addAll(getWronglyNamedCurrentlyReused(avoid));
		//items.addAll(getWronglyNamedNotReused(avoid));

		optionsComponent.setItems(items);
		if(items.size() > 0) {
			optionsComponent.setSelectedItem(items.get(0));
		}
		
		optionsComponent.addValueChangeListener(
				new HasValue.ValueChangeListener() {
				    public void valueChange(ValueChangeEvent event) {
				    	updateChosenFunctionMessage();
				    }
				});
		return optionsComponent;
	}
	
	private void updateChosenFunctionMessage() {
		FunctionDefListItem choice = null;
		try {
			Optional<FunctionDefListItem> op = optionsComponent.getSelectedItem();
			choice = op.get();
		}
		catch(Exception e) {}
		
		if(choice == null) {
			statusComponent.setValue("Vald till null");
			return;
		}
		FunctionDefReuse fdr = choice.getFunctionDefReuse();
		if(fdr == null) {
			FunctionDef fd = choice.getFunctionDef();
			String name = fd.getFunctionname();
			if(name.equals(functionname)) {
				statusComponent.setValue("Rätt namn, ej återanvänd förut");
				return;
			}
			else {
				statusComponent.setValue("Fel namn, ej återanvänd förut");
				return;
			}
		}
		else {
			FunctionDef fd = choice.getFunctionDef();
			String name = fd.getFunctionname();
			if(name.equals(functionname)) {
				statusComponent.setValue("Rätt namn, återanvänd sedan tidigare");
				return;
			}
			else {
				statusComponent.setValue("Fel namn, återanvänd sedan tidigare");
				return;
			}
		}
	}
	
	public FunctionDef getCurrentFunctionDef() {
		FunctionDefListItem out = null;
		try {
			Optional<FunctionDefListItem> op = optionsComponent.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		if(out == null) {
			return null;
		}
		return out.getFunctionDef();
	}
	
	private static class FunctionDefListItem {
		private FunctionDefReuse fdr = null;
		private FunctionDef fd = null;
		private String source;
		public FunctionDefListItem(FunctionDefReuse fdr) {
			this.fdr = fdr;
			this.fd = fdr.getFunctionDef();
		}
		public FunctionDefListItem(FunctionDef fd, String source) {
			this.fd = fd;
			this.source = source;
		}
		public String toString() {
			if(fdr != null) {
				return fd.getFunctionname()+"("+fd.getId()+")"+"/"+fdr.getProcessdef().getProcessname();
			}
			else {
				return fd.getFunctionname()+"("+fd.getId()+")"+"/"+source;				
			}
		}
		public FunctionDefReuse getFunctionDefReuse() {
			return fdr;
		}
		public FunctionDef getFunctionDef() {
			return fd;
		}
	}

}
