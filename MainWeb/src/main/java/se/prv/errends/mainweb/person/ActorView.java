package se.prv.errends.mainweb.person;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Statedef;
import se.prv.errends.mainweb.errend.ErrendInStateView;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.SessionManagement;

public class ActorView extends Panel {
	
	private List<Actor> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Actor> grid = null;
	private Person person = null;
	private Session session = null;

	public ActorView(List<Actor> rows) {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);

		//rows = ActorDB.getAllActiveActorsOfPersonId(person.getId());
		this.rows = rows;

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Errend e = row.getErrend();
        	if(e == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getErrend().getAdminusername());
        	}
        }).setCaption("Benämning"); 
        grid.addComponentColumn(row -> {
        	Errend e = row.getErrend();
        	if(e == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getErrend().getPersonname());
        	}
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	Errend e = row.getErrend();
        	if(e == null) {
        		return new Label("Ärendelösa aktören");
        	}
        	else {
        		return new Label(e.isActive() ? "Ja" : "Nej");
        	}
        }).setCaption("Aktivstatus"); 
        grid.addComponentColumn(row -> {
        	Errend e = row.getErrend();
        	if(e == null) {
        		return new Label("--");
        	}
        	else {
            	Date date = e.getCreated();
            	Label out = new Label(Adminuser.presentDate(date));
            	return out;
        	}
        }).setCaption("Skapat"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getRolename());
        	}
        }).setCaption("Rollnamn"); 
        grid.addComponentColumn(row -> {
        	Statedef sd = null;
        	try {
        		sd = row.getErrend().getManagedErrend().getCurrentStatedef();
        	}
        	catch(Exception e) {}
        	if(sd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(sd.getStatename());
        	}
        }).setCaption("Tillstånd"); 
        grid.addComponentColumn(row -> {
        	Organisation oacc = row.getAgent().getOrganisation();
        	if(oacc == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(oacc.getOrgname());
        	}
        }).setCaption("Ombud för"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getProcessdef().getProcessname());
        	}
        }).setCaption("Ärendetyp"); 
		grid.addComponentColumn(row -> {
			Errend e = row.getErrend();
			if(e != null && person.getId().equals(row.getAgent().getPersonId())) {
				Layout out = new HorizontalLayout();
				Actor a = session.getActor();
				Button toSessionBtn = null;
				if (a.getErrendId() == null) {
					toSessionBtn = new Button("Ny session");
				} else {
					if (row.getId().equals(a.getId())) {
						toSessionBtn = new Button("Återuppta session");
					} else {
						toSessionBtn = new Button("Ny session");
					}
				}
				boolean active = row.getAgent().isActive();
				toSessionBtn.setEnabled(active);
				if(active) {
					toSessionBtn.addClickListener(click -> goToErrend(row));
				}
				out.addComponent(toSessionBtn);
				return out;
			} else {
				return new Label("--");
			}
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		
		setContent(layout);
	}

	private void goToErrend(Actor row) {
		ErrendManagement em = null;
		try {
			em = ErrendManagement.getErrendFromDBStructure(row);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(em.isError()) {
			Notification.show(em.getErrorMessage(), Notification.Type.ERROR_MESSAGE);
			return;
		}

		Session previousSession = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		Session session = SessionManagement.switchAsNecessary(previousSession, row);
		UI.getCurrent().getSession().setAttribute(CommonUI.sessionlookup, session);
		UI.getCurrent().getSession().setAttribute(CommonUI.currenterrend, em);
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new ErrendInStateView());
	}
	
}
