package se.prv.errends.mainweb.adminuser;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.edgeconfig.parse.PrvErrendEdgeConfigParser;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public class VerifyConfigurationSyntax extends Panel {
	
	private TextArea name = null;
	
	public VerifyConfigurationSyntax() {
		setWidth("100%");
		
		FormLayout content = new FormLayout();
		name = new TextArea();
		name.setWidth("100%");
		content.addComponent(name);
		content.setWidth("100%");
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button checkNodeBtn = new Button("Nod");
		checkNodeBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				checkNode();
			}
			
		});
		buttons.addComponent(checkNodeBtn);
		
		Button checkEdgeBtn = new Button("Kant");
		checkEdgeBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				checkEdge();
			}
			
		});
		buttons.addComponent(checkEdgeBtn);
		
		content.addComponent(buttons);
		
		content.setSizeFull();
		//content.setMargin(true);
		setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void checkNode() {
        try {
            InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(name.getValue().getBytes()));
        	NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "Node");
			Notification.show("Rätt", Notification.Type.HUMANIZED_MESSAGE);
        }
        catch(Exception e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
	}

	private void checkEdge() {
        try {
            InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(name.getValue().getBytes()));
        	EdgeConfiguration nc = PrvErrendEdgeConfigParser.parseEdgeConfig(isr, "Node");
			Notification.show("Rätt", Notification.Type.HUMANIZED_MESSAGE);
        }
        catch(Exception e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
	}

	
}
