package se.prv.errends.mainweb.adminuser;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.mainweb.errend.HistoryInErrendView;
import se.prv.errends.mainweb.errend.ParticipantsInErrendView;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class ActiveErrendsView extends Panel {
	
	private List<Errend> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Errend> grid = null;
	private boolean sessionIsOfSuper = false;
	private Adminuser current = null;
	
	public ActiveErrendsView() {
		this.current = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = ErrendDB.getAllErrendsOfActiveState(true);
		try {
			if(current == null) {
				//System.out.println("Superstatus - null "+sessionIsOfSuper);
				sessionIsOfSuper = false;
			}
			sessionIsOfSuper = current.isSuperprivilegies();
		}
		catch(Exception e) {
			//System.out.println("Superstatus - fel "+sessionIsOfSuper);
			sessionIsOfSuper = false;
			e.printStackTrace();
		}
		
		//System.out.println("Superstatus - slut "+sessionIsOfSuper);

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	if(row.getAdminusername() == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getAdminusername());
        	}
        }).setCaption("Benämning"); 
        grid.addComponentColumn(row -> {
        	if(row.getPersonname() == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getPersonname());
        	}
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getManagedErrend().getProcessdef().getProcessname());
        }).setCaption("Ärendetyp"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getManagedErrend().getCurrentStatedef().getStatename());
        }).setCaption("Tillstånd"); 
        grid.addComponentColumn(row -> {
        	Date date = row.getCreated();
        	Label out = new Label(Adminuser.presentDate(date));
        	return out;
        }).setCaption("Skapat"); 
        grid.addComponentColumn(row -> {
        	CheckBox out = new CheckBox();
        	out.setValue(row.isActive());
        	out.addValueChangeListener(event ->  {
        		row.setActive(out.getValue());
        		row.save();
        	});
        	return out;
        }).setCaption("Aktivstatus"); 
		grid.addComponentColumn(row -> {
        	Layout out = new HorizontalLayout();
        	Button participantsBtn = new Button("Deltagare");
        	participantsBtn.addClickListener(click -> seeParticipants(row));
	        out.addComponent(participantsBtn);
        	Button initBtn = new Button("Rolltilldelning");
	        initBtn.addClickListener(click -> goToErrend(row));
	        out.addComponent(initBtn);
        	Button historyBtn = new Button("Historik");
        	historyBtn.addClickListener(click -> errendHistory(row));
	        out.addComponent(historyBtn);
	        if(current.isSuperprivilegies()) {
		        Button deleteBtn = new Button("Radera");
		        deleteBtn.addClickListener(click -> removeErrend(row));
		        out.addComponent(deleteBtn);
	        }
	        return out;
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void goToErrend(Errend row) {
		((AdminuserUI) UI.getCurrent()).setup(new AdminuserSetupErrendRoles(
				row,
				new Runnable() {
					@Override
					public void run() {
						((AdminuserUI) UI.getCurrent()).setWorkContent(ActiveErrendsView.this);
					}
				}
				));
	}
	
	private void seeParticipants(Errend row) {
		((AdminuserUI) UI.getCurrent()).setup(new ParticipantsInErrendView(row, null, null));
	}
	
	private void removeErrend(Errend errend) {
		errend.deleteRecursively();
		rows.remove(errend);
		grid.getDataProvider().refreshAll();		
	}
	
	private void errendHistory(Errend row) {
		((AdminuserUI) UI.getCurrent()).setWorkContent(new HistoryInErrendView(row, null));
	}
	

}
