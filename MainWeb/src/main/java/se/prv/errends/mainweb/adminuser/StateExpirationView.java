package se.prv.errends.mainweb.adminuser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.StateExpirationDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.StateExpiration;

public class StateExpirationView extends Panel {
	
	private List<StateExpiration> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<StateExpiration> grid = null;
	//private Adminuser adminuser = null;

	public StateExpirationView() {
		//adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = StateExpirationDB.getAllStateExpirations();

		grid = new Grid<>();
		
		grid.addComponentColumn(row -> {
			return new Label(row.getManagedErrend().getErrend().getPersonname());
		}).setCaption("Externt namn");
		 
		grid.addComponentColumn(row -> {
			return new Label(row.getManagedErrend().getErrend().getAdminusername());
		}).setCaption("Internt namn");
		 
		grid.addComponentColumn(row -> {
			return new Label(row.getManagedErrend().getProcessdef().getProcessname());
		}).setCaption("Ärendetyp");
		 
		grid.addComponentColumn(row -> {
			return new Label(row.getManagedErrend().getCurrentStatedef().getStatename());
		}).setCaption("Tillstånd");
		 
		grid.addComponentColumn(row -> {
			return new Label(Adminuser.presentDate(row.getExpirey()));
		}).setCaption("Frist");
		 
		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button deleteBtn = new Button("Radera");
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      return out;
		}).setCaption("Hantering");
		
		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void delete(StateExpiration pf) {
		pf.delete();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
	}
		
}
