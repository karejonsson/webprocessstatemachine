package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Link;

import se.prv.errends.domain.Processdef;

public class ExportedProcessdefLink extends Link {
	
	private Processdef pd;

	public ExportedProcessdefLink(Processdef pd) {
		super();
		this.pd = pd;
		setCaption("graphml");
		setDescription("Exportera flödet som yEd/graphml-diagram. Hämta filen till din dator");
		setTargetName("_blank");
	}

	@Override
	public void attach() {
		super.attach(); // Must call.

		StreamResource.StreamSource source = new StreamResource.StreamSource() {
			public InputStream getStream() {
				try {
					return new ByteArrayInputStream(EmbeddedExportCreator.exportToGraphml(pd));
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		StreamResource resource = new StreamResource (source, "exported_"+pd.getProcessfile().getFilename());
		resource.setMIMEType("application/xml");
		resource.setCacheTime(0);
		setResource(resource);
	}
	
}