package se.prv.errends.mainweb.person;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.PersonDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class PersonView extends Panel {
	
	private List<Person> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Person> grid = null;
	private Adminuser adminuser = null;

	public PersonView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = PersonDB.getAllActivePersons();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox();
            box.setValue(row.isActive());
            box.setEnabled(false);
            return box;
        }).setCaption("Aktivstatus"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getFirstname());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getFamilyname());
        }).setCaption("Efternamn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getEmail());
        }).setCaption("E-post"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getSwedishpnr());
        }).setCaption("Personnummer"); 
        if(adminuser.isSuperprivilegies()) {
	        grid.addComponentColumn(row -> {
				  Layout out = new HorizontalLayout();
			      Button removeBtn = new Button("Radera");
			      removeBtn.setEnabled(true);
			      removeBtn.addClickListener(click -> removePerson(row));
			      out.addComponent(removeBtn);
			      return out;
	        }).setCaption("Hantering"); 
        }

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button searchBtn = new Button("Sök");
		searchBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				search();
			}

		});
		buttons.addComponent(searchBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void search() {
		SearchPersonForm spf = new SearchPersonForm(
				/*
				new SearchPersonForm.NotifyAboutPick() {
					@Override
					public void picked(Person p) {
						System.out.println("PersonView valdes "+p);
					}
				}*/
				null,
				new Runnable() {
					@Override
					public void run() {
						((AdminuserUI) UI.getCurrent()).setup(PersonView.this);
					}
				});
		((AdminuserUI) UI.getCurrent()).openSubwindow(spf);
	}
	
	public void removePerson(Person person) {
		person.deleteRecursively();
		rows.remove(person);
		grid.getDataProvider().refreshAll();
	}

}
