package se.prv.errends.mainweb.adminuser;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.OrganizationUser;

public class OrganizationUserChangeCredentialsForm extends Window {
	
	private Runnable onClose = null;
	
	private PasswordField oldPassword = new PasswordField("Old password");
	private PasswordField newPassword = new PasswordField("New password");
	private PasswordField repeatPassword = new PasswordField("Repeat password");
	private OrganizationUserView ouv = null;
	private OrganizationUser ou = null;
	
	public OrganizationUserChangeCredentialsForm(OrganizationUserView ouv, OrganizationUser ou, Runnable onClose) {
		this.ouv = ouv;
		this.ou = ou;
		this.onClose = onClose;
		Panel panel = new Panel("Login");
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		content.addComponent(oldPassword);
		content.addComponent(newPassword);
		content.addComponent(repeatPassword);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		Button upload = new Button("Uppdatera");
		upload.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				updateCredentials(event.getSource());
			}
			
		});
		buttons.addComponent(upload);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void updateCredentials(Object obj) {
		//System.out.println("1 befintligt "+au.getPassword());
		//System.out.println("AdministratorChangeCredentialsForm.updateCredentials "+obj.getClass().getName());
		try {
			String oldPwd = Adminuser.calculateChecksum(oldPassword.getValue().trim());
			//System.out.println("2 gamla från dialogen "+oldPwd);
			if(!oldPwd.equals(ou.getPassword())) {
				Notification.show("Felaktigt lösenord", Type.HUMANIZED_MESSAGE);
				return;
			}
		}
		catch(Exception e) {
			Notification.show("Kunde inte verifiera befintligt lösenord", Type.ERROR_MESSAGE);
			return;
		}
		String newPwd = null;
		try {
			newPwd = Adminuser.calculateChecksum(newPassword.getValue().trim());
			//System.out.println("3 nytt pwd "+newPwd+", rått "+newPassword.getValue().trim());
			String repeatPwd = Adminuser.calculateChecksum(repeatPassword.getValue().trim());
			//System.out.println("4 nytt repeterat pwd "+repeatPwd+", rått "+repeatPassword.getValue().trim());
			if(!newPwd.equals(repeatPwd)) {
				Notification.show("Nytt lösenord ej korrekt repeterat", Type.ERROR_MESSAGE);
				return;
			}
		}
		catch(Exception e) {
			Notification.show("Kunde inte verifiera befintligt lösenord", Type.ERROR_MESSAGE);
			return;
		}
		try {
			ou.setPassword(newPwd);
			ou.save();
		} 
		catch (Exception e) {
			Notification.show("Kunde inte byta till det nya lösenordet", Type.ERROR_MESSAGE);
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
