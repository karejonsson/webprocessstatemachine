package se.prv.errends.mainweb.adminuser;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.dbc.AgentDB;
import se.prv.errends.dbc.FunctionDefDB;
import se.prv.errends.dbc.ProcessdefDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.adminuser.AskForAgentAndNameForm.NotifyAboutAgentAndName;
import se.prv.errends.mainweb.adminuser.processeditor.ExportedProcessdefLink;
import se.prv.errends.mainweb.adminuser.processeditor.FunctionFormEdit;
import se.prv.errends.mainweb.adminuser.processeditor.ProcessEditorMain;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.AgentManagement;
import se.prv.errends.mgmt.ErrendManagement;

public class FunctionDefView extends Panel {
	
	private List<FunctionDef> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<FunctionDef> grid = null;
	
	private Adminuser adminuser = null;
	private Person person = null;
	private Organisation organization = null;
	private OrganizationUser orguser = null;

	public FunctionDefView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		orguser = (OrganizationUser) UI.getCurrent().getSession().getAttribute(CommonUI.orguserlookup);
		organization = orguser != null ? orguser.getOrganization() : null; 
		
		rows = FunctionDefDB.getAllFunctionDefs();

		grid = new Grid<>();
        grid.addColumn(FunctionDef::getFunctionname).setCaption("Namn").setEditorComponent(new TextField(), FunctionDef::setFunctionname).setEditable(adminuser != null);
		grid.addComponentColumn(row -> {
		      return new Label(""+row.getId());
		}).setCaption("Id");
        grid.addColumn(FunctionDef::getTypename).setCaption("Typ").setEditorComponent(new TextField(), FunctionDef::setFunctionname).setEditable(false);
		grid.getEditor().setEnabled(true);

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      if(adminuser != null) {
			      Button deleteBtn = new Button("Radera");
			      deleteBtn.addClickListener(click -> delete(row));
			      out.addComponent(deleteBtn);
			      if(adminuser.isSuperprivilegies()) {
				      Button deleteRecursivelyBtn = new Button("Radera rekursivt");
				      deleteRecursivelyBtn.addClickListener(click -> deleteRecursively(row));
				      out.addComponent(deleteRecursivelyBtn);
			      }
			      Button editErrendBtn = new Button("Redigera");
			      editErrendBtn.addClickListener(click -> editFunctionDef(row));
			      out.addComponent(editErrendBtn);
		      }
		      return out;
		}).setCaption("Hantering");
		
		grid.getEditor().setEnabled(adminuser != null);
		
		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		if(adminuser != null) {
			HorizontalLayout buttons = new HorizontalLayout();
			Button saveBtn = new Button("Spara");
			saveBtn.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					save();
				}
			});		
			buttons.addComponent(saveBtn);		
			Button newProcessdefBtn = new Button("Ny");
			newProcessdefBtn.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					newFunctionDef();
				}
			});		
			buttons.addComponent(newProcessdefBtn);		
			layout.addComponent(buttons);
		}
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void delete(FunctionDef pf) {
		pf.deleteRecursive();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
		//System.out.println("delete "+pf);
	}
	
	private void deleteRecursively(FunctionDef pf) {
		pf.deleteRecursive();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
	}
	
	public void save() {
		for(FunctionDef pf : rows) {
			pf.save();
		}
	}
	
	public void notifyAboutUpdatedFunctionDef(FunctionDef pf) {
		if(pf != null) {
			if(!rows.contains(pf)) {
				rows.add(pf);
			}
		}
		grid.getDataProvider().refreshAll();
	}
	
	private ProcessdefFormNew npff = null;
	
	public void newFunctionDef() {
		editFunctionDef(new FunctionDef());
	}
	
	public void editFunctionDef(FunctionDef fd) {
		String name = fd.getFunctionname();
		((AdminuserUI) UI.getCurrent()).openSubwindow(new FunctionFormEdit(fd, name, new Runnable() {
			@Override
			public void run() {
				grid.getDataProvider().refreshAll();
				FunctionDefView.this.notifyAboutUpdatedFunctionDef(fd);
			}
		}));
	}
	
}
