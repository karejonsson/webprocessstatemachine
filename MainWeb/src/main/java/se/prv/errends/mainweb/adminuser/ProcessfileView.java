package se.prv.errends.mainweb.adminuser;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import se.prv.errends.dbc.ProcessfileDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Processfile;
import se.prv.errends.dossierconfig.parse.PrvDossierConfigParser;
import se.prv.errends.dossierconfig.struct.DossierConfiguration;
import se.prv.errends.initialize.DBInitialisation;
import se.prv.errends.initialize.InitializationReporter;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.CreateExhaustiveInstances;
import se.prv.errends.producerconfig.struct.GroovyBooleanEvaluable;
import se.prv.errends.producerconfig.struct.GroovyStringEvaluable;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class ProcessfileView extends Panel {
	
	private List<Processfile> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Processfile> grid = null;
	private Adminuser adminuser = null;

	public ProcessfileView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = ProcessfileDB.getAllProcessfiles();

		grid = new Grid<>();

        grid.addColumn(Processfile::getDescription).setCaption("Beskrivning").setEditorComponent(new TextField(), Processfile::setDescription);
        grid.addColumn(Processfile::getFilename).setCaption("Filnamn").setEditorComponent(new TextField(), Processfile::setFilename);
		grid.getEditor().setEnabled(true);

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button deleteBtn = new Button("Radera");
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      if(adminuser.isSuperprivilegies()) {
			      Button deleteRecursivelyBtn = new Button("Radera rekursivt");
			      deleteRecursivelyBtn.addClickListener(click -> deleteRecursively(row));
			      out.addComponent(deleteRecursivelyBtn);
		      }
		      Button control = new Button("Kontroll");
		      control.addClickListener(click -> control(row));
		      out.addComponent(control);
		      Button create = new Button("Skapa");
		      create.addClickListener(click -> create(row));
		      out.addComponent(create);
		      Button generateFunctionsBTN = new Button("Generera funktioner");
		      generateFunctionsBTN.addClickListener(click -> generateFunctions(row));
		      out.addComponent(generateFunctionsBTN);
		      return out;
		}).setCaption("Hantering");
		
		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		
		buttons.addComponent(saveBtn);
		
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newProcessfile();
			}
		});
		
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void delete(Processfile pf) {
		if(pf.getReferringProcessdefs().size() != 0) {
			Notification.show("Kan ej radera pga tillhörande processdefinition", Notification.Type.ERROR_MESSAGE);
			return;
		}
		pf.delete();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
		//System.out.println("delete "+pf);
	}
	
	private void deleteRecursively(Processfile pf) {
		pf.deleteRecursively();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
	}
	
	public void save() {
		for(Processfile pf : rows) {
			pf.save();
		}
	}
	
	public void newProcessfile() {
		ProcessfileFormNew npff = new ProcessfileFormNew(this, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ProcessfileView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	public void notifyAboutNewProcessfile(Processfile pf) {
		rows.add(pf);
		//System.out.println("ProcessFileView.notifyAboutNewProcessfile : ID = "+pf.getId());
		grid.getDataProvider().refreshAll();
	}
	
	private void control(Processfile pf) {
		String filename = pf.getFilename();
		if(filename.endsWith(".graphml")) {
			controlProcessdef(pf);
			return;
		}
		if(filename.contains(".prod") || filename.contains(".func")) {
			controlFunctions(pf);
			return;
		}
		if(filename.contains(".doss")) {
			controlDossier(pf);
			return;
		}
		Notification.show("Kontroll misslyckades.\nKänner inte till filformatet utifrån namnet", Notification.Type.ERROR_MESSAGE);
	}

	private void controlProcessdef(Processfile pf) {
		//System.out.println("ProcessFileView.control : ID = "+pf.getId());
		try {
			String report = DBInitialisation.getInitiationReport(pf, pf.getDescription(), pf.getFilename());
			//System.out.println("ProcessFileView.control : report = "+report);
			((AdminuserUI) UI.getCurrent()).openSubwindow(new ShowLongText("Analys av filen "+pf.getFilename(), report));
		} catch (Exception e) {
			Notification.show("Kontroll misslyckades", Notification.Type.ERROR_MESSAGE);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void controlFunctions(Processfile pf) {
		try {
			byte[] data = pf.getData();
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			InputStreamReader isr = new InputStreamReader(bais);
	    	List<ProducerConfiguration> producers = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Processfile "+pf.getId());
			Notification.show("OK! "+producers.size()+" funktioner", Notification.Type.HUMANIZED_MESSAGE);
		} catch (Exception e) {
			Notification.show("Kontroll fallerade", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void controlDossier(Processfile pf) {
		try {
			byte[] data = pf.getData();
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			InputStreamReader isr = new InputStreamReader(bais);
	    	DossierConfiguration dossierConfig = PrvDossierConfigParser.parseDossierConfig(isr, "Processfile "+pf.getId());
			Notification.show("OK! "+dossierConfig.getTypes().size()+" typer, "+dossierConfig.getAttributes().size()+" attribut", Notification.Type.HUMANIZED_MESSAGE);
		} catch (Exception e) {
			Notification.show("Kontroll fallerade", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void create(Processfile pf) {
		String filename = pf.getFilename();
		if(filename.endsWith(".graphml")) {
			createProcessdef(pf);
			return;
		}
		if(filename.contains(".prod") || filename.contains(".func")) {
			createFunctions(pf);
			return;
		}
		if(filename.contains(".doss")) {
			createDossier(pf);
			return;
		}
		Notification.show("Skapande misslyckades.\nKänner inte till filformatet utifrån namnet", Notification.Type.ERROR_MESSAGE);
	}
	
	private void generateFunctions(Processfile pf) {
		String filename = pf.getFilename();
		if(filename.endsWith(".graphml")) {
			generateFunctionsFromApprovedPostfix(pf);
			return;
		}
		if(filename.contains(".prod") || filename.contains(".func")) {
			Notification.show("Funktionsgenerering misslyckades.\nGenererar bara utifrån graphml-filer.", Notification.Type.ERROR_MESSAGE);
			return;
		}
		Notification.show("Funktionsgenerering misslyckades.\nKänner inte till filformatet utifrån namnet", Notification.Type.ERROR_MESSAGE);
	}
	
	private void generateFunctionsFromApprovedPostfix(Processfile pf) {
		try {
	    	InitializationReporter ir = new InitializationReporter();
	    	DBInitialisation.inititializeProcessNodes(pf, "Some description", "Some process name", ir);

	    	List<String> booleanFs = ir.getBooleanFunctions();
	    	List<String> stringFs = ir.getStringFunctions();
	    	
	    	StringBuffer gen = new StringBuffer();

	    	//System.out.println("Boolean functions ("+booleanFs.size()+")");
	    	for(String bf : booleanFs) {
	    		ProducerConfiguration pc = new ProducerConfiguration();
	    		pc.setName(bf);
	    		GroovyBooleanEvaluable gbe = new GroovyBooleanEvaluable("return true;");
	    		pc.setGroovyEvaluable(gbe);
	    		gen.append(pc.toStringReproduce()+"\n");
	    	}
	    	
	    	//System.out.println("String functions ("+stringFs.size()+")");
	    	for(String sf : stringFs) {
	    		ProducerConfiguration pc = new ProducerConfiguration();
	    		pc.setName(sf);
	    		GroovyStringEvaluable gse = new GroovyStringEvaluable("return 'x';");
	    		pc.setGroovyEvaluable(gse);
	    		gen.append(pc.toStringReproduce()+"\n");
	    	}
	    	
	    	gen.append(CreateExhaustiveInstances.getAllInComments());

			ShowLongText slt = new ShowLongText("Genererade funktioner till "+pf.getFilename(), gen.toString());
			slt.setEnabledText(true);

			((AdminuserUI) UI.getCurrent()).openSubwindow(slt);
		} catch (Exception e) {
			Notification.show("Kontroll fallerade", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void createProcessdef(Processfile pf) {		
		ProcessdefFormNew npff = new ProcessdefFormNew(pf, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ProcessfileView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	private void createFunctions(Processfile pf) {		
		FunctionsFileimportForm ffn = new FunctionsFileimportForm(pf, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ProcessfileView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(ffn);
	}
	
	private void createDossier(Processfile pf) {	
		DossierConfigurationImportForm ffn = new DossierConfigurationImportForm(pf, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ProcessfileView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(ffn);
	}
	
	
}
