package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ErrendCommentDB;
import se.prv.errends.dbc.ReportDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.ErrendComment;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.nodeconfig.struct.CommentView;
import se.prv.errends.nodeconfig.struct.ReportView;

public class CommentsInErrendView extends Panel {
	
	private List<ErrendComment> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<ErrendComment> grid = null;

	private Errend errend = null;
	private Session session = null; 
	private Actor actor = null; 
	private ErrendInStateView eisv = null;
	private CommentView commentView = null;
	
	public CommentsInErrendView(Errend errend, ErrendInStateView eisv, CommentView  commentView) {
		this.errend = errend;
		this.eisv = eisv;
		this.commentView = commentView;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		actor = session.getActor();
		
		if(seeall()) {
			rows = ErrendCommentDB.getAllErrendCommentsOfErrend(errend.getId());
		}
		else {
			rows = ErrendCommentDB.getAllErrendCommentsOfErrendAndActor(errend.getId(), actor.getId());
		}

		grid = new Grid<>();

		grid.addComponentColumn(row -> {
			return new Label(row.getTitle());
		}).setCaption("Titel").setExpandRatio(1);

		grid.addComponentColumn(row -> {
			Actor actor = row.getSession().getActor();
			Agent agent = actor.getAgent();
			Person p = agent.getPerson();
			String pr = agent.getOrganisation().getOrgname()+" / "+p.getFirstname()+" "+p.getFamilyname();
			return new Label(actor.getRoledef().getRolename()+" : "+pr);
		}).setCaption("Av").setExpandRatio(1);

		grid.addComponentColumn(row -> {
			Layout out = new HorizontalLayout();
			Button editBtn = null;
			if(actor.getAgent().getPersonId().equals(row.getSession().getActor().getAgent().getPersonId())) {
				editBtn = new Button("Redigera");
			}
			else {
				editBtn = new Button("Läs");
			}
			editBtn.addClickListener(click -> editErrendComment(row));
			out.addComponent(editBtn);
			Button deleteBtn = new Button("Radera");
			deleteBtn.addClickListener(click -> deleteErrendComment(row));
			out.addComponent(deleteBtn);
			return out;
		}).setCaption("Hantering").setExpandRatio(1);
		
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newErrendComment();
			}
		});
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private boolean seeall() {
		return evalBooleanProperty("seeall", true);
	}
	
	private boolean evalBooleanProperty(String key, boolean defaultResponse) {
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: "+key+", default "+defaultResponse);
		if(!commentView.hasKey(key)) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 1 "+defaultResponse);
			return defaultResponse;
		}
		String value = commentView.getValue(key);
		if(value == null) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 2 "+defaultResponse);
			return defaultResponse;
		}
		if(value.toLowerCase().contains("n")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 3 false");
			return false;
		}
		if(value.toLowerCase().contains("y") || value.toLowerCase().contains("j")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 true");
			return true;
		}
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 "+defaultResponse);
		return defaultResponse;
	}

	public Errend getErrend() {
		return errend;
	}
	
	private Object editErrendComment(ErrendComment row) { 
		try {
			((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new ErrendCommentFormEdit(this, row, actor, new Runnable() {
				@Override
				public void run() {
					((SimplifiedContentUI) UI.getCurrent()).setWorkContent(eisv);
				}
			}));
		} catch (Exception e) {
			Notification.show("Redigering misslyckades", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return null;
	}
	
	private void deleteErrendComment(ErrendComment row) {
		rows.remove(row);
		row.delete();
		grid.getDataProvider().refreshAll();
	}
	
	private void newErrendComment() {
		ErrendComment ec = new ErrendComment();
		ec.setErrend(errend);
		ec.setSession(session);
		ec.setTitle("Titel ej inskriven");
		ec.save();
		rows.add(ec);
		grid.getDataProvider().refreshAll();
	}

	public void notifyAboutChangedErrendComment() {
		grid.getDataProvider().refreshAll();
	}

}
