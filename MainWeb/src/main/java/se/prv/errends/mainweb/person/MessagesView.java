package se.prv.errends.mainweb.person;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.MessageDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Message;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.CommonUI;

public class MessagesView extends Panel {
	
	private List<Message> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Message> grid = null;
	private Session session = null;
	private Person person = null;

	public MessagesView() {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		rows = MessageDB.getAllMessagesOfPerson(person.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Actor actor = row.getSourceActor();
        	if(actor == null) {
        		return new Label("--");
        	}
        	Agent agent = actor.getAgent();
        	Person p = agent.getPerson();
        	Organisation orgaccount = agent.getOrganisation();
        	String orgpart = "";
        	if(orgaccount != null) {
        		orgpart = orgaccount.getOrgname()+" / ";
        	}
        	String rolepart = "";
        	Roledef roledef = actor.getRoledef();
        	if(roledef != null) {
        		rolepart = roledef+" / ";
        	}
        	Label out = new Label(orgpart+rolepart+p.getFirstname()+" "+p.getFamilyname());
        	return out;
        }).setCaption("Från"); 
        grid.addComponentColumn(row -> {
        	Actor actor = row.getTargetActor();
        	if(actor == null) {
        		return new Label("--");
        	}
        	Agent agent = actor.getAgent();
        	Person p = agent.getPerson();
        	Organisation orgaccount = agent.getOrganisation();
        	String orgpart = "";
        	if(orgaccount != null) {
        		orgpart = orgaccount.getOrgname()+" / ";
        	}
        	String rolepart = "";
        	Roledef roledef = actor.getRoledef();
        	if(roledef != null) {
        		rolepart = roledef+" / ";
        	}
        	Label out = new Label(orgpart+rolepart+p.getFirstname()+" "+p.getFamilyname());
        	return out;
        }).setCaption("Till"); 
        grid.addComponentColumn(row -> {
        	try {
				return new Label(Adminuser.presentDate(row.getSend()));
			} catch (Exception e) {
				e.printStackTrace();
				return new Label(""+row.getSend());
			}
        }).setCaption("Klockslag"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getTitle());
        }).setCaption("Ämne"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.isUnread() ? "Nej" : "Ja");
        }).setCaption("Läst"); 
		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button readBtn = new Button("Läs");
		      readBtn.setEnabled(person != null);
		      readBtn.addClickListener(click -> read(row));
		      out.addComponent(readBtn);
		      Button deleteBtn = new Button("Radera");
		      deleteBtn.setEnabled(person != null);
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      return out;
		}).setCaption("Hantering");
        
		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button writeMessageBtn = new Button("Nytt meddelande");
		writeMessageBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				writeMessage();
			}
		});
		buttons.addComponent(writeMessageBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
		
		int ctrUnread = 0;
		for(Message m : rows) {
			if(m.getTargetActor().getAgent().getPersonId().equals(person.getId())) {
				if(m.isUnread()) {
					ctrUnread++;
				}
			}
		}
		System.out.println("Antal olästa "+ctrUnread);
		if(ctrUnread != 0) {
			Notification.show("Du har "+ctrUnread+" oläst"+(ctrUnread != 1 ? "a" : "")+" meddelande"+(ctrUnread != 1 ? "n" : ""), Type.HUMANIZED_MESSAGE);
		}
	}

	private Object read(Message row) {
		if(row.getTargetActor().getAgent().getPersonId().equals(person.getId())) {
			if(row.isUnread()) {
				row.setUnread(false);
				row.save();
			}
		}

		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new ShowLongText(row.getTitle(), row.getWrittenmessage()));
		grid.getDataProvider().refreshAll();
		return null;
	}
	
	private Object delete(Message row) {
		if(row.getSourceActor() != null && row.getSourceActor().getAgent().getPersonId().equals(person.getId())) {
			row.setSourceActorId(null);
			row.save();
			rows.remove(row);
			grid.getDataProvider().refreshAll();
			return null;
		}
		if(row.getTargetActor() != null && row.getTargetActor().getAgent().getPersonId().equals(person.getId())) {
			row.setTargetActorId(null);
			row.save();
			rows.remove(row);
			grid.getDataProvider().refreshAll();
			return null;
		}
		return null;
	}
	
	public interface NotifyAboutMessage {
		public void sent(Message m);
	}
	
	private void writeMessage() {
		MessageFormWrite mfw = new MessageFormWrite(
				session.getActor(), 
				null, 
				new Runnable() {
			@Override
			public void run() {
				grid.getDataProvider().refreshAll();
				((SimplifiedContentUI) UI.getCurrent()).setup(MessagesView.this);
			}
		},
				new NotifyAboutMessage() {
					@Override
					public void sent(Message m) {
						rows.add(m);
						grid.getDataProvider().refreshAll();
					}
		});
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(mfw);
	}
	
}
