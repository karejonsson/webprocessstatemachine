package se.prv.errends.mainweb.adminuser;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Processfile;
import se.prv.errends.initialize.DBInitialisation;

public class ProcessdefFormNew extends Window {
	
	private Runnable onClose = null;
	
	private TextArea description = null;
	private TextField errendtypename = null;
	private Processfile pf = null;
	private Processdef pd = null;
	
	public ProcessdefFormNew(Processfile pf, Runnable onClose) {
		this.pf = pf;
		this.onClose = onClose;
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		errendtypename = new TextField("Ärendetyp");
		errendtypename.setValue(pf != null ? pf.getFilename() : "");
		content.addComponent(errendtypename);

		description = new TextArea("Beskrivning");
		description.setValue(pf != null ? pf.getDescription() : "");
		content.addComponent(description);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		if(pf != null) {
			Button control = new Button("Kontroll");
			control.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void buttonClick(ClickEvent event) {
					control();
				}
				
			});
			buttons.addComponent(control);
		}

		Button create = new Button("Skapa");
		create.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				create();
			}
			
		});
		buttons.addComponent(create);

		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void control() {
		try {
			String report = DBInitialisation.getInitiationReport(pf, pf.getDescription(), pf.getFilename());
			Notification.show(report, Notification.Type.HUMANIZED_MESSAGE);
		} catch (Exception e) {
			Notification.show("Kontroll fallerade", Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void create() {
		if(pf != null) {
			try {
				pd = DBInitialisation.initializeProcessNodes(pf, description.getValue(), errendtypename.getValue());
				Notification.show("Skapad!", Notification.Type.HUMANIZED_MESSAGE);
			} catch (Exception e) {
				Notification.show("Skapande fallerade", Notification.Type.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		else {
			pd = new Processdef();
			pd.setDescription(description.getValue());
			pd.setProcessname(errendtypename.getValue());
			pd.setProcessfileId(null);
			pd.save();
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	public Processdef getProcessdef() {
		return pd;
	}

}
