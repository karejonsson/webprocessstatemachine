package se.prv.errends.mainweb.reuse;

import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ShowLongText extends Window {
	
	private TextArea ta = null;

    public ShowLongText(String frame, String textToShow) {
        super(frame);
        setModal(true); 

        ta = new TextArea();
    	ta.setValue(textToShow);
    	ta.setSizeFull();
    	ta.setHeight(90, Unit.PERCENTAGE);
    	ta.setWidth(100, Unit.PERCENTAGE);
    	ta.setEnabled(false);
    	
    	VerticalLayout layout = new VerticalLayout();
        layout.addComponent(ta);
        layout.setSizeFull();
        setSizeFull();
        setContent(layout);
    }
    
    public void setEnabledText(boolean enabledState) {
    	ta.setEnabled(enabledState);
    }
    
}
