package se.prv.errends.mainweb.errend;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Actor;
import se.prv.errends.domain.ErrendComment;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;

public class ErrendCommentFormEdit extends Window {
	
	private Runnable onClose = null;
	private CommentsInErrendView ciev = null;
	private ErrendComment errendComment = null;
	private Session session = null;
	private Actor actor = null;
	private TextField title = null;
	private TextArea comment = null;
	private boolean editable = true;

	public ErrendCommentFormEdit(CommentsInErrendView ciev, ErrendComment errendComment, Actor actor, Runnable onClose) {
		this.ciev = ciev;
		this.onClose = onClose;
		this.errendComment = errendComment;
		this.actor = actor;
		editable = actor.getAgent().getPersonId() == errendComment.getSession().getActor().getAgent().getPersonId();
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		setModal(true);
		setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");

		FormLayout content = new FormLayout();
		content.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		
		title = new TextField("Titel");
		title.setValue(errendComment.getTitle() != null ? errendComment.getTitle() : "");
		title.setEnabled(editable);
		title.setWidth("100%");
		content.addComponent(title);
		
		comment = new TextArea("Kommentar");
		comment.setValue(errendComment.getComment() != null ? errendComment.getComment() : "");
		comment.setWidth("100%");
		comment.setEnabled(editable);
		content.addComponent(comment);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Åter");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancelBtn);
		
		Button saveBtn = new Button("Spara");
		saveBtn.setEnabled(editable);
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				save();
			} 
			
		});
		buttons.addComponent(saveBtn);

		content.addComponent(buttons);
		setContent(content);
	}
	
	protected void save() {
		errendComment.setTitle(title.getValue());
		errendComment.setComment(comment.getValue());
		errendComment.save();
		Notification.show("Kommentaren sparades", Notification.Type.HUMANIZED_MESSAGE);
	}

	protected void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
