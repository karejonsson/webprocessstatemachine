package se.prv.errends.mainweb.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;

import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.domain.Dossier;
import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierEvent;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;

public class DossierFormEdit extends Window {
	
	public static final String noDossiertypesMessage = "Akt kan ej skapas då inga akttyper finns i systemet";
	
	private Runnable onClose = null;
	private Dossier dossier = null;
	private TextField name = null;
	private TextField description = null;
	private ComboBox<DossierDefListItem> optionsComponent = null;
	private Session session;
	
	public DossierFormEdit(Dossier dossier, Runnable onClose) {
		this.onClose = onClose;
		this.dossier = dossier;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		name = new TextField("Namn");
		name.setValue(dossier.getName() != null ? dossier.getName() : "");
		content.addComponent(name);
		
		description = new TextField("Beskrivning");
		description.setValue(dossier.getDescription() != null ? dossier.getDescription() : "");
		content.addComponent(description);
		
		optionsComponent = new ComboBox<DossierDefListItem>();
		List<DossierDef> items = DossierDefDB.getAllDossierDefs();
		if(items.size() > 0) {
			List<DossierDefListItem> comboBoxItems = new ArrayList<DossierDefListItem>();
			items.forEach(e -> comboBoxItems.add(new DossierDefListItem(e)));
			optionsComponent.setItems(comboBoxItems);
			optionsComponent.setSelectedItem(comboBoxItems.get(0));
			optionsComponent.setEmptySelectionAllowed(false);
		}
		else {
			optionsComponent.setDescription(noDossiertypesMessage);
		}
		content.addComponent(optionsComponent);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		content.addComponent(buttons);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		if(items.size() == 0) {
			Notification.show(noDossiertypesMessage, Type.ERROR_MESSAGE);
		}
	}
	
	private static class DossierDefListItem {
		private DossierDef dd;
		public DossierDefListItem(DossierDef dd) {
			this.dd = dd;
		}
		public DossierDef getDossierDef() {
			return dd;
		}
		public String toString() {
			return dd.getDescription();
		}
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		boolean newDossier = dossier.getId() == null;
		if(newDossier) {
			dossier.setName(name.getValue());
			dossier.setDescription(description.getValue());
			if(dossier.getSessionId() == null) {
				dossier.setSessionId(session.getId());
			}
			
			Optional<DossierDefListItem> op = optionsComponent.getSelectedItem();
			DossierDefListItem choice = op.get();
			dossier.setDossierDefId(choice.getDossierDef().getId());
			
			String idChange = "Aktens ID före "+dossier.getId();
			dossier.save();
			idChange += ", efter "+dossier.getId();
			DossierEvent dev = new DossierEvent();
			dev.setCodeEnum(DossierEvent.DossierEventType.CREATE_GUI);
			dev.setDossier(dossier);
			dev.setSessionId(session.getId());
			dev.setDescription("Ingen tidigare version");
			dev.setSysteminfo(idChange);
			dev.save();
		}
		else {
			StringBuffer sb = new StringBuffer();
			sb.append("Nytt namn "+name.getValue()+", tidigare "+dossier.getName()+"\n");
			dossier.setName(name.getValue());
			sb.append("Ny beskrivning "+description.getValue()+", tidigare "+dossier.getDescription()+"\n");
			dossier.setDescription(description.getValue());
			if(dossier.getSessionId() == null) {
				dossier.setSessionId(session.getId());
			}
			
			Optional<DossierDefListItem> op = optionsComponent.getSelectedItem();
			DossierDefListItem choice = op.get();
			DossierDef dd = choice.getDossierDef();
			Long dossierDefIdBefore = dd.getId();
			dossier.setDossierDefId(choice.getDossierDef().getId());
			Long dossierDefIdAfter = dossier.getDossierDefId();
					
			String idChange = "Aktens ID före "+dossier.getId();
			dossier.save();
			idChange += ", efter "+dossier.getId()+". Aktdef före "+dossierDefIdBefore+", och efter "+dossierDefIdAfter;
			DossierEvent dev = new DossierEvent();
			dev.setCodeEnum(DossierEvent.DossierEventType.METADATACHANGE);
			dev.setDossier(dossier);
			dev.setSessionId(session.getId());
			dev.setDescription(sb.toString());
			dev.setSysteminfo(idChange);
			dev.save();
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
