package se.prv.errends.mainweb.orguser;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification.Type;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.OrganisationRoleGroup;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.person.ChoseRoledefForm;
import se.prv.errends.mainweb.person.SearchPersonForm;
import se.prv.errends.mainweb.ui.CommonUI;

public class OrganizationsGroupView extends Panel {
	
	private List<OrganisationRoleGroup> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<OrganisationRoleGroup> grid = null;
	private Organisation organization = null;
	private Session session = null;

	public OrganizationsGroupView(Organisation organization, Runnable onClose) {
		this.organization = organization;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);

		rows = organization.getReferringOrganisationRoleGroups();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getName());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getRoledef().getRolename());
        }).setCaption("Rollnamn i typ"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getRoledef().getProcessdef().getProcessname());
        }).setCaption("Processnamn"); 
		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
			  {
			      Button deleteBtn = new Button("Radera");
			      deleteBtn.addClickListener(click -> delete(row));
			      out.addComponent(deleteBtn);
			  }
			  {
			      Button groupsBtn = new Button("Ombud");
			      groupsBtn.addClickListener(click -> manageAgents(row));
			      out.addComponent(groupsBtn);
			  }
		      return out;
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		{
			Button newAgentBtn = new Button("Nytt");
			newAgentBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
				@Override
				public void buttonClick(ClickEvent event) {
					newOrganisationRoleGroup();
				}
			});
			buttons.addComponent(newAgentBtn);
		}

		{
			if(onClose != null) {
				Button doneBtn = new Button("Klar");
				doneBtn.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;
					@Override
					public void buttonClick(ClickEvent event) {
						onClose.run();
					}
				});
				buttons.addComponent(doneBtn);
			}
		}

		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void manageAgents(OrganisationRoleGroup row) {
		ManageAgentsInGroup ofe = new ManageAgentsInGroup(row, new Runnable() {
			@Override
			public void run() {
				((SimplifiedContentUI) UI.getCurrent()).setWorkContent(OrganizationsGroupView.this);
			}
		});
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(ofe);
	}

	private void newOrganisationRoleGroup() {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(
				new ChoseRoledefForm(new ChoseRoledefForm.NotifyAboutRoledefPick() {
					@Override
					public void picked(Roledef roledef, String name) {
						OrganisationRoleGroup org = new OrganisationRoleGroup();
						org.setOrganisation(organization);
						org.setRoledef(roledef); 
						org.setName(name);
						org.save();
						rows.add(org);
						grid.getDataProvider().refreshAll();
					}
				}, null));
	}

	private void delete(OrganisationRoleGroup org) {
		try {
			org.deleteRecursively();
			rows.remove(org);
			grid.getDataProvider().refreshAll();
		}
		catch(Exception e) {
			Notification.show("Aktören kan ej raderas pga befintliga engagemang", Type.ERROR_MESSAGE);
			return;
		}
	}
	
}
