package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;

import se.prv.errends.domain.Dossier;
import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.edgeconfig.struct.EdgeConfiguration;
import se.prv.errends.evaluation.GeneralProducerVisitor;
import se.prv.errends.mainweb.person.DossierView;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.ProducerConfigurationManagement;
import se.prv.errends.mgmt.TransitiondefManaged;
import se.prv.errends.nodeconfig.struct.ConfiguredTypesafeDossierRules;
import se.prv.errends.nodeconfig.struct.DefaultTypesafeDossierRules;
import se.prv.errends.nodeconfig.struct.DossierRules;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;
import se.prv.errends.nodeconfig.struct.NodeLayout;
import se.prv.errends.nodeconfig.struct.TypesafeDossierRules;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class ErrendInStateView extends Panel {
	
	private VerticalLayout layout = new VerticalLayout();
	private ErrendManagement em = null;
	private Session session = null;

	public ErrendInStateView() {
		em = (ErrendManagement) UI.getCurrent().getSession().getAttribute(CommonUI.currenterrend);
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
				
		List<TransitiondefManaged> errendactions = null;;
		try {
			errendactions = em.getErrendActions();
		} catch (Exception e) {
			Notification.show("Internt fel vid beräkning av operationer.", Notification.Type.ERROR_MESSAGE);
			return;
		}
		
		HorizontalLayout hlActions = new HorizontalLayout();
		if(errendactions != null && errendactions.size() > 0) {
			Label actions = new Label("Operationer");
			layout.addComponent(actions);
			
			for(TransitiondefManaged tdm : errendactions) {
				Button btn = new Button(tdm.getTransitionname());
				btn.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;
					@Override
					public void buttonClick(ClickEvent event) {
						click(tdm);
					}
				});
				btn.setDescription(getTooltip(tdm));
				hlActions.addComponent(btn);
			}
		}
		{
			Button help = new Button("Hjälp");
			help.addClickListener(e -> Notification.show(getTooltip(), Type.HUMANIZED_MESSAGE));
			help.setDescription("Få ledning om vad som skall hända näst i ärendet");
			hlActions.addComponent(help);
		}
		{
			Dossier dossier = em.getErrend().getReferringDossier();
			if(dossier != null) {
				Button handleDossierBtn = new Button("Hantera akt");
				handleDossierBtn.addClickListener(e -> handleDossier(dossier));
				handleDossierBtn.setDescription("Hantera dokumenten i akten");
				hlActions.addComponent(handleDossierBtn);
			}
		}
		
		layout.addComponent(hlActions);		
		
		Component customComponent = getCustomizedComponent();
		if(customComponent != null) {
			layout.addComponent(customComponent);
		}
		
		setContent(layout);
	}
	
	private void handleDossier(Dossier dossier) {
		TypesafeDossierRules tdr = getDossierRules();
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new DossierView(dossier, tdr, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(ErrendInStateView.this);
			}
		}));
	}

	private String getTooltip(TransitiondefManaged tdm) {
		EdgeConfiguration ec = tdm.getEc();
		if(ec.getExplanationLiteral() != null) {
			return ec.getExplanationLiteral();
		}
		Processdef pd = tdm.getTransitiondef().getSourcestatedef().getProcessdef();
		String functionname = ec.getExplanationFunctionName();
		if(functionname == null) {
			return "";
		}
		FunctionDef fd = pd.getFunctionDef(functionname);
		GeneralProducerVisitor gpv = new GeneralProducerVisitor(null, session.getActor(), tdm.getTransitiondef(), null);
		ProducerConfiguration pc = ProducerConfigurationManagement.getProducerConfigurationFromFunctionDef(fd);
		pc.accept(gpv);
 		return gpv.getOut().toString().trim();
	}
	 
	private String getTooltip() {
		Statedef sd = em.getCurrentState().getStatedef();
		NodeConfiguration nc = em.getCurrentState().getNc();
		if(nc.getExplanationLiteral() != null) {
			return nc.getExplanationLiteral();
		}
		Processdef pd = sd.getProcessdef();
		String functionname = nc.getExplanationFunctionName();
		if(functionname == null) {
			return "";
		}
		FunctionDef fd = pd.getFunctionDef(functionname);
		GeneralProducerVisitor gpv = new GeneralProducerVisitor(null, session.getActor(), (Transitiondef)null, null);
		ProducerConfiguration pc = ProducerConfigurationManagement.getProducerConfigurationFromFunctionDef(fd);
		pc.accept(gpv);
 		return gpv.getOut().toString().trim();
	}
	
	private TypesafeDossierRules getDossierRules() {
		Statedef sd = em.getCurrentState().getStatedef();
		NodeConfiguration nc = em.getCurrentState().getNc();
		DossierRules dr = nc.getDossierRules(session.getActor().getRoledef().getRolename());
		if(dr != null) {
			return new ConfiguredTypesafeDossierRules(dr);
		}
		else {
			return new DefaultTypesafeDossierRules(false);
		}
	}
	 
	private void click(TransitiondefManaged tdm) {
		//System.out.println("Operation "+transitionname);
		try {
			em.performTransition(tdm.getTransitionname(), session);
		} catch (Exception e) {
			e.printStackTrace();
			Notification.show("Operationen misslyckades: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);		
			return;
		}
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new ErrendInStateView());
	}
	
	private Component getCustomizedComponent() { 
		NodeConfiguration nc = em.getCurrentState().getNc();
		String rolename = session.getActor().getRoledef().getRolename();
		List<NodeLayout> layout = nc.getNodelayouts(rolename);
		if(layout == null) {
			return null;
		}
		VerticalLayout vl = new VerticalLayout();
		for(NodeLayout nl : layout) {
			NodeConfigurationLayoutVisitor visitor = new NodeConfigurationLayoutVisitor(em.getErrend(), this);
			nl.accept(visitor);
			vl.addComponent(visitor.getComponent());
		}
		return vl;
	}

}
