package se.prv.errends.mainweb.person;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Message;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.person.MessagesView;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ActorManagement;
import se.prv.errends.mgmt.EmailManagement;
import se.prv.errends.reuse.cli.PropertyNames;

public class MessageFormWrite extends Window {
	
	private Actor from = null;
	private Actor to = null;
	private Runnable onClose = null;
	private TextField recieverComponent = null;
	private TextField title = null;
	private TextArea writtenmessage = null;
	private CheckBox notifyWithEmail = null;
	private MessagesView.NotifyAboutMessage newMessageNotifyer;
	
	public MessageFormWrite(Actor from, Actor to, Runnable onClose, MessagesView.NotifyAboutMessage newMessageNotifyer) {
		this.onClose = onClose;
		this.from = from;
		this.newMessageNotifyer = newMessageNotifyer;
		System.out.println("From "+from);
		from.getErrendId();
		this.to = to;
		
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		recieverComponent = new TextField("Mottagare");
		recieverComponent.setEnabled(false);
		setTo(to);
		content.addComponent(recieverComponent);
		
		if(to == null) {
			Button setRecieverBtn = new Button("Välj mottagare");
			setRecieverBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
				@Override
				public void buttonClick(ClickEvent event) {
					setReciever();
				}			
			});
			content.addComponent(setRecieverBtn);
		}
		
		title = new TextField("Ämne");
		content.addComponent(title);
		
		writtenmessage = new TextArea("Meddelande");
		content.addComponent(writtenmessage);
		
		notifyWithEmail = new CheckBox("Notifiera med E-post, dvs titeln men inte meddelandet men en NOTIFIERING");
		notifyWithEmail.setValue(false);
		content.addComponent(notifyWithEmail);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Skicka");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void setTo(Actor toActor) {
		System.out.println("MessageFormView.setTo("+toActor+")");
		if(toActor == null) {
			return;
		}
		to = toActor;
		Person p = toActor.getAgent().getPerson();
		recieverComponent.setValue(p.getFirstname()+" "+p.getFamilyname());
	}

	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		if(to == null) {
			Notification.show("Saknar mottagare", Notification.Type.ERROR_MESSAGE);
			return;
		}
		
		Message message = new Message();
		message.setSend(new Date(System.currentTimeMillis()));
		message.setSourceActor(from);
		message.setTargetActor(to);
		message.setTitle(title.getValue());
		message.setWrittenmessage(writtenmessage.getValue());	
		message.save();
		
		if(notifyWithEmail.getValue()) {
			if(!EmailManagement.sendMessageFromFunctionByProperties(to, PropertyNames.youhaveamessagetitle, PropertyNames.youhaveamessagemessage, from, message)) {
				Notification.show("Problem när e-posten skulle skickas", Notification.Type.ERROR_MESSAGE);
			};
		}
		
		if(newMessageNotifyer != null) {
			newMessageNotifyer.sent(message);
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void setReciever() {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(
				new SearchPersonForm(new SearchPersonForm.NotifyAboutPersonPick() {
					@Override
					public void picked(Person p) {
						setTo(getRightActor(p));
					}
				}, null));
	}
	
	private Actor getRightActor(Person p) {
		try {
			Errend errend = from.getErrend();
			if(errend != null) {
				List<Actor> actors = ActorDB.getAllActorsOfErrendIdAndPersonId(errend.getId(), p.getId());
				if(actors != null && actors.size() > 0) {
					return actors.get(0);
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ActorManagement.getErrendlessActor(p);
	}
	
}
