package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;
import se.prv.errends.processengine.reuse.StringFileManipulation;

public class EmbeddedExportCreator {
	
	public static byte[] exportToGraphml(Processdef pd) throws Exception {
    	String whole = StringFileManipulation.readFullyToString(EmbeddedExportCreator.class.getClassLoader().getResourceAsStream("graphml-whole.template"));
    	String node = StringFileManipulation.readFullyToString(EmbeddedExportCreator.class.getClassLoader().getResourceAsStream("graphml-node.template"));
    	String edge = StringFileManipulation.readFullyToString(EmbeddedExportCreator.class.getClassLoader().getResourceAsStream("graphml-edge.template"));
    	
    	List<Statedef> statedefs = pd.getReferringStatedefs();
    	ArrayList<Transitiondef> transdefs = new ArrayList<Transitiondef>();
    	
    	StringBuffer nodes = new StringBuffer();
    	
    	long nctr = 0;
    	Map<Long, Long> nodemap = new HashMap<Long, Long>();
    	
    	for(Statedef statedef : statedefs) {
    		List<Transitiondef> statesTransdefs = statedef.getSourceTransitiondefs();
    		transdefs.addAll(statesTransdefs);
    		nodemap.put(statedef.getId(), nctr);
    		String p1 = node.replaceAll("NODEREFERENCE", "n"+nctr);
    		String p2 = p1.replaceAll("TEXTUALINSPECIFCATION", statedef.getLayout());
    		String p3 = p2.replaceAll("NODENAME", statedef.getStatename());
    		nodes.append(p3+"\n");
    		nctr++;
    	}
    	
    	StringBuffer edges = new StringBuffer();
    	long ectr = 0;
    	
    	for(Transitiondef transdef : transdefs) {
    		String p1 = edge.replaceAll("SOURCENODEREFERENCE", "n"+nodemap.get(transdef.getSourcestatedefId()));
    		String p2 = p1.replaceAll("TARGETNODEREFERENCE", "n"+nodemap.get(transdef.getTargetstatedefId()));
    		String p3 = p2.replaceAll("TEXTUALINSPECIFCATION", transdef.getLayout());
    		String p4 = p3.replaceAll("TRANSITIONNAME", transdef.getTransitionname());
    		String p5 = p4.replaceAll("EDGEREFERENCE", "e"+ectr);
    		edges.append(p5+"\n");
    		ectr++;
    	}
    	
    	String p1 = whole.replaceAll("REPETITIONOFNODES", nodes.toString());
    	String p2 = p1.replaceAll("REPETITIONOFEDGES", edges.toString());
    	
		return p2.getBytes("UTF8");
	}
	
}
