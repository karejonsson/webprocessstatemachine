package se.prv.errends.mainweb.adminuser.processeditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.vaadin.ui.Component;
import com.vaadin.ui.ComboBox;

import se.prv.errends.domain.Statedef;
import se.prv.errends.domain.Transitiondef;

public class TransitiondefRowHolder {
	
	private Transitiondef td = null;
	private Map<Long, Statedef> lookup = null;
	private ComboBox<ListItem> sourceStateComponent = null;
	private ComboBox<ListItem> targetStateComponent = null;
	
	private List<ListItem> items = new ArrayList<ListItem>();
	private Map<Long, ListItem> itemLookup = new HashMap<Long, ListItem>();

	public TransitiondefRowHolder(Transitiondef td, Map<Long, Statedef> lookup) {
		//System.out.println("TransitiondefRowHolder.<ctor> fick "+lookup.keySet().size()+" nycklar och "+lookup.values().size()+" värden");
		this.td = td;
		this.lookup = lookup;
		for(Statedef sd : lookup.values()) {
			if(sd.isPersisted()) {
				ListItem li = new ListItem(sd.getStatename(), sd.getId());
				items.add(li);
				itemLookup.put(li.getId(), li);
			}
		}
	}
	
	public void alertStatedefRemoved(Statedef sd) {
		if(sd.isPersisted()) {
			lookup.remove(sd.getId());
			ListItem li = itemLookup.get(sd.getId());
			Optional<ListItem> op = sourceStateComponent.getSelectedItem();
			if(op != null) {
				if(li.getId().equals(op.get().getId())) {
					sourceStateComponent.setSelectedItem(null);
				}
			}
			op = targetStateComponent.getSelectedItem();
			if(op != null) {
				if(li.getId().equals(op.get().getId())) {
					targetStateComponent.setSelectedItem(null);
				}
			}		
			itemLookup.remove(li.getId());
		}
	}
	
	public Component getSourceStateComponent() {
		if(sourceStateComponent != null) {
			return sourceStateComponent;
		}
		sourceStateComponent = new ComboBox<ListItem>();
		
		sourceStateComponent.setItems(items);
		Long sId = td.getSourcestatedefId();
		if(sId != null) {
			ListItem li = itemLookup.get(sId);
			sourceStateComponent.setSelectedItem(li);
		}
		
		return sourceStateComponent;
	}

	public String getTransitionname() {
		return td.getTransitionname();
	}

	public Component getTargetStateComponent() {
		if(targetStateComponent != null) {
			return targetStateComponent;
		}
		targetStateComponent = new ComboBox<ListItem>();

		targetStateComponent.setItems(items);
		Long tId = td.getTargetstatedefId();
		if(tId != null) {
			ListItem li = itemLookup.get(tId);
			targetStateComponent.setSelectedItem(li);
		}

		return targetStateComponent;
	}

	public void smoothDelete() {
		if(td.isPersisted()) {
			td.delete();
		}
	}

	public String getLayout() {
		return td.getLayout();
	}
	
	private static class ListItem {
		private String name = null;
		private Long id = null;
		public ListItem(String name, Long id) {
			this.name = name;
			this.id = id;
		}
		public String toString() {
			return name;
		}
		public Long getId() {
			return id;
		}
		public void setName(String statename) {
			this.name = statename;
		}
	}

	public void alertNewStatedef(Statedef sd) {
		if(sd.isPersisted()) {
			items.add(new ListItem(sd.getStatename(), sd.getId()));
			if(sourceStateComponent != null) {
				sourceStateComponent.getDataProvider().refreshAll();
			}
			if(targetStateComponent != null) {
				targetStateComponent.getDataProvider().refreshAll();		
			}
		}
	}

	public void alertStatedefChanged(Statedef sd) {
		if(sd.isPersisted()) {
			ListItem li = itemLookup.get(sd.getId());
			if(li != null) {
				li.setName(sd.getStatename());
				if(sourceStateComponent != null) {
					sourceStateComponent.getDataProvider().refreshAll();
				}
				if(targetStateComponent != null) {
					targetStateComponent.getDataProvider().refreshAll();		
				}
			}
		}
	}

	public String getErrormessageForSave() {
		Optional<ListItem> op = sourceStateComponent.getSelectedItem();
		if(op == null || !op.isPresent() || op.get() == null) {
			return "Övergång "+td.getTransitionname()+" har ingen startpunkt";
		}
		op = targetStateComponent.getSelectedItem();
		if(op == null || !op.isPresent() || op.get() == null) {
			return "Övergång "+td.getTransitionname()+" har ingen slutpunkt";
		}
		return null;
	}

	public void save() {
		Optional<ListItem> op = sourceStateComponent.getSelectedItem();
		Long sourceId = op.get().getId();
		td.setSourcestatedefId(sourceId);
		
		op = targetStateComponent.getSelectedItem();
		Long targetId = op.get().getId();
		td.setTargetstatedefId(targetId);
		
		td.save();
	}

	public Transitiondef getTransitiondef() {
		return td;
	}

}

