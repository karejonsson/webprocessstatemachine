package se.prv.errends.mainweb.adminuser;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.ManagedErrend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Processdef;
import se.prv.errends.domain.Roledef;
import se.prv.errends.mainweb.person.ChoseAgentForm;
import se.prv.errends.mainweb.person.SearchPersonForm;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ActorManagement;
import se.prv.errends.mgmt.AgentWithRoledef;

public class AdminuserSetupErrendRoles extends Panel {

	private List<Roledef> roledefs = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<AgentWithRoledef> grid = null;
	private Adminuser current = null;
	private Errend errend = null;
	private ManagedErrend managederrend = null;
	private Processdef processdef = null;
	private TextField adminusername = null;
	private Runnable onFinish = null;

	private List<AgentWithRoledef> personsWithRoledef = new ArrayList<AgentWithRoledef>();

	private Roledef getRoledef(Long id) {
		for(Roledef rd : roledefs) {
			if(id.equals(rd.getId())) {
				return rd;
			}
		}
		return null;
	}

	public AdminuserSetupErrendRoles(Errend errend, Runnable onFinish) {
		this.onFinish = onFinish;
		this.current = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		this.errend = errend;
		managederrend = errend.getManagedErrend();
		processdef = managederrend.getProcessdef();
		roledefs = processdef.getRoledefs();

		List<Actor> actors = ActorDB.getAllActorsOfErrendId(errend.getId());
		List<Long> roledefsTaken = new ArrayList<Long>();

		for(Actor actor : actors) {
			personsWithRoledef.add(new AgentWithRoledef(actor.getAgent(), actor.getRoledef()));
			roledefsTaken.add(actor.getRoledef().getId());
		}

		for(Roledef roledef : roledefs) {
			if(!roledefsTaken.contains(roledef.getId())) {
				personsWithRoledef.add(new AgentWithRoledef(null, roledef));
			}
		}

		grid = new Grid<>();

		grid.addComponentColumn(row -> {
			HorizontalLayout hl = new HorizontalLayout();
			Agent oa = row.getAgent();
			Organisation oacc = oa != null ? oa.getOrganisation() : null;
			Person p = oa != null ? oa.getPerson() : null;
			String label = "";
			if(oacc != null) {
				label += oacc.getOrgname();
			}
			if(p != null) {
				if(label.length() != 0) {
					label += " : ";
				}
				label += p.getFirstname()+" "+p.getFamilyname();
			}
			Label l = new Label(label);
			hl.addComponent(l);
			hl.setComponentAlignment(l, Alignment.MIDDLE_LEFT);

			Button b = new Button("Välj");
			b.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
				@Override
				public void buttonClick(ClickEvent event) {
					setAgent(row);
				}
			});
			b.setWidth(10, Unit.PERCENTAGE);
			hl.addComponent(b);
			hl.setComponentAlignment(b, Alignment.MIDDLE_RIGHT);
			hl.setSizeFull();
			return hl;
		});

		grid.addComponentColumn(row -> {
			ComboBox role = new ComboBox();
			role.setItems(roledefs);
			role.setValue(getRoledef(row.getRoledef().getId()));
			return role;
		});

		grid.setItems(personsWithRoledef);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();

		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		buttons.addComponent(saveBtn);

		adminusername = new TextField();
		buttons.addComponent(adminusername);
		adminusername.setWidth("50%");
		adminusername.setValue(errend.getAdminusername() != null ? errend.getAdminusername() : "");

		buttons.setComponentAlignment(adminusername, Alignment.MIDDLE_RIGHT);
		buttons.setWidth("100%");

		layout.addComponent(buttons);
		layout.addComponent(grid);

		setContent(layout);
	}

	private void setAgent(AgentWithRoledef owr) {
		((AdminuserUI) UI.getCurrent()).openSubwindow(
				new SearchPersonForm(new SearchPersonForm.NotifyAboutPersonPick() {
					@Override
					public void picked(Person p) {
						setAgentFromPerson(owr, p);
					}
				}, null));
	}

	private void setAgentFromPerson(AgentWithRoledef owr, Person p) {
		((AdminuserUI) UI.getCurrent()).openSubwindow(
				new ChoseAgentForm(new ChoseAgentForm.NotifyAboutPick() {
					@Override
					public void picked(Agent oa) {
						owr.setAgent(oa);
						grid.getDataProvider().refreshAll();
					}
				}, p, null));
	}

	private void save() {
		try {
			String message = ActorManagement.saveErrendSetupReturnUserText(errend, roledefs, adminusername.getValue(), personsWithRoledef, ActorManagement.getErrendlessActor(current.getPerson()));
			if(message != null && message.trim().length() > 0) {
				Notification.show(message, Notification.Type.ASSISTIVE_NOTIFICATION);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}

		if(onFinish != null) {
			onFinish.run();
		}
	}

}
