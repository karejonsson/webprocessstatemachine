package se.prv.errends.mainweb.person;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Window;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import se.prv.errends.domain.Document;
import se.prv.errends.domain.DocumentEvent;
import se.prv.errends.domain.DossierDoctypeDef;
import se.prv.errends.domain.DocumentEvent.DocumentEventType;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.person.DossierView.GeneralizedDocumentEvent;
import se.prv.errends.mainweb.person.DossierView.GeneralizedEvent;
import se.prv.errends.mainweb.ui.CommonUI;

public class DocumentFormEdit extends Window {
	
	private Runnable onClose = null;
	
	private TextField description = null;
	private ComboBox<DossierDoctypeDefListItem> optionsComponent = null;
	private DateField datefield = null;
	private Upload upload = null;
	private ByteArrayOutputStream bais = null;
	private ImageUploader receiver = null;
	private TextField filenameOverride = null;
	private String filename;
	private String mimetype;
	private Document document = null;
	private Session session = null;
	private List<GeneralizedEvent> allEventsRows;
	private Grid<GeneralizedEvent> allEventsGrid;
	
	public DocumentFormEdit(Document document, List<GeneralizedEvent> allEventsRows, Grid<GeneralizedEvent> allEventsGrid, Runnable onClose) {
		this.document = document;
		filename = document.getFilename();
		mimetype = document.getMimetype();
		System.out.println("DocumentFormEdit<ctor> document = "+document);
		this.onClose = onClose;
		this.allEventsRows = allEventsRows;
		this.allEventsGrid = allEventsGrid;
		
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		setModal(true);
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		filenameOverride = new TextField("Filnamn");
		filenameOverride.setValue(document.getFilename() != null ? document.getFilename() : "");
		content.addComponent(filenameOverride);
		
		description = new TextField("Beskrivning");
		description.setValue(document.getDescription() != null ? document.getDescription() : "");
		content.addComponent(description);
		
		optionsComponent = new ComboBox<DossierDoctypeDefListItem>("Filtyp");
		List<DossierDoctypeDef> items = document.getDossier().getDossierDef().getReferringDossierDoctypeDefs();
		if(items.size() > 0) { 
			List<DossierDoctypeDefListItem> comboBoxItems = new ArrayList<DossierDoctypeDefListItem>();
			items.forEach(e -> comboBoxItems.add(new DossierDoctypeDefListItem(e)));
			optionsComponent.setItems(comboBoxItems);
			DossierDoctypeDefListItem dddli = getCurrent(comboBoxItems, document.getDossierDoctypeDefId());
			if(dddli == null) {
				optionsComponent.setSelectedItem(comboBoxItems.get(0));
			}
			else {
				optionsComponent.setSelectedItem(dddli);
			}
			optionsComponent.setEmptySelectionAllowed(false);
		}
		content.addComponent(optionsComponent);
		
		datefield = new DateField("Inkommet");
		Date d = document.getDateOfReception();
		if(d == null) {
			d = new Date();
		}
		
		ZoneId localZoneId = ZoneId.systemDefault();
		Instant instant = d.toInstant();
		LocalDate localDate = instant.atZone(localZoneId).toLocalDate();
		
		datefield.setValue(localDate);
		content.addComponent(datefield);
		
		receiver = new ImageUploader();
		upload = new Upload("Dokument", receiver);
		upload.addSucceededListener(receiver);
		upload.setImmediateMode(false);
		
		content.addComponent(upload);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(e -> cancel());
		buttons.addComponent(cancel);
		
		Button saveMeta = new Button("Spara metadata");
		saveMeta.addClickListener(e -> saveMeta());
		buttons.addComponent(saveMeta);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void saveMeta() {
		StringBuffer sb = new StringBuffer();
		String metadataBefore = document.toString();
    	String nameFromGui = filenameOverride.getValue();
    	sb.append("Filnamn innan "+document.getFilename());
    	if(nameFromGui == null || nameFromGui.trim().length() == 0) {
    		document.setFilename(filename);
    	}
    	else {
    		document.setFilename(filenameOverride.getValue());
    	}
    	sb.append(", filnamn efter "+document.getFilename()+".\n");
    	sb.append("Beskrivning innan "+document.getDescription());
		document.setDescription(description.getValue()); 
    	sb.append(", efter "+document.getDescription()+".\n");
		document.setMimetype(mimetype);
		document.setSessionId(session.getId());
		
    	sb.append("Filtyp innan "+document.getDossierDoctypeDef().getDossierdoctypename());
		Optional<DossierDoctypeDefListItem> op = optionsComponent.getSelectedItem();
		DossierDoctypeDefListItem choice = op.get();
		if(choice != null) {
			document.setDossierDoctypeDefId(choice.getDossierDoctypeDef().getId());
		}
    	sb.append(", efter "+document.getDossierDoctypeDef().getDossierdoctypename()+".\n");
		
		LocalDate dateChosen = datefield.getValue();
		ZoneId localZoneId = ZoneId.systemDefault();
		Date d = Date.from(dateChosen.atStartOfDay(localZoneId).toInstant());
    	sb.append("Datum innan "+document.getDateOfReception());
		document.setDateOfReception(d);
    	sb.append(", efter "+document.getDateOfReception()+".\n");
		
		document.setOrigin("");
		document.save();
		
		DocumentEvent dev = new DocumentEvent();
		dev.setCodeEnum(DocumentEventType.CREATENOMETA_GUI);
		dev.setDocument(document);
		dev.setSessionId(session.getId());
		dev.setSysteminfo("Innan "+metadataBefore);
		dev.setDescription("Sparade metadata\n"+sb.toString());
		dev.save();
		
		allEventsRows.add(new GeneralizedDocumentEvent(dev));
		allEventsGrid.getDataProvider().refreshAll();

		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private static DossierDoctypeDefListItem getCurrent(List<DossierDoctypeDefListItem> comboBoxItems, Long dossierdoctypedefId) {
		if(dossierdoctypedefId == null) {
			return null;
		}
		for(DossierDoctypeDefListItem comboBoxItem : comboBoxItems) {
			if(comboBoxItem.getDossierDoctypeDef().getId().equals(dossierdoctypedefId)) {
				return comboBoxItem;
			}
		}
		return null;
	}
	
	private static class DossierDoctypeDefListItem {
		private DossierDoctypeDef ddd;
		public DossierDoctypeDefListItem(DossierDoctypeDef ddd) {
			this.ddd = ddd;
		}
		public DossierDoctypeDef getDossierDoctypeDef() {
			return ddd;
		}
		public String toString() {
			return ddd.getDossierdoctypename();
		}
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		upload.startUpload();
	}
	
	private void uploadDone() {
    	String nameFromGui = filenameOverride.getValue();
    	if(nameFromGui == null || nameFromGui.trim().length() == 0) {
    		document.setFilename(filename);
    	}
    	else {
    		document.setFilename(filenameOverride.getValue());
    	}

		document.setDescription(description.getValue());
		document.setMimetype(mimetype);
		document.setSessionId(session.getId());
		
		Optional<DossierDoctypeDefListItem> op = optionsComponent.getSelectedItem();
		DossierDoctypeDefListItem choice = op.get();
		if(choice != null) {
			document.setDossierDoctypeDefId(choice.getDossierDoctypeDef().getId());
		}
		
		LocalDate dateChosen = datefield.getValue();
		ZoneId localZoneId = ZoneId.systemDefault();
		Date d = Date.from(dateChosen.atStartOfDay(localZoneId).toInstant());
		document.setDateOfReception(d);
		
		document.setActive(true);
		document.setOrigin("");
		document.save();
		
		document.setData(bais.toByteArray());
		
		DocumentEvent dev = new DocumentEvent();
		dev.setCodeEnum(DocumentEventType.CREATE_GUI);
		dev.setDocument(document);
		dev.setSessionId(session.getId());
		dev.setSysteminfo("Filnamn "+filename);
		dev.setDescription("Uppladdat från egna datorn");
		dev.save();
		
		allEventsRows.add(new GeneralizedDocumentEvent(dev));
		allEventsGrid.getDataProvider().refreshAll();

		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	class ImageUploader implements Receiver, SucceededListener {

	    public OutputStream receiveUpload(String filename,
	                                      String mimeType) {
	    	DocumentFormEdit.this.filename = filename;
	    	DocumentFormEdit.this.mimetype = mimeType;
	        bais = new ByteArrayOutputStream();
	    	return bais;
	    }

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			uploadDone();
		}
	};
	
}
