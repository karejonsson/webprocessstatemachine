package se.prv.errends.mainweb.adminuser;

import java.security.NoSuchAlgorithmException;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.person.PersonFormEdit;
import se.prv.errends.mainweb.person.SearchPersonForm;
import se.prv.errends.mainweb.ui.AdminuserUI;

public class AdminuserFormEdit extends Window {
	
	private Runnable onClose = null;
	private Adminuser adminuser = null;
	private TextField username = null;
	private TextField password = null;
	private CheckBox superprivilegies = null;
	private Label personField = null;
	private CheckBox active = null;
	private Person person = null;
	
	public AdminuserFormEdit(Adminuser adminuser, Runnable onClose) {
		this.onClose = onClose;
		this.adminuser = adminuser;
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		username = new TextField("Användarnamn");
		username.setValue(adminuser != null ? (adminuser.getUsername() == null ? "" : adminuser.getUsername()): "");
		content.addComponent(username);
		
		active = new CheckBox("Aktiv");
		active.setValue(adminuser != null ? adminuser.isActive() : true);
		content.addComponent(active);

		password = new TextField("Lösenord");
		password.setValue("");
		content.addComponent(password); 

		superprivilegies = new CheckBox("Super");
		superprivilegies.setValue(false);
		content.addComponent(superprivilegies);
		
		personField = new Label();
		String personFieldDefault = "Person ej vald";
		if(adminuser != null && adminuser.getPerson() != null) {
			Person p = adminuser.getPerson();
			personFieldDefault = p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname();
		}
		personField.setValue(personFieldDefault);
		content.addComponent(personField);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button createPersonBtn = new Button("Skapa person");
		createPersonBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Person p = new Person();
				((AdminuserUI) UI.getCurrent()).openSubwindow(new PersonFormEdit(p, new Runnable() {
					@Override
					public void run() {
						person = p;
						personField.setValue(p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname());
					}
				}));
			}
			
		});
		buttons.addComponent(createPersonBtn);
		
		Button chosePersonBtn = new Button("Välj person");
		chosePersonBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				((AdminuserUI) UI.getCurrent()).openSubwindow(new SearchPersonForm(
						new SearchPersonForm.NotifyAboutPersonPick() {
							@Override
							public void picked(Person p) {
								person = p;
								personField.setValue(p.getSwedishpnr()+", "+p.getFirstname()+" "+p.getFamilyname());
							}
						},
						null
						/*
						new Runnable() {
							@Override
							public void run() {
								((AdminuserUI) UI.getCurrent()).openSubwindow(AdminuserFormEdit.this);
							}
						}
						*/
						));
			}
			
		});
		buttons.addComponent(chosePersonBtn);
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		if(adminuser == null) {
			adminuser = new Adminuser();
		}
		if(username.getValue() != null && username.getValue().length() < 4) {
			Notification.show("Användarnamnet för kort", Notification.Type.ERROR_MESSAGE);
			return;
		}
		adminuser.setUsername(username.getValue());
		adminuser.setActive(true);
		try {
			adminuser.setPassword(Adminuser.calculateChecksum(password.getValue()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		adminuser.setPerson(person);
		adminuser.setSuperprivilegies(superprivilegies.getValue());
		adminuser.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
