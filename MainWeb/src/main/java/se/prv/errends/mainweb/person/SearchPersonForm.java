package se.prv.errends.mainweb.person;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.PersonDB;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.ui.CommonUI;

public class SearchPersonForm extends Window {
	
	private Runnable onClose = null;
	private Person person = null;
	private TextField whatever = null;
	private TextField familyname = null;
	private TextField swedishPnr = null;
	private Grid<Person> grid = null;
	private NotifyAboutPersonPick notify = null;
	
	public SearchPersonForm(NotifyAboutPersonPick notify, Runnable onClose) {
		super("Välj person");
		this.onClose = onClose;
		this.notify = notify;
		
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		whatever = new TextField("Fritext");
		content.addComponent(whatever);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		
		Button searchBtn = new Button("Sök");
		searchBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				search();
			}
			
		});
		buttons.addComponent(searchBtn);	
		content.addComponent(buttons);
		
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getFirstname());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getFamilyname());
        }).setCaption("Efternamn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getSwedishpnr());
        }).setCaption("Personnummer"); 
        if(notify != null) {
	        grid.addComponentColumn(row -> {
	        	Button pick = new Button("Välj");
	    		pick.addClickListener(new ClickListener() {
	    			private static final long serialVersionUID = 1L;
	    			@Override
	    			public void buttonClick(ClickEvent event) {
	    				picked(row);
	    			}
	    		});
	        	return pick;
	        }).setCaption("Välj"); 
        }
        
		grid.getEditor().setEnabled(false);

		content.addComponent(grid);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void search() {
		String name = whatever.getValue();
		if(name == null || name.trim().length() < 1) {
			name = "%";
		}
		else {
			if(!name.contains("*")) {
				name = "%"+name.trim()+"%";
			}
			else {
				name = name.trim().replace("*", "%");
			}
		}
		//System.out.println("SearchPersonForm.search: Verklig sökning "+name);
		List<Person> hits = PersonDB.searchActive(name);
		grid.setItems(hits);
	}
	
	public interface NotifyAboutPersonPick {
		public void picked(Person p);
	}
	
	private void picked(Person p) {
		if(notify != null) {
			notify.picked(p);
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
