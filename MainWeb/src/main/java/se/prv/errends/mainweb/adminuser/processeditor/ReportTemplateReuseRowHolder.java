package se.prv.errends.mainweb.adminuser.processeditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;

import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.ReportTemplateReuse;

public class ReportTemplateReuseRowHolder {
	
	private ReportTemplateReuse rtr = null;
	private Map<Long, ReportTemplate> lookup = null;
	private List<ReportTemplate> reporttemplates = null;
	private ComboBox<ListItem> reportTemplateComponent = null;
	
	private List<ListItem> items = new ArrayList<ListItem>();
	private Map<Long, ListItem> itemLookup = new HashMap<Long, ListItem>();

	public ReportTemplateReuseRowHolder(ReportTemplateReuse rtr, List<ReportTemplate> reporttemplates, Map<Long, ReportTemplate> lookup) {
		this.rtr = rtr;
		this.lookup = lookup; 
		this.reporttemplates = reporttemplates;
		for(ReportTemplate rt : lookup.values()) {
			if(rt.isPersisted()) {
				ListItem li = new ListItem(rt.getReporttitle(), rt.getId());
				items.add(li);
				itemLookup.put(li.getId(), li);
			}
		}
	}
	
	public Component getReportTemplateComponent() {
		if(reportTemplateComponent != null) {
			return reportTemplateComponent;
		}
		reportTemplateComponent = new ComboBox<ListItem>();
		
		reportTemplateComponent.setItems(items);
		Long sId = rtr.getReporttemplateId();
		if(sId != null) {
			ListItem li = itemLookup.get(sId);
			reportTemplateComponent.setSelectedItem(li);
		}
		reportTemplateComponent.setWidth("90%");
		
		return reportTemplateComponent;
	}
	
	public void smoothDelete() {
		if(rtr.isPersisted()) {
			rtr.delete();
		}
	}

	public String getErrormessageForSave() {
		Optional<ListItem> op = reportTemplateComponent.getSelectedItem();
		if(op == null || !op.isPresent() || op.get() == null) {
			return "Välj rapport i alla rader eller ta bort överflödiga";
		}
		return null;
	}
	
	public void save() {
		Optional<ListItem> op = reportTemplateComponent.getSelectedItem();
		Long reporttemplateId = op.get().getId();
		rtr.setReporttemplateId(reporttemplateId);
		
		rtr.save();
	}
	
	private static class ListItem {
		private String name = null;
		private Long id = null;
		public ListItem(String name, Long id) {
			this.name = name;
			this.id = id;
		}
		public String toString() {
			return name;
		}
		public Long getId() {
			return id;
		}
		public void setName(String statename) {
			this.name = statename;
		}
	}

}
