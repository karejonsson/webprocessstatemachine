package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Link;

import se.prv.errends.domain.FunctionDef;
import se.prv.errends.domain.Processdef;

public class ExportedFunctionDefLink extends Link {
	
	private Processdef pd;

	public ExportedFunctionDefLink(Processdef pd) {
		super();
		this.pd = pd;
		setCaption("funktioner");
		setDescription("Exportera flödet som funktionsfil. Hämta filen till din dator");
		setTargetName("_blank");
	}

	@Override
	public void attach() {
		super.attach(); // Must call.

		StreamResource.StreamSource source = new StreamResource.StreamSource() {
			public InputStream getStream() {
				StringBuffer sb = new StringBuffer();
				List<FunctionDef> funcs = pd.getFunctionDefs();
				for(FunctionDef func : funcs) {
					sb.append(func.getSource()+"\n");
				}
				try {
					return new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		StreamResource resource = new StreamResource (source, pd.getProcessfile().getFilename()+".functions");
		resource.setMIMEType("application/text");
		resource.setCacheTime(0);
		setResource(resource);
	}
	
}