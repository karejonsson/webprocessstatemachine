package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.person.MessageFormWrite;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.nodeconfig.struct.ParticipantView;

public class ParticipantsInErrendView extends Panel {
	
	private List<Actor> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Actor> grid = null;

	private Errend errend = null;
	//private Session session = null;
	private ErrendInStateView eisv = null;
	private Session session = null;
	private ParticipantView participantView = null;
	
	public ParticipantsInErrendView(Errend errend, ParticipantView participantView, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
		this.participantView = participantView; 
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		//session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.authenticated);
		rows = ActorDB.getAllActorsOfErrendId(errend.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
         	Agent agent = row.getAgent();
        	Person p = agent.getPerson();
        	Organisation organization = agent.getOrganisation();
        	String orgpart = "";
        	if(organization != null) {
        		orgpart = organization.getOrgname()+" / ";
        	}
        	Label out = new Label(orgpart+p.getFirstname()+" "+p.getFamilyname());
            return out;
        }).setCaption("Ombud");
        grid.addComponentColumn(row -> {
        	Label out = new Label(row.getRoledef().getRolename());
            return out;
        }).setCaption("Roll");
        
        if(messaging()) {
			grid.addComponentColumn(row -> {
				  Layout out = new HorizontalLayout();
			      Button describeBtn = new Button("Meddelande");
			      boolean isOther = !session.getActor().getAgent().getPersonId().equals(row.getAgent().getPersonId());
			      boolean isDeamon = row.getRoledef().getRolename().equals("deamon");
			      describeBtn.setEnabled(isOther && !isDeamon);
			      describeBtn.addClickListener(click -> sendMessage(row));
			      out.addComponent(describeBtn);
			      return out;
			}).setCaption("Hantering");
        }

		grid.getEditor().setEnabled(true);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		setContent(layout);
	}
	
	private boolean messaging() {
		return evalBooleanProperty("messaging", true);
	}
	
	private boolean evalBooleanProperty(String key, boolean defaultResponse) {
		if(participantView == null) {
			return defaultResponse;
		}
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: "+key+", default "+defaultResponse);
		if(!participantView.hasKey(key)) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 1 "+defaultResponse);
			return defaultResponse;
		}
		String value = participantView.getValue(key);
		if(value == null) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 2 "+defaultResponse);
			return defaultResponse;
		}
		if(value.toLowerCase().contains("n")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 3 false");
			return false;
		}
		if(value.toLowerCase().contains("y") || value.toLowerCase().contains("j")) {
			//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 true");
			return true;
		}
		//System.out.println("DocumentsInErrendView.evalBooleanProperty: 4 "+defaultResponse);
		return defaultResponse;
	}


	public void sendMessage(Actor actor) {
		Runnable doAfter = null;
		if(eisv != null) {
			doAfter = new Runnable() {
				@Override
				public void run() {
					grid.getDataProvider().refreshAll();
					((SimplifiedContentUI) UI.getCurrent()).setup(eisv);
				}
			};
		}
		else {
			doAfter = new Runnable() {
				@Override
				public void run() {
					grid.getDataProvider().refreshAll();
					((SimplifiedContentUI) UI.getCurrent()).setWorkContent(ParticipantsInErrendView.this);
		
				}
			};
		}
		MessageFormWrite mfw = new MessageFormWrite(
				session.getActor(), 
				actor, 
				doAfter, 
				null
				);
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(mfw);
	}
	
}
