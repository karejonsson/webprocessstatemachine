package se.prv.errends.mainweb.person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Document;
import se.prv.errends.domain.DocumentEvent;
import se.prv.errends.domain.DocumentEvent.DocumentEventType;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.nodeconfig.struct.TypesafeDossierRules;
import se.prv.errends.domain.Dossier;
import se.prv.errends.domain.DossierEvent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;

public class DossierView extends Panel {
	
	private TabSheet tabsheet = new TabSheet();
	
	private List<Document> currentDocumentsRows = new ArrayList<Document>();
	private List<Document> allDocumentsRows = null;
	
	private Grid<Document> currentDocumentsGrid = null;
	private Grid<Document> allDocumentsGrid = null;
	
	public interface GeneralizedEvent {
		String object();
		String person();
		String story();
		String description();
		Date time();
	}

	private List<GeneralizedEvent> allEventsRows = new ArrayList<GeneralizedEvent>();
	private Grid<GeneralizedEvent> allEventsGrid = null;

	private Dossier dossier = null;
	private Session session = null;
	private Runnable onClose = null;
	private TypesafeDossierRules rules = null;

	public DossierView(Dossier dossier, TypesafeDossierRules rules, Runnable onClose) {
		this.dossier = dossier;
		this.rules = rules;
		this.onClose = onClose;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		allDocumentsRows = dossier.getReferringDocuments();
		allDocumentsRows.forEach(d -> { if(d.isActive()) { currentDocumentsRows.add(d); } });

		tabsheet.setWidth("100%");
		tabsheet.addTab(getCurrentDocumentsComponent(), "Aktuella dokument");
		tabsheet.addTab(getAllDocumentsComponent(), "Alla dokument");
		tabsheet.addTab(getAllEventsComponent(), "Händelser");
		
		/*
		HorizontalLayout layout = new HorizontalLayout();
		layout.addComponent(tabsheet);
		setContent(layout);	
		setSizeFull();
		// Kanske layout.setWidth("100%");
		*/
		setContent(tabsheet);	
	}
	
	/*
	private List<GeneralizedEvent> allEventsRows = null;
	private Grid<GeneralizedEvent> allEventsGrid = null;
	 */
	private Component getAllEventsComponent() {
		
		List<DossierEvent> dosEvs = dossier.getReferringDossierEvents();
		dosEvs.forEach(e ->  allEventsRows.add(new GeneralizedDossierEvent(e)));
		
		List<Document> docs = dossier.getReferringDocuments();
		for(Document doc : docs) {
			List<DocumentEvent> docEvs = doc.getReferringDocumentEvents();
			docEvs.forEach(e ->  allEventsRows.add(new GeneralizedDocumentEvent(e)));
		}
		
		Collections.sort(allEventsRows, new Comparator<GeneralizedEvent>() {
			@Override
			public int compare(GeneralizedEvent ge1, GeneralizedEvent ge2) {
				return ge1.time().getTime()-ge2.time().getTime() > 0 ? 1 : -1;
			}
		});
		
		allEventsGrid = new Grid<>();
		allEventsGrid.setWidth("100%");
		allEventsGrid.getEditor().setEnabled(false);
		allEventsGrid.setItems(allEventsRows);
		allEventsGrid.setSelectionMode(SelectionMode.SINGLE);
		allEventsGrid.setSizeFull();

		allEventsGrid.addComponentColumn(row -> {
        	return new Label(row.object());
        }).setCaption("Objekt"); 
		allEventsGrid.addComponentColumn(row -> {
        	return new Label(row.person());
        }).setCaption("Utförare"); 
		allEventsGrid.addComponentColumn(row -> {
        	return new Label(row.story());
        }).setCaption("Händelse"); 
		allEventsGrid.addComponentColumn(row -> {
			Button describeBtn = new Button("Beskriv");
			describeBtn.addClickListener(e -> describe(row));
        	return describeBtn;
        }).setCaption("Beskrivning"); 
		allEventsGrid.addComponentColumn(row -> {
        	return new Label(Adminuser.presentDate(row.time()));
        }).setCaption("Tid"); 

        VerticalLayout out = new VerticalLayout();
        out.addComponent(allEventsGrid);
        return out;
	}
	
	private void describe(GeneralizedEvent row) {
		Notification.show(row.description(), Type.HUMANIZED_MESSAGE);
	}

	private static class GeneralizedDossierEvent implements GeneralizedEvent {
		private DossierEvent event = null;
		public GeneralizedDossierEvent(DossierEvent event) {
			this.event = event;
		}
		@Override
		public String story() {
			return event.getCodeEnum().text();
		}
		@Override
		public String description() {
			return event.getDescription();
		}
		@Override
		public Date time() {
			return event.getTimeofevent();
		}
		@Override
		public String object() {
			return "akt "+event.getDossier().getName();
		}
		@Override
		public String person() {
			Person p = event.getSession().getActor().getAgent().getPerson();
			return p.getFirstname()+" "+p.getFamilyname();
		}
	}

	public static class GeneralizedDocumentEvent implements GeneralizedEvent {
		private DocumentEvent event = null;
		public GeneralizedDocumentEvent(DocumentEvent event) {
			this.event = event;
		}
		@Override
		public String story() {
			return event.getCodeEnum().text();
		}
		@Override
		public String description() {
			return event.getDescription();
		}
		@Override
		public Date time() {
			return event.getTimeofevent();
		}
		@Override
		public String object() {
			return "fil "+event.getDocument().getFilename() ;
		}
		@Override
		public String person() {
			Person p = event.getSession().getActor().getAgent().getPerson();
			return p.getFirstname()+" "+p.getFamilyname();
		}
	}

	private Component getCurrentDocumentsComponent() {
		currentDocumentsGrid = new Grid<>();
		currentDocumentsGrid.setWidth("100%");
		currentDocumentsGrid.getEditor().setEnabled(false);
		currentDocumentsGrid.setItems(currentDocumentsRows);
		currentDocumentsGrid.setSelectionMode(SelectionMode.SINGLE);
		currentDocumentsGrid.setSizeFull();

        currentDocumentsGrid.addComponentColumn(row -> {
			if(configuration_enableDocumentDownloadAmongCurrents()) {
				return new DocumentLink(row);
			}
			else {
				return new Label(row.getFilename());
			}
        }).setCaption("Filnamn"); 
        currentDocumentsGrid.addComponentColumn(row -> {
        	return new Label(Adminuser.presentDate(row.getDateOfReception()));
        }).setCaption("Inkommet"); 
        currentDocumentsGrid.addComponentColumn(row -> {
        	return new Label(row.getMimetype());
        }).setCaption("Mimetyp"); 
        currentDocumentsGrid.addComponentColumn(row -> {
        	Button editBtn = new Button("Redigera");
        	editBtn.addClickListener(e -> editDocument(row));
        	return editBtn;
        }).setCaption("Hantera"); 
        currentDocumentsGrid.addComponentColumn(row -> {
			try {
				return new Label(row.getDossierDoctypeDef().getDossierdoctypename());
			}
			catch(Exception e) {
				return new Label("Typ ej vald");
			}
        }).setCaption("Dokumenttyp"); 
        currentDocumentsGrid.addComponentColumn(row -> {
			Button attributes = new Button("Se/redigera");
			attributes.addClickListener(e -> editAttributes(row));
			return attributes;
        }).setCaption("Egenskaper"); 
        currentDocumentsGrid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox();
        	box.setValue(row.isActive());
        	box.setEnabled(configuration_enableActivationOnCurrentTab());
        	box.addValueChangeListener(event ->  {
        		String activation = "Aktivstatus före "+row.isActive();
        		row.setActive(box.getValue());
        		row.save();
        		activation += ", efter "+row.isActive();
        		DocumentEvent dev = new DocumentEvent();
        		dev.setCodeEnum(row.isActive() ? DocumentEventType.ACTIVATED_GUI : DocumentEventType.INACTIVATED_GUI);
        		dev.setSysteminfo(activation);
        		dev.setDocument(row);
        		dev.setSessionId(session.getId());
        		dev.setDescription("Ändrade aktivstatus för dokumentent");
        		dev.save();
        		GeneralizedDocumentEvent gde = new GeneralizedDocumentEvent(dev);
        		allEventsRows.add(gde);
        		allEventsGrid.getDataProvider().refreshAll();
        		if(!row.isActive()) {
        			currentDocumentsRows.remove(row);
        			currentDocumentsGrid.getDataProvider().refreshAll();
        		}
        	});
            return box;
        }).setCaption("Aktiv");
        VerticalLayout out = new VerticalLayout();
        HorizontalLayout buttons = new HorizontalLayout();
        Button newDocument = new Button("Nytt dokument");
        newDocument.setEnabled(configuration_enableNewDocumentOnCurrentTab());
        buttons.addComponent(newDocument);
        newDocument.addClickListener(e -> newDocument());
        if(onClose != null) {
            Button backBtn = new Button("Åter");
            buttons.addComponent(backBtn);
            backBtn.addClickListener(e -> backAction());
        }
        out.addComponent(buttons);
        out.addComponent(currentDocumentsGrid);
        return out;
	}

	private void editDocument(Document document) {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DocumentFormEdit(document, allEventsRows, allEventsGrid, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierView.this);
	    		currentDocumentsGrid.getDataProvider().refreshAll();
		    	allDocumentsGrid.getDataProvider().refreshAll();
			}
		}));
	}

	private void backAction() {
		onClose.run();
	}

	private boolean configuration_enableDocumentDownloadAmongCurrents() {
		return rules.enableDocumentDownloadAmongCurrents();
	}

	private boolean configuration_enableActivationOnCurrentTab() {
		return rules.enableActivationOnCurrentTab();
	}

	private boolean configuration_enableActivationOnAllTab() {
		return rules.enableActivationOnAllTab();
	}

	private boolean configuration_enableNewDocumentOnCurrentTab() {
		return rules.enableNewDocumentOnCurrentTab();
	}
	
	private boolean configuration_enableDocumentDownloadAmongAll() {
		return rules.enableDocumentDownloadAmongAll();
	}

	private Component getAllDocumentsComponent() {
		allDocumentsGrid = new Grid<>();
		allDocumentsGrid.setWidth("100%");
		allDocumentsGrid.getEditor().setEnabled(false);
		allDocumentsGrid.setItems(allDocumentsRows);
		allDocumentsGrid.setSelectionMode(SelectionMode.SINGLE);
		allDocumentsGrid.setSizeFull();

		allDocumentsGrid.addComponentColumn(row -> {
			if(configuration_enableDocumentDownloadAmongAll()) {
				return new DocumentLink(row);
			}
			else {
				return new Label(row.getFilename());
			}
        }).setCaption("Filnamn"); 
		allDocumentsGrid.addComponentColumn(row -> {
        	return new Label(row.getMimetype());
        }).setCaption("Mimetyp"); 
		allDocumentsGrid.addComponentColumn(row -> {
			try {
				return new Label(row.getDossierDoctypeDef().getDossierdoctypename());
			}
			catch(Exception e) {
				return new Label("Typ ej vald");
			}
        }).setCaption("Dokumenttyp"); 
		allDocumentsGrid.addComponentColumn(row -> {
			Button attributes = new Button("Se/redigera");
			attributes.addClickListener(e -> editAttributes(row));
			return attributes;
        }).setCaption("Egenskaper"); 
		allDocumentsGrid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox();
        	box.setValue(row.isActive());
        	box.setEnabled(configuration_enableActivationOnAllTab());
        	box.addValueChangeListener(event ->  {
        		String activation = "Aktivstatus före "+row.isActive();
        		row.setActive(box.getValue());
        		row.save();
        		activation += ", efter "+row.isActive();
        		DocumentEvent dev = new DocumentEvent();
        		dev.setCodeEnum(row.isActive() ? DocumentEventType.ACTIVATED_GUI : DocumentEventType.INACTIVATED_GUI);
        		dev.setSysteminfo(activation);
        		dev.setDocument(row);
        		dev.setSessionId(session.getId());
        		dev.setDescription("Ändrade aktivstatus för dokumentet");
        		dev.save();
        		GeneralizedDocumentEvent gde = new GeneralizedDocumentEvent(dev);
        		allEventsRows.add(gde);
        		allEventsGrid.getDataProvider().refreshAll();
        		if(!row.isActive()) {
        			currentDocumentsRows.remove(row);
        		}
        		else {
        			currentDocumentsRows.add(row);
        		}
    			currentDocumentsGrid.getDataProvider().refreshAll();
        	});
            return box;
        }).setCaption("Aktiv");
		return allDocumentsGrid;
	}

	private void editAttributes(Document row) {
		DocumentAttributeEdit ofe = new DocumentAttributeEdit(row, allEventsRows, allEventsGrid, new Runnable() {
			@Override
			public void run() {
				((SimplifiedContentUI) UI.getCurrent()).setWorkContent(DossierView.this);
			}
		});
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(ofe);
	}

	private void newDocument() {
		final Document document = new Document();
		document.setDossier(dossier);
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DocumentFormEdit(document, allEventsRows, allEventsGrid, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierView.this);
	    		if(document.getId() != null) {
	    			currentDocumentsRows.add(document);
	    			currentDocumentsGrid.getDataProvider().refreshAll();
	    			allDocumentsRows.add(document);
		    		allDocumentsGrid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("NEW ");
	}
	
}
