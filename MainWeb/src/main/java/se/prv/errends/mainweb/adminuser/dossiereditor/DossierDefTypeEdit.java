package se.prv.errends.mainweb.adminuser.dossiereditor;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierDoctypeDef;

public class DossierDefTypeEdit extends Window {
	
	private Runnable onClose = null;
	private DossierDoctypeDef dossierdoctypedef = null;
	private TextField name = null;
	
	public DossierDefTypeEdit(DossierDoctypeDef dossierdoctypedef, Runnable onClose) {
		this.onClose = onClose;
		this.dossierdoctypedef = dossierdoctypedef;
		setModal(true);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		name = new TextField("Namn");
		name.setValue(dossierdoctypedef.getDossierdoctypename() != null ? dossierdoctypedef.getDossierdoctypename() : "");
		content.addComponent(name);
		
		HorizontalLayout buttons = new HorizontalLayout();
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(e -> cancel());
		buttons.addComponent(cancel);
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(e -> finish());	
		buttons.addComponent(finishBtn);
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		DossierDef dossierdef = dossierdoctypedef.getDossierDef();
		dossierdef.save();
		dossierdoctypedef.setDossierdef(dossierdef);
		dossierdoctypedef.setDossierdoctypename(name.getValue());
		dossierdoctypedef.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
