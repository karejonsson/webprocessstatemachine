package se.prv.errends.mainweb.orguser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification.Type;

import se.prv.errends.domain.Agent;
import se.prv.errends.domain.OrganisationGroupMember;
import se.prv.errends.domain.OrganisationRoleGroup;
import se.prv.errends.domain.Person;

public class ManageAgentsInGroup extends Panel {
	
	private OrganisationRoleGroup group;
	private Runnable onClose;
	private List<OrganisationGroupMember> memberships;

	private VerticalLayout layout = new VerticalLayout();
	private List<Agent> rows = null;
	private Grid<Agent> grid = null;
	
	private boolean isMember(Agent agent) {
		return getMembership(agent) != null;
	}
	
	private OrganisationGroupMember getMembership(Agent agent) {
		for(OrganisationGroupMember membership : memberships) {
			if(membership.getAgentId().equals(agent.getId())) {
				return membership;
			}
		}
		return null;
	}

	public ManageAgentsInGroup(OrganisationRoleGroup group, Runnable onClose) {
		this.group = group;
		this.onClose = onClose;
		
		memberships = group.getReferringOrganisationGroupMembers();
		rows = group.getOrganisation().getReferringAgents();
		
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Person p = row.getPerson();
        	return new Label(p.getFirstname()+" "+p.getFamilyname());
        }).setCaption("Namn"); 

        grid.addComponentColumn(row -> {
        	final CheckBox box = new CheckBox();
        	box.setValue(isMember(row));
        	box.setEnabled(true);
        	box.addValueChangeListener(event ->  {
        		updateMembership(box, row);
        	});
            return box;
        }).setCaption("Medlemsstatus");

		grid.getEditor().setEnabled(false);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();

		HorizontalLayout about = new HorizontalLayout();
		if(onClose != null) {
			Button closeBtn = new Button("Klar");
			closeBtn.addClickListener(e -> onClose.run());
			about.addComponent(closeBtn);
		}
		about.addComponent(new Label("Ombud i gruppen "+group.getName()+" för organisationen "+group.getOrganisation().getOrgname()));
		layout.addComponent(about);
		layout.addComponent(grid);
		setContent(layout);
	}
	
	private void updateMembership(CheckBox box, Agent agent) {
		if(box.getValue()) {
			// Aktivera medlemsskap
			OrganisationGroupMember membership = getMembership(agent);
			if(membership != null) {
				Notification.show("Internt fel. Medlemsskapet finns redan och kan inte skapas", Type.ERROR_MESSAGE);
			}
			else {
				membership = new OrganisationGroupMember();
				membership.setAgentId(agent.getId());
				membership.setRolegroupId(group.getId());
				membership.save();
			}
		}
		else {
			// Ta bort medlemsskap
			OrganisationGroupMember membership = getMembership(agent);
			if(membership == null) {
				Notification.show("Internt fel. Medlemsskapet finns inte och kan inte tas bort", Type.ERROR_MESSAGE);
			}
			else {
				membership.delete();
			}
		}
	}
	
}

