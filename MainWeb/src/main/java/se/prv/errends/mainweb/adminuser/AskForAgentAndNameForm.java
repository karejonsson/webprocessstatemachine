package se.prv.errends.mainweb.adminuser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Processdef;

public class AskForAgentAndNameForm extends Window {
	
	private NotifyAboutAgentAndName notify = null;
	private Runnable onClose = null;
	private List<Agent> allowedAgents = null;
	private List<Processdef> allowedProcessDefs = null;
	private List<AgentListItem> agentsInCombo = null;
	private List<ProcessdefListItem> processdefsInCombo = null;
	private ComboBox<AgentListItem> choseAgentComponent = null;
	private ComboBox<ProcessdefListItem> choseProcessdefComponent = null;

	private TextField name = null;
	
	public AskForAgentAndNameForm(List<Processdef> allowedProcessDefs, List<Agent> allowedAgents, NotifyAboutAgentAndName notify, Runnable onClose) {
		this.allowedAgents = allowedAgents;
		this.allowedProcessDefs = allowedProcessDefs;
		this.notify = notify;
		this.onClose = onClose;
		
		agentsInCombo = new ArrayList<AgentListItem>();
		for(Agent a : allowedAgents) {
			agentsInCombo.add(new AgentListItem(a));
		}
		
		processdefsInCombo = new ArrayList<ProcessdefListItem>();
		for(Processdef pd : allowedProcessDefs) {
			processdefsInCombo.add(new ProcessdefListItem(pd));
		}
		
		setModal(true);
		
		setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.7)+"px");
		
		FormLayout content = new FormLayout();
		name = new TextField("Namn");
		name.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.5)+"px");
		content.addComponent(name);
		
		choseAgentComponent = new ComboBox();
		choseAgentComponent.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.5)+"px");
		choseAgentComponent.setItems(agentsInCombo);
		choseAgentComponent.setSelectedItem(agentsInCombo.get(0));
		content.addComponent(choseAgentComponent);
		
		choseProcessdefComponent = new ComboBox();
		choseProcessdefComponent.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.5)+"px");
		choseProcessdefComponent.setItems(processdefsInCombo);
		choseProcessdefComponent.setSelectedItem(processdefsInCombo.get(0));
		content.addComponent(choseProcessdefComponent);
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Avbryt");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancelBtn);
		Button okBtn = new Button("OK");
		okBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				ok();
			}
			
		});
		buttons.addComponent(okBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	public interface NotifyAboutAgentAndName {
		public void agentAndName(Processdef pf, Agent agent, String name);
	}
	
	private void ok() {
		if(notify != null) {
			Optional<AgentListItem> opa = choseAgentComponent.getSelectedItem();
			if(opa == null || !opa.isPresent() || opa.get() == null) {
				Notification.show("Val av agent tvunget", Type.ERROR_MESSAGE);
				return;
			}
			opa = choseAgentComponent.getSelectedItem();
			AgentListItem ali = opa.get();
			Agent agent = ali.getAgent();

			Optional<ProcessdefListItem> opp = choseProcessdefComponent.getSelectedItem();
			if(opp == null || !opp.isPresent() || opp.get() == null) {
				Notification.show("Val av process tvunget", Type.ERROR_MESSAGE);
				return;
			}
			opp = choseProcessdefComponent.getSelectedItem();
			ProcessdefListItem pdli = opp.get();
			Processdef processdef = pdli.getProcessdef();

			notify.agentAndName(processdef, agent, name.getValue());
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private static class AgentListItem {
		private Agent agent = null;
		public AgentListItem(Agent agent) {
			this.agent = agent;
		}
		public String toString() {
			return agent.getOrganisation().getOrgname()+" / "+agent.getPerson().getFirstname()+" "+agent.getPerson().getFamilyname();
		}
		public Agent getAgent() {
			return agent;
		}
	}
	
	private static class ProcessdefListItem {
		private Processdef processdef = null;
		public ProcessdefListItem(Processdef processdef) {
			this.processdef = processdef;
		}
		public String toString() {
			return processdef.getProcessname()+"/"+processdef.getDescription();
		}
		public Processdef getProcessdef() {
			return processdef;
		}
	}
	
}
