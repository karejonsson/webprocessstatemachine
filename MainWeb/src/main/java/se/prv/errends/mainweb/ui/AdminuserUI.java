package se.prv.errends.mainweb.ui;

import java.util.Date;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.MenuBar.MenuItem;

import general.reuse.database.parameters.domain.PersistentValue;
import general.reuse.vaadinreusable.parameters.PersistentValuePersister;
import general.reuse.vaadinreusable.parameters.PersistentValueView;
import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.adminuser.ActiveErrendsView;
import se.prv.errends.mainweb.adminuser.AdminuserErrendsNotInitiatedView;
import se.prv.errends.mainweb.adminuser.AdminuserLoginPage;
import se.prv.errends.mainweb.adminuser.AdminuserView;
import se.prv.errends.mainweb.adminuser.FunctionDefView;
import se.prv.errends.mainweb.adminuser.OrganizationUserView;
import se.prv.errends.mainweb.adminuser.OrganizationView;
import se.prv.errends.mainweb.adminuser.ProcessdefView;
import se.prv.errends.mainweb.adminuser.ProcessfileView;
import se.prv.errends.mainweb.adminuser.ReportTemplateView;
import se.prv.errends.mainweb.adminuser.SchedulationControlView;
import se.prv.errends.mainweb.adminuser.StateExpirationView;
import se.prv.errends.mainweb.adminuser.VerifyConfigurationSyntax;
import se.prv.errends.mainweb.adminuser.dossiereditor.DossierDefListView;
import se.prv.errends.mainweb.person.ActorView;
import se.prv.errends.mainweb.person.DossierListView;
import se.prv.errends.mainweb.person.PersonFormEdit;
import se.prv.errends.mainweb.person.PersonView;
import se.prv.errends.mainweb.person.SessionView;
import se.prv.errends.mgmt.EmailManagement;
import se.prv.errends.mgmt.PersonManagement;
import se.prv.errends.processengine.reuse.StringFileManipulation;

@SpringBootApplication
@SpringUI(path="/admin")
public class AdminuserUI extends SimplifiedContentUI implements CommonUI {
	
	private VaadinRequest request;
	
	public VaadinRequest getRequest() {
		return request;
	}

	@Override
	protected void init(VaadinRequest request) {
		this.request = request;
		setContent(new AdminuserLoginPage());
	}
	
	private MenuItem system = null;
	private MenuItem personmenu = null;
	private Person person = null;
	
	public AdminuserUI() {
	}
	
	public void setup(Component c) {
		person = (Person) getSession().getAttribute(CommonUI.personlookup);
		super.setup(c);
	}	
	
    public void setupGeneralMenubar() {
		if(system == null) {
			Adminuser au = (Adminuser) getSession().getAttribute(CommonUI.adminuserlookup);
			system = menubar.addItem("System", null);
			MenuItem errends = system.addItem("Ärenden", null);
			MenuItem processfiles = errends.addItem("Processflöden", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ProcessfileView());
	            }  
	        });
			MenuItem errendtypes = errends.addItem("Ärendetyper", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ProcessdefView());
	            }  
	        });
			MenuItem dossiertypes = errends.addItem("Akttyper", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new DossierDefListView());
	            }  
	        });
			
			MenuItem errendinitation = errends.addItem("Ärendeinitiering", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new AdminuserErrendsNotInitiatedView());
	            }  
	        });
			MenuItem activeerrends = errends.addItem("Aktiva", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ActiveErrendsView());
	            }  
	        });
			MenuItem respites = errends.addItem("Löpande frister", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new StateExpirationView());
	            }  
	        });
			
			
			MenuItem reporttemplate = errends.addItem("Rapportmallar", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new ReportTemplateView());
	            }  
	        });		
			
			MenuItem settings = system.addItem("Inställningar", null);
			MenuItem parameters = settings.addItem("Parametrar", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	PersistentValuePersister pvp = new PersistentValuePersister() {
						@Override
						public List<PersistentValue> getAllPersistentValues() throws Exception {
							return PersistentValuesDB.getAllPersistentValues();
						}
						@Override
						public void setStringValue(String key, String value) throws Exception {
							PersistentValuesDB.setStringValue(key, value);
						}
						@Override
						public void removeValue(String key) throws Exception {
							PersistentValuesDB.removeValue(key);
						}
	            	};
	            	setWorkContent(new PersistentValueView(pvp));
	            }  
	        });
			MenuItem functions = settings.addItem("Funktioner", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new FunctionDefView());
	            }  
	        });
			MenuItem deamonscheduling = settings.addItem("Demonschedulering", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		    		((AdminuserUI) UI.getCurrent()).openSubwindow(new SchedulationControlView(null));
	            }  
	        });
			MenuItem accounts = system.addItem("Konton", null);
			MenuItem persons = accounts.addItem("Personregister", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new PersonView());
	            }  
	        });
			MenuItem systemadministrators = accounts.addItem("Systemadministratörer", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new AdminuserView(au));
	            }  
	        });
			MenuItem orgadministrators = accounts.addItem("Organisationsadministratörer", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new OrganizationUserView());
	            }  
	        });
			MenuItem orgs = accounts.addItem("Organisationer", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new OrganizationView());
	            }  
	        });
			MenuItem tools = system.addItem("Verktyg", null);
			MenuItem syntaxcheck = tools.addItem("Syntaxkontroll", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new VerifyConfigurationSyntax());
	            }  
	        });
			MenuItem testmail = tools.addItem("Testa e-post", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	EmailManagement.sendTestMail();;
	            }  
	        });
			MenuItem version = tools.addItem("Version", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	displayCurrentVersion();
	            }  
	        });
			MenuItem logout = system.addItem("Logga ut", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	logout();
	        		//setContent(new PersonLoginPage());
	            }  
	        });
		}
    }

    private void logout() {
    	Session sess = (Session) getSession().getAttribute(CommonUI.sessionlookup);
    	sess.setFinish(new Date(System.currentTimeMillis()));
    	sess.save();
      	Page.getCurrent().setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath()+"/admin");
    	VaadinSession.getCurrent().close();
    }
    
    private void displayCurrentVersion() {
    	String version = "";
    	try {
    		version = StringFileManipulation.readFullyToString(getClass().getClassLoader().getResourceAsStream("version.txt"));
    	}
    	catch(Exception e) {
    	}
    	String datetime = "";
    	try {
    		datetime = StringFileManipulation.readFullyToString(getClass().getClassLoader().getResourceAsStream("datetime.txt"));
    	}
    	catch(Exception e) {
    	}
    	Notification.show(version+"\n"+datetime, Type.HUMANIZED_MESSAGE); 	
    }
    
    public void setupParticularMenubar() {
		if(personmenu == null) {
			//Person p = (Person) getSession().getAttribute(personlookup);
			personmenu = menubar.addItem("Användare", null);
			MenuItem personinfo = personmenu.addItem("Personuppgifter", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		    		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new PersonFormEdit(person, null));
	            }  
	        });
			MenuItem processfiles = personmenu.addItem("Aktörer", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	//System.out.println(""+person);
					Person person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
					List<Actor> actors = ActorDB.getAllActiveActorsOfPersonId(person.getId());
	            	setWorkContent(new ActorView(actors));
	            }  
	        });
			MenuItem sessions = personmenu.addItem("Sessioner", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new SessionView());
	            }  
	        });
			MenuItem dossiers = personmenu.addItem("Akter", new MenuBar.Command() {
	            private static final long serialVersionUID = 4483012525105015694L;
	            public void menuSelected(MenuItem selectedItem) {
	            	if(!PersonManagement.personalDataComplete(person)) {
						Notification.show("Skriv in personuppgifter först.", Notification.Type.ERROR_MESSAGE);
						return;
	            	}
	            	setWorkContent(new DossierListView());
	            }  
	        });
		}
    }

}
