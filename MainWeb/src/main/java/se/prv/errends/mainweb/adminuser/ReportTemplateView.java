package se.prv.errends.mainweb.adminuser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import net.sf.jasperreports.engine.JRException;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ReportTemplateDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.mainweb.adminuser.processeditor.StatedefFormEdit;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class ReportTemplateView extends Panel {
	
	private List<ReportTemplate> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<ReportTemplate> grid = null;
	private Adminuser adminuser = null;

	public ReportTemplateView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = ReportTemplateDB.getAllReportTemplates();

		grid = new Grid<>();

        grid.addColumn(ReportTemplate::getFilename).setCaption("Filnamn");
        grid.addColumn(ReportTemplate::getReporttitle).setCaption("Titel").setEditorComponent(new TextField(), ReportTemplate::setReporttitle);
        grid.addColumn(ReportTemplate::getDescription).setCaption("Beskrivning").setEditorComponent(new TextField(), ReportTemplate::setDescription);
		grid.getEditor().setEnabled(true);

		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
		      Button editBtn = new Button("Redigera");
		      editBtn.addClickListener(click -> edit(row));
		      out.addComponent(editBtn);
		      Button deleteBtn = new Button("Radera");
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      if(adminuser.isSuperprivilegies()) {
			      Button deleteRecursivelyBtn = new Button("Radera rekursivt");
			      deleteRecursivelyBtn.addClickListener(click -> deleteRecursively(row));
			      out.addComponent(deleteRecursivelyBtn);
		      }
		      return out;
		}).setCaption("Hantering");
		
		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();

		Button saveBtn = new Button("Spara");
		saveBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}
		});
		
		buttons.addComponent(saveBtn);
		
		Button newBtn = new Button("Ny");
		newBtn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				newReportTemplate();
			}
		});
		
		buttons.addComponent(newBtn);
		
		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void delete(ReportTemplate rt) {
		if(rt.getReferringReportTemplateReuses().size() != 0) {
			Notification.show("Kan ej radera pga andra processers återanvändning", Notification.Type.ERROR_MESSAGE);
			return;
		}
		rt.delete();
		rows.remove(rt);
		grid.getDataProvider().refreshAll();
		//System.out.println("delete "+pf);
	}
	
	private void edit(ReportTemplate rt) {
		((AdminuserUI) UI.getCurrent()).openSubwindow(new ReportTemplateParameterForm(rt, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ReportTemplateView.this);
			}
		}));
	}
	
	private void deleteRecursively(ReportTemplate pf) {
		pf.deleteRecursively();
		rows.remove(pf);
		grid.getDataProvider().refreshAll();
	}
	
	public void save() {
		for(ReportTemplate pf : rows) {
			String reporttitle = pf.getReporttitle();
			if(reporttitle == null || reporttitle.trim().length() == 0) {
				String filename = pf.getFilename();
				reporttitle = filename.substring(0, filename.indexOf("."));
				pf.setReporttitle(reporttitle);
			}
			String descr = pf.getDescription();
			if(descr == null || descr.trim().length() == 0) {
				pf.setDescription(reporttitle);
			}
			pf.save();
		}
	}
	
	public void newReportTemplate() {
		ReportTemplateFormNew npff = new ReportTemplateFormNew(this, new Runnable() {
			@Override
			public void run() {
				((AdminuserUI) UI.getCurrent()).setup(ReportTemplateView.this);
			}
		});
		((AdminuserUI) UI.getCurrent()).openSubwindow(npff);
	}
	
	public void notifyAboutNewReportTemplate(ReportTemplate pf) {
		rows.add(pf);
		//System.out.println("ProcessFileView.notifyAboutNewProcessfile : ID = "+pf.getId());
		grid.getDataProvider().refreshAll();
	}
	
}
