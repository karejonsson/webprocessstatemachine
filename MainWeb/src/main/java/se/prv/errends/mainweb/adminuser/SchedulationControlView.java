package se.prv.errends.mainweb.adminuser;

import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.PersistentValuesDB;
import se.prv.errends.mgmt.ManageableJob;
import se.prv.errends.mgmt.SchedulationJobWrapper;
import se.prv.errends.mgmt.SchedulerManagement;

public class SchedulationControlView extends Window {
	
	private Runnable onClose = null;
	private Map<String, TextField> cronschemas = new HashMap<String, TextField>();
	private Button stopBtn = null;
	private Button startBtn = null;
	
	public SchedulationControlView(Runnable onClose) {
		this.onClose = onClose;
		setModal(true);
		//person = (Person) UI.getCurrent().getSession().getAttribute(MainwebUI.personlookup);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		for(SchedulationJobWrapper wrapper : SchedulerManagement.jobs) {
			TextField cronschema = new TextField(wrapper.getTheme());
			cronschemas.put(wrapper.getTheme(), cronschema);
			try {
				cronschema.setValue(PersistentValuesDB.getStringValue(wrapper.getCronSchemaProperty()));
			} catch (Exception e1) {
				if(onClose != null) {
					onClose.run();
				}
				close();
			}
			content.addComponent(cronschema);
			Button runJobBtn = new Button("Run");
			runJobBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
				@Override
				public void buttonClick(ClickEvent event) {
					runJob(wrapper);
				}
			});
			content.addComponent(runJobBtn);
		}
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		stopBtn = new Button("Stop");
		stopBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					SchedulerManagement.stop();
					stopBtn.setEnabled(SchedulerManagement.isActive());
					startBtn.setEnabled(!SchedulerManagement.isActive());
				} catch (SchedulerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		buttons.addComponent(stopBtn);
		
		startBtn = new Button("Start");
		startBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					for(SchedulationJobWrapper wrapper : SchedulerManagement.jobs) {
						TextField cronschema = cronschemas.get(wrapper.getTheme());
						PersistentValuesDB.setStringValue(wrapper.getCronSchemaProperty(), cronschema.getValue());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SchedulerManagement.initialize();
				stopBtn.setEnabled(SchedulerManagement.isActive());
				startBtn.setEnabled(!SchedulerManagement.isActive());
			}
			
		});
		buttons.addComponent(startBtn);

		stopBtn.setEnabled(SchedulerManagement.isActive());
		startBtn.setEnabled(!SchedulerManagement.isActive());
		
		Button cancelBtn = new Button("Stäng");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				closeDialog();
			}
			
		});
		buttons.addComponent(cancelBtn);
		
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				finish();
			}
			
		});
		buttons.addComponent(finishBtn);
		
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void closeDialog() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		try {
			for(SchedulationJobWrapper wrapper : SchedulerManagement.jobs) {
				TextField cronschema = cronschemas.get(wrapper.getTheme());
				PersistentValuesDB.setStringValue(wrapper.getCronSchemaProperty(), cronschema.getValue());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void runJob(SchedulationJobWrapper wrapper) {
		Class clazz = wrapper.getJobClass();
		System.out.println("SchedulationControlView: class name "+clazz.getName());
		Object obj = null;
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return;
		}
		if(!(obj instanceof ManageableJob)) {
			Notification.show("Exekveringsklassen är inte rätt typad", Type.ERROR_MESSAGE);
			return;
		}
		ManageableJob job = (ManageableJob) obj;
		try {
			job.execute();
		} catch (JobExecutionException e) {
			Notification.show("Jobbet är inte körbart", Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

}
