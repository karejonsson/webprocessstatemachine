package se.prv.errends.mainweb.adminuser;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.AdminuserDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.SessionManagement;

public class AdminuserLoginPage extends VerticalLayout implements View {
	private static final long serialVersionUID = 1L;

	public AdminuserLoginPage() {
		Panel panel = new Panel("Admin Login");
		panel.setSizeUndefined();
		addComponent(panel);
		
		FormLayout content = new FormLayout();
		TextField username = new TextField("Username");
		content.addComponent(username);
		//username.setValue("kare");
		PasswordField password = new PasswordField("Password");
		//password.setValue("super");
		content.addComponent(password);

		Button send = new Button("Enter");
		send.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if(credentialsConfirmed(username.getValue(), password.getValue())) {
					((AdminuserUI) UI.getCurrent()).setup(new ProcessfileView());
				}
				else {
					Notification.show("Felaktiga behörigheter", Notification.Type.ERROR_MESSAGE);
				}
			}
			
		});
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	private boolean credentialsConfirmed(String username, String password) {
		Adminuser adminuser = AdminuserDB.getAdminuserFromUsername(username);
		if(adminuser == null) {
			//System.out.println("Adminuser null");
			return false;
		}
		if(!adminuser.isActive()) {
			//System.out.println("Adminuser ej aktiv");
			return false;
		}
		String pwd = null;
		//System.out.println("Lösenord klartext "+password);
		try {
			pwd = Adminuser.calculateChecksum(password);
		}
		catch(Exception e) {
			//System.out.println("Misslyckades med checksumma");
			return false;
		}
		if(!pwd.equals(adminuser.getPassword())) {
			//System.out.println("Olika. Skriver "+pwd+", i databasen "+adminuser.getPassword());
			return false;
		}
		
		Session sess = SessionManagement.getErrendlessSession(adminuser.getPerson());
		UI.getCurrent().getSession().setAttribute(CommonUI.sessionlookup, sess);
		UI.getCurrent().getSession().setAttribute(CommonUI.adminuserlookup, adminuser);
		UI.getCurrent().getSession().setAttribute(CommonUI.personlookup, adminuser.getPerson());
		return true;
	}
	
}
