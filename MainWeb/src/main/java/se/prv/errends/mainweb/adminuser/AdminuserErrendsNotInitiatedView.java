package se.prv.errends.mainweb.adminuser;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.ErrendDB;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class AdminuserErrendsNotInitiatedView extends Panel {
	
	private List<Errend> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Errend> grid = null;
	private Adminuser adminuser = null;

	public AdminuserErrendsNotInitiatedView() {
		adminuser = (Adminuser) UI.getCurrent().getSession().getAttribute(CommonUI.adminuserlookup);
		rows = ErrendDB.getAllNotInitiatedErrends();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	if(row.getAdminusername() == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getAdminusername());
        	}
        }).setCaption("Benämning"); 
        grid.addComponentColumn(row -> {
        	if(row.getPersonname() == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.getPersonname());
        	}
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	Date date = row.getCreated();
        	Label out = new Label(Adminuser.presentDate(date));
        	return out;
        }).setCaption("Skapat"); 
		grid.addComponentColumn(row -> {
        	Layout out = new HorizontalLayout();
	        Button initBtn = new Button("Initiera");
	        initBtn.addClickListener(click -> goToErrend(row));
	        out.addComponent(initBtn);
	        if(adminuser.isSuperprivilegies()) {
		        Button deleteBtn = new Button("Radera");
		        deleteBtn.addClickListener(click -> removeErrend(row));
		        out.addComponent(deleteBtn);
	        }
	        return out;
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		
		setContent(layout);
	}

	private void goToErrend(Errend row) {
		((AdminuserUI) UI.getCurrent()).setup(new AdminuserSetupErrendRoles(
				row,
				new Runnable() {
					@Override
					public void run() {
						((AdminuserUI) UI.getCurrent()).setWorkContent(new AdminuserErrendsNotInitiatedView());
					}
				}
				));
		//System.out.println("Initiera ärende "+row);
	}
	
	private void removeErrend(Errend errend) {
		errend.deleteRecursively();
		rows.remove(errend);
		grid.getDataProvider().refreshAll();		
	}
	
}
