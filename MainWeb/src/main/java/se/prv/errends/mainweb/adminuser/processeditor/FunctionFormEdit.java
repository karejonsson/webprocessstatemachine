package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.FunctionDef;
import se.prv.errends.producerconfig.parse.PrvErrendProducersConfigParser;
import se.prv.errends.producerconfig.struct.ProducerConfiguration;

public class FunctionFormEdit extends Window {
	
	private Runnable onClose = null;
	//private FunctionRowHolder frh = null;
	
	private TextField functionname = null;
	private TextArea layout = null;
	
	private FunctionDef functiondef = null;

	public FunctionFormEdit(FunctionDef functiondef, String name/*FunctionRowHolder frh*/, Runnable onClose) {
		//this.frh = frh;
		this.onClose = onClose;
		//functiondef = frh.getCurrentFunctionDef();
		//functiondef = frh.getCurrentFunctionDef();
		this.functiondef = functiondef;
		if(functiondef == null) {
			this.functiondef = new FunctionDef();
		}
		setModal(true);

		FormLayout content = new FormLayout();
		content.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");
		content.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.8)+"px");
		
		functionname = new TextField("Namn");
		functionname.setValue(name != null ? name : "");
		content.addComponent(functionname);

		layout = new TextArea("Specifikation");
		String source = this.functiondef.getSource();
		if(source == null) {
			source = "";
		}
		layout.setValue(source);
		layout.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		layout.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.6)+"px");
		content.addComponent(layout);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Avbryt");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}	
		});
		buttons.addComponent(cancelBtn);
		
		Button verifyBtn = new Button("Verifiera");
		verifyBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				verifyLayout();
			}		
		});
		buttons.addComponent(verifyBtn);
		
		Button saveBtn = new Button("Uppdatera");
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}			
		});
		buttons.addComponent(saveBtn);
		
		content.addComponent(buttons);
		setContent(content);
	}
	
	private ProducerConfiguration getConfigForOneProducer() throws Exception {
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(layout.getValue().getBytes()));
    	List<ProducerConfiguration> pcl = PrvErrendProducersConfigParser.parseProducerConfig(isr, "Edited function definition");
    	if(pcl.size() == 1) {
			return pcl.get(0);
    	}
    	return null;
	}

	protected void verifyLayout() {
        try {
        	ProducerConfiguration pc = getConfigForOneProducer();
        	if(pc != null) {
        		if(pc.getName().equals(functionname.getValue())) {
        			Notification.show("Rätt", Notification.Type.HUMANIZED_MESSAGE);        			
        		}
        		else {
        			Notification.show("Rätt syntaktiskt men namnet är fel.\nSkall vara som ovanför.", Notification.Type.WARNING_MESSAGE);        			
        		}
        	}
        	else {
    			Notification.show("Fel! Rätt syntax men fel antal functioner. Endast en får förekomma.", Notification.Type.ERROR_MESSAGE);
        	}
        }
        catch(Throwable e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
	}

	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void save() {
		ProducerConfiguration pc = null;
        try {
        	pc = getConfigForOneProducer();
        	if(pc == null) {
    			Notification.show("Fel! Rätt syntax men fel antal functioner.\nEndast en får förekomma.", Notification.Type.ERROR_MESSAGE);
    			return;
        	}
        	if(!pc.getName().equals(functionname.getValue())) {
    			Notification.show("Rätt syntaktiskt men namnet är fel.\nSkall vara som ovanför.", Notification.Type.ERROR_MESSAGE);
    			return;
        	}
        }
        catch(Throwable e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
			return;
        }
		
		functiondef.setFunctionname(pc.getName());
		functiondef.setSource(layout.getValue());
		functiondef.setTypename(pc.getTypename());
		functiondef.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
