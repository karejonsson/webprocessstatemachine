package se.prv.errends.mainweb.errend;

import java.util.List;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.dbc.SessionDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;

public class SessionsInErrendView extends Panel {
	
	private List<Session> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Session> grid = null;

	private Errend errend = null;
	//private Session session = null;
	private ErrendInStateView eisv = null;
	
	public SessionsInErrendView(Errend errend, ErrendInStateView eisv) {
		this.errend = errend;
		this.eisv = eisv;
		//session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.authenticated);
		rows = SessionDB.getAllSessionsOfErrend(errend.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	Label out;
			try {
				out = new Label(Adminuser.presentDate(row.getStart()));
			} catch (Exception e) {
				out = new Label(""+row.getStart());
				e.printStackTrace();
			}
            return out;
        }).setCaption("Start");
        grid.addComponentColumn(row -> {
        	Label out = new Label(Adminuser.presentDate(row.getFinish()));
            return out;
        }).setCaption("Slut");
        /*
        grid.addComponentColumn(row -> {
        	Label out = new Label(row.getActor().getErrend().getAdminusername());
            return out;
        }).setCaption("Formellt namn");
        grid.addComponentColumn(row -> {
        	Label out = new Label(row.getActor().getErrend().getPersonname());
            return out;
        }).setCaption("Eget namn");
        */
        grid.addComponentColumn(row -> {
        	Label out = new Label(row.getActor().getRoledef().getRolename());
            return out;
        }).setCaption("Roll");
        grid.addComponentColumn(row -> {
        	Actor actor = row.getActor();
        	Agent agent = actor.getAgent();
        	Person p = agent.getPerson();
        	Organisation organization = agent.getOrganisation();
        	String orgpart = "";
        	if(organization != null) {
        		orgpart = organization.getOrgname()+" / ";
        	}
        	String rolepart = "";
        	Roledef roledef = actor.getRoledef();
        	if(roledef != null) {
        		rolepart = roledef+" / ";
        	}
        	Label out = new Label(orgpart+rolepart+p.getFirstname()+" "+p.getFamilyname());
            return out;
        }).setCaption("Ombud");

		grid.getEditor().setEnabled(true);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		setContent(layout);
	}

	public Errend getErrend() {
		return errend;
	}
	
}
