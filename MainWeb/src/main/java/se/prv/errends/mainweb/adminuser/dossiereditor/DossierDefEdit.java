package se.prv.errends.mainweb.adminuser.dossiereditor;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

import se.prv.errends.domain.DossierDef;

public class DossierDefEdit extends Window {
	
	private Runnable onClose = null;
	private DossierDef dossierdef = null;
	private TextField name = null;
	private TextField description = null;
	
	public DossierDefEdit(DossierDef dossierdef, Runnable onClose) {
		this.onClose = onClose;
		this.dossierdef = dossierdef;
		setModal(true);
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		name = new TextField("Namn");
		name.setValue(dossierdef.getDossiertypename() != null ? dossierdef.getDossiertypename() : "");
		content.addComponent(name);
		
		description = new TextField("Beskrivning");
		description.setValue(dossierdef.getDescription() != null ? dossierdef.getDescription() : "");
		content.addComponent(description);
		
		HorizontalLayout buttons = new HorizontalLayout();
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(e -> cancel());
		buttons.addComponent(cancel);
		Button finishBtn = new Button("Spara");
		finishBtn.addClickListener(e -> finish());	
		buttons.addComponent(finishBtn);
		content.addComponent(buttons);
		
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
	}
	
	private void cancel() {
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		dossierdef.setDossiertypename(name.getValue());
		dossierdef.setDescription(description.getValue());
		dossierdef.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
}
