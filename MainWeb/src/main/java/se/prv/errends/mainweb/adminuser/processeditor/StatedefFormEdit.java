package se.prv.errends.mainweb.adminuser.processeditor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Statedef;
import se.prv.errends.nodeconfig.parse.PrvErrendNodeConfigParser;
import se.prv.errends.nodeconfig.struct.NodeConfiguration;

public class StatedefFormEdit extends Window {
	
	private Runnable onClose = null;
	private Statedef sd = null;
	
	private TextField statename = null;
	private TextArea layout = null;

	public StatedefFormEdit(Statedef sd, Runnable onClose) {
		this.sd = sd;
		this.onClose = onClose;
		setModal(true);

		FormLayout content = new FormLayout();
		content.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");
		content.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.8)+"px");
		
		statename = new TextField("Namn");
		statename.setValue(sd.getStatename() != null ? sd.getStatename() : "");
		content.addComponent(statename);

		layout = new TextArea("Specifikation");
		layout.setValue(sd.getLayout() != null ? sd.getLayout() : "");
		layout.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		layout.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.6)+"px");
		content.addComponent(layout);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button("Avbryt");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}	
		});
		buttons.addComponent(cancelBtn);
		
		Button verifyBtn = new Button("Verifiera");
		verifyBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				verifyLayout();
			}		
		});
		buttons.addComponent(verifyBtn);
		
		Button saveBtn = new Button("Uppdatera");
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				save();
			}			
		});
		buttons.addComponent(saveBtn);
		
		content.addComponent(buttons);
		setContent(content);
	}

	protected void verifyLayout() {
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(layout.getValue().getBytes()));
        try {
        	NodeConfiguration nc = PrvErrendNodeConfigParser.parseNodeConfig(isr, "Node");
			Notification.show("Rätt", Notification.Type.HUMANIZED_MESSAGE);

        }
        catch(Exception e) {
			Notification.show("Fel: "+e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
	}

	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void save() {
		String name = statename.getValue().trim();
		sd.setStatename(name);
		String spec = layout.getValue();
		sd.setLayout(spec);
		sd.save();
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
