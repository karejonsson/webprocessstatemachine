package se.prv.errends.mainweb.person;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification.Type;

import se.prv.errends.domain.Document;
import se.prv.errends.domain.DocumentAttribute;
import se.prv.errends.domain.DocumentEvent;
import se.prv.errends.domain.DossierDocattributeDef;
import se.prv.errends.domain.DossierDoctypeDef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.DocumentEvent.DocumentEventType;
import se.prv.errends.mainweb.person.DossierView.GeneralizedDocumentEvent;
import se.prv.errends.mainweb.person.DossierView.GeneralizedEvent;
import se.prv.errends.mainweb.ui.CommonUI;

public class DocumentAttributeEdit extends Panel {
	
	private Document document;
	private Runnable onClose;
	private Session session = null;

	private List<DocumentAttribute> attributes;

	private VerticalLayout layout = new VerticalLayout();
	private List<DossierDocattributeDef> rows = null;
	private Grid<DossierDocattributeDef> grid = null;
	private List<GeneralizedEvent> allEventsRows;
	private Grid<GeneralizedEvent> allEventsGrid;
	
	private boolean hasAttribute(DossierDocattributeDef agent) {
		return getAttribute(agent) != null;
	}
	
	private DocumentAttribute getAttribute(DossierDocattributeDef ddd) {
		for(DocumentAttribute attribute : attributes) {
			if(attribute.getDossierdocattributedefId().equals(ddd.getId())) {
				return attribute;
			}
		}
		return null;
	}

	public DocumentAttributeEdit(Document document, List<GeneralizedEvent> allEventsRows, Grid<GeneralizedEvent> allEventsGrid, Runnable onClose) {
		this.document = document;
		this.onClose = onClose;
		this.allEventsRows = allEventsRows;
		this.allEventsGrid = allEventsGrid;
		
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		
		attributes = document.getReferringDocumentAttributes();
		rows = document.getDossier().getDossierDef().getReferringDossierDocattributeDef();
		
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getDossierdocattributename());
        }).setCaption("Attribut"); 

        grid.addComponentColumn(row -> {
        	final CheckBox box = new CheckBox();
        	box.setValue(hasAttribute(row));
        	box.setEnabled(document.isActive());
        	box.addValueChangeListener(event ->  {
        		updateAttribute(box, row);
        	});
            return box;
        }).setCaption("Har attribut");

		grid.getEditor().setEnabled(false);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();

		HorizontalLayout about = new HorizontalLayout();
		if(onClose != null) {
			Button closeBtn = new Button("Klar");
			closeBtn.addClickListener(e -> onClose.run());
			about.addComponent(closeBtn);
		}
		DossierDoctypeDef ddd = document.getDossierDoctypeDef();
		String type = "Ingen typ";
		if(ddd != null) {
			type = ddd.getDossierdoctypename();
		}
		about.addComponent(new Label("Attribut för dokumentet "+document.getFilename()+" typ "+type+", med beskrivning "+document.getDescription()));
		layout.addComponent(about);
		layout.addComponent(grid);
		setContent(layout);
	}
	
	private void updateAttribute(CheckBox box, DossierDocattributeDef dossierdocattributedef) {
		if(box.getValue()) {
			// Sätt attribut
			DocumentAttribute attribute = getAttribute(dossierdocattributedef);
			if(attribute != null) {
				Notification.show("Internt fel. Attributet är redan satt och kan inte läggas till", Type.ERROR_MESSAGE);
			}
			else {
				attribute = new DocumentAttribute();
				attribute.setDocumentId(document.getId());
				attribute.setDossierdocattributedefId(dossierdocattributedef.getId());
				attribute.setSessionId(session.getId());
				attribute.save();
				
				attributes.add(attribute);
				
        		DocumentEvent dev = new DocumentEvent();
        		dev.setCodeEnum(DocumentEventType.ADD_ATTRIBUTE);
        		dev.setSysteminfo("dossierdocattributedef.id = "+dossierdocattributedef.getId());
        		dev.setDocument(document);
        		dev.setSessionId(session.getId());
        		dev.setDescription("Lade till attribut "+dossierdocattributedef.getDossierdocattributename());
        		dev.save();
        		GeneralizedDocumentEvent gde = new GeneralizedDocumentEvent(dev);
        		allEventsRows.add(gde);
        		allEventsGrid.getDataProvider().refreshAll();
			}
		}
		else {
			// Ta bort attribut
			DocumentAttribute attribute = getAttribute(dossierdocattributedef);
			if(attribute == null) {
				Notification.show("Internt fel. Attributet är inte satt och kan inte tas bort", Type.ERROR_MESSAGE);
			}
			else {
				attribute.delete();			
				
				attributes.remove(attribute);
				
        		DocumentEvent dev = new DocumentEvent();
        		dev.setCodeEnum(DocumentEventType.REMOVE_ATTRIBUTE);
        		dev.setSysteminfo("dossierdocattributedef.id = "+dossierdocattributedef.getId());
        		dev.setDocument(document);
        		dev.setSessionId(session.getId());
        		dev.setDescription("Tog bort attribut "+dossierdocattributedef.getDossierdocattributename());
        		dev.save();
        		GeneralizedDocumentEvent gde = new GeneralizedDocumentEvent(dev);
        		allEventsRows.add(gde);
        		allEventsGrid.getDataProvider().refreshAll();
			}
		}
	}

	
}
