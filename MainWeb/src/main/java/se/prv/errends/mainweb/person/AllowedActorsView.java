package se.prv.errends.mainweb.person;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.errends.dbc.ActorDB;
import se.prv.errends.dbc.AllowedActorDB;
import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.AllowedActor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.OrganisationRoleGroup;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Roledef;
import se.prv.errends.domain.Session;
import se.prv.errends.domain.Statedef;
import se.prv.errends.mainweb.errend.ErrendInStateView;
import se.prv.errends.mainweb.ui.CommonUI;
import se.prv.errends.mgmt.ErrendManagement;
import se.prv.errends.mgmt.SessionManagement;

public class AllowedActorsView extends Panel {
	
	private List<AllowedActor> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<AllowedActor> grid = null;
	private Person person = null;
	private Session session = null;
	private List<Actor> existingActors = null;
	
	private Actor getExistingActor(Long roledefId, Long agentId, Long errendId) {
		for(Actor existingActor : existingActors) {
			if(existingActor.isSameAs(roledefId, agentId, errendId)) {
				return existingActor;
			}
		}
		return null;
	}

	public AllowedActorsView() {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		existingActors = ActorDB.getAllActiveActorsOfPersonId(person.getId());

		rows = AllowedActorDB.getAllowedActorsForPerson(person.getId());

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	if(row.errend == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.errend.getAdminusername());
        	}
        }).setCaption("Benämning"); 
        grid.addComponentColumn(row -> {
        	if(row.errend == null) {
        		return new Label("--");
        	}
        	else {
            	return new Label(row.errend.getPersonname());
        	}
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	if(row.errend == null) {
        		return new Label("--");
        	}
        	else {
            	Date date = row.errend.getCreated();
            	Label out = new Label(Adminuser.presentDate(date));
            	return out;
        	}
        }).setCaption("Skapat"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.groupMember.getOrganisationRoleGroup().getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getRolename());
        	}
        }).setCaption("Rollnamn"); 
        grid.addComponentColumn(row -> {
        	Statedef sd = null;
        	try {
        		sd = row.errend.getManagedErrend().getCurrentStatedef();
        	}
        	catch(Exception e) {}
        	if(sd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(sd.getStatename());
        	}
        }).setCaption("Tillstånd"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.groupMember.getOrganisationRoleGroup().getOrganisation().getOrgname());
        }).setCaption("Ombud för"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.groupMember.getOrganisationRoleGroup().getName());
        }).setCaption("Grupp"); 
        grid.addComponentColumn(row -> {
        	Roledef rd = row.groupMember.getOrganisationRoleGroup().getRoledef();
        	if(rd == null) {
        		return new Label("--");
        	}
        	else {
        		return new Label(rd.getProcessdef().getProcessname());
        	}
        }).setCaption("Ärendetyp"); 
		grid.addComponentColumn(row -> {
			Errend e = row.errend;
			Agent agent = row.groupMember.getAgent();
			if(e != null && person.getId().equals(agent.getPersonId())) {
				Layout out = new HorizontalLayout();
				final Actor currentActor = session.getActor();
				final Actor existingActor = getExistingActor(
						row.groupMember.getOrganisationRoleGroup().getRoledefId(), 
						row.groupMember.getAgentId(), 
						e.getId());
				String buttonText = "Logik-fel";
				
				if(existingActor == null) {
					buttonText = "Ny aktör & session";
				}
				else {
					if(existingActor.getId().equals(currentActor.getId())) {
						buttonText = "Återuppta session";
					}
					else {
						if(currentActor.getErrendId() == null) {
							buttonText = "Inled session";
						}
						else {
							buttonText = "Ny session";
						}
					}
				}
 				
				boolean active = agent.isActive();
				Button toSessionBtn = new Button(buttonText);
				toSessionBtn.setEnabled(active);
				if(active) {
					toSessionBtn.addClickListener(click -> {
						if(existingActor == null) {
							Actor newActor = new Actor();
							newActor.setRoledefId(row.groupMember.getOrganisationRoleGroup().getRoledefId());
							newActor.setAgentId(row.groupMember.getAgentId());
							newActor.setErrendId(e.getId());
							newActor.save();
							goToErrend(newActor);
						}
						else {
							goToErrend(existingActor);
						}
					});
				}
				out.addComponent(toSessionBtn);
				return out;
			} else {
				return new Label("--");
			}
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);
		grid.setItems(rows);//.stream().map(ProcessfileGridrow::valueOf).collect(Collectors.toList()));
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		layout.addComponent(grid);
		setContent(layout);
	}

	private void goToErrend(Actor row) {
		ErrendManagement em = null;
		try {
			em = ErrendManagement.getErrendFromDBStructure(row);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(em.isError()) {
			Notification.show(em.getErrorMessage(), Notification.Type.ERROR_MESSAGE);
			return;
		}
		Session sessionToUse = SessionManagement.switchAsNecessary(session, row);
		UI.getCurrent().getSession().setAttribute(CommonUI.sessionlookup, sessionToUse);
		UI.getCurrent().getSession().setAttribute(CommonUI.currenterrend, em);
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new ErrendInStateView());
	}
	
}
