package se.prv.errends.mainweb.ui;

public interface CommonUI {

	public static final String adminuserlookup = "adminuserlookup";
	public static final String personlookup = "personlookup";
	public static final String orguserlookup = "orguserlookup";
	public static final String currenterrend = "currenterrend";    
	public static final String sessionlookup = "errendsession";    
	
}
