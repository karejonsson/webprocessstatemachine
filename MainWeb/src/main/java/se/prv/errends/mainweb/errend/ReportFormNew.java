package se.prv.errends.mainweb.errend;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.domain.Actor;
import se.prv.errends.domain.Errend;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Report;
import se.prv.errends.domain.ReportTemplate;
import se.prv.errends.domain.ReportTemplateReuse;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.ui.CommonUI;

public class ReportFormNew extends Window {
	
	private Runnable onClose = null;
	
	private ReportsInErrendView riev = null;
	private Session session = null;
	private Errend errend = null;
	private List<Actor> referringActors = null;
	private List<ReportTemplateReuse> reusedReportTemplates = null;
	private ComboBox<ActorListItem> choseActor = new ComboBox<ActorListItem>("Mottagare");
	private ComboBox<ReportTemplateReuseListItem> choseReportTemplate = new ComboBox<ReportTemplateReuseListItem>("Rapportmall");

	public ReportFormNew(ReportsInErrendView riev, Runnable onClose) {
		this.riev = riev;
		this.onClose = onClose;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);
		errend = riev.getErrend();
		referringActors = errend.getReferringActors();
		
		List<ActorListItem> actorChoiceItems = new ArrayList<ActorListItem>();
		for(Actor referringActor : referringActors) {
			actorChoiceItems.add(new ActorListItem(referringActor));
		}
		choseActor.setItems(actorChoiceItems);
		
		reusedReportTemplates = errend.getManagedErrend().getProcessdef().getReferringReportTemplateReuses();
		
		List<ReportTemplateReuseListItem> reusedReportTemplateChoiceItems = new ArrayList<ReportTemplateReuseListItem>();
		for(ReportTemplateReuse reusedReportTemplate : reusedReportTemplates) {
			reusedReportTemplateChoiceItems.add(new ReportTemplateReuseListItem(reusedReportTemplate));
		}
		choseReportTemplate.setItems(reusedReportTemplateChoiceItems);
		
		setModal(true);
		
		FormLayout content = new FormLayout();
		content.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.8)+"px");
		//content.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.8)+"px");
		
		choseActor.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		content.addComponent(choseActor);
		choseReportTemplate.setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.6)+"px");
		content.addComponent(choseReportTemplate);

		HorizontalLayout buttons = new HorizontalLayout();		
		Button createBtn = new Button("Skapa");
		createBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				create();
			}
			
		});
		buttons.addComponent(createBtn);
		Button backBtn = new Button("Åter");
		backBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				back();
			}
			
		});
		buttons.addComponent(backBtn);
		content.addComponent(buttons);
		setContent(content);
	}
	
	private void back() {
		//System.out.println("ReportFormNew.back");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

	private void finish() {
		//System.out.println("ReportFormNew.finish");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void create() {
		Report report = new Report();
		report.setErrend(errend);
		Optional<ActorListItem> op = choseActor.getSelectedItem();
		Actor actor = op.get().getActor();
		report.setActor(actor);
		Optional<ReportTemplateReuseListItem> rtrli = choseReportTemplate.getSelectedItem();
		ReportTemplateReuse rtr = rtrli.get().getReportTemplateReuse(); // Null pointer om man inte valt.
		report.setReportTemplateReuse(rtr);
		report.save();

		riev.notifyAboutNewReport(report);
	}
	
	private static class ActorListItem {
		private Actor actor = null;
		public ActorListItem(Actor actor) {
			this.actor = actor;
		}
		public String toString() {
         	Agent agent = actor.getAgent();
        	Person p = agent.getPerson();
        	Organisation organisation = agent.getOrganisation();
        	String orgpart = "";
        	if(organisation != null) {
        		orgpart = organisation.getOrgname()+":"+organisation.getOrgnr()+" / ";
        	}
        	return orgpart+p.getFirstname()+" "+p.getFamilyname();
		}
		public Actor getActor() {
			return actor;
		}
	}
	
	private static class ReportTemplateReuseListItem {
		private ReportTemplateReuse rtr = null;
		public ReportTemplateReuseListItem(ReportTemplateReuse rtr) {
			this.rtr = rtr;
		}
		public String toString() {
			ReportTemplate rt = rtr.getReportTemplate();
			return rt.getReporttitle()+"/"+rt.getDescription();
		}
		public ReportTemplateReuse getReportTemplateReuse() {
			return rtr;
		}
	}
	
}
