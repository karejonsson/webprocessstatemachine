package se.prv.errends.mainweb.adminuser.dossiereditor;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.HorizontalLayout;

import se.prv.errends.dbc.DossierDefDB;
import se.prv.errends.domain.DossierDef;
import se.prv.errends.domain.DossierDocattributeDef;
import se.prv.errends.domain.DossierDoctypeDef;
import se.prv.errends.domain.Person;
import se.prv.errends.mainweb.reuse.ShowLongText;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class DossierDefListView extends Panel {
	
	private VerticalLayout layout = new VerticalLayout();
	private List<DossierDef> rows = null;
	private Grid<DossierDef> grid = null;
	private Person person = null;

	public DossierDefListView() {
		person = (Person) UI.getCurrent().getSession().getAttribute(CommonUI.personlookup);
		rows = DossierDefDB.getAllDossierDefs();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getDossiertypename());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getDescription());
        }).setCaption("Beskrivning"); 
        grid.addComponentColumn(row -> {
        	HorizontalLayout hl = new HorizontalLayout();
    		{
            	Button deleteBtn = new Button("Radera");
            	hl.addComponent(deleteBtn);
            	deleteBtn.addClickListener(e -> deleteDossierDef(row));
    		}
    		{
            	Button editBtn = new Button("Redigera meta");
            	hl.addComponent(editBtn);
            	editBtn.addClickListener(e -> editDossierDef(row));
    		}
    		{
            	Button editBtn = new Button("Redigera värden");
            	hl.addComponent(editBtn);
            	editBtn.addClickListener(e -> editDossierDefValues(row));
    		}
    		{
            	Button exportBtn = new Button("Exportera");
            	hl.addComponent(exportBtn);
            	exportBtn.addClickListener(e -> exportDossierDefValues(row));
    		}
        	return hl;
        }).setCaption("Hantering"); 
        
		grid.getEditor().setEnabled(false);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		Button newDossier = new Button("Ny aktdefinition");
		newDossier.addClickListener(e -> newDossierDef());
		layout.addComponent(newDossier);
		layout.addComponent(grid);
		setContent(layout);
	}

	private void editDossierDef(DossierDef row) {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefEdit(row, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefListView.this);
	    		if(row.getId() != null) {
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("EDIT "+row);
	}
	
	private void editDossierDefValues(DossierDef row) {
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new DossierDefView(row, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setup(DossierDefListView.this);
			}
		}));
		System.out.println("EDIT "+row);
	}

	private void exportDossierDefValues(DossierDef row) {
		StringBuffer out = new StringBuffer();
		out.append("dossiertype\n");
		List<DossierDoctypeDef> types = row.getReferringDossierDoctypeDefs();
		out.append("types ");
		if(types.size() == 1) {
			out.append("'"+types.get(0).getDossierdoctypename()+"';\n");
		}
		else {
			out.append("'"+types.get(0).getDossierdoctypename()+"'");
			for(int i = 1 ; i < types.size(); i++) {
				out.append(", '"+types.get(i).getDossierdoctypename()+"'");
			}
			out.append(";\n");
		}
		
		List<DossierDocattributeDef> attributes = row.getReferringDossierDocattributeDef();
		out.append("attributes ");
		if(attributes.size() == 1) {
			out.append("'"+attributes.get(0).getDossierdocattributename()+"';\n");
		}
		else {
			out.append("'"+attributes.get(0).getDossierdocattributename()+"'");
			for(int i = 1 ; i < attributes.size(); i++) {
				out.append(", '"+attributes.get(i).getDossierdocattributename()+"'");
			}
			out.append(";\n");
		}
		
		ShowLongText slt = new ShowLongText("Genererad aktspecifikation till "+row.getDossiertypename(), out.toString());
		slt.setEnabledText(true);

		((AdminuserUI) UI.getCurrent()).openSubwindow(slt);

		System.out.println("EXPORT "+out.toString());
	}

	
	private void newDossierDef() {
		final DossierDef dossierdef = new DossierDef();
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(new DossierDefEdit(dossierdef, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(DossierDefListView.this);
	    		if(dossierdef.getId() != null) {
		    		rows.add(dossierdef);
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("NEW ");
	}

	private void deleteDossierDef(DossierDef row) {
		rows.remove(row);
		row.deleteRecursive();
		grid.getDataProvider().refreshAll();
		System.out.println("DELETE "+row);
	}

	private void handleDossier(DossierDef row) {
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(new DossierDefView(row, new Runnable() {
			@Override
			public void run() {
	    		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(DossierDefListView.this);
	    		if(row.getId() != null) {
		    		grid.getDataProvider().refreshAll();
	    		}
			}
		}));
		System.out.println("HANDLE "+row);
	}

}
