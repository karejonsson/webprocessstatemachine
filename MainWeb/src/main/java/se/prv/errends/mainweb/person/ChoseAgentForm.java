package se.prv.errends.mainweb.person;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import se.prv.errends.dbc.AgentDB;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.Person;

public class ChoseAgentForm extends Window {
	
	private Runnable onClose = null;
	private Grid<Agent> grid = null;
	private NotifyAboutPick notify = null;
	private Person person = null;
	
	public ChoseAgentForm(NotifyAboutPick notify, Person person, Runnable onClose) {
		super("Välj ombud");
		this.onClose = onClose;
		this.notify = notify;
		this.person = person;
		
		Panel panel = new Panel();
		panel.setSizeUndefined();
		setContent(panel);
		
		FormLayout content = new FormLayout();
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancel = new Button("Avbryt");
		cancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				cancel();
			}
			
		});
		buttons.addComponent(cancel);
		content.addComponent(buttons);
				
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getOrganisation().getOrgname());
        }).setCaption("Namn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getOrganisation().getOrgnr());
        }).setCaption("Orgnr"); 
        if(notify != null) {
	        grid.addComponentColumn(row -> {
	        	Button pick = new Button("Välj");
	    		pick.addClickListener(new ClickListener() {
	    			private static final long serialVersionUID = 1L;
	    			@Override
	    			public void buttonClick(ClickEvent event) {
	    				picked(row);
	    			}
	    		});
	        	return pick;
	        }).setCaption("Välj"); 
        }
        
		grid.getEditor().setEnabled(false);

		search();

		content.addComponent(grid);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		//setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}
	
	private void cancel() {
		//System.out.println("NewProcessfileForm.cancel");
		if(onClose != null) {
			onClose.run();
		}
		close();
	}
	
	private void search() {
		List<Agent> hits = AgentDB.getAllAgentsOfPerson(person.getId());
		grid.setItems(hits);
	}
	
	public interface NotifyAboutPick {
		public void picked(Agent agent);
	}
	
	private void picked(Agent agent) {
		if(notify != null) {
			notify.picked(agent);
		}
		if(onClose != null) {
			onClose.run();
		}
		close();
	}

}
