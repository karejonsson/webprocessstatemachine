package se.prv.errends.mainweb.orguser;

import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Grid.SelectionMode;

import se.prv.errends.domain.Agent;
import se.prv.errends.domain.OrganisationGroupMember;
import se.prv.errends.domain.OrganisationRoleGroup;

public class ManageGroupsOfAgent extends Panel {
	
	private Agent agent;
	private Runnable onClose;
	private List<OrganisationGroupMember> memberships;

	private VerticalLayout layout = new VerticalLayout();
	private List<OrganisationRoleGroup> rows = null;
	private Grid<OrganisationRoleGroup> grid = null;
	
	private boolean isMember(OrganisationRoleGroup group) {
		return getMembership(group) != null;
	}
	
	private OrganisationGroupMember getMembership(OrganisationRoleGroup group) {
		for(OrganisationGroupMember membership : memberships) {
			if(membership.getOrganisationRoleGroup().getId().equals(group.getId())) {
				return membership;
			}
		}
		return null;
	}

	public ManageGroupsOfAgent(Agent agent, Runnable onClose) {
		this.agent = agent;
		this.onClose = onClose;
		
		memberships = agent.getReferringOrganisationGroupMembers();
		rows = agent.getOrganisation().getReferringOrganisationRoleGroups();
		
		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getName());
        }).setCaption("Namn"); 

        grid.addComponentColumn(row -> {
        	final CheckBox box = new CheckBox();
        	box.setValue(isMember(row));
        	box.setEnabled(true);
        	box.addValueChangeListener(event ->  {
        		updateMembership(box, row);
        	});
            return box;
        }).setCaption("Medlemsstatus");

		grid.getEditor().setEnabled(false);
		grid.setItems(rows);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();

		HorizontalLayout about = new HorizontalLayout();
		if(onClose != null) {
			Button closeBtn = new Button("Klar");
			closeBtn.addClickListener(e -> onClose.run());
			about.addComponent(closeBtn);
		}
		about.addComponent(new Label("Grupper för "+agent.getPerson().getFirstname()+" "+agent.getPerson().getFamilyname()+" för organisationen "+agent.getOrganisation().getOrgname()));
		layout.addComponent(about);
		layout.addComponent(grid);
		setContent(layout);
	}
	
	private void updateMembership(CheckBox box, OrganisationRoleGroup group) {
		if(box.getValue()) {
			// Aktivera medlemsskap
			OrganisationGroupMember membership = getMembership(group);
			if(membership != null) {
				Notification.show("Internt fel. Medlemsskapet finns redan och kan inte skapas", Type.ERROR_MESSAGE);
			}
			else {
				membership = new OrganisationGroupMember();
				membership.setAgentId(agent.getId());
				membership.setRolegroupId(group.getId());
				membership.save();
			}
		}
		else {
			// Ta bort medlemsskap
			OrganisationGroupMember membership = getMembership(group);
			if(membership == null) {
				Notification.show("Internt fel. Medlemsskapet finns inte och kan inte tas bort", Type.ERROR_MESSAGE);
			}
			else {
				membership.delete();
			}
		}
	}
	
}

