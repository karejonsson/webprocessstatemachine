package se.prv.errends.mainweb.orguser;

import java.util.Date;
import java.util.List;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification.Type;

import se.prv.errends.domain.Adminuser;
import se.prv.errends.domain.Agent;
import se.prv.errends.domain.OrganizationUser;
import se.prv.errends.domain.Person;
import se.prv.errends.domain.Organisation;
import se.prv.errends.domain.Session;
import se.prv.errends.mainweb.person.SearchPersonForm;
import se.prv.errends.mainweb.ui.AdminuserUI;
import se.prv.errends.mainweb.ui.CommonUI;

public class OrganizationsAgentView extends Panel {
	
	private List<Agent> rows = null;
	private VerticalLayout layout = new VerticalLayout();
	private Grid<Agent> grid = null;
	private Organisation organization = null;
	private Session session = null;

	public OrganizationsAgentView(Organisation organization, Runnable onClose) {
		this.organization = organization;
		session = (Session) UI.getCurrent().getSession().getAttribute(CommonUI.sessionlookup);

		rows = organization.getReferringAgents();

		grid = new Grid<>();

        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getEmail());
        }).setCaption("E-post"); 
        grid.addComponentColumn(row -> {
        	return new Label(Adminuser.presentDate(row.getRegisterred()));
        }).setCaption("Registrerad"); 
        grid.addComponentColumn(row -> {
        	CheckBox box = new CheckBox("Aktiv");
        	box.setValue(row.isActive());
        	box.addValueChangeListener(event ->  {
        		row.setActive(box.getValue());
        		row.save();
        	});
            return box;
        }).setCaption("Aktivstatus"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getFirstname());
        }).setCaption("Förnamn"); 
        grid.addComponentColumn(row -> {
        	return new Label(row.getPerson().getFamilyname());
        }).setCaption("Efterrnamn"); 
		grid.addComponentColumn(row -> {
			  Layout out = new HorizontalLayout();
			  {
			      Button deleteBtn = new Button("Radera");
			      deleteBtn.addClickListener(click -> delete(row));
			      out.addComponent(deleteBtn);
			  }
			  {
			      Button groupsBtn = new Button("Grupper");
			      groupsBtn.addClickListener(click -> manageGroups(row));
			      out.addComponent(groupsBtn);
			  }
		      return out;
		}).setCaption("Hantering");

		grid.getEditor().setEnabled(false);

		grid.setItems(rows);

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();
		
		HorizontalLayout buttons = new HorizontalLayout();
		
		{
			Button newAgentBtn = new Button("Nytt");
			newAgentBtn.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;
				@Override
				public void buttonClick(ClickEvent event) {
					newAgent();
				}
			});
			buttons.addComponent(newAgentBtn);
		}

		{
			if(onClose != null) {
				Button doneBtn = new Button("Klar");
				doneBtn.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;
					@Override
					public void buttonClick(ClickEvent event) {
						onClose.run();
					}
				});
				buttons.addComponent(doneBtn);
			}
		}

		layout.addComponent(buttons);
		layout.addComponent(grid);
		
		setContent(layout);
	}
	
	private void manageGroups(Agent row) {
		ManageGroupsOfAgent ofe = new ManageGroupsOfAgent(row, new Runnable() {
			@Override
			public void run() {
				((SimplifiedContentUI) UI.getCurrent()).setWorkContent(OrganizationsAgentView.this);
			}
		});
		((SimplifiedContentUI) UI.getCurrent()).setWorkContent(ofe);
	}

	private void newAgent() {
		((SimplifiedContentUI) UI.getCurrent()).openSubwindow(
				new SearchPersonForm(new SearchPersonForm.NotifyAboutPersonPick() {
					@Override
					public void picked(Person p) {
						Agent agent = new Agent();
						agent.setOrganization(organization);
						agent.setPerson(p);
						agent.setRegisterred(new Date(System.currentTimeMillis()));
						agent.setActive(true);
						agent.save();
						rows.add(agent);
						grid.getDataProvider().refreshAll();
					}
				}, null));
	}
	
	private void delete(Agent agent) {
		try {
			agent.deleteRecursively();
			grid.getDataProvider().refreshAll();
		}
		catch(Exception e) {
			Notification.show("Aktören kan ej raderas pga befintliga engagemang", Type.ERROR_MESSAGE);
			return;
		}
	}
	
}
